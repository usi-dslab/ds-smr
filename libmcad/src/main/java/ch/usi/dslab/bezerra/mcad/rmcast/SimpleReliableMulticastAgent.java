package ch.usi.dslab.bezerra.mcad.rmcast;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.mcad.ReliableMulticastAgent;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPDestination;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPReceiver;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPSender;

public class SimpleReliableMulticastAgent implements ReliableMulticastAgent {

   Map<Group, Set<TCPDestination>> groupDestinations;
   TCPSender   rmcastSender;
   TCPReceiver rmcastReceiver;
   
   @SuppressWarnings({ "unchecked", "unused" })
   public SimpleReliableMulticastAgent(Integer id, String configFile) {
      rmcastSender = new TCPSender();
      groupDestinations = new ConcurrentHashMap<Group, Set<TCPDestination>>();
      
      try {
         JSONParser parser = new JSONParser();
         
         Object obj = parser.parse(new FileReader(configFile));         
         JSONObject config = (JSONObject) obj;         

         JSONArray groupMembersArray = (JSONArray) config.get("group_members");         
         Iterator<Object> it_groupMember = groupMembersArray.iterator();

         while (it_groupMember.hasNext()) {
            JSONObject jsgroupmember = (JSONObject) it_groupMember.next();
            int pid     = ((Long  ) jsgroupmember.get("pid"  )).intValue();
            int group   = ((Long  ) jsgroupmember.get("group")).intValue();
            String host = (String ) jsgroupmember.get("host" );
            int port    = ((Long  ) jsgroupmember.get("port" )).intValue();
            String rmcast_address = ( String) jsgroupmember.get("rmcast_address");
            int rmcast_port       = ((Long  ) jsgroupmember.get("rmcast_port")).intValue();
            
            RmcastHostInfo.saveInfo(pid, rmcast_address, rmcast_port);
            
            if (id != null && pid == id.intValue()) {
               rmcastReceiver = new TCPReceiver(rmcast_port);
            }
         }
         
      } catch (IOException e) {
         e.printStackTrace();
      } catch (ParseException e) {
         e.printStackTrace();
      }
   }
   
   public void reliableMulticast(Group single_destination, Message message) {
      List<Group> l = new ArrayList<>(1);
      l.add(single_destination);
      reliableMulticast(l, message);
   }
   
   public void reliableMulticast(List<Group> destinations, Message message) {
      Set<TCPDestination> tcpdests = new HashSet<TCPDestination>();
      for (Group g : destinations) {
         Set<TCPDestination> gtcpd = groupDestinations.get(g);
         if (gtcpd == null) {
            gtcpd = saveGroupDestinations(g);
         }
         tcpdests.addAll(gtcpd);
      }
      rmcastSender.multiDestinationSend(message, new ArrayList<TCPDestination>(tcpdests));
   }
   
   public Message reliableDeliver() {
      Message m = rmcastReceiver.receive().getContents();
      m.rewind();
      return m;
   }
   
   private Set<TCPDestination> saveGroupDestinations(Group g) {
      List<Integer> members = g.getMembers();
      Set<TCPDestination> membersAddresses = new HashSet<TCPDestination>();
      for (int m : members) {
         RmcastHostInfo info = RmcastHostInfo.getInfo(m);
         final String host = info.host;
         final int    port = info.port;
         TCPDestination madd = new TCPDestination() {
            @Override
            public int getPort() {
               return port;
            }
            @Override
            public String getAddress() {
               return host;
            }
         };
         membersAddresses.add(madd);
      }
      groupDestinations.put(g, membersAddresses);
      return membersAddresses;
   }
}