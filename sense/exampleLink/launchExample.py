#!/usr/bin/python

# Sense - A library for easy monitoring in Java
# Copyright (C) 2014, University of Lugano
# 
# This file is part of Sense.
# 
# Sense is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
# 
# author Eduardo Bezerra - eduardo.bezerra@usi.ch

import inspect
import os

def script_dir():
   #    returns the module path without the use of __file__.  Requires a function defined
   #    locally in the module.
   #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
   return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

cp = script_dir() + "/../../../../../../../target/libsense-git.jar"

processCommand  = "java -cp " + cp + " ch.usi.dslab.bezerra.sense.example.MonitoredProcess &"
print(processCommand)
# <command> <port> <directory> [<resource> <nodetype> <count>]+
gathererCommand = "java -cp " + cp + " ch.usi.dslab.bezerra.sense.DataGatherer 60000 /tmp/senseExample latency myprocess 1 throughput myprocess 1"
print(gathererCommand)

os.system(processCommand)
os.system(gathererCommand)