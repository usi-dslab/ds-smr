/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.datapoints.LatencyDataPoint;

public class LatencyPassiveMonitor extends PassiveMonitor {
   
   DescriptiveStatistics intervalLatencies;
   long firstLatencyInstant, lastLatencyInstant;
   
   public LatencyPassiveMonitor(int monitoredPid, String monitoredName) {
      super(monitoredPid, monitoredName);
      intervalLatencies = new DescriptiveStatistics();
      firstLatencyInstant = lastLatencyInstant = 0;
   }
   
   public LatencyPassiveMonitor(int monitoredPid, String monitoredName, boolean recquired) {
      super(monitoredPid, monitoredName, recquired);
      intervalLatencies = new DescriptiveStatistics();
      firstLatencyInstant = lastLatencyInstant = 0;
   }

   // if receive time is not given, calculate it INSIDE the if
   
   public void logLatency(long sendTimeNano) {
      logLatency(sendTimeNano, 0, true);
   }
   
   public void logLatency(long sendTimeNano, long recvTimeNano) {
      logLatency(sendTimeNano, recvTimeNano, false);
   }
   
   synchronized private void logLatency(long sendTimeNano, long recvTimeNano, boolean calculateRecvTime) {
      if (active == false) {
         return;
      }
      
      // get everything method
//      long currentTimeMS = System.currentTimeMillis();
//      if (calculateRecvTime) recvTimeNano = System.nanoTime();
//      long latency = recvTimeNano - sendTimeNano;
//      log.add(new LatencyDataPoint(currentTimeMS, currentTimeMS, latency));
      
      // sampling method
//      long currentTimeMS = System.currentTimeMillis();
//      if (currentTimeMS > lastLogTime + DataGatherer.LOG_INTERVAL_MS) {
//         if (calculateRecvTime) recvTimeNano = System.nanoTime();
//         long latency = recvTimeNano - sendTimeNano;
//         log.add(new LatencyDataPoint(lastLogTime, currentTimeMS, latency));
//         lastLogTime = currentTimeMS;
//      }
      
      // interval average method
    long currentTimeMS = System.currentTimeMillis();
    if (calculateRecvTime) recvTimeNano = System.nanoTime();
    long latency = recvTimeNano - sendTimeNano;
    if (intervalLatencies.getN() == 0)
       firstLatencyInstant = currentTimeMS;
    lastLatencyInstant = currentTimeMS;
    intervalLatencies.addValue(latency);
    if (currentTimeMS > lastLogTime + DataGatherer.LOG_INTERVAL_MS) {
       log.add(new LatencyDataPoint(firstLatencyInstant, lastLatencyInstant, intervalLatencies.getN(), intervalLatencies.getMean()));
       intervalLatencies.clear();
       lastLogTime = currentTimeMS;
    }

   }

   @Override
   public void saveToFile() {
      saveToFile(new LatencyDataPoint());
   }

   @Override
   public void sendToGatherer() {
      sendToGatherer(new LatencyDataPoint());
   }
}
