/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.DataPointList;
import ch.usi.dslab.bezerra.sense.datapoints.*;

public class CPUMonitor {
   
   static String delims = " +";
   
   long durationMS;
   
   List<DataPoint> cpuLog;
   
   String monitoredName;
   int monitoredId;
   
   public CPUMonitor(int monitoredId, String monitoredName) {
      this.monitoredId = monitoredId;
      this.monitoredName = monitoredName;
      this.durationMS = DataGatherer.getDuration();
      this.cpuLog = new ArrayList<DataPoint>();
   }
   
   // returns user cpu usage
   public void logCpu() {
      try {
         // start up the command in child process
         String cmd = "mpstat -P ALL 1 1";
         Process child = Runtime.getRuntime().exec(cmd);

         // hook up child process output to parent
         InputStream lsOut = child.getInputStream();
         InputStreamReader r = new InputStreamReader(lsOut);
         BufferedReader in = new BufferedReader(r);

         // read the child process' output
         String line = in.readLine();
         if (line != null)
            line = in.readLine();
         if (line != null)
            line = in.readLine();
         if (line != null)
            line = in.readLine();
         
         CPUDataPoint clp = new CPUDataPoint(System.currentTimeMillis());
         while (line != null && !line.equals("")) {
            String[] columns = line.split(delims);
            double user = Double.parseDouble(columns[3]);
            double sys  = Double.parseDouble(columns[5]);
            double idle = Double.parseDouble(columns[11]);
            double load = user + sys;
            line = in.readLine();
            
            clp.appendCoreLoad(load, user, sys, idle);
         }
         
         in.close();
         
         cpuLog.add(clp);
      } catch (IOException e) { // exception thrown
         e.printStackTrace();
         System.out.println("!!! Error when getting cpu (and cores) load.");
         System.exit(1);
      }
   }
   
   public void log() {
      long startMS, currentMS;
      startMS = currentMS = System.currentTimeMillis();

      while (currentMS - startMS < durationMS) {
         logCpu();
         currentMS = System.currentTimeMillis();
      }
   }
   
   void sendToGatherer() {
      DataPointList dpList = new DataPointList((new CPUDataPoint()).getName(), monitoredName, monitoredId, true, cpuLog);
      DataPointList.sendToGatherer(dpList);
   }

   public static void main(String[] args) {
      
      if (args.length != 6) {
         System.out.println("usage: nodeTypeName nodeId gathererIp gathererPort duration(s) logDirectory");
         System.exit(1);
      }
      
      String nodeType     = args[0];
      int    id           = Integer.parseInt(args[1]);
      String gaddr        = args[2];
      int    gport        = Integer.parseInt(args[3]);
      long   durationSecs = Long.parseLong(args[4]);
      String logDirectory = args[5];
      
      DataGatherer.configure(durationSecs, logDirectory, gaddr, gport);
      
      CPUMonitor cm = new CPUMonitor(id, nodeType);
      
      cm.log();
      cm.sendToGatherer();
   }

}
