/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.DataPointList;
import ch.usi.dslab.bezerra.sense.datapoints.*;

public class BWMonitor {
   
   public class InstantNWValues {
      long upBytes, upPackets, downBytes, downPackets;
      
      public InstantNWValues(long downBytes, long downPackets, long upBytes, long upPackets) {
         this.upBytes     = upBytes;
         this.upPackets   = upPackets;
         this.downBytes   = downBytes;
         this.downPackets = downPackets;
      }
      
      @Override
      public String toString() {
         return "downBytes: " + downBytes + "; downPackets: " + downPackets
               + "; upBytes: " + upBytes + "; upPackets: " + upPackets;
      }
   }
   
   public static final int logInterval = 1000; // milliseconds
   
   public static final String DEFAULT_INTERFACE = "eth0";
   
   long durationMS;
   
   List<DataPoint> bwLog;
   
   String gathererAddress;
   int gathererPort;
   SocketChannel gathererConnection;
   
   String monitoredName;
   int monitoredId;
   
   public BWMonitor(int monitoredId, String monitoredName) {
      this.monitoredId = monitoredId;
      this.monitoredName = monitoredName;
      this.durationMS = DataGatherer.getDuration();
      this.bwLog = new ArrayList<DataPoint>();
   }
   
   public InstantNWValues getInstantValues(String nwInterface) {
      InstantNWValues valuesObject = null;
      try {
         // start up the command in child process
         String cmd = "cat /proc/net/dev";
         Process child = Runtime.getRuntime().exec(cmd);

         // hook up child process output to parent
         InputStream lsOut = child.getInputStream();
         InputStreamReader r = new InputStreamReader(lsOut);
         BufferedReader in = new BufferedReader(r);

         // read the child process' output
         String line = in.readLine();
         while (line != null && !line.contains(nwInterface))
            line = in.readLine();
         
         if (line == null) {
            System.err.println("!!! couldn't read statistics for " + nwInterface);            
            System.exit(1);
         }
         
         in.close();
         
         String pureValues;
         
         int delimIndex = line.indexOf(":");
         if (line.charAt(delimIndex) + 1 == ' ')
            pureValues = line.substring(delimIndex + 2);
         else
            pureValues = line.substring(delimIndex + 1);
         
         String[] values = pureValues.split(" +");
         
         long downBytes   = Long.parseLong(values[0]);
         long downPackets = Long.parseLong(values[1]);
         long upBytes     = Long.parseLong(values[8]);
         long upPackets   = Long.parseLong(values[9]);
         
         valuesObject = new InstantNWValues(downBytes, downPackets, upBytes, upPackets);
         
      } catch (IOException e) { // exception thrown
         e.printStackTrace();
         System.err.println("!!! Error when getting cpu (and cores) load.");
         System.exit(1);
      }
      
      return valuesObject;
   }
   
   void log(String nwInterface) {
      long startTimeMS, currentTimeMS, lastTimeMS;
      startTimeMS = currentTimeMS = System.currentTimeMillis();
      InstantNWValues previousValues, currentValues;
      
      // TODO: convert duration to milliseconds
      currentValues = getInstantValues(nwInterface);
      while (currentTimeMS - startTimeMS < durationMS) {
         lastTimeMS = currentTimeMS;
         previousValues = currentValues;
         try {
            Thread.sleep(logInterval);
         }
         catch (InterruptedException e) {
            e.printStackTrace();
            System.err.println("!!! Failed while putting thread to sleep");
            System.exit(1);
         }
         currentValues = getInstantValues(nwInterface);
         currentTimeMS = System.currentTimeMillis();
         
         double downByteRate    = (double) (currentValues.downBytes   - previousValues.downBytes  ) / (((double) (currentTimeMS - lastTimeMS)) / 1000.0d);
         double downPacketsRate = (double) (currentValues.downPackets - previousValues.downPackets) / (((double) (currentTimeMS - lastTimeMS)) / 1000.0d);
         double upByteRate      = (double) (currentValues.upBytes     - previousValues.upBytes  )   / (((double) (currentTimeMS - lastTimeMS)) / 1000.0d);
         double upPacketsRate   = (double) (currentValues.upPackets   - previousValues.upPackets)   / (((double) (currentTimeMS - lastTimeMS)) / 1000.0d);
         
         BWDataPoint dataPoint = new BWDataPoint(lastTimeMS, currentTimeMS, downByteRate, downPacketsRate, upByteRate, upPacketsRate);
         bwLog.add(dataPoint);
      }
   }
   
   void sendToGatherer() {
      DataPointList dpList = new DataPointList((new BWDataPoint()).getName(), monitoredName, monitoredId, true, bwLog);
      DataPointList.sendToGatherer(dpList);
   }

   public static void main(String[] args) {
      
      if (args.length != 6 && args.length != 7) {
         System.out.println("usage: nodeTypeName nodeId gathererIp gathererPort duration(s) logDirectory [interface]");
         System.exit(1);
      }
      
      String nodeType     = args[0];
      int    id           = Integer.parseInt(args[1]);
      String gaddr        = args[2];
      int    gport        = Integer.parseInt(args[3]);
      long   durationSecs = Long.parseLong(args[4]);
      String logDirectory = args[5];
      String iface        = args.length > 6 ? args[6] : BWMonitor.DEFAULT_INTERFACE; 
      
      DataGatherer.configure(durationSecs, logDirectory, gaddr, gport);
      
      BWMonitor bwm = new BWMonitor(id, nodeType);
      
      bwm.log(iface);
      bwm.sendToGatherer();
   }

}
