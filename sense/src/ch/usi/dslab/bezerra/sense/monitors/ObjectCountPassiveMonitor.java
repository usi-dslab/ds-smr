/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import java.util.concurrent.atomic.AtomicLong;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.datapoints.ObjectCountDataPoint;

public class ObjectCountPassiveMonitor extends PassiveMonitor {

   AtomicLong currentObjectCount = new AtomicLong();

   public ObjectCountPassiveMonitor(int monitoredPid, String monitoredName) {
      super(monitoredPid, monitoredName);
   }

   public ObjectCountPassiveMonitor(int monitoredPid, String monitoredName, boolean recquired) {
      super(monitoredPid, monitoredName, recquired);
   }

   public void logCount(long count) {
      if (active == false)
         return;
      currentObjectCount.set(count);
      log();
   }

   synchronized void log() {
      if (active == false) {
         return;
      }
      long now = System.currentTimeMillis();
      if (now > lastLogTime + DataGatherer.LOG_INTERVAL_MS) {
         log.add(new ObjectCountDataPoint(lastLogTime, now, currentObjectCount.get()));
         lastLogTime = now;
      }
   }

   @Override
   public void saveToFile() {
      saveToFile(new ObjectCountDataPoint());
   }

   @Override
   public void sendToGatherer() {
      sendToGatherer(new ObjectCountDataPoint());
   }
}
