/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Pair;
import org.javatuples.Triplet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPDestination;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPMessage;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPReceiver;
import ch.usi.dslab.bezerra.sense.datapoints.DataPoint;

public class DataGatherer implements Runnable {
    public static final int LOG_INTERVAL_MS = 1000;            // milliseconds


    /*
     *  CONSTANTS
     */
    public static final int DEFAULT_WARM_UP_PERIOD_MS = 30000; // milliseconds
    public static final int CPU = 0;
    public static final int BW = 1;
    public static final int TP = 2;
    public static final int LAT = 3;
    public static final int DISK = 4;
    public static final int TIMELINE = 5;
    public static final int SERIALIZATION = 6;
    public static final int SERVER = 0;
    public static final int CLIENT = 1;
    public static final int CLIENTS_NODE = 2;
    public static final int RING_NODE = 3;
    public static final int DISK_NODE = 4;
    static Logger logger = LogManager.getLogger("DataGatherer");


    /*
     *  GLOBALLY ACCESSIBLE & SETTABLE VALUES
     */
    static long duration;
    static boolean saveToFileEnabled, sendToGathererEnabled;
    static String logDirectory;
    static String gathererAddress;
    static int gathererPort;
    static long warmUpPeriodToRemove_ms = DEFAULT_WARM_UP_PERIOD_MS;
    TCPReceiver tcpReceiver;
    // <<resource name, node type>, logs>
    Map<Pair<String, String>, Integer> remainingLogs = new HashMap<Pair<String, String>, Integer>();
    // <<resource name, node type, node id>, log>
    Map<Triplet<String, String, Integer>, DataPointList> receivedLogs
            = new ConcurrentHashMap<>();
    // <<resource name, node type>, average>
    Map<Pair<String, String>, DataPoint> averages
            = new ConcurrentHashMap<>();
    // <<resource name, node type>, aggregate>
    Map<Pair<String, String>, DataPoint> aggregates
            = new ConcurrentHashMap<>();

    public DataGatherer(int port, List<Triplet<String, String, Integer>> expectedLogs) {
        tcpReceiver = new TCPReceiver(port);
        for (Triplet<String, String, Integer> expectedLog : expectedLogs) {
            String resourceName = expectedLog.getValue0();
            String nodeType = expectedLog.getValue1();
            int logCount = expectedLog.getValue2();
            Pair<String, String> resourceAndNodetype = new Pair<String, String>(resourceName, nodeType);
            remainingLogs.put(resourceAndNodetype, logCount);
        }
    }

    public static void configure(long durationSeconds, String fileDirectory, String gathererAddress, int gathererPort) {
        configure(durationSeconds, fileDirectory, gathererAddress, gathererPort, DEFAULT_WARM_UP_PERIOD_MS);
    }

    public static void configure(long durationSeconds, String fileDirectory, String gathererAddress, int gathererPort, int warmUpPeriodToRemove_ms) {
        setDuration(durationSeconds);
        if (fileDirectory != null)
            enableFileSave(fileDirectory);
        if (gathererAddress != null) {
            enableGathererSend(gathererAddress, gathererPort);
        }
        DataGatherer.warmUpPeriodToRemove_ms = warmUpPeriodToRemove_ms;
    }

    public static void enableFileSave(String dir) {
        saveToFileEnabled = true;
        logDirectory = dir;
        new File(logDirectory).mkdirs();
    }

    public static void enableGathererSend(String address, int port) {
        sendToGathererEnabled = true;
        gathererAddress = address;
        gathererPort = port;
    }






   /*
    *  NETWORK LOG GATHERER
    */

    /**
     * @return Experiment duration in milliseconds
     */
    public static long getDuration() {
        return DataGatherer.duration;
    }

    public static void setDuration(long durationSeconds) {
        DataGatherer.duration = durationSeconds * 1000l;
    }

    public static boolean isSavingToDisk() {
        return saveToFileEnabled;
    }

    public static String getLogDirectory() {
        return logDirectory;
    }

    public static boolean isSendingToGatherer() {
        return sendToGathererEnabled;
    }

    public static TCPDestination getGathererDestination() {
        final String address = gathererAddress;
        final int port = gathererPort;
        TCPDestination gathererDestination = new TCPDestination() {
            public String getAddress() {
                return address;
            }

            public int getPort() {
                return port;
            }
        };
        return gathererDestination;
    }

    public static void main(String[] args) {
        //             0         1           2          3         4
        // <command> <port> <directory> {<resource> <nodetype> <count>}+

        int port = Integer.parseInt(args[0]);
        String directory = args[1];

        String[] expectedLogsParameters = Arrays.copyOfRange(args, 2, args.length);

        List<Triplet<String, String, Integer>> expectedLogs = parseArguments(expectedLogsParameters);

        DataGatherer gatherer = new DataGatherer(port, expectedLogs);

        gatherer.run();
        gatherer.stop();

        gatherer.removeNonFullyOverlappingAndWarmupSectionsFromLogs(DEFAULT_WARM_UP_PERIOD_MS);
        gatherer.createSummaries();
        gatherer.saveLogsToDisk(directory);
        gatherer.saveSummariesToDisk(directory);

        System.out.println("DataGatherer done.");
    }

    static List<Triplet<String, String, Integer>> parseArguments(String[] expectedLogsParameters) {
        List<Triplet<String, String, Integer>> retval = new ArrayList<Triplet<String, String, Integer>>();
        for (int i = 0; i < expectedLogsParameters.length; i++) {
            System.out.print(expectedLogsParameters[i] + " - ");
        }
        System.out.println();
        for (int i = 0; i < expectedLogsParameters.length - 2; i += 3) {
            String resourceName = new String(expectedLogsParameters[i]);
            String nodeType = new String(expectedLogsParameters[i + 1]);
            int nodeCount = Integer.parseInt(expectedLogsParameters[i + 2]);
            retval.add(new Triplet<String, String, Integer>(resourceName, nodeType, nodeCount));
        }

        return retval;
    }

    @Override
    public void run() {
        while (allLogsReceived() == false) {
            TCPMessage tcpMessage = tcpReceiver.receive();
            Message msg = tcpMessage.getContents();
            DataPointList dpList = DataPointList.receiveAtGatherer(msg);

            String resourceName = dpList.monitoredResource;
            String nodeType = dpList.monitoredType;
            int nodePid = dpList.monitoredId;

            Pair<String, String> resourceAndNodetype = new Pair<String, String>(resourceName, nodeType);
            System.out.println(String.format("resource: %s, nodetype: %s, nodePid: %d", resourceName, nodeType, nodePid));
            if (remainingLogs.containsKey(resourceAndNodetype)) {
                int remainingCount = remainingLogs.get(resourceAndNodetype);
                remainingCount--;
                remainingLogs.put(resourceAndNodetype, remainingCount);

                Triplet<String, String, Integer> logId = new Triplet<String, String, Integer>(resourceName, nodeType, nodePid);
                receivedLogs.put(logId, dpList);
                System.out.println("log stored for later processing");
            } else {
                System.out.println("log ignored. it was not expected...");
            }
        }
    }

    public void stop() {
        tcpReceiver.stop();
    }

    void removeNonFullyOverlappingAndWarmupSectionsFromLogs(long warm_up_period_ms) {
        long latestStartTime = 0;
        long earliestFinishTime = Long.MAX_VALUE;
        Set<Pair<String, String>> skippedLogs = new HashSet<Pair<String, String>>();

        for (Entry<Triplet<String, String, Integer>, DataPointList> logEntry : receivedLogs.entrySet()) {
            System.out.println("working on " + logEntry.getValue().monitoredType + " - " + logEntry.getValue().dataPointList.size());

            DataPointList dpl = logEntry.getValue();
            Pair<String, String> logType = dpl.getLogType();
            if (!dpl.recquired && dpl.isEmpty() || dpl.monitoredType.equals("server_object_count") ||
                    dpl.monitoredType.equals("client_query_rate") || dpl.monitoredType.equals("client_move_rate") ||
                    dpl.monitoredType.equals("client_retry_rate") || dpl.monitoredType.equals("client_global_command_rate") ||
                    dpl.monitoredType.equals("client_local_command_rate")) {
                System.out.println("Add metric to ignore list " + dpl.monitoredType);
                skippedLogs.add(logType);
            }
            if (skippedLogs.contains(logType))
                continue;
            dpl.abortProcessIfEmpty();
            if (dpl.getFirstTime() > latestStartTime) latestStartTime = dpl.getFirstTime();
            if (dpl.getLastTime() < earliestFinishTime) earliestFinishTime = dpl.getLastTime();
        }

        // removing warm-up period
        latestStartTime += warm_up_period_ms;

        if (latestStartTime > earliestFinishTime)
            logger.error("latestStartTime > earliestFinishTime ==== all datapointlists will be empty");

        for (Entry<Triplet<String, String, Integer>, DataPointList> logEntry : receivedLogs.entrySet()) {
            DataPointList dpl = logEntry.getValue();
            if (skippedLogs.contains(dpl.getLogType()))
                continue;
            for (Iterator<DataPoint> it = dpl.getList().iterator(); it.hasNext(); ) {
                DataPoint dp = it.next();
                if (dp.getInstant() < latestStartTime || dp.getInstant() > earliestFinishTime)
                    it.remove();
            }

            // L.Le tolerate empty datapoint list
            if (dpl.isEmpty()) {
                System.err.println(String.format("ERROR: DataGatherer received an empty DataPointList of %s from %s %d", dpl.monitoredResource, dpl.monitoredType, dpl.monitoredId));
                receivedLogs.entrySet().remove(logEntry);
            }
//            dpl.abortProcessIfEmpty();
        }

        Iterator<Entry<Triplet<String, String, Integer>, DataPointList>> it = receivedLogs.entrySet().iterator();
        while (it.hasNext()) {
            Entry<?, ?> pair = it.next();
            Pair<?, ?> logType = ((DataPointList) pair.getValue()).getLogType();
            if (skippedLogs.contains(logType))
                it.remove();
        }

    }

    void createSummaries() {
        Map<Pair<String, String>, DataPointList> allConcatenatedLists = new HashMap<Pair<String, String>, DataPointList>();
        Map<Pair<String, String>, DataPointList> averageLists = new HashMap<Pair<String, String>, DataPointList>();

        // creating superLists (one per resource-nodetype)
        for (Entry<Triplet<String, String, Integer>, DataPointList> singleIdLog : receivedLogs.entrySet()) {
            String resourceName = singleIdLog.getKey().getValue0();
            String nodeType = singleIdLog.getKey().getValue1();
            DataPointList dplist = singleIdLog.getValue();
            Pair<String, String> resourceAndNodetype = new Pair<String, String>(resourceName, nodeType);
            DataPointList allLogsConcatenated = allConcatenatedLists.get(resourceAndNodetype);
            if (allLogsConcatenated == null) {
                allLogsConcatenated = new DataPointList(resourceName, nodeType, 0, dplist.recquired, new ArrayList<DataPoint>());
                allConcatenatedLists.put(resourceAndNodetype, allLogsConcatenated);
            }
            allLogsConcatenated.appendList(dplist);

            if (dplist.getOne().isAggregatable() == false)
                continue;

            DataPointList averageList = averageLists.get(resourceAndNodetype);
            if (averageList == null) {
                averageList = new DataPointList(resourceName, nodeType, 0, dplist.recquired, new ArrayList<DataPoint>());
                averageLists.put(resourceAndNodetype, averageList);
            }
            averageList.addDataPoint(dplist.getAverage());
        }

        for (Entry<Pair<String, String>, DataPointList> concatenatedListEntry : allConcatenatedLists.entrySet()) {
            averages.put(concatenatedListEntry.getKey(), concatenatedListEntry.getValue().getAverage());
            // standardDeviations.put ...
            // confidenceIntervals.put ...
            // cdfs.put ...
        }

        for (Entry<Pair<String, String>, DataPointList> averageListEntry : averageLists.entrySet()) {
            aggregates.put(averageListEntry.getKey(), averageListEntry.getValue().getAggregate());
        }
    }

    void saveLogsToDisk(String directory) {
        new File(directory).mkdirs();
        try {

            for (Entry<Triplet<String, String, Integer>, DataPointList> logEntry : receivedLogs.entrySet()) {
                DataPointList dpl = logEntry.getValue();
                Path logPath = Paths.get(directory, dpl.monitoredResource + "_" + dpl.monitoredType + "_" + dpl.monitoredId + ".log");

                BufferedWriter writer = Files.newBufferedWriter(logPath, StandardCharsets.UTF_8);
                writer.write(String.format("# %s %s %d\n", dpl.monitoredResource, dpl.monitoredType, dpl.monitoredId));
                writer.write(String.format("# %s\n", dpl.getOne().getLogHeader()));
                writer.write(String.format("# average: %s\n", dpl.getAverage()));
                for (DataPoint dp : dpl.getList()) {
                    writer.write(String.format("%s\n", dp));
                }
                writer.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    void saveSummariesToDisk(String directory) {
        saveAveragesToDisk(directory);
        saveAggregatesToDisk(directory);
    }

    boolean allLogsReceived() {
        boolean allReceived = true;

        System.out.println("\nRemaining logs: ");

        for (Entry<Pair<String, String>, Integer> entry : remainingLogs.entrySet()) {
            String resourceName = entry.getKey().getValue0();
            String nodeType = entry.getKey().getValue1();
            int logCount = entry.getValue();
            System.out.println(String.format("%s - %s : %d", resourceName, nodeType, logCount));

            if (logCount > 0)
                allReceived = false;
        }

        return allReceived;
    }

    void saveAveragesToDisk(String directory) {
        for (Entry<Pair<String, String>, DataPoint> averageEntry : averages.entrySet()) {
            String resourceName = averageEntry.getKey().getValue0();
            String nodeType = averageEntry.getKey().getValue1();
            DataPoint dp = averageEntry.getValue();

            Path summaryPath = Paths.get(directory, resourceName + "_" + nodeType + "_average.log");

            try {
                BufferedWriter writer = Files.newBufferedWriter(summaryPath, StandardCharsets.UTF_8);
                writer.write(String.format("# %s %s\n", resourceName, nodeType));
                writer.write(String.format("# %s\n", dp.getLogHeader()));
                writer.write(String.format("%s\n", dp));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    void saveAggregatesToDisk(String directory) {
        for (Entry<Pair<String, String>, DataPoint> aggregateEntry : aggregates.entrySet()) {
            String resourceName = aggregateEntry.getKey().getValue0();
            String nodeType = aggregateEntry.getKey().getValue1();
            DataPoint dp = aggregateEntry.getValue();

            Path summaryPath = Paths.get(directory, resourceName + "_" + nodeType + "_aggregate.log");

            try {
                BufferedWriter writer = Files.newBufferedWriter(summaryPath, StandardCharsets.UTF_8);
                writer.write(String.format("# %s %s\n", resourceName, nodeType));
                writer.write(String.format("# %s\n", dp.getLogHeader()));
                writer.write(String.format("%s\n", dp));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
}
