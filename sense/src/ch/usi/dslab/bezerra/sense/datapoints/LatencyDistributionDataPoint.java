/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2015, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ch.usi.dslab.bezerra.sense.monitors.LatencyDistributionPassiveMonitor;

@SuppressWarnings("serial")
public class LatencyDistributionDataPoint implements DataPoint {

   public static class MutableLong implements Serializable {
      public long value = 0;
   }
   
   public static class BucketSet implements Serializable {
      Map<Long, MutableLong> buckets;
      public BucketSet() {
         buckets = new HashMap<Long, MutableLong>();
      }
      public boolean isEmpty() {
         return buckets.isEmpty();
      }
      public long increment(Long key) {
         return increment(key, 1);
      }
      public long increment(Long key, long incrementValue) {
         MutableLong counter = buckets.get(key);
         if (counter == null) {
            counter = new MutableLong();
            buckets.put(key, counter);
         }
         counter.value += incrementValue;
         return counter.value;
      }
      public void addBucketSet(BucketSet other) {
         for (Entry<Long, MutableLong> e : other.buckets.entrySet()) {
            increment(e.getKey(), e.getValue().value);
         }
      }
      public List<Bucket> getOrderedBuckets() {
         List<Bucket> orderedBuckets = new ArrayList<Bucket>(buckets.size());
         for (Entry<Long, MutableLong> e : buckets.entrySet()) {
            Bucket bucket = new Bucket(e.getKey(), e.getValue().value);
            orderedBuckets.add(bucket);
         }
         Collections.sort(orderedBuckets);
         return orderedBuckets;
      }
   }
   
   public static class Bucket implements Serializable, Comparable<Bucket> {
      long range;
      long count;
      public Bucket(long range, long count) {
         this.range = range;
         this.count = count;
      }
      @Override
      public int compareTo(Bucket o) {
         if (range < o.range) return -1;
         if (range > o.range) return  1;
         else return 0;
      }
   }
   
   private long intervalStart, intervalEnd;
   private BucketSet bucketList;
   
   public LatencyDistributionDataPoint(){}

   public LatencyDistributionDataPoint(long intervalStart, long intervalEnd, BucketSet intervalBuckets) {
      this.intervalStart = intervalStart;
      this.intervalEnd   = intervalEnd;
      this.bucketList    = intervalBuckets;
   }
   
   @Override
   public long getInstant() {
      return intervalStart;
   }

   @Override
   public long getEnd() {
      return intervalEnd;
   }

   @Override
   public DataPoint getAverage(List<DataPoint> values) {
      BucketSet bucketAverage = new BucketSet();
      for (DataPoint dp : values) {
         LatencyDistributionDataPoint lcdfdp = (LatencyDistributionDataPoint) dp;
         bucketAverage.addBucketSet(lcdfdp.bucketList);
      }
      return new LatencyDistributionDataPoint(0, 0, bucketAverage);
   }

   @Override
   public DataPoint getAggregate(List<DataPoint> values) {
      BucketSet bucketAggregate = new BucketSet();
      for (DataPoint dp : values) {
         LatencyDistributionDataPoint lcdfdp = (LatencyDistributionDataPoint) dp;
         bucketAggregate.addBucketSet(lcdfdp.bucketList);
      }
      
      // create cumulative (aggregate) distribution
      List<Long> bucketRanges = new ArrayList<Long>(bucketAggregate.buckets.keySet());
      Collections.sort(bucketRanges);
      Long currentAggregate = 0L;
      for (long range : bucketRanges) {
         currentAggregate = bucketAggregate.increment(range, currentAggregate);
      }
      
      return new LatencyDistributionDataPoint(0, 0, bucketAggregate);
   }

   @Override
   public boolean isAggregatable() {
      return true;
   }

   @Override
   public String getName() {
      return "latencydistribution";
   }

   @Override
   public String getLogHeader() {
      return String.format("latencydistribution (%d ns buckets)", LatencyDistributionPassiveMonitor.getBucketWidthNano());
   }
   
   @Override
   public String toString() {
      List<Bucket> orderedBuckets = bucketList.getOrderedBuckets();
      StringBuffer sb = new StringBuffer();
      sb.append(String.format("%d ", getInstant()));
      for (Bucket b : orderedBuckets)
         sb.append(String.format("%d %d ", b.range, b.count));
      return sb.toString();
   }

}
