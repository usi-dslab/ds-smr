/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.util.List;

public class BWDataPoint implements DataPoint {
   private static final long serialVersionUID = 1L;
   
   public long   begin, end;
   public double totalByteRate;
   public double totalPacketRate;
   public double downByteRate;
   public double downPacketRate;
   public double upByteRate;
   public double upPacketRate;
   
   public BWDataPoint(){}
   
   public BWDataPoint(long begin, long end, double downByteRate, double downPacketRate,
         double upByteRate, double upPacketRate) {
      this.begin  = begin;
      this.end    = end;
      this.downByteRate   = downByteRate;
      this.downPacketRate = downPacketRate;
      this.upByteRate   = upByteRate;
      this.upPacketRate = upPacketRate;
      this.totalByteRate   = downByteRate   + upByteRate;
      this.totalPacketRate = downPacketRate + upPacketRate;
   }
   
   @Override
   public long getInstant() {
      return begin;
   }

   @Override
   public long getEnd() {
      return end;
   }
   
   void add(BWDataPoint other) {
      this.totalByteRate   += other.totalByteRate;
      this.totalPacketRate += other.totalPacketRate;
      this.downByteRate    += other.downByteRate;
      this.downPacketRate  += other.downPacketRate;
      this.upByteRate      += other.upByteRate;
      this.upPacketRate    += other.upPacketRate;
   }

   @Override
   public DataPoint getAverage(List<DataPoint> values) {
      BWDataPoint avg = new BWDataPoint();
      long totalWeight = 0;
      for (DataPoint dp : values) {
         BWDataPoint bwdp = (BWDataPoint) dp;
         long weight = bwdp.end - bwdp.begin;
      
         totalWeight += weight;
         
         avg.totalByteRate   += bwdp.totalByteRate   * weight;
         avg.totalPacketRate += bwdp.totalPacketRate * weight;
         avg.downByteRate    += bwdp.downByteRate    * weight;
         avg.downPacketRate  += bwdp.downPacketRate  * weight;
         avg.upByteRate      += bwdp.upByteRate      * weight;
         avg.upPacketRate    += bwdp.upPacketRate    * weight;
         
      }

      avg.totalByteRate   /= totalWeight;
      avg.totalPacketRate /= totalWeight;
      avg.downByteRate    /= totalWeight;
      avg.downPacketRate  /= totalWeight;
      avg.upByteRate      /= totalWeight;
      avg.upPacketRate    /= totalWeight;
      
      return avg;
   }

   @Override
   public String getName() {
      return "bandwidth";
   }
      
   @Override
   public String getLogHeader() {
      return "bandwidth (totalBps totalPps downBps downPps upBps upPps; B = Byte, P = Packet) ";
   }
   
   @Override
   public String toString() {
      return String.format("%f %f %f %f %f %f", this.totalByteRate, this.totalPacketRate, this.downByteRate, this.downPacketRate, this.upByteRate, this.upPacketRate);
   }

   @Override
   public DataPoint getAggregate(List<DataPoint> values) {
      BWDataPoint aggregate = new BWDataPoint();
      for (DataPoint value : values) {
         BWDataPoint bwvalue = (BWDataPoint) value;
         aggregate.add(bwvalue);
      }
      return aggregate;
   }

   @Override
   public boolean isAggregatable() {
      return true;
   }

}
