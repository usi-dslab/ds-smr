/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.util.List;

public class ObjectCountDataPoint implements DataPoint {
   private static final long serialVersionUID = 1L;

   long begin, end;
    long count;

   public ObjectCountDataPoint(){}

   public ObjectCountDataPoint(long begin, long end, long count) {
      this.begin = begin;
      this.end = end;
      this.count = count;
   }

   @Override
   public long getInstant() {
      return begin;
   }

   @Override
   public long getEnd() {
      return end;
   }

   @Override
   public DataPoint getAverage(List<DataPoint> values) {
      ObjectCountDataPoint avg = new ObjectCountDataPoint();
      long totalWeight = 0;
      for (DataPoint dp : values) {
         ObjectCountDataPoint mrdp = (ObjectCountDataPoint) dp;
         long weight = mrdp.end - mrdp.begin;
         totalWeight += weight;
         avg.count += mrdp.count * weight;
      }
      avg.count /= (double) totalWeight;
      return avg;
   }

   @Override
   public String getName() {
      return "object";
   }

   @Override
   public String getLogHeader() {
      return "objects (objs) ";
   }

   @Override
   public String toString() {
       return String.format("%d %d %d", getInstant(), end - begin, count);
   }

   @Override
   public DataPoint getAggregate(List<DataPoint> values) {
      return null;
   }

   @Override
   public boolean isAggregatable() {
      return false;
   }

}
