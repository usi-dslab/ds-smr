/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
public class CPUDataPoint implements Serializable, DataPoint {
   
   public long instant;
   public long end;
   // loads refer to the overall load, plus the load of each individual base
   // thus, loads.size() == 1 + system cores
   public ArrayList<CoreLoad> coreLoads = new ArrayList<CoreLoad>();
   public CPUDataPoint() {}
   
   public CPUDataPoint (long instant) {
      this.instant = instant;
   }
   
   public void appendCoreLoad(double load, double user, double sys, double idle) {
      CoreLoad cm = new CoreLoad(load, user, sys, idle);
      this.coreLoads.add(cm);
   }
   
   public void appendCoreLoad(CoreLoad load) {
      CoreLoad cm = new CoreLoad(load.load, load.user, load.sys, load.idle);
      this.coreLoads.add(cm);
   }
   
   public void add(CPUDataPoint other) {
      for (int i = 0 ; i < other.coreLoads.size(); i++) {
         CoreLoad otherCoreLoad = other.coreLoads.get(i);
         if (i >= this.coreLoads.size()) {
            this.appendCoreLoad(otherCoreLoad);
         }
         else {
            CoreLoad thisCoreLoad = this.coreLoads.get(i);
            thisCoreLoad.add(otherCoreLoad);
         }
      }
   }
   
   public void div(double divisor) {
      for (CoreLoad coreLoad : coreLoads) {
         coreLoad.load /= divisor;
         coreLoad.user /= divisor;
         coreLoad.sys  /= divisor;
         coreLoad.idle /= divisor;
      }
   }
   
   @Override
   public long getInstant() {
      return instant;
   }

   @Override
   public long getEnd() {
      return end;
   }

   @Override
   public DataPoint getAverage(List<DataPoint> values) {
      CPUDataPoint average = new CPUDataPoint();
      for (DataPoint dcpulog : values) {
         CPUDataPoint cpuentry = (CPUDataPoint) dcpulog;
         for (int i = 0 ; i < cpuentry.coreLoads.size() ; i++) {
            CoreLoad loadEntry = cpuentry.coreLoads.get(i);
            if (i >= average.coreLoads.size()) {
               average.appendCoreLoad(loadEntry);
            }
            else {
               CoreLoad avgload = average.coreLoads.get(i);
               avgload.add(loadEntry);
            }
         }
      }
      average.div(values.size());
      return average;
   }

   @Override
   public String getName() {
      return "cpu";
   }

   @Override
   public String getLogHeader() {
      String header = "cpu (all: load user sys idle";
      for (int i = 0 ; i < coreLoads.size() -1 ; i++)
         header += "; cpu" + i + ": load user sys idle";
      header += ")";
      return header;
   }

   @Override
   public String toString() {
      String vis = String.format("%d", instant);
      for (CoreLoad coreLoad : coreLoads)
         vis += String.format(" %.1f %.1f %.1f %.1f", coreLoad.load, coreLoad.user, coreLoad.sys, coreLoad.idle);
      return vis;
   }

   @Override
   public DataPoint getAggregate(List<DataPoint> values) {
      CPUDataPoint aggregate = new CPUDataPoint();
      for (DataPoint dp : values) {
         CPUDataPoint cpudp = (CPUDataPoint) dp;
         aggregate.add(cpudp);
      }
      return aggregate;
   }

   @Override
   public boolean isAggregatable() {
      return true;
   }

   public static class CoreLoad implements Serializable {

      public double load;
      public double user;
      public double sys;
      public double idle;

      public CoreLoad() {
      }

      public CoreLoad(double load, double user, double sys, double idle) {
         this.load = load;
         this.user = user;
         this.sys = sys;
         this.idle = idle;
      }

      public void add(CoreLoad other) {
         this.load += other.load;
         this.user += other.user;
         this.sys += other.sys;
         this.idle += other.idle;
      }

   }
}
