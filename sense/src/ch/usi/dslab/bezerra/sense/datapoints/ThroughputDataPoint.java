/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.util.List;

public class ThroughputDataPoint implements DataPoint {
   private static final long serialVersionUID = 1L;

   long begin, end;
   double tp;

   public ThroughputDataPoint(){}

   public ThroughputDataPoint(long begin, long end, double value) {
      this.begin = begin;
      this.end   = end;
      this.tp    = value;
   }

   @Override
   public long getInstant() {
      return begin;
   }

   @Override
   public long getEnd() {
      return end;
   }
   
   public void add(ThroughputDataPoint other) {
      this.tp += other.tp;
   }

   @Override
   public DataPoint getAverage(List<DataPoint> values) {
      ThroughputDataPoint avg = new ThroughputDataPoint();
      long totalWeight = 0;
      for (DataPoint dp : values) {
         ThroughputDataPoint tdp = (ThroughputDataPoint) dp;
         long weight = tdp.end - tdp.begin;
         totalWeight += weight;
         avg.tp += tdp.tp * weight;
      }
      avg.tp /= (double) totalWeight;
      return avg;
   }

   @Override
   public String getName() {
      return "throughput";
   }

   @Override
   public String getLogHeader() {
      return "throughput (cps) ";
   }

   @Override
   public String toString() {
      return String.format("%d %d %f", getInstant(), end - begin, tp);
   }

   @Override
   public DataPoint getAggregate(List<DataPoint> values) {
      ThroughputDataPoint aggregate = new ThroughputDataPoint();
      for (DataPoint dp : values) {
         ThroughputDataPoint tdp = (ThroughputDataPoint) dp;
         aggregate.add(tdp);
      }
      return aggregate;
   }

   @Override
   public boolean isAggregatable() {
      return true;
   }

}
