/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TimelineDataPoint implements DataPoint {
   
   public static class Pair<T0, T1> implements Serializable {
      private static final long serialVersionUID = 1L;
      T0 v0;
      T1 v1;
      public Pair() {}
      public Pair(T0 v0, T1 v1) {
         this.v0 = v0;
         this.v1 = v1;
      }
      public T0 getValue0() {
         return v0;
      }
      public T1 getValue1() {
         return v1;
      }
   }
   
   private static final long serialVersionUID = 1L;
   
   private int weight;
   List<Pair<String, Double>> timeStamps;

   public TimelineDataPoint() {}
   
   public TimelineDataPoint(int weight, Object... stampTokens) {
      if (stampTokens.length % 2 != 0) {
         System.err.println("TimelineDataPoint needs an even number of tokens. Exiting...");
         System.exit(1);
      }
      this.weight = weight;
      timeStamps = new ArrayList<Pair<String, Double>>(stampTokens.length / 2);
      for (int i = 0 ; i < stampTokens.length ; i += 2) {
         String stampName  = (String) stampTokens[i];
         double stampValue = ((Number)  stampTokens[i+1]).doubleValue();
         Pair<String, Double> stamp = new Pair<String, Double>(stampName, stampValue);
         timeStamps.add(stamp);
      }
   }

   public TimelineDataPoint(TimelineDataPoint reference) {
      this.timeStamps = new ArrayList<Pair<String, Double>>();
      for (Pair<String, Double> stamp : reference.timeStamps) {
         Pair<String, Double> zeroStamp = new Pair<String, Double>(stamp.getValue0(), 0D);
         this.timeStamps.add(zeroStamp);
      }
   }

   public int getNumStamps() {
      return timeStamps.size();
   }

   public void add(TimelineDataPoint other) {
      this.weight += other.weight;
      for (int stampIndex = 0 ; stampIndex < timeStamps.size() ; stampIndex++) {
         String stampName  = this .timeStamps.get(stampIndex).getValue0();
         double thisValue  = this .timeStamps.get(stampIndex).getValue1();
         double otherValue = other.timeStamps.get(stampIndex).getValue1();
         Pair<String, Double> sumStamp = new Pair<String, Double>(stampName, thisValue + otherValue);
         this.timeStamps.set(stampIndex, sumStamp);
      }
   }

   public void multiply(double factor) {
      for (int stampIndex = 0 ; stampIndex < timeStamps.size() ; stampIndex++) {
         String stampName     = this.timeStamps.get(stampIndex).getValue0();
         double originalValue = this.timeStamps.get(stampIndex).getValue1();
         Pair<String, Double> mulStamp = new Pair<String, Double>(stampName, originalValue * factor);
         this.timeStamps.set(stampIndex, mulStamp);
      }
   }

   public void div(double divisor) {
      for (int stampIndex = 0 ; stampIndex < timeStamps.size() ; stampIndex++) {
         String stampName     = this.timeStamps.get(stampIndex).getValue0();
         double originalValue = this.timeStamps.get(stampIndex).getValue1();
         Pair<String, Double> divStamp = new Pair<String, Double>(stampName, originalValue / divisor);
         this.timeStamps.set(stampIndex, divStamp);
      }
   }

   public void multiplyStampsByWeight() {
      multiply(weight);
   }

   public void divideStampsByWeight() {
      div(weight);
   }

   @Override
   public long getInstant() {
      final int first = 0;
      return Math.round(Math.floor(timeStamps.get(first).getValue1()));
   }

   @Override
   public long getEnd() {
      final int last = timeStamps.size() - 1;
      return Math.round(Math.ceil(timeStamps.get(last).getValue1()));
   }

   /**
     * This method assumes that all TimelineDataPoint objects in values have
     * the same number of Pair objects
     */
   @Override
   public DataPoint getAverage(List<DataPoint> values) {
      if (values.isEmpty()
            || ((TimelineDataPoint)(values.get(0))).timeStamps.isEmpty()
            || ((TimelineDataPoint)(values.get(0))).timeStamps.get(0) == null)
         return null;

      TimelineDataPoint firstTimeline = (TimelineDataPoint) values.get(0);

      double totalWeight = 0d;
      TimelineDataPoint avg = new TimelineDataPoint(firstTimeline);
      for (DataPoint dp : values) {
         TimelineDataPoint tldp = (TimelineDataPoint) dp;
         tldp.multiplyStampsByWeight();
         totalWeight += tldp.weight;
         avg.add(tldp);
      }
      avg.div(totalWeight);
      return avg;
   }

   @Override
   public DataPoint getAggregate(List<DataPoint> values) {
      return null;
   }

   @Override
   public boolean isAggregatable() {
      return false;
   }

   @Override
   public String getName() {
      return "timeline";
   }

   @Override
   public String getLogHeader() {
      return "timeline ([stampName stampValue]+)";
   }
   
   @Override
   public String toString() {
      String str = String.format("%d %d", getInstant(), weight);
      for (int i = 0 ; i < timeStamps.size() ; i++) {
         Pair<String, Double> stamp = timeStamps.get(i);
         str += String.format(" %s %.2f", stamp.getValue0(), stamp.getValue1());
      }
      return str;
   }
}
