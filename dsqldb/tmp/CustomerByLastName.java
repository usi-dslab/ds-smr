/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.dsqldb.tables;

import java.util.ArrayList;
import java.util.List;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.Partition;

/**
 * Created by longle on 18/02/16.
 */
public class CustomerByLastName extends Base {
    public final MODEL model = MODEL.CUSTOMER;
    public List<Integer> c_ids = new ArrayList<>();
    public int c_w_id;
    public int c_d_id;
    public String c_last;

    public CustomerByLastName() {
    }

    public CustomerByLastName(String c_last, int c_w_id, int c_d_id) {
        int first = genPKId(MODEL.CUSTOMER, c_last.hashCode());
        int second = genPKId(MODEL.WAREHOUSE, c_w_id);
        int third = genPKId(MODEL.DISTRICT, c_d_id);
        this.setId(new ObjId(first, second, third));
        this.c_d_id = c_d_id;
        this.c_w_id = c_w_id;
        this.c_last = c_last;
    }

    @Override
    public Message getSuperDiff(List<Partition> destinations) {
        return new Message(this.c_d_id, this.c_w_id, this.c_last);
    }

    @Override
    public void updateFromDiff(Message objectDiff) {
        this.c_d_id = (int) objectDiff.getNext();
        this.c_w_id = (int) objectDiff.getNext();
        this.c_last = (String) objectDiff.getNext();
    }

    @Override
    public String toString() {
        return (
                "\n***************** Customer ********************" +
                        "\n*          c_ids = " + c_ids +
                        "\n*         c_d_id = " + c_d_id +
                        "\n*         c_w_id = " + c_w_id +
                        "\n**********************************************"
        );
    }
}
