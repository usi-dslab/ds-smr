package ch.usi.dslab.lel.dsqldb.jtpcc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.ApplicationProcedure;
import ch.usi.dslab.lel.aerie.CacheMultimap;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.client.PromiseClient;
import ch.usi.dslab.lel.dsqldb.tables.Base;

/**
 * Author: longle, created on 14/07/16.
 */
public class jTPCCConnection extends PromiseClient {
    public jTPCCConnection(int clientId, String systemConfigFile, String partitioningFile, ApplicationProcedure applicationProcedure) {
        super(clientId, systemConfigFile, partitioningFile, applicationProcedure, new CacheMultimap());
    }

    public static ObjId getWarehouseObjId(int w_id) {
        return new ObjId(Base.genObjId(Base.MODEL.WAREHOUSE,
                new String[]{"w_id", String.valueOf(w_id)}));
    }

    public static ObjId getDistrictObjId(int d_id, int d_w_id) {
        return new ObjId(Base.genObjId(Base.MODEL.DISTRICT,
                new String[]{"d_id", String.valueOf(d_id), "d_w_id", String.valueOf(d_w_id)}));
    }

    public static ObjId getCustomerObjId(int c_id, int c_d_id, int c_w_id) {
        return new ObjId(Base.genObjId(Base.MODEL.CUSTOMER,
                new String[]{"c_id", String.valueOf(c_id),
                        "c_d_id", String.valueOf(c_d_id),
                        "c_w_id", String.valueOf(c_w_id)}));
    }

    public static ObjId getCustomerObjId(String c_last, int c_d_id, int c_w_id) {
        return new ObjId(Base.genObjId(Base.MODEL.CUSTOMER,
                new String[]{"c_d_id", String.valueOf(c_d_id),
                        "c_w_id", String.valueOf(c_w_id),
                        "c_last", String.valueOf(c_last)}));
    }

    public static ObjId getNewOrderObjId(int no_o_id, int no_d_id, int no_w_id) {
        return Base.genObjId(Base.MODEL.NEWORDER,
                new String[]{"no_o_id", String.valueOf(no_o_id),
                        "no_d_id", String.valueOf(no_d_id),
                        "no_w_id", String.valueOf(no_w_id)});
    }

    public static ObjId getStockObjId(int s_w_id, int s_i_id) {
        return new ObjId(Base.genObjId(Base.MODEL.STOCK, new String[]{
                "s_w_id", String.valueOf(s_w_id),
                "s_i_id", String.valueOf(s_i_id),
        }));
    }

    public static ObjId getItemId(int i_id) {
        return new ObjId(Base.genObjId(Base.MODEL.ITEM,
                new String[]{"i_id", String.valueOf(i_id)}));
    }

    public static ObjId getOrderId(int o_id, int o_w_id, int o_d_id, int o_c_id) {
        return Base.genObjId(Base.MODEL.ORDER,
                new String[]{"o_id", String.valueOf(o_id),
                        "o_w_id", String.valueOf(o_w_id),
                        "o_d_id", String.valueOf(o_d_id),
                        "o_c_id", String.valueOf(o_c_id)});
    }

    public static ObjId getOrderIdWithCustomerId(int o_w_id, int o_d_id, int o_c_id) {
        return Base.genObjId(Base.MODEL.ORDER, new String[]{
                "o_d_id", String.valueOf(o_d_id),
                "o_w_id", String.valueOf(o_w_id),
                "o_c_id", String.valueOf(o_c_id)});
    }

    public static ObjId getOrderIdWithOrderId(int o_w_id, int o_d_id, int o_id) {
        return Base.genObjId(Base.MODEL.ORDER, new String[]{
                "o_id", String.valueOf(o_id),
                "o_d_id", String.valueOf(o_d_id),
                "o_w_id", String.valueOf(o_w_id),
        });
    }

    public static ObjId getOrderLineObjId(int ol_number, int ol_w_id, int ol_d_id, int ol_o_id) {
        return Base.genObjId(Base.MODEL.ORDERLINE, new String[]{
                "ol_number", String.valueOf(ol_number),
                "ol_w_id", String.valueOf(ol_w_id),
                "ol_d_id", String.valueOf(ol_d_id),
                "ol_o_id", String.valueOf(ol_o_id)
        });
    }

    public static ObjId getOrderLineObjId(int ol_o_id, int ol_d_id, int ol_w_id) {
        return Base.genObjId(Base.MODEL.ORDERLINE, new String[]{
                "ol_o_id", String.valueOf(ol_o_id),
                "ol_d_id", String.valueOf(ol_d_id),
                "ol_w_id", String.valueOf(ol_w_id)
        });
    }


    public static <T extends Base> T getObjectFromReply(Class<T> cls, Object reply) {
        ((Message) reply).rewind();
        Object ret = ((Message) reply).getNext();
        Class clazz = ret.getClass();
        if (Set.class.isAssignableFrom(clazz)) {
            List<Base> res = new ArrayList((HashSet) ret);
            for (Base obj : res) {
                if (cls.isInstance(obj)) return cls.cast(obj);
            }
            return null;
        } else if (Base.class.isAssignableFrom(clazz)) {
            if (cls.isInstance(ret)) return cls.cast(ret);
            else return null;
        } else {
            return null;
        }
    }

    public static <T extends Base> List<T> getObjectsFromReply(Class<T> cls, Object reply) {
        ((Message) reply).rewind();
        Object ret = ((Message) reply).getNext();
        Class clazz = ret.getClass();
        List<T> result = new ArrayList<>();
        if (Set.class.isAssignableFrom(clazz)) {
            List<Base> res = new ArrayList((HashSet) ret);
            for (Base obj : res) {
                if (cls.isInstance(obj)) result.add(cls.cast(obj));
            }
            return result;
        } else {
            return new ArrayList();
        }
    }
}
