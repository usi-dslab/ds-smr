package ch.usi.dslab.lel.dsqldb.tpcc;/*
 * jTPCCUtil - utility functions for the Open Source Java implementation of
 *    the TPC-C benchmark
 *
 * Copyright (C) 2003, Raul Barbosa
 * Copyright (C) 2004-2016, Denis Lussier
 *
 */


import java.util.Random;

public class jTPCCUtil extends jTPCCConfig {

    public static String getSysProp(String inSysProperty, String defaultValue) {

        String outPropertyValue = null;

        try {
            outPropertyValue = System.getProperty(inSysProperty, defaultValue);
        } catch (Exception e) {
            System.err.println("Error Reading Required System Property '" + inSysProperty + "'");
        }

        return (outPropertyValue);

    } // end getSysProp


    public static String randomStr(long strLen) {

        char freshChar;
        String freshString;
        freshString = "";

        while (freshString.length() < (strLen - 1)) {

            freshChar = (char) (Math.random() * 128);
            if (Character.isLetter(freshChar)) {
                freshString += freshChar;
            }
        }

        return (freshString);

    } // end randomStr


    public static String getCurrentTime() {
        return dateFormat.format(new java.util.Date());
    }

    public static String formattedDouble(double d) {
        String dS = "" + d;
        return dS.length() > 6 ? dS.substring(0, 6) : dS;
    }

    public static int getItemID(Random r) {
//        return nonUniformRandom(8191, 1, 100000, r);
        return nonUniformRandom(((Long) Math.round(jTPCCConfig.configItemCount * 0.08191)).intValue(), 1, jTPCCConfig.configItemCount, r);
    }

    public static int getCustomerID(Random r) {
//        return nonUniformRandom(1023, 1, 3000, r);
        return nonUniformRandom(((Long) Math.round(jTPCCConfig.configCustPerDist * 0.341)).intValue(), 1, jTPCCConfig.configCustPerDist, r);
    }

    public static String getLastName(Random r) {
        int num = (int) nonUniformRandom(255, 0, 999, r);
        return nameTokens[num / 100] + nameTokens[(num / 10) % 10] + nameTokens[num % 10];
    }

    public static int randomNumber(int min, int max, Random r) {
        return (int) (r.nextDouble() * (max - min + 1) + min);
    }


    // A: A is a constant chosen according to the size of the range [x .. y] for C_LAST, the range is [0 .. 999] and A = 255
    //    for C_ID, the range is [1 .. 3000] and A = 1023
    //    for OL_I_ID, the range is [1 .. 100000] and A = 8191
    public static int nonUniformRandom(int A, int min, int max, Random r) {
        return (((randomNumber(0, A, r) | randomNumber(min, max, r)) + randomNumber(0, A, r)) % (max - min + 1)) + min;
    }

    public static String leftPad(String str, int left) {
        return org.apache.commons.lang.StringUtils.leftPad(str, left, " ");
    }

} // end jTPCCUtil
