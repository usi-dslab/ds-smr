package ch.usi.dslab.lel.dsqldb.tpcc;/*
 * jTPCCTerminal - Terminal emulator code for jTPCC (transactions)
 *
 * Copyright (C) 2003, Raul Barbosa
 * Copyright (C) 2004-2016, Denis Lussier
 * Copyright (C) 2016, Jan Wieck
 *
 */

import com.google.common.collect.ComparisonChain;
import com.google.common.util.concurrent.AtomicDouble;

import org.apache.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.dsqldb.DSqlCommand;
import ch.usi.dslab.lel.dsqldb.tables.Base;
import ch.usi.dslab.lel.dsqldb.tables.Customer;
import ch.usi.dslab.lel.dsqldb.tables.District;
import ch.usi.dslab.lel.dsqldb.tables.History;
import ch.usi.dslab.lel.dsqldb.tables.Item;
import ch.usi.dslab.lel.dsqldb.tables.NewOrder;
import ch.usi.dslab.lel.dsqldb.tables.Order;
import ch.usi.dslab.lel.dsqldb.tables.OrderLine;
import ch.usi.dslab.lel.dsqldb.tables.Stock;
import ch.usi.dslab.lel.dsqldb.tables.Warehouse;


public class jTPCCTerminal extends jTPCCConfig implements Runnable {
    private static Logger log = Logger.getLogger(jTPCCTerminal.class);
    long terminalStartTime = 0;
    long transactionEnd = 0;
    jTPCC parent;
    private String terminalName;
    private int terminalWarehouseID, terminalDistrictID;
    private int paymentWeight, orderStatusWeight, deliveryWeight, stockLevelWeight, limPerMin_Terminal;
    private Random gen;
    private int transactionCount = 1, numTransactions, numWarehouses, newOrderCounter;
    private long totalTnxs = 1;
    private StringBuffer query = null;
    private int result = 0;
    private boolean stopRunningSignal = false;

    public jTPCCTerminal
            (String terminalName, int terminalWarehouseID, int terminalDistrictID, int numTransactions, int paymentWeight, int orderStatusWeight,
             int deliveryWeight, int stockLevelWeight, int numWarehouses, int limPerMin_Terminal, jTPCC parent) throws SQLException {
        this.terminalName = terminalName;

        this.terminalWarehouseID = terminalWarehouseID;
        this.terminalDistrictID = terminalDistrictID;
        this.parent = parent;
        this.numTransactions = numTransactions;
        this.paymentWeight = paymentWeight;
        this.orderStatusWeight = orderStatusWeight;
        this.deliveryWeight = deliveryWeight;
        this.stockLevelWeight = stockLevelWeight;
        this.numWarehouses = numWarehouses;
        this.newOrderCounter = 0;
        this.limPerMin_Terminal = limPerMin_Terminal;

        terminalMessage("");
        terminalMessage("Terminal \'" + terminalName + "\' has WarehouseID=" + terminalWarehouseID + " and DistrictID=" + terminalDistrictID + ".");
        terminalStartTime = System.currentTimeMillis();
    }

    public void run() {
        gen = new Random(System.nanoTime());

        executeTransactions(numTransactions);

        printMessage("");
        printMessage("Terminal \'" + terminalName + "\' finished after " + (transactionCount - 1) + " transaction(s).");

        parent.signalTerminalEnded(this, newOrderCounter);

    }

    public void stopRunningWhenPossible() {
        stopRunningSignal = true;
        printMessage("");
        printMessage("Terminal received stop signal!");
        printMessage("Finishing current transaction before exit...");
    }

    private void executeTransactions(int numTransactions) {
        boolean stopRunning = false;

        if (numTransactions != -1)
            printMessage("Executing " + numTransactions + " transactions...");
        else
            printMessage("Executing for a limited time...");
        ArrayList<CompletableFuture<Void>> trans = new ArrayList<>();
        CompletableFuture<Void> execution;
        for (int i = 0; (i < numTransactions || numTransactions == -1) && !stopRunning; i++) {

            long transactionType = jTPCCUtil.randomNumber(1, 100, gen);
            int skippedDeliveries = 0, newOrder = 0;
            String transactionTypeName;

            long transactionStart = System.currentTimeMillis();
            Timeline tl = new Timeline();
            tl.addMileStone("transaction-prepare", transactionStart);
            if (transactionType <= paymentWeight) {
                transactionTypeName = "Payment";
                tl.setTransaction(transactionTypeName);
                execution = executeTransaction(PAYMENT, tl);
            } else if (transactionType <= paymentWeight + stockLevelWeight) {
                transactionTypeName = "Stock-Level";
                tl.setTransaction(transactionTypeName);
                execution = executeTransaction(STOCK_LEVEL, tl);
            } else if (transactionType <= paymentWeight + stockLevelWeight + orderStatusWeight) {
                transactionTypeName = "Order-Status";
                tl.setTransaction(transactionTypeName);
                execution = executeTransaction(ORDER_STATUS, tl);
            } else if (transactionType <= paymentWeight + stockLevelWeight + orderStatusWeight + deliveryWeight) {
                transactionTypeName = "Delivery";
                tl.setTransaction(transactionTypeName);
                execution = executeTransaction(DELIVERY, tl);
            } else {
                transactionTypeName = "New-Order";
                tl.setTransaction(transactionTypeName);
                execution = executeTransaction(NEW_ORDER, tl);
                trans.add(execution);
                newOrderCounter++;
                newOrder = 1;
            }
            try {
                execution.get();
                tl.addMileStone("transaction-end", System.currentTimeMillis());
                terminalMessage(tl.toString());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            long transactionEnd = System.currentTimeMillis();

            if (!transactionTypeName.equals("Delivery")) {
                parent.signalTerminalEndedTransaction(i, this.terminalName, transactionTypeName, transactionEnd - transactionStart, null, newOrder);
            } else {
                parent.signalTerminalEndedTransaction(i, this.terminalName, transactionTypeName, transactionEnd - transactionStart, (skippedDeliveries == 0 ? "None" : "" + skippedDeliveries + " delivery(ies) skipped."), newOrder);
            }

            if (limPerMin_Terminal > 0) {
                long elapse = transactionEnd - transactionStart;
                long timePerTx = 60000 / limPerMin_Terminal;

                if (elapse < timePerTx) {
                    try {
                        long sleepTime = timePerTx - elapse;
                        Thread.sleep((sleepTime));
                    } catch (Exception e) {
                    }
                }
            }


            if (stopRunningSignal) stopRunning = true;
        }

    }


    private CompletableFuture<Void> executeTransaction(int transaction, Timeline tl) {
        int result = 0;
        int districtID;
        int customerID;
        int numItems;
        int[] itemIDs;
        int[] supplierWarehouseIDs;
        int[] orderQuantities;
        boolean customerByName = false;
        String customerLastName = null;
        int y = jTPCCUtil.randomNumber(1, 100, gen);
        transactionCount++;
        switch (transaction) {
            case NEW_ORDER:
                districtID = jTPCCUtil.randomNumber(1, jTPCCConfig.configDistPerWhse, gen);
                customerID = jTPCCUtil.getCustomerID(gen);

                numItems = (int) jTPCCUtil.randomNumber(5, 15, gen);
                itemIDs = new int[numItems];
                supplierWarehouseIDs = new int[numItems];
                orderQuantities = new int[numItems];
                int allLocal = 1;
                for (int i = 0; i < numItems; i++) {
                    itemIDs[i] = jTPCCUtil.getItemID(gen);
                    if (jTPCCUtil.randomNumber(1, 100, gen) > 1) {
                        supplierWarehouseIDs[i] = terminalWarehouseID;
                    } else {
                        do {
                            supplierWarehouseIDs[i] = jTPCCUtil.randomNumber(1, numWarehouses, gen);
                        }
                        while (supplierWarehouseIDs[i] == terminalWarehouseID && numWarehouses > 1);
                        allLocal = 0;
                    }
                    orderQuantities[i] = jTPCCUtil.randomNumber(1, 10, gen);
                }

                // we need to cause 1% of the new orders to be rolled back.
                if (jTPCCUtil.randomNumber(1, 100, gen) == 1)
                    itemIDs[numItems - 1] = -12345;

                terminalMessage("");
                terminalMessage("Starting txn:" + terminalName + ":" +
                        transactionCount + " (New-Order)");
                tl.addMileStone("transaction-start", System.currentTimeMillis());
                return newOrderTransaction(terminalWarehouseID, districtID, customerID, numItems, allLocal, itemIDs, supplierWarehouseIDs, orderQuantities, tl);
            case PAYMENT:
                districtID = jTPCCUtil.randomNumber(1, jTPCCConfig.configDistPerWhse, gen);

                int x = jTPCCUtil.randomNumber(1, 100, gen);
                int customerDistrictID;
                int customerWarehouseID;
                if (x <= 85) {
                    customerDistrictID = districtID;
                    customerWarehouseID = terminalWarehouseID;
                } else {
                    customerDistrictID = jTPCCUtil.randomNumber(1, jTPCCConfig.configDistPerWhse, gen);
                    do {
                        customerWarehouseID = jTPCCUtil.randomNumber(1, numWarehouses, gen);
                    }
                    while (customerWarehouseID == terminalWarehouseID && numWarehouses > 1);
                }

                y = jTPCCUtil.randomNumber(1, 100, gen);
                customerByName = false;
                customerLastName = null;
                customerID = -1;
                if (y <= 60) {
                    // 60% lookups by last name
                    customerByName = true;
                    customerLastName = jTPCCUtil.getLastName(gen);
                    printMessage("Last name lookup = " + customerLastName);
                } else {
                    // 40% lookups by customer ID
                    customerByName = false;
                    customerID = jTPCCUtil.getCustomerID(gen);
                }

                float paymentAmount = (float) (jTPCCUtil.randomNumber(100, 500000, gen) / 100.0);

                terminalMessage("");
                terminalMessage("Starting transaction #" + transactionCount + " (Payment)...");
                tl.addMileStone("transaction-start", System.currentTimeMillis());
                return paymentTransaction(terminalWarehouseID, customerWarehouseID, paymentAmount, districtID, customerDistrictID, customerID, customerLastName, customerByName, tl);

            case STOCK_LEVEL:
                int threshold = jTPCCUtil.randomNumber(10, 20, gen);

                terminalMessage("");
                terminalMessage("Starting transaction #" + transactionCount + " (Stock-Level)...");
                tl.addMileStone("transaction-start", System.currentTimeMillis());
                return stockLevelTransaction(terminalWarehouseID, terminalDistrictID, threshold, tl);

            case ORDER_STATUS:
                districtID = jTPCCUtil.randomNumber(1, jTPCCConfig.configDistPerWhse, gen);

                y = jTPCCUtil.randomNumber(1, 100, gen);
                customerByName = false;
                customerLastName = null;
                customerID = -1;
                if (y <= 60) {
                    // 60% lookups by last name
                    customerByName = true;
                    customerLastName = jTPCCUtil.getLastName(gen);
                    printMessage("Last name lookup = " + customerLastName);
                } else {
                    // 40% lookups by customer ID
                    customerByName = false;
                    customerID = jTPCCUtil.getCustomerID(gen);
                }

                terminalMessage("");
                terminalMessage("Starting transaction #" + transactionCount + " (Order-Status)...");
                tl.addMileStone("transaction-start", System.currentTimeMillis());
                return orderStatusTransaction(terminalWarehouseID, districtID, customerID, customerLastName, customerByName, tl);

            case DELIVERY:
                int orderCarrierID = jTPCCUtil.randomNumber(1, 10, gen);

                terminalMessage("");
                terminalMessage("Starting transaction #" + transactionCount + " (Delivery)...");
                tl.addMileStone("transaction-start", System.currentTimeMillis());
                return deliveryTransaction(terminalWarehouseID, orderCarrierID, tl);

            default:
                error("EMPTY-TYPE");
                break;
        }
        return null;
    }


    public <T extends Base> T getObjectFromResultSet(Class<T> cls, List<Base> res) {
        for (Base obj : res) {
            if (cls.isInstance(obj)) return cls.cast(obj);
        }
        return null;
    }

    public <T extends Base> List<T> getObjectsFromResultSet(Class<T> cls, List<Base> res) {
        List<T> ret = new ArrayList<>();
        for (Base obj : res) {
            if (cls.isInstance(obj)) ret.add(cls.cast(obj));
        }
        return ret;
    }

    public <T extends Base> T getObjectFromReply(Class<T> cls, Object reply) {
        ((Message) reply).rewind();
        Object ret = ((Message) reply).getNext();
        Class clazz = ret.getClass();
        if (Set.class.isAssignableFrom(clazz)) {
            List<Base> res = new ArrayList((HashSet) ret);
            for (Base obj : res) {
                if (cls.isInstance(obj)) return cls.cast(obj);
            }
            return null;
        } else if (Base.class.isAssignableFrom(clazz)) {
            if (cls.isInstance(ret)) return cls.cast(ret);
            else return null;
        } else {
            return null;
        }
    }

    public <T extends Base> List<T> getObjectsFromReply(Class<T> cls, Object reply) {
        ((Message) reply).rewind();
        Object ret = ((Message) reply).getNext();
        Class clazz = ret.getClass();
        List<T> result = new ArrayList<>();
        if (Set.class.isAssignableFrom(clazz)) {
            List<Base> res = new ArrayList((HashSet) ret);
            for (Base obj : res) {
                if (cls.isInstance(obj)) result.add(cls.cast(obj));
            }
            return result;
        } else {
            return new ArrayList();
        }
    }

    private CompletableFuture<Void> newOrderTransaction
            (int w_id, int d_id, int c_id, int o_ol_cnt, int o_all_local, int[] itemIDs, int[] supplierWarehouseIDs, int[] orderQuantities, Timeline tl) {
        final List<Double> itemPrices = Arrays.asList(new Double[o_ol_cnt + 1]);
        final List<String> itemNames = Arrays.asList(new String[o_ol_cnt + 1]);
        final List<Double> orderLineAmounts = Arrays.asList(new Double[o_ol_cnt + 1]);
        final List<Integer> stockQuantities = Arrays.asList(new Integer[o_ol_cnt + 1]);
        final List<Character> brandGeneric = Arrays.asList(new Character[o_ol_cnt + 1]);

        final AtomicInteger exceptionCount = new AtomicInteger(0);
        final AtomicDouble total_amount = new AtomicDouble(0);
        final Map<String, Object> tmp = new HashMap<>();
        // The row in the WAREHOUSE table with matching W_ID is selected and W_TAX, the warehouse
        // tax rate, is retrieved.
        ObjId w_objId = new ObjId(Base.genObjId(Base.MODEL.WAREHOUSE,
                new String[]{"w_id", String.valueOf(w_id)}));

        // The row in the DISTRICT table with matching D_W_ID and D_ ID is selected, D_TAX, the
        // district tax rate, is retrieved, and D_NEXT_O_ID, the next available order number for
        // the district, is retrieved and incremented by one.
        ObjId d_objId = new ObjId(Base.genObjId(Base.MODEL.DISTRICT,
                new String[]{"d_id", String.valueOf(d_id), "d_w_id", String.valueOf(w_id)}));

        // The row in the CUSTOMER table with matching C_W_ID, C_D_ID, and C_ID is selected
        // and C_DISCOUNT, the customer's discount rate, C_LAST, the customer's last name,
        // and C_CREDIT, the customer's credit status, are retrieved.
        ObjId c_objId = new ObjId(Base.genObjId(Base.MODEL.CUSTOMER,
                new String[]{"c_id", String.valueOf(c_id),
                        "c_d_id", String.valueOf(d_id),
                        "c_w_id", String.valueOf(w_id)}));
        Set<ObjId> objIds = new HashSet<>();
        objIds.add(c_objId);
        objIds.add(w_objId);
        objIds.add(d_objId);
        Command command = new Command(GenericCommand.READ_BATCH, objIds);
        return parent.dbAsyncClient.readBatch(command).thenCompose(replyReadBatch -> {
//            System.out.println("got reply " + replyReadBatch);
            tl.addMileStone("read-batch-warehouse-district-customer", System.currentTimeMillis());
            Customer cus = getObjectFromReply(Customer.class, replyReadBatch);
            Warehouse warehouse = getObjectFromReply(Warehouse.class, replyReadBatch);
            District district = getObjectFromReply(District.class, replyReadBatch);
            tmp.put("customer", cus);
            tmp.put("warehouse", warehouse);
            tmp.put("district", district);

            // A new row is inserted into both the NEW-ORDER table and the ORDER table to reflect the
            // creation of the new order. O_CARRIER_ID is set to a null value. If the order includes
            // only home order-lines, then O_ALL_LOCAL is set to 1, otherwise O_ALL_LOCAL is set to 0.
            NewOrder new_order = new NewOrder(district.d_next_o_id, d_id, w_id);
            ObjId oidNewOrder = Base.genObjId(Base.MODEL.NEWORDER,
                    new String[]{"no_o_id", String.valueOf(district.d_next_o_id),
                            "no_d_id", String.valueOf(d_id),
                            "no_w_id", String.valueOf(w_id)});
            DSqlCommand commandNewOrder = new DSqlCommand(GenericCommand.CREATE, oidNewOrder, new_order.toHashMap());
            tmp.put("new_order", new_order);
            return commandNewOrder.execute(parent.dbAsyncClient);
        }).thenCompose(replyUpdateOrder -> {
            District district = (District) tmp.get("district");
//                System.out.println("got reply " + replyUpdateOrder);
            tl.addMileStone("create-new-order", System.currentTimeMillis());
            //Update district next order
            DSqlCommand updateDistrict = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                    d_objId, new String[]{"d_next_o_id", String.valueOf(++district.d_next_o_id)});
            return updateDistrict.execute(parent.dbAsyncClient);
        }).thenCompose(replyUpdateDistrict -> {
//                System.out.println("got reply " + replyUpdateDistrict);
            NewOrder new_order = (NewOrder) tmp.get("new_order");
            tl.addMileStone("update-district", System.currentTimeMillis());
            ObjId oidOrder = Base.genObjId(Base.MODEL.ORDER,
                    new String[]{"o_id", String.valueOf(new_order.no_o_id),
                            "o_w_id", String.valueOf(w_id),
                            "o_d_id", String.valueOf(d_id),
                            "o_c_id", String.valueOf(c_id)});
            Order order = new Order(new_order.no_o_id, d_id, w_id, c_id);
            order.o_entry_d = System.currentTimeMillis();

            // The number of items, O_OL_CNT, is computed to match ol_cnt
            order.o_ol_cnt = o_ol_cnt;
            order.o_all_local = o_all_local;
            DSqlCommand createOrder = new DSqlCommand(GenericCommand.CREATE, oidOrder, order.toHashMap());
            return createOrder.execute(parent.dbAsyncClient);
        }).thenCompose(replyOrder -> {
//                System.out.println("got reply " + replyOrder);
            tl.addMileStone("create-order", System.currentTimeMillis());
            District district = (District) tmp.get("district");
            Warehouse warehouse = (Warehouse) tmp.get("warehouse");
            Order order = getObjectFromReply(Order.class, replyOrder);
            List<CompletableFuture<Message>> cmds = new ArrayList<>();
            // For each O_OL_CNT item on the order:
            for (int index = 1; index <= o_ol_cnt; index++) {
                int ol_number = index;
                int ol_supply_w_id = supplierWarehouseIDs[ol_number - 1];
                int ol_i_id = itemIDs[ol_number - 1];
                int ol_quantity = orderQuantities[ol_number - 1];

                // If I_ID has an unused value (see Clause 2.4.1.5), a "not-found" condition is signaled,
                // resulting in a rollback of the database transaction (see Clause 2.4.2.3).
                if (ol_i_id == -12345) {
                    // an expected condition generated 1% of the time in the test data...
                    // we throw an illegal access exception and the transaction gets rolled back later on
                    exceptionCount.getAndIncrement();
                }
                // The row in the ITEM table with matching I_ID (equals OL_I_ID) is selected
                // and I_PRICE, the price of the item, I_NAME, the name of the item, and I_DATA are retrieved.
                ObjId i_objId = new ObjId(Base.genObjId(Base.MODEL.ITEM,
                        new String[]{"i_id", String.valueOf(ol_i_id)}));

                DSqlCommand readItem = new DSqlCommand(GenericCommand.READ, i_objId);
                CompletableFuture<Message> processItemAsync = readItem.execute(parent.dbAsyncClient).thenCompose(replyReadItem -> {
                    tl.addMileStone("read-item-count" + ol_number, System.currentTimeMillis());
//                        System.out.println("got reply " + replyReadItem);
                    Item item = getObjectFromReply(Item.class, replyReadItem);
                    if (item != null) {
                        itemPrices.set(ol_number - 1, item.i_price);
                        itemNames.set(ol_number - 1, item.i_name);

                        ObjId s_oid = new ObjId(Base.genObjId(Base.MODEL.STOCK, new String[]{
                                "s_w_id", String.valueOf(ol_supply_w_id),
                                "s_i_id", String.valueOf(ol_i_id),
                        }));
                        DSqlCommand readStock = new DSqlCommand(GenericCommand.READ, s_oid);

                        return readStock.execute(parent.dbAsyncClient).thenCompose(replyReadStock -> {
//                                System.out.println("got reply " + replyReadStock);
                            tl.addMileStone("read-stock", System.currentTimeMillis());
                            Stock stock = getObjectFromReply(Stock.class, replyReadStock);
                            int s_remote_cnt_increment;
                            stockQuantities.set(ol_number - 1, stock.s_quantity);
                            if (stock.s_quantity - ol_quantity >= 10) {
                                stock.s_quantity -= ol_quantity;
                            } else {
                                stock.s_quantity += -ol_quantity + 91;
                            }

                            if (ol_supply_w_id == w_id) {
                                s_remote_cnt_increment = 0;
                            } else {
                                s_remote_cnt_increment = 1;
                            }
                            stock.s_ytd += ol_quantity;
                            stock.s_remote_cnt += s_remote_cnt_increment;
                            String[] update = new String[]{
                                    "s_quantity", String.valueOf(stock.s_quantity),
                                    "s_ytd", String.valueOf(stock.s_ytd),
                                    "s_remote_cnt", String.valueOf(stock.s_remote_cnt)
                            };
                            DSqlCommand updateStock = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE, s_oid, update);

                            return updateStock.execute(parent.dbAsyncClient);

                        }).thenCompose(replyUpdateStock -> {
                            tl.addMileStone("update-stock", System.currentTimeMillis());
//                                System.out.println("got reply " + replyUpdateStock);
                            Stock stock = getObjectFromReply(Stock.class, replyUpdateStock);
                            double ol_amount = ol_quantity * item.i_price;
                            orderLineAmounts.set(ol_number - 1, ol_amount);
//                                                total_amount.addAndGet(ol_amount);
//                                                total_amount.addAndGet((1 + warehouse.w_tax + district.d_tax) * (1 - cus.c_discount));

                            if (item.i_data.indexOf("GENERIC") != -1 && stock.s_data.indexOf("GENERIC") != -1) {
                                brandGeneric.set(ol_number - 1, 'B');
                            } else {
                                brandGeneric.set(ol_number - 1, 'G');
                            }
                            String ol_dist_info = null;
                            switch (district.d_id) {
                                case 1:
                                    ol_dist_info = stock.s_dist_01;
                                    break;
                                case 2:
                                    ol_dist_info = stock.s_dist_02;
                                    break;
                                case 3:
                                    ol_dist_info = stock.s_dist_03;
                                    break;
                                case 4:
                                    ol_dist_info = stock.s_dist_04;
                                    break;
                                case 5:
                                    ol_dist_info = stock.s_dist_05;
                                    break;
                                case 6:
                                    ol_dist_info = stock.s_dist_06;
                                    break;
                                case 7:
                                    ol_dist_info = stock.s_dist_07;
                                    break;
                                case 8:
                                    ol_dist_info = stock.s_dist_08;
                                    break;
                                case 9:
                                    ol_dist_info = stock.s_dist_09;
                                    break;
                                case 10:
                                    ol_dist_info = stock.s_dist_10;

                                    break;
                            }

                            ObjId ol_oid = Base.genObjId(Base.MODEL.ORDERLINE, new String[]{
                                    "ol_number", String.valueOf(ol_number),
                                    "ol_w_id", String.valueOf(warehouse.w_id),
                                    "ol_d_id", String.valueOf(district.d_id),
                                    "ol_o_id", String.valueOf(order.o_id)
                            });
                            OrderLine orderLine = new OrderLine(ol_number, order.o_id, district.d_id, warehouse.w_id);
                            orderLine.ol_i_id = ol_i_id;
                            orderLine.ol_supply_w_id = ol_supply_w_id;
                            orderLine.ol_quantity = ol_quantity;
                            orderLine.ol_amount = ol_amount;
                            orderLine.ol_dist_info = ol_dist_info;

                            DSqlCommand createOrderLine = new DSqlCommand(GenericCommand.CREATE, ol_oid, orderLine.toHashMap());

                            return createOrderLine.execute(parent.dbAsyncClient);
                        });
                    } else {
                        return CompletableFuture.supplyAsync(() -> new Message("null"));
                    }
                });
                cmds.add(processItemAsync);
            }
            return CompletableFuture.allOf(cmds.toArray(new CompletableFuture[cmds.size()]));
        }).thenAccept(reply -> {
            District district = (District) tmp.get("district");
            Warehouse warehouse = (Warehouse) tmp.get("warehouse");
            Customer customer = (Customer) tmp.get("customer");
            tl.addMileStone("create-orderline", System.currentTimeMillis());
            terminalMessage("+--------------------------- NEW-ORDER ---------------------------+");
            terminalMessage(" Date: " + jTPCCUtil.getCurrentTime());
            terminalMessage(" ");
            terminalMessage(" Warehouse: " + warehouse.w_id);
            terminalMessage("   Tax:     " + warehouse.w_tax);
            terminalMessage(" District:  " + district.d_id);
            terminalMessage("   Tax:     " + district.d_tax);
            terminalMessage(" ");
            terminalMessage(" Customer:  " + customer.c_id);
            terminalMessage("   Name:    " + customer.c_last);
            terminalMessage("   Credit:  " + customer.c_credit);
            terminalMessage("   %Disc:   " + customer.c_discount);
            terminalMessage(" ");
            terminalMessage(" Order-Line List [" +
                    jTPCCUtil.leftPad("Supp_W", 8) +
                    jTPCCUtil.leftPad("Item_ID", 8) +
                    jTPCCUtil.leftPad("Item Name", 25) +
                    jTPCCUtil.leftPad("Qty", 5) +
                    jTPCCUtil.leftPad("Stock", 8) +
                    jTPCCUtil.leftPad("B/G", 5) +
                    jTPCCUtil.leftPad("Price", 10) +
                    jTPCCUtil.leftPad("Amount", 10) + "]");
            for (int i = 1; i <= o_ol_cnt; i++) {
                if (itemNames.get(i) != null)
                    terminalMessage("                 [" +
                            jTPCCUtil.leftPad(supplierWarehouseIDs[i] + "", 8) +
                            jTPCCUtil.leftPad(itemIDs[i] + "", 8) +
                            jTPCCUtil.leftPad(itemNames.get(i) + "", 25) +
                            jTPCCUtil.leftPad(orderQuantities[i] + "", 5) +
                            jTPCCUtil.leftPad(stockQuantities.get(i) + "", 8) +
                            jTPCCUtil.leftPad(brandGeneric.get(i) + "", 5) +
                            jTPCCUtil.leftPad(jTPCCUtil.formattedDouble(itemPrices.get(i)), 10) +
                            jTPCCUtil.leftPad(jTPCCUtil.formattedDouble(orderLineAmounts.get(i)), 10) + "]");
            }
            terminalMessage(" Total Amount: " + total_amount);
            terminalMessage(" ");
            terminalMessage(" Execution Status: New order placed!");
            terminalMessage("+-----------------------------------------------------------------+");
        });

    }

    private CompletableFuture<Void> paymentTransaction(int w_id, int c_w_id, float h_amount, int d_id, int c_d_id, int c_id, String c_last, boolean c_by_name, Timeline tl) {
        final Map<String, Object> tmp = new HashMap<>();
        ObjId w_objId = new ObjId(Base.genObjId(Base.MODEL.WAREHOUSE,
                new String[]{"w_id", String.valueOf(w_id)}));
        DSqlCommand updateWarehouse = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                w_objId, new String[]{"w_ytd", "+" + h_amount});

        return updateWarehouse.execute(parent.dbAsyncClient).thenCompose(replyUpdateWarehouse -> {
            tl.addMileStone("update-warehouse", System.currentTimeMillis());
//            System.out.println("got reply " + replyUpdateWarehouse);
            DSqlCommand readWarehouse = new DSqlCommand(GenericCommand.READ, w_objId);
            return readWarehouse.execute(parent.dbAsyncClient);
        }).thenCompose(replyReadWarehouse -> {
            tl.addMileStone("read-warehouse ", System.currentTimeMillis());
            System.out.println("Read warehouse"+w_objId);
            System.out.println("got reply Read warehouse" + replyReadWarehouse);
            Warehouse warehouse = getObjectFromReply(Warehouse.class, replyReadWarehouse);
            tmp.put("w_name", warehouse.w_name);
            tmp.put("warehouse", warehouse);
            ObjId d_objId = new ObjId(Base.genObjId(Base.MODEL.DISTRICT,
                    new String[]{"d_id", String.valueOf(d_id), "d_w_id", String.valueOf(w_id)}));
            DSqlCommand updateDistrict = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                    d_objId, new String[]{"d_ytd", "+" + h_amount});
            return updateDistrict.execute(parent.dbAsyncClient);
        }).thenCompose(replyUpdateDistrict -> {
            tl.addMileStone("update-district", System.currentTimeMillis());
//            System.out.println("got reply " + replyUpdateDistrict);
            ObjId d_objId = new ObjId(Base.genObjId(Base.MODEL.DISTRICT,
                    new String[]{"d_id", String.valueOf(d_id), "d_w_id", String.valueOf(w_id)}));
            DSqlCommand readDistrict = new DSqlCommand(GenericCommand.READ, d_objId);
            return readDistrict.execute(parent.dbAsyncClient);
        }).thenCompose(replyReadDistrict -> {
            tl.addMileStone("read-district", System.currentTimeMillis());
//            System.out.println("got reply " + replyReadDistrict);
            District district = getObjectFromReply(District.class, replyReadDistrict);
            tmp.put("d_name", district.d_name);
            tmp.put("district", district);
            if (c_by_name) {
                return getCustomersByName(w_id, d_id, c_last, tl);
            } else {
                ObjId c_objId = new ObjId(Base.genObjId(Base.MODEL.CUSTOMER,
                        new String[]{"c_id", String.valueOf(c_id), "c_d_id", String.valueOf(d_id), "c_w_id", String.valueOf(w_id)}));
                DSqlCommand readCustomerById = new DSqlCommand(GenericCommand.READ, c_objId);
                return readCustomerById.execute(parent.dbAsyncClient);
            }
        }).thenCompose(readCustomerReply -> {
            tl.addMileStone("found-customer", System.currentTimeMillis());
//            System.out.println("got reply " + readCustomerReply);
            Customer customer = getObjectFromReply(Customer.class, readCustomerReply);

            tmp.put("customer", customer);
            double c_balance = customer.c_balance + h_amount;

            if (customer.c_credit.equals("BC")) {  // bad credit
                String c_data = customer.c_data;
                String c_new_data = c_id + " " + c_d_id + " " + c_w_id + " " + d_id + " " + w_id + " " + h_amount + " |";
                if (c_data.length() > c_new_data.length()) {
                    c_new_data += c_data.substring(0, c_data.length() - c_new_data.length());
                } else {
                    c_new_data += c_data;
                }
                if (c_new_data.length() > 500) c_new_data = c_new_data.substring(0, 500);
                DSqlCommand updateCustomer = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                        customer.getId(), new String[]{"c_balance", String.valueOf(c_balance), "c_data", c_new_data});
                return updateCustomer.execute(parent.dbAsyncClient);
            } else { // GoodCredit
                DSqlCommand updateCustomer = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                        customer.getId(), new String[]{"c_balance", String.valueOf(c_balance)});
                return updateCustomer.execute(parent.dbAsyncClient);
            }
        }).thenCompose(updateCustomerReply -> {
            tl.addMileStone("update-customer", System.currentTimeMillis());
//            System.out.println("got reply " + updateCustomerReply);
            String w_name = (String) tmp.get("w_name");
            String d_name = (String) tmp.get("d_name");
            if (w_name.length() > 10) w_name = w_name.substring(0, 10);
            if (d_name.length() > 10) d_name = d_name.substring(0, 10);
            String h_data = w_name + "    " + d_name;
            ObjId h_objId = Base.genObjId(Base.MODEL.HISTORY, new String[]{
                    "h_c_id", String.valueOf(c_d_id),
                    "h_c_d_id", String.valueOf(c_d_id),
                    "h_c_w_id", String.valueOf(c_w_id)
            });
            History history = new History(c_d_id, c_d_id, c_w_id);
            history.h_d_id = d_id;
            history.h_w_id = w_id;
            history.h_date = System.currentTimeMillis();
            history.h_amount = h_amount;
            history.h_data = h_data;
            DSqlCommand createHistory = new DSqlCommand(GenericCommand.CREATE, h_objId, history.toHashMap());
            return createHistory.execute(parent.dbAsyncClient);
        }).thenAccept(reply -> {
            tl.addMileStone("create-history", System.currentTimeMillis());
            Warehouse warehouse = (Warehouse) tmp.get("warehouse");
            District district = (District) tmp.get("district");
            Customer customer = (Customer) tmp.get("customer");

            printMessage("Succesful INSERT into history table");

            StringBuffer terminalMessage = new StringBuffer();
            terminalMessage("+---------------------------- PAYMENT ----------------------------+");
            terminalMessage(" Date: " + jTPCCUtil.getCurrentTime());
            terminalMessage(" ");
            terminalMessage(" Warehouse: " + warehouse.w_id);
            terminalMessage("   Street:  " + warehouse.w_street_1);
            terminalMessage("   Street:  " + warehouse.w_street_2);
            terminalMessage("   City:    " + warehouse.w_city + "   State: " + warehouse.w_state + "  Zip: " + warehouse.w_zip);
            terminalMessage(" ");
            terminalMessage(" District:  " + district.d_id);
            terminalMessage("   Street:  " + district.d_street_1);
            terminalMessage("   Street:  " + district.d_street_2);
            terminalMessage("   City:    " + district.d_city + "   State: " + district.d_state + "  Zip: " + district.d_zip);
            terminalMessage(" ");
            terminalMessage(" Customer:  " + customer.c_id);
            terminalMessage("   Name:    " + customer.c_first + " " + customer.c_middle + " " + c_last);
            terminalMessage("   Street:  " + customer.c_street_1);
            terminalMessage("   Street:  " + customer.c_street_2);
            terminalMessage("   City:    " + customer.c_city + "   State: " + customer.c_state + "  Zip: " + customer.c_zip);

            terminalMessage("");
            if (customer.c_since != 0) {
                terminalMessage("   Since:   " + customer.c_since);
            } else {
                terminalMessage("   Since:   ");
            }
            terminalMessage("   Credit:  " + customer.c_credit);
            terminalMessage("   %Disc:   " + customer.c_discount);
            terminalMessage("   Phone:   " + customer.c_phone);
            terminalMessage(" ");
            terminalMessage(" Amount Paid:      " + h_amount);
            terminalMessage(" Credit Limit:     " + customer.c_credit_lim);
            terminalMessage(" New Cust-Balance: " + customer.c_balance);

            if (customer.c_credit.equals("BC")) {
                if (customer.c_data.length() > 50) {
                    terminalMessage.append(" Cust-Data: " + customer.c_data.substring(0, 50));
                    int data_chunks = customer.c_data.length() > 200 ? 4 : customer.c_data.length() / 50;
                    for (int n = 1; n < data_chunks; n++)
                        terminalMessage.append("            " + customer.c_data.substring(n * 50, (n + 1) * 50));
                    terminalMessage(terminalMessage.toString());
                } else {
                    terminalMessage(" Cust-Data: " + customer.c_data);
                }
            }

            terminalMessage("+-----------------------------------------------------------------+");
        });
    }

    private CompletableFuture<Void> deliveryTransaction(int w_id, int o_carrier_id, Timeline tl) {

//        List<Integer> orderIDs = new ArrayList<>(jTPCCConfig.configDistPerWhse);
        final int orderIDs[] = new int[jTPCCConfig.configDistPerWhse];
        final Map<String, Object> tmp = new HashMap<>();
        List<CompletableFuture<Void>> transList = new ArrayList<>();
        for (int i = 1; i <= jTPCCConfig.configDistPerWhse; i++) {
            int d_id = i;
            ObjId no_objId = new ObjId(Base.MODEL.NEWORDER.getName() + ":" + "no_d_id=" + String.valueOf(d_id) + ":no_w_id=" + String.valueOf(w_id)
                    + "#no_o_id#min");

            DSqlCommand readNO = new DSqlCommand(GenericCommand.READ_BATCH, no_objId);

            CompletableFuture<Void> tran = readNO.execute(parent.dbAsyncClient).thenCompose(replyReadNO -> {
                tl.addMileStone("read-new-order", System.currentTimeMillis());
//                terminalMessage("Got reply replyReadNO1!!!" + replyReadNO);
                ArrayList<NewOrder> newOrders = (ArrayList<NewOrder>) getObjectsFromReply(NewOrder.class, replyReadNO);

                if (newOrders.size() > 0) {
//                    terminalMessage("Got reply replyReadNO2!!!" + replyReadNO);
                    ObjId no_objId_delete = newOrders.get(0).getId();
                    orderIDs[d_id - 1] = newOrders.get(0).no_o_id;
                    terminalMessage("deleteNO!!!" + no_objId_delete);
                    terminalMessage("newOrders length: " + newOrders.size());
                    DSqlCommand deleteNO = new DSqlCommand(GenericCommand.DELETE, no_objId_delete);
                    return deleteNO.execute(parent.dbAsyncClient).thenCompose(replyDeleteNO -> {
                        tl.addMileStone("delete-new-order", System.currentTimeMillis());
                        terminalMessage("got reply replyDeleteNO of no_objId_delete" + no_objId_delete);
                        ObjId o_objId = Base.genObjId(Base.MODEL.ORDER, new String[]{
                                "o_id", String.valueOf(newOrders.get(0).no_o_id),
                                "o_d_id", String.valueOf(d_id),
                                "o_w_id", String.valueOf(w_id)
                        });
                        DSqlCommand readOrder = new DSqlCommand(GenericCommand.READ, o_objId);
                        return readOrder.execute(parent.dbAsyncClient);
                    }).thenCompose(replyReadOrder -> {
                        tl.addMileStone("read-order", System.currentTimeMillis());
                        terminalMessage("got reply replyReadOrder of no_objId_delete" + no_objId_delete);
                        Order order = getObjectFromReply(Order.class, replyReadOrder);
                        if (order != null) order.o_carrier_id = o_carrier_id;
                        else throw new RuntimeException();
                        DSqlCommand updateOrder = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                                order.getId(), new String[]{"o_carrier_id", String.valueOf(o_carrier_id)});
                        return updateOrder.execute(parent.dbAsyncClient);
                    }).thenCompose(replyUpdateOrder -> {
                        tl.addMileStone("update-order", System.currentTimeMillis());
                        terminalMessage("got reply replyUpdateOrder " + no_objId_delete);
                        Order order = getObjectFromReply(Order.class, replyUpdateOrder);
                        tmp.put("order", order);
                        ObjId ol_objId = Base.genObjId(Base.MODEL.ORDERLINE, new String[]{
                                "ol_o_id", String.valueOf(order.o_id),
                                "ol_d_id", String.valueOf(d_id),
                                "ol_w_id", String.valueOf(w_id)
                        });
                        DSqlCommand updateOrderLine = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE_BATCH,
                                ol_objId, new String[]{"ol_delivery_d", String.valueOf(System.currentTimeMillis())});
                        return updateOrderLine.execute(parent.dbAsyncClient);
                    }).thenCompose(replyUpdateOrderLine -> {
                        tl.addMileStone("update-order-line", System.currentTimeMillis());
                        terminalMessage("got reply replyUpdateOrderLine " + no_objId_delete);
                        OrderLine orderLine = getObjectFromReply(OrderLine.class, replyUpdateOrderLine);
                        ObjId ol_objId = Base.genObjId(Base.MODEL.ORDERLINE, new String[]{
                                "ol_o_id", String.valueOf(orderLine.ol_o_id),
                                "ol_d_id", String.valueOf(d_id),
                                "ol_w_id", String.valueOf(w_id)
                        });
                        DSqlCommand readAllOrderLine = new DSqlCommand(GenericCommand.READ_BATCH, ol_objId);
                        return readAllOrderLine.execute(parent.dbAsyncClient);
                    }).thenCompose(replyReadAllOrderLine -> {
                        tl.addMileStone("read-batch-order-line", System.currentTimeMillis());
                        terminalMessage("got reply replyReadAllOrderLine " + no_objId_delete);
                        List<OrderLine> ols = getObjectsFromReply(OrderLine.class, replyReadAllOrderLine);
                        final double[] total = {0};
                        ols.stream().forEach(ol -> total[0] = total[0] + ol.ol_amount);
                        Order order = (Order) tmp.get("order");
                        ObjId c_objId = new ObjId(Base.genObjId(Base.MODEL.CUSTOMER,
                                new String[]{"c_id", String.valueOf(order.o_c_id),
                                        "c_d_id", String.valueOf(d_id),
                                        "c_w_id", String.valueOf(w_id)}));
                        DSqlCommand updateCustomer = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                                c_objId, new String[]{"c_balance", "+" + total[0]});
                        return updateCustomer.execute(parent.dbAsyncClient);
                    }).thenAccept(replyUpdateCustomer -> {
//                        System.out.println("got reply replyUpdateCustomer" + replyUpdateCustomer);
                    });
                } else {
                    return CompletableFuture.supplyAsync(() -> null);
                }
            }).exceptionally(e -> {
                System.out.println("Exceptionally");
                return null;
            });
            transList.add(tran);
        }
        return CompletableFuture.allOf(transList.toArray(new CompletableFuture[transList.size()])).thenAccept(reply -> {
            tl.addMileStone("update-customer", System.currentTimeMillis());
            int skippedDeliveries = 0;
            terminalMessage("+---------------------------- DELIVERY ---------------------------+");
            terminalMessage(" Date: " + jTPCCUtil.getCurrentTime());
            terminalMessage(" ");
            terminalMessage(" Warehouse: " + w_id);
            terminalMessage(" Carrier:   " + o_carrier_id);
            terminalMessage(" ");
            terminalMessage(" Delivered Orders");
            terminalMessage(" ");
            for (int i = 1; i <= jTPCCConfig.configDistPerWhse; i++) {
                if (orderIDs[i - 1] >= 0) {
                    terminalMessage("  District " + (i < 10 ? " " : "") + i + ": Order number " + orderIDs[i - 1] + " was delivered.");
                } else {
                    terminalMessage("  District " + (i < 10 ? " " : "") + i + ": No orders to be delivered.");
                    skippedDeliveries++;
                }
            }
            terminalMessage(" Skipped Delivery: " + skippedDeliveries);
            terminalMessage("+-----------------------------------------------------------------+");
        });
    }

    private CompletableFuture<Void> orderStatusTransaction(int w_id, int d_id, int c_id, String c_last, boolean c_by_name, Timeline tl) {
        CompletableFuture<Customer> getCustomer = null;
        final ArrayList<String> strOrderLines = new ArrayList<>();
        final Map<String, Object> tmp = new HashMap<>();
        if (c_by_name) {
            // payment is by customer name
            getCustomer = getCustomersByName(w_id, d_id, c_last, tl).thenCompose(replyReadCustomerByName -> {
                tl.addMileStone("get-customer-by-name", System.currentTimeMillis());
                ArrayList<Customer> customers = (ArrayList<Customer>) getObjectsFromReply(Customer.class, replyReadCustomerByName);
                customers.sort((o1, o2) -> ComparisonChain.start()
                        .compare(o1.c_w_id, o2.c_w_id)
                        .compare(o1.c_d_id, o2.c_d_id)
                        .compare(o1.c_last, o2.c_last)
                        .compare(o1.c_first, o2.c_first)
                        .result());
                int count = customers.size();
                if (count % 2 == 1) count--;
                final int index = count / 2;
                return CompletableFuture.supplyAsync(() -> customers.get(index));
            });
        } else {
            ObjId c_objId = new ObjId(Base.genObjId(Base.MODEL.CUSTOMER,
                    new String[]{"c_id", String.valueOf(c_id), "c_d_id", String.valueOf(d_id), "c_w_id", String.valueOf(w_id)}));
            DSqlCommand readCustomerById = new DSqlCommand(GenericCommand.READ, c_objId);
            getCustomer = readCustomerById.execute(parent.dbAsyncClient).thenCompose(replyReadCustomerById -> {
                tl.addMileStone("get-customer-by-id", System.currentTimeMillis());
                Customer customers = getObjectFromReply(Customer.class, replyReadCustomerById);
                return CompletableFuture.supplyAsync(() -> customers);
            });
        }
        assert getCustomer != null;
        return getCustomer.thenCompose(customer -> {
            tmp.put("customer", customer);
            tl.addMileStone("found-customer", System.currentTimeMillis());
            ObjId o_objId = Base.genObjId(Base.MODEL.ORDER, new String[]{
                    "o_d_id", String.valueOf(d_id),
                    "o_w_id", String.valueOf(w_id),
                    "o_c_id", String.valueOf(customer.c_id),
            });
            DSqlCommand readOrder = new DSqlCommand(GenericCommand.READ_BATCH, o_objId);
            return readOrder.execute(parent.dbAsyncClient);
        }).thenCompose(replyReadOrder -> {
            tl.addMileStone("read-batch-order", System.currentTimeMillis());
            ArrayList<Order> orders = (ArrayList<Order>) getObjectsFromReply(Order.class, replyReadOrder);
            if (orders.size() > 0) {
                // find the newest order for the customer
                // retrieve the carrier & order date for the most recent order.
                orders.sort((o1, o2) -> o2.o_id - o1.o_id);
                Order order = orders.get(0);
                tmp.put("order", order);
                // retrieve the order lines for the most recent order
                ObjId ol_objId = Base.genObjId(Base.MODEL.ORDERLINE, new String[]{
                        "ol_o_id", String.valueOf(order.o_id),
                        "ol_d_id", String.valueOf(d_id),
                        "ol_w_id", String.valueOf(w_id)
                });
                DSqlCommand readOrderLine = new DSqlCommand(GenericCommand.READ_BATCH, ol_objId);
                return readOrderLine.execute(parent.dbAsyncClient);
            } else {
                tmp.put("order", new Order(-1, -1, -1, -1));
                return CompletableFuture.supplyAsync(() -> null);
            }
        }).thenAccept(replyReadOrderLine -> {
            tl.addMileStone("read-batch-order-line", System.currentTimeMillis());
            Customer customer = (Customer) tmp.get("customer");
            Order order = (Order) tmp.get("order");

            terminalMessage("");
            terminalMessage("+-------------------------- ORDER-STATUS -------------------------+");
            terminalMessage(" Date: " + jTPCCUtil.getCurrentTime());
            terminalMessage(" ");
            terminalMessage(" Warehouse: " + w_id);
            terminalMessage(" District:  " + d_id);
            terminalMessage(" ");
            terminalMessage(" Customer:  " + c_id);
            terminalMessage("   Name:    " + customer.c_first + " " + customer.c_middle + " " + c_last);
            terminalMessage("   Balance: " + customer.c_balance);
            terminalMessage("");
            if (order.o_id == -1) {
                terminalMessage(" Customer has no orders placed.");
            } else {
                ArrayList<OrderLine> orderLines = (ArrayList<OrderLine>) getObjectsFromReply(OrderLine.class, replyReadOrderLine);

                orderLines.stream().forEach(ol -> {
                    StringBuffer str = new StringBuffer();
                    str.append("[");
                    str.append(ol.ol_supply_w_id);
                    str.append(" - ");
                    str.append(ol.ol_i_id);
                    str.append(" - ");
                    str.append(ol.ol_quantity);
                    str.append(" - ");
                    str.append(jTPCCUtil.formattedDouble(ol.ol_amount));
                    str.append(" - ");
                    if (ol.ol_delivery_d != 0)
                        str.append(ol.ol_delivery_d);
                    else
                        str.append("99-99-9999");
                    str.append("]");
                    strOrderLines.add(str.toString());
                });
                terminalMessage(" Order-Number: " + order.o_id);
                terminalMessage("    Entry-Date: " + order.o_entry_d);
                terminalMessage("    Carrier-Number: " + order.o_carrier_id);
                terminalMessage("");
                if (strOrderLines.size() != 0) {
                    terminalMessage(" [Supply_W - Item_ID - Qty - Amount - Delivery-Date]");
                    strOrderLines.stream().forEach(this::terminalMessage);
                } else {
                    terminalMessage(" This Order has no Order-Lines.");
                }
            }
            terminalMessage("+-----------------------------------------------------------------+");
        });
    }

    private CompletableFuture<Void> stockLevelTransaction(int w_id, int d_id, int threshold, Timeline tl) {
        Map<String, Object> tmp = new HashMap<>();
        printMessage("Stock Level Txn for W_ID=" + w_id + ", D_ID=" + d_id + ", threshold=" + threshold);
        ObjId d_objId = new ObjId(Base.genObjId(Base.MODEL.DISTRICT,
                new String[]{"d_id", String.valueOf(d_id), "d_w_id", String.valueOf(w_id)}));
        DSqlCommand readDistrict = new DSqlCommand(GenericCommand.READ, d_objId);
        return readDistrict.execute(parent.dbAsyncClient).thenCompose(replyReadDistrict -> {
            tl.addMileStone("read-district", System.currentTimeMillis());
            District district = getObjectFromReply(District.class, replyReadDistrict);
            tmp.put("district", district);
            printMessage("Next Order ID for District = " + district.d_next_o_id);

            ObjId ol_oid = new ObjId(Base.MODEL.ORDERLINE.getName() + ":" + "ol_d_id=" + String.valueOf(d_id) + ":ol_w_id=" + String.valueOf(w_id)
                    + "#ol_o_id#" + (district.d_next_o_id - 20) + "#" + district.d_next_o_id);

            DSqlCommand readOrderLines = new DSqlCommand(GenericCommand.READ_BATCH, ol_oid);
            return readOrderLines.execute(parent.dbAsyncClient);
        }).thenCompose(replyReadOrderLines -> {
            tl.addMileStone("read-order-line", System.currentTimeMillis());
            ArrayList<OrderLine> orderLines = (ArrayList<OrderLine>) getObjectsFromReply(OrderLine.class, replyReadOrderLines);
            tmp.put("orderLines", orderLines);

            Set<Integer> olNumbers = new HashSet<Integer>();
            orderLines.stream().forEach(orderLine -> olNumbers.add(orderLine.ol_i_id));

            Set<ObjId> objIds = new HashSet<ObjId>();
            olNumbers.stream().forEach(ol_i_id -> {
                objIds.add(Base.genObjId(Base.MODEL.STOCK, new String[]{
                        "s_w_id", String.valueOf(w_id),
                        "s_i_id", String.valueOf(ol_i_id)
                }));
            });

            Command command = new Command(GenericCommand.READ_BATCH, objIds);
            return parent.dbAsyncClient.readBatch(command);
//            DSqlCommand readStocks = new DSqlCommand(GenericCommand.READ_BATCH, Base.genObjId(Base.MODEL.STOCK, new String[]{
//                    "s_w_id", String.valueOf(w_id)
//            }));
//            return readStocks.execute(parent.dbAsyncClient);

        }).thenAccept(replyReadStocks -> {
            tl.addMileStone("read-stocks", System.currentTimeMillis());
            ArrayList<Stock> stocks = (ArrayList<Stock>) getObjectsFromReply(Stock.class, replyReadStocks);
            ArrayList<OrderLine> orderLines = (ArrayList<OrderLine>) tmp.get("orderLines");
            Set<Integer> olNumbers = new HashSet<Integer>();
            orderLines.stream().forEach(orderLine -> olNumbers.add(orderLine.ol_i_id));
            stocks.stream().filter(stock -> (stock.s_quantity < threshold && (olNumbers.contains(stock.s_i_id))));
            Set<Integer> s_i_ids = new HashSet<Integer>();
            stocks.stream().forEach(stock -> s_i_ids.add(stock.s_i_id));

            terminalMessage("+-------------------------- STOCK-LEVEL --------------------------+");
            terminalMessage(" Warehouse: " + w_id);
            terminalMessage(" District:  " + d_id);
            terminalMessage(" ");
            terminalMessage(" Stock Level Threshold: " + threshold);
            terminalMessage(" Low Stock Count:       " + s_i_ids.size());
            terminalMessage("+-----------------------------------------------------------------+");
        });
    }

    private CompletableFuture<Message> getCustomersByName(int w_id, int d_id, String c_last, Timeline tl) {
        ObjId c_objId = new ObjId(Base.genObjId(Base.MODEL.CUSTOMER,
                new String[]{"c_d_id", String.valueOf(d_id), "c_w_id", String.valueOf(w_id), "c_last", c_last}));
        DSqlCommand readCustomerByName = new DSqlCommand(GenericCommand.READ_BATCH, c_objId);
        return readCustomerByName.execute(parent.dbAsyncClient).thenCompose(replyReadCustomerByName -> {
            ArrayList<Customer> customers = (ArrayList<Customer>) getObjectsFromReply(Customer.class, replyReadCustomerByName);
            if (customers != null && customers.size() > 0) {
                return CompletableFuture.supplyAsync(() -> replyReadCustomerByName);
            } else {
                String customerLastName = jTPCCUtil.getLastName(gen);
//                printMessage("New last name lookup = " + customerLastName);
                return getCustomersByName(w_id, d_id, customerLastName, tl);
            }
        });
    }


    private void error(String type) {
        log.error(terminalName + ", TERMINAL=" + terminalName + "  TYPE=" + type + "  COUNT=" + transactionCount);
        System.out.println(terminalName + ", TERMINAL=" + terminalName + "  TYPE=" + type + "  COUNT=" + transactionCount);
    }

    private void logException(Exception e) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        printWriter.close();
        log.error(stringWriter.toString());
    }

    private void terminalMessage(String message) {
        log.debug(terminalName + ", " + message);
    }

    private void printMessage(String message) {
        log.debug(terminalName + ", " + message);

    }

    private class Timeline {
        private List<Milestone> milestones = new ArrayList<>();
        private String transaction;

        public void setTransaction(String transaction) {
            this.transaction = transaction;
        }


        private void addMileStone(String event, long timestamp) {
            this.milestones.add(new Milestone(event, timestamp));
        }

        @Override
        public String toString() {
            StringBuffer ret = new StringBuffer(this.transaction + " :: " + (milestones.get(milestones.size() - 1).timestamp - milestones.get(0).timestamp) + "\n");
            for (int i = 1; i < milestones.size(); i++) {
                ret.append(milestones.get(i).event + " : " + (milestones.get(i).timestamp - milestones.get(i - 1).timestamp) + "\n");
            }
            return ret.toString();
        }

        private class Milestone {
            public long timestamp;
            public String event;

            public Milestone(String event, long timestamp) {
                this.event = event;
                this.timestamp = timestamp;
            }
        }
    }
}
