package ch.usi.dslab.lel.dsqldb.jtpcc;

/**
 * Created by longle on 18/07/2016.
 */
public interface jTPCCInterface {
    public void initMonitoring(String gathererHost, int gathererPort, String fileDirectory, int gathererDuration, int warmupTime);
    public void run() throws Exception;
}
