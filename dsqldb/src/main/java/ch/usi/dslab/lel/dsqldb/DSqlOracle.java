/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.dsqldb;

import ch.usi.dslab.lel.aerie.CacheMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Sets;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import ch.usi.dslab.lel.aerie.CacheHashMap;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.OracleStateMachine;
import ch.usi.dslab.lel.dsqldb.tables.Base;
import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCDataGenerator;

/**
 * Created by longle on 17/02/16.
 */
public class DSqlOracle extends OracleStateMachine {

    public DSqlOracle(int serverId, int partitionId, String systemConfig, String partitionsConfig) {
        super(serverId, partitionId, systemConfig, partitionsConfig, new DSqlApplicationProcedure(), new CacheMultimap());
    }

    static int id;

    public static void main(String args[]) {
        String systemConfigFile;
        String partitionConfigFile;
        int oracleId;
        int partitionId;
        if (args.length == 4) {
            oracleId = Integer.parseInt(args[0]);
            id = oracleId;
            partitionId = Integer.parseInt(args[1]);
            systemConfigFile = args[2];
            partitionConfigFile = args[3];
            DSqlOracle oracle = new DSqlOracle(oracleId, partitionId, systemConfigFile, partitionConfigFile);
            oracle.runStateMachine();
        } else if (args.length == 5) {
            oracleId = Integer.parseInt(args[0]);
            id = oracleId;
            partitionId = Integer.parseInt(args[1]);
            systemConfigFile = args[2];
            partitionConfigFile = args[3];
            String dataFile = args[4];
            DSqlOracle oracle = new DSqlOracle(oracleId, partitionId, systemConfigFile, partitionConfigFile);
//            oracle.loadDataToCache(dataFile);
            oracle.streamDataToCache(dataFile);
            oracle.runStateMachine();
        } else {
            System.out.print("Usage: <oracleId> <partitionId> <system config> <partition config>");
            System.exit(0);
        }
    }

    public void loadDataToCache(String dataFile) {
        long start = System.currentTimeMillis();
        System.out.println("[ORACLE] Loading sample data..");
        Map<String, Object> config = jTPCCDataGenerator.loadCSVData(dataFile);
        System.out.println("[ORACLE] File read, takes " + (System.currentTimeMillis() - start));
        for (Base obj : (Set<Base>) config.get("objs")) {
            ObjId objId = obj.getId();
            CacheHashMap.PRObjectLocation loc = new CacheHashMap.PRObjectLocation(objId, DSqlApplicationProcedure.mapIdToPartition(objId));
            this.getCache().set(objId, loc);
        }
        System.out.println("[ORACLE] Data loaded, takes " + (System.currentTimeMillis() - start));
    }

    public void streamDataToCache(String dataFile) {
        System.out.println("[ORACLE] loading sample data...");
        long start = System.currentTimeMillis();
        AtomicInteger count = new AtomicInteger(0);
        jTPCCDataGenerator.loadCSVData(dataFile, line -> {

            String[] tmp = line.split(",");
            ObjId objId = null;
            if (tmp.length == 2) {
            } else {
                switch (tmp[0]) {
                    case "Header":
                        break;
                    default:
                        objId = new ObjId(tmp[1]);
                        break;
                }
            }
            if (objId != null) {
                CacheHashMap.PRObjectLocation loc = new CacheHashMap.PRObjectLocation(objId, DSqlApplicationProcedure.mapIdToPartition(objId));
                this.getCache().set(objId, loc);
//                if (id % 2 == 0)
//                    System.out.println("Cache size: " + Sets.newHashSet(((HashMultimap) this.getCache().getInstance()).values()).size() + " -- vs Object count " + count.incrementAndGet());
//                    System.out.println("Cache size: " +  this.getCache().getSize() + " -- vs Object count " + count.incrementAndGet());
            }
        });
        System.out.println("[ORACLE] Data loaded, takes " + (System.currentTimeMillis() - start));
    }
}