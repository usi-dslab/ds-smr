/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.dsqldb.tables;

import org.json.simple.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import ch.usi.dslab.bezerra.netwrapper.codecs.Codec;
import ch.usi.dslab.bezerra.netwrapper.codecs.CodecUncompressedKryo;
import ch.usi.dslab.lel.aerie.LocalMethodCall;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.PRObject;

/**
 * Created by longle on 17/02/16. notes: ObjId: model:id: generated form
 */
public abstract class Base extends PRObject {

    private static ScriptEngineManager mgr = new ScriptEngineManager();
    private static ScriptEngine engine = mgr.getEngineByName("JavaScript");

    @LocalMethodCall
    public static Object interpret(String s) {
        if (s.matches(".*[a-df-zA-DF-Z|]+.*")) return s;
        Scanner sc = new Scanner(s);
        return sc.hasNextInt() ? sc.nextInt() :
                sc.hasNextLong() ? sc.nextLong() :
                        sc.hasNextDouble() ? sc.nextDouble() :
                                sc.hasNext() ? sc.next() :
                                        s;
    }

    // StrId in the form of: ['w_id', '1', 'd_id', 2] => MODEL:w_id=1:d_id=2
    @LocalMethodCall
    public static ObjId genObjId(Base.MODEL model, String[] strId) {
        StringBuffer id = new StringBuffer(model.getName());
        for (int i = 0; i < strId.length - 1; i++) {
            id.append(":" + strId[i] + "=" + strId[++i]);
        }
        return new ObjId(id.toString());
    }

    @LocalMethodCall
    public static Class<?>[] getConstructorParams() {
        Class<?>[] ret = new Class<?>[1];
        ret[0] = ObjId.class;
        return ret;
    }

    @LocalMethodCall
    public static MODEL getModelFromObjId(ObjId id) {
        String modelName = id.value.split(":")[0];
        return getModelFromName(modelName);
    }

    @LocalMethodCall
    public static Base.MODEL getModelFromName(String name) {
        for (Base.MODEL model : Base.MODEL.values()) {
            if (model.getName().equals(name)) {
                return model;
            }
        }
        return null;
    }

    @LocalMethodCall
    public static String[] extractAttributes(ObjId objId) {
        return parseObjId(objId).keySet().toArray(new String[parseObjId(objId).keySet().size()]);
    }

    @LocalMethodCall
    public static boolean set(Object object, String fieldName, Object fieldValue) {
        Class<?> clazz = object.getClass();
        Field field = null;
        Class fieldType = null;
        while (clazz != null) {
            try {
                field = clazz.getDeclaredField(fieldName);
                fieldType = interpret(String.valueOf(fieldValue)).getClass();
                field.setAccessible(true);
                if ((fieldType == java.lang.Integer.class || fieldType == java.lang.Long.class || fieldType == java.lang.Double.class)
                        && (String.valueOf(fieldValue).matches("^[+-/\\*]+.*"))) {
                    String value = String.valueOf(engine.eval(String.valueOf(field.get(object)) + fieldValue));
                    field.set(object, fieldType.cast(interpret(value)));
                } else {
                    field.set(object, fieldType.cast(interpret(String.valueOf(fieldValue))));
                }
                return true;
            } catch (IllegalAccessException | ScriptException | NoSuchFieldException | IllegalArgumentException e) {
                System.out.println(field.getName() + " - " + fieldType.getName() + " - " + fieldValue + " - " + interpret(String.valueOf(fieldValue)).toString());
                e.printStackTrace();
            }
        }
        return false;
    }

    @LocalMethodCall
    public static Map<String, Object> parseObjId(ObjId objId) {
        Map<String, Object> ret = new HashMap<>();
        String[] str = objId.value.split(":");
        for (int i = 1; i < str.length; i++) {
            String[] tmp = str[i].split("=");
            Class fieldType = interpret(String.valueOf(tmp[1])).getClass();
            ret.put(tmp[0], fieldType.cast(interpret(String.valueOf(tmp[1]))));
        }
        return ret;
    }

    @LocalMethodCall
    public Map<String, Object> toHashMap() {
        Map<String, Object> ret = new HashMap<>();
        Field[] fields = this.getClass().getFields();
        try {
            for (Field f : fields) {
                if (!f.getName().equals("model") && !f.getName().equals("id"))
                    ret.put(f.getName(), f.get(this));
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return ret;
    }

    @LocalMethodCall
    public JSONObject toJSON() {
        JSONObject ret = new JSONObject();
        Map<String, Object> attrs = this.toHashMap();
        for (String key : attrs.keySet()) {
            ret.put(key, attrs.get(key));
        }
        ret.put("objId", this.getObjIdString());
        return ret;
    }

    @LocalMethodCall
    public String toCSVString() {
        List<String> ret = new ArrayList<>();
        ret.add(this.getModelName());
        ret.add(this.getObjIdString());
        Map<String, Object> attrs = this.toHashMap();
        for (String key : attrs.keySet()) {
            ret.add(String.valueOf(attrs.get(key)));
        }

        return String.join(",", ret);
    }

    @LocalMethodCall
    public String toCSVHeader() {
        List<String> ret = new ArrayList<>();
        ret.add("Header");
        ret.add(this.getModelName());
        ret.add("id");
        Map<String, Object> attrs = this.toHashMap();
        for (String key : attrs.keySet()) {
            ret.add(String.valueOf(key));
        }
        return String.join(",", ret);
    }

    @LocalMethodCall
    public <T extends Base> T fromCSV(String[] headers, String[] values) {
        for (int i = 1; i < headers.length; i++) {
            if (headers[i].equals("id")) {
                try {
                    Class<?> clazz = this.getClass();
                    Field field = clazz.getField("id");
                    field.set(this, new ObjId(values[i]));
                    continue;
                } catch (IllegalAccessException | NoSuchFieldException e) {
                    e.printStackTrace();
                }
            }
            if (values[i] != null) {
                set(this, headers[i], values[i]);
            }
        }
        return (T) this;
    }

    @LocalMethodCall
    public static Map<String, Object> csvToHashMap(String[] headers, String[] values) {
        Map<String, Object> ret = new HashMap<>();
        for (int i = 1; i < headers.length; i++) {
            if (headers[i].equals("id") || headers[i].equals("model")) continue;
            ret.put(headers[i], values[i]);
        }
        return ret;
    }

    @LocalMethodCall
    public <T extends Base> T fromJSON(JSONObject json) {
        Field[] fields = this.getClass().getFields();
        for (Field f : fields) {
            if (json.get(f.getName()) != null) {
                set(this, f.getName(), json.get(f.getName()));
            }
        }
        return (T) this;
    }

    @LocalMethodCall
    public abstract String getObjIdString();

    @LocalMethodCall
    public abstract String[] getPrimaryKeys();

    @LocalMethodCall
    public abstract String getModelName();

    @Override
    public PRObject deepClone() {
        Codec codec = new CodecUncompressedKryo();
        return (PRObject) codec.deepDuplicate(this);
    }

    public enum MODEL {
        WAREHOUSE(1, "Warehouse"), DISTRICT(2, "District"), CUSTOMER(3, "Customer"), HISTORY(4, "History"),
        NEWORDER(5, "NewOrder"), ORDER(6, "Order"), ORDERLINE(7, "OrderLine"), ITEM(8, "Item"), STOCK(9, "Stock");
        int value;
        String name;

        MODEL(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int getValue() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }

    }
}
