package ch.usi.dslab.lel.dsqldb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.ApplicationProcedure;
import ch.usi.dslab.lel.aerie.Partition;

/**
 * Author: longle, created on 18/02/16.
 */
public class DSqlApplicationProcedure implements ApplicationProcedure {
    static Random randomGenerator = new Random(System.nanoTime());

    @Override
    public int getMoveDestination(Map<ObjId, Integer> objectMap) {
        ArrayList<ObjId> objIds = new ArrayList(Arrays.asList(objectMap.keySet().toArray()));
        if (objIds.size() == 0) return -1;
        ArrayList<Integer> partitions = new ArrayList<>();
        int count = 1, tempCount;
        partitions.addAll(objIds.stream().map(objectMap::get).collect(Collectors.toList()));
//        return partitions.get(randomGenerator.nextInt(partitions.size()));
//        return partitions.get(0);
        int index = 0;
        ObjId popularObj = objIds.get(0);
        int temp = 0;
        for (int i = 0; i < (partitions.size() - 1); i++) {
            temp = partitions.get(i);
            tempCount = 0;
            for (int j = 1; j < partitions.size(); j++) {
                if (temp == partitions.get(j))
                    tempCount++;
            }
            if (tempCount > count) {
                index = i;
                count = tempCount;
            }
        }
        return partitions.get(index);
    }


    @Override
    public int getObjectPlacement(ObjId objId) {
        return mapIdToPartition(objId);
    }

    public static int mapIdToPartition(ObjId objId) {
        return objId.hashCode() % Partition.getPartitionList().size() + 1;
    }
}

