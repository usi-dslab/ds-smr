package ch.usi.dslab.lel.dsqldb.jtpcc;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.dsqldb.DSqlCommand;
import ch.usi.dslab.lel.dsqldb.tables.*;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Author: longle, created on 14/07/16.
 */
public class jTPCCTDataSync {
    protected int numWarehouses = 0;
    private Logger log = Logger.getLogger(jTPCCTDataSync.class);
    public final static int
            TT_NEW_ORDER = 0,
            TT_PAYMENT = 1,
            TT_ORDER_STATUS = 2,
            TT_STOCK_LEVEL = 3,
            TT_DELIVERY = 4,
            TT_DELIVERY_BG = 5,
            TT_NONE = 6,
            TT_DONE = 7;

    public final static String transTypeNames[] = {
            "NEW_ORDER", "PAYMENT", "ORDER_STATUS", "STOCK_LEVEL",
            "DELIVERY", "DELIVERY_BG", "NONE", "DONE"};

    public int sched_code;
    public long sched_fuzz;
    public jTPCCTDataSync term_left;
    public jTPCCTDataSync term_right;
    public int tree_height;

    public boolean isLocalTransaction = true;

    private int transType;
    private long transDue;
    private long transStart;
    private long transEnd;
    private boolean transRbk;
    private String transError;

    private int terminalWarehouse = 0;
    private int terminalDistrict = 0;

    private static Object traceLock = new Object();

    private StringBuffer resultSB = new StringBuffer();
    private Formatter resultFmt = new Formatter(resultSB);

    private NewOrderData newOrder = null;
    private PaymentData payment = null;
    private OrderStatusData orderStatus = null;
    private StockLevelData stockLevel = null;
    private DeliveryData delivery = null;
    private DeliveryBGData deliveryBG = null;


    public void setNumWarehouses(int num) {
        numWarehouses = num;
    }

    public void setWarehouse(int warehouse) {
        terminalWarehouse = warehouse;
    }

    public int getWarehouse() {
        return terminalWarehouse;
    }

    public void setDistrict(int district) {
        terminalDistrict = district;
    }

    public int getDistrict() {
        return terminalDistrict;
    }

    public int getSkippedDeliveries() {
        int numSkipped = 0;

        for (int i = 0; i < 10; i++) {
            if (deliveryBG.delivered_o_id[i] < 0)
                numSkipped++;
        }

        return numSkipped;
    }


    public void execute(jTPCCConnection db) throws Exception {
        transStart = System.currentTimeMillis();
        if (transDue == 0)
            transDue = transStart;
        CompletableFuture<Void> trans = null;
        switch (transType) {
            case TT_NEW_ORDER:
                executeNewOrder(db);
                break;
            case TT_PAYMENT:
                executePayment(db);
                break;

            case TT_ORDER_STATUS:
                executeOrderStatus(db);
                break;

            case TT_STOCK_LEVEL:
                executeStockLevel(db);
                break;

            case TT_DELIVERY:
                executeDelivery(db);
                break;

            case TT_DELIVERY_BG:
                executeDeliveryBG(db);
                break;

            default:
                throw new Exception("Unknown transType " + transType);
        }
        transEnd = System.currentTimeMillis();
    }

    private void executeNewOrder(jTPCCConnection db) throws Exception {
        final int[] o_id = new int[1];
        int o_all_local = 1;
        long o_entry_d;
        int ol_cnt;
        final double[] total_amount = {0.0};
        int currentPartition = -1;

        int ol_seq[] = new int[15];

        // The o_entry_d is now.
        o_entry_d = System.currentTimeMillis();
        newOrder.o_entry_d = new Timestamp(o_entry_d).toString();

        /*
         * When processing the order lines we must select the STOCK rows
         * FOR UPDATE. This is because we must perform business logic
         * (the juggling with the S_QUANTITY) here in the application
         * and cannot do that in an atomic UPDATE statement while getting
         * the original value back at the same time (UPDATE ... RETURNING
         * may not be vendor neutral). This can lead to possible deadlocks
         * if two transactions try to lock the same two stock rows in
         * opposite order. To avoid that we process the order lines in
         * the order of the order of ol_supply_w_id, ol_i_id.
         */
        for (ol_cnt = 0; ol_cnt < 15 && newOrder.ol_i_id[ol_cnt] != 0; ol_cnt++) {
            ol_seq[ol_cnt] = ol_cnt;

            // While looping we also determine o_all_local.
            if (newOrder.ol_supply_w_id[ol_cnt] != newOrder.w_id)
                o_all_local = 0;
        }

        for (int x = 0; x < ol_cnt - 1; x++) {
            for (int y = x + 1; y < ol_cnt; y++) {
                if (newOrder.ol_supply_w_id[ol_seq[y]] < newOrder.ol_supply_w_id[ol_seq[x]]) {
                    int tmp = ol_seq[x];
                    ol_seq[x] = ol_seq[y];
                    ol_seq[y] = tmp;
                } else if (newOrder.ol_supply_w_id[ol_seq[y]] == newOrder.ol_supply_w_id[ol_seq[x]] &&
                        newOrder.ol_i_id[ol_seq[y]] < newOrder.ol_i_id[ol_seq[x]]) {
                    int tmp = ol_seq[x];
                    ol_seq[x] = ol_seq[y];
                    ol_seq[y] = tmp;
                }
            }
        }

        // The above also provided the output value for o_ol_cnt;
        newOrder.o_ol_cnt = ol_cnt;

        try {

            // Retrieve the required data from DISTRICT, CUSTOMER and WAREHOUSE
            ObjId w_objId = jTPCCConnection.getWarehouseObjId(newOrder.w_id);
            ObjId c_objId = jTPCCConnection.getCustomerObjId(newOrder.c_id, newOrder.d_id, newOrder.w_id);
            ObjId d_objId = jTPCCConnection.getDistrictObjId(newOrder.d_id, newOrder.w_id);

            Set<ObjId> objIds = new HashSet<>();
            objIds.add(c_objId);
            objIds.add(w_objId);
            objIds.add(d_objId);

            Command command = new Command(GenericCommand.READ_BATCH, objIds);
            final Customer[] customer = new Customer[1];
            final Warehouse[] warehouse = new Warehouse[1];
            final District[] district = new District[1];
            final int finalOl_cnt = ol_cnt;
            final int finalO_all_local = o_all_local;
            Message replyReadBatch = db.readBatch(command).get();
            currentPartition = replyReadBatch.sourcePartitionId;
            if (replyReadBatch.sourcePartitionId!=currentPartition) isLocalTransaction = false;

            //            System.out.println("got reply " + replyReadBatch);
            customer[0] = db.getObjectFromReply(Customer.class, replyReadBatch);
            warehouse[0] = db.getObjectFromReply(Warehouse.class, replyReadBatch);
            district[0] = db.getObjectFromReply(District.class, replyReadBatch);

            newOrder.d_tax = district[0].d_tax;
            newOrder.o_id = district[0].d_next_o_id;
            newOrder.w_tax = warehouse[0].w_tax;
            newOrder.c_last = customer[0].c_last;
            newOrder.c_credit = customer[0].c_credit;
            newOrder.c_discount = customer[0].c_discount;
            o_id[0] = newOrder.o_id;

            // Update the DISTRICT bumping the D_NEXT_O_ID
            DSqlCommand updateDistrict = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                    d_objId, new String[]{"d_next_o_id", String.valueOf(++district[0].d_next_o_id)});
            Message replyUpdateDistrict = updateDistrict.execute(db).get();
            if (replyUpdateDistrict.sourcePartitionId!=currentPartition) isLocalTransaction = false;
            //              System.out.println("got reply " + replyUpdateDistrict);

            // Insert the ORDER row
            ObjId oidOrder = jTPCCConnection.getOrderId(o_id[0], newOrder.w_id, newOrder.d_id, newOrder.c_id);
            Order order = new Order(o_id[0], newOrder.w_id, newOrder.d_id, newOrder.c_id);
            order.o_entry_d = System.currentTimeMillis();
            // The number of items, O_OL_CNT, is computed to match ol_cnt
            order.o_ol_cnt = finalOl_cnt;
            order.o_all_local = finalO_all_local;
            DSqlCommand createOrder = new DSqlCommand(GenericCommand.CREATE, oidOrder, order.toHashMap());
            Message replyCreateOrder = createOrder.execute(db).get();
            if (replyCreateOrder.sourcePartitionId!=currentPartition) isLocalTransaction = false;
            //            System.out.println("got reply " + replyCreateOrder);

            // A new row is inserted into both the NEW-ORDER table and the ORDER table to reflect the
            // creation of the new order. O_CARRIER_ID is set to a null value. If the order includes
            // only home order-lines, then O_ALL_LOCAL is set to 1, otherwise O_ALL_LOCAL is set to 0.
            NewOrder new_order = new NewOrder(district[0].d_next_o_id, district[0].d_id, district[0].d_w_id);
            ObjId oidNewOrder = jTPCCConnection.getNewOrderObjId(district[0].d_next_o_id, district[0].d_id, district[0].d_w_id);
            DSqlCommand commandNewOrder = new DSqlCommand(GenericCommand.CREATE, oidNewOrder, new_order.toHashMap());
            Message replyUpdateOrder = commandNewOrder.execute(db).get();
            if (replyUpdateOrder.sourcePartitionId!=currentPartition) isLocalTransaction = false;
            //            System.out.println("got reply " + replyUpdateOrder);

            // Per ORDER_LINE
            for (int i = 0; i < finalOl_cnt; i++) {
                int ol_number = i + 1;
                int seq = ol_seq[i];
                final String[] i_data = new String[1];

                ObjId i_objId = jTPCCConnection.getItemId(newOrder.ol_i_id[seq]);
                DSqlCommand readItem = new DSqlCommand(GenericCommand.READ, i_objId);
                Message replyReadItem = readItem.execute(db).get();
                if (replyReadItem.sourcePartitionId!=currentPartition) isLocalTransaction = false;
                Item item = db.getObjectFromReply(Item.class, replyReadItem);
                if (item != null) {
                    // Found ITEM
                    newOrder.i_name[seq] = item.i_name;
                    newOrder.i_price[seq] = item.i_price;
                    i_data[0] = item.i_data;

                    // Select STOCK for update.
                    ObjId s_oid = jTPCCConnection.getStockObjId(newOrder.ol_supply_w_id[seq], newOrder.ol_i_id[seq]);
                    DSqlCommand readStock = new DSqlCommand(GenericCommand.READ, s_oid);
                    Message replyReadStock = readStock.execute(db).get();
                    if (replyReadStock.sourcePartitionId!=currentPartition) isLocalTransaction = false;
                    Stock stock = jTPCCConnection.getObjectFromReply(Stock.class, replyReadStock);
                    if (stock == null) {
                        throw new Exception("STOCK with" +
                                " S_W_ID=" + newOrder.ol_supply_w_id[seq] +
                                " S_I_ID=" + newOrder.ol_i_id[seq] +
                                " not fount");
                    }
                    newOrder.s_quantity[seq] = stock.s_quantity;

                    newOrder.ol_amount[seq] = newOrder.i_price[seq] * newOrder.ol_quantity[seq];
                    if (i_data[0].contains("ORIGINAL") && stock.s_data.contains("ORIGINAL"))
                        newOrder.brand_generic[seq] = new String("B");
                    else
                        newOrder.brand_generic[seq] = new String("G");

                    total_amount[0] += newOrder.ol_amount[seq] *
                            (1.0 - newOrder.c_discount) *
                            (1.0 + newOrder.w_tax + newOrder.d_tax);

                    // Update the STOCK row.
                    if (newOrder.s_quantity[seq] >= newOrder.ol_quantity[seq] + 10)
                        stock.s_quantity = newOrder.s_quantity[seq] - newOrder.ol_quantity[seq];
                    else
                        stock.s_quantity = newOrder.s_quantity[seq] + 91;

                    stock.s_ytd += newOrder.ol_quantity[seq];

                    if (newOrder.ol_supply_w_id[seq] == newOrder.w_id)
                        stock.s_remote_cnt += 0;
                    else
                        stock.s_remote_cnt += 1;

                    String[] update = new String[]{
                            "s_quantity", String.valueOf(stock.s_quantity),
                            "s_ytd", String.valueOf(stock.s_ytd),
                            "s_remote_cnt", String.valueOf(stock.s_remote_cnt)
                    };
                    DSqlCommand updateStock = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE, s_oid, update);

                    Message replyUpdateStock = updateStock.execute(db).get();
                    if (replyUpdateStock.sourcePartitionId!=currentPartition) isLocalTransaction = false;

//                    System.out.println("got reply " + replyUpdateStock);
                    stock = jTPCCConnection.getObjectFromReply(Stock.class, replyUpdateStock);
                    String ol_dist_info = null;
                    switch (district[0].d_id) {
                        case 1:
                            ol_dist_info = stock.s_dist_01;
                            break;
                        case 2:
                            ol_dist_info = stock.s_dist_02;
                            break;
                        case 3:
                            ol_dist_info = stock.s_dist_03;
                            break;
                        case 4:
                            ol_dist_info = stock.s_dist_04;
                            break;
                        case 5:
                            ol_dist_info = stock.s_dist_05;
                            break;
                        case 6:
                            ol_dist_info = stock.s_dist_06;
                            break;
                        case 7:
                            ol_dist_info = stock.s_dist_07;
                            break;
                        case 8:
                            ol_dist_info = stock.s_dist_08;
                            break;
                        case 9:
                            ol_dist_info = stock.s_dist_09;
                            break;
                        case 10:
                            ol_dist_info = stock.s_dist_10;
                            break;
                    }
                    ObjId ol_oid = jTPCCConnection.getOrderLineObjId(ol_number, newOrder.w_id, newOrder.d_id, o_id[0]);
                    OrderLine orderLine = new OrderLine();
                    orderLine.ol_i_id = newOrder.ol_i_id[seq];
                    orderLine.ol_supply_w_id = newOrder.ol_supply_w_id[seq];
                    orderLine.ol_quantity = newOrder.ol_quantity[seq];
                    orderLine.ol_amount = newOrder.ol_amount[seq];
                    orderLine.ol_dist_info = ol_dist_info;
                    DSqlCommand createOrderLine = new DSqlCommand(GenericCommand.CREATE, ol_oid, orderLine.toHashMap());
                    Message replyCreateOrderLine = createOrderLine.execute(db).get();
                    if (replyCreateOrderLine.sourcePartitionId!=currentPartition) isLocalTransaction = false;
                    newOrder.execution_status = new String("Order placed");
                    newOrder.total_amount = total_amount[0];
                } else {
                             /*
                             * 1% of NEW_ORDER transactions use an unused item
                             * in the last line to simulate user entry errors.
                             * Make sure this is precisely that case.
                             */
                    if (transRbk && (newOrder.ol_i_id[seq] < 1 ||
                            newOrder.ol_i_id[seq] > 100000)) {
                            /*
                             * Clause 2.4.2.3 mandates that the entire
                             * transaction profile up to here must be executed
                             * before we can roll back, except for retrieving
                             * the missing STOCK row and inserting this
                             * ORDER_LINE row. Note that we haven't updated
                             * STOCK rows or inserted any ORDER_LINE rows so
                             * far, we only batched them up. So we must do
                             * that now in order to satisfy 2.4.2.3.
                             */
                        newOrder.total_amount = total_amount[0];
                        newOrder.execution_status = "Item number is not valid";
                        return;
                    }
                    // This ITEM should have been there.
                    throw new Exception("ITEM " + newOrder.ol_i_id[seq] + " not found");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    /* **********************************************************************
        * **********************************************************************
        * ***** NEW_ORDER related methods and subclass. ************************
        * **********************************************************************
        * *********************************************************************/
    public void generateNewOrder(jTPCCRandom rnd, long due) {
        int o_ol_cnt;
        int i = 0;

        transType = TT_NEW_ORDER;
        transDue = due;
        transStart = 0;
        transEnd = 0;
        transRbk = false;
        transError = null;

        newOrder = new NewOrderData();
        payment = null;
        orderStatus = null;
        stockLevel = null;
        delivery = null;
        deliveryBG = null;

        newOrder.w_id = terminalWarehouse;    // 2.4.1.1
        newOrder.d_id = rnd.nextInt(1, jTPCCConfig.configDistPerWhse);   // 2.4.1.2
        newOrder.c_id = rnd.getCustomerID();
        o_ol_cnt = rnd.nextInt(5, 15);   // 2.4.1.3

        while (i < o_ol_cnt)                    // 2.4.1.5
        {
            newOrder.ol_i_id[i] = rnd.getItemID();
            if (rnd.nextInt(1, 100) <= 99)
                newOrder.ol_supply_w_id[i] = terminalWarehouse;
            else
                newOrder.ol_supply_w_id[i] = rnd.nextInt(1, numWarehouses);
            newOrder.ol_quantity[i] = rnd.nextInt(1, 10);
            i++;
        }

        if (rnd.nextInt(1, 100) == 1)           // 2.4.1.4
        {
            newOrder.ol_i_id[i - 1] += (rnd.nextInt(1, 9) * jTPCCConfig.configItemCount);
            transRbk = true;
        }

        // Zero out remainint lines
        while (i < 15) {
            newOrder.ol_i_id[i] = 0;
            newOrder.ol_supply_w_id[i] = 0;
            newOrder.ol_quantity[i] = 0;
            i++;
        }
    }

    /* **********************************************************************
     * **********************************************************************
     * ***** PAYMENT related methods and subclass. **************************
     * **********************************************************************
     * *********************************************************************/

    private void executePayment(jTPCCConnection db) throws Exception {
        ObjId w_objId = jTPCCConnection.getWarehouseObjId(payment.w_id);
        ObjId d_objId = jTPCCConnection.getDistrictObjId(payment.d_id, payment.w_id);
        final District[] district = {null};
        final Warehouse[] warehouse = {null};
        int currentPartition = -1;
        try {
            // UPDATE district
            DSqlCommand updateDistrict = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                    d_objId, new String[]{"d_ytd", "+" + payment.h_amount});
            Message replyUpdateDistrict = updateDistrict.execute(db).get();
            currentPartition = replyUpdateDistrict.sourcePartitionId;
            if (replyUpdateDistrict.sourcePartitionId!=currentPartition) isLocalTransaction = false;
//        System.out.println("got reply " + replyUpdateDistrict);

            // READ district
            DSqlCommand readDistrict = new DSqlCommand(GenericCommand.READ, d_objId);
            Message replyReadDistrict = readDistrict.execute(db).get();
            if (replyReadDistrict.sourcePartitionId!=currentPartition) isLocalTransaction = false;

//        System.out.println("got reply " + replyReadDistrict);
            district[0] = jTPCCConnection.getObjectFromReply(District.class, replyReadDistrict);
            payment.d_name = district[0].d_name;
            payment.d_street_1 = district[0].d_street_1;
            payment.d_street_2 = district[0].d_street_2;
            payment.d_city = district[0].d_city;
            payment.d_state = district[0].d_state;
            payment.d_zip = district[0].d_zip;

            // UPDATE Warehouse
            DSqlCommand updateWarehouse = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                    w_objId, new String[]{"w_ytd", "+" + payment.h_amount});
            Message replyUpdateWarehouse = updateWarehouse.execute(db).get();
            if (replyUpdateWarehouse.sourcePartitionId!=currentPartition) isLocalTransaction = false;

//        System.out.println("got reply " + replyUpdateWarehouse);

            // READ Warehouse
            DSqlCommand readWarehouse = new DSqlCommand(GenericCommand.READ, w_objId);
            Message replyReadWarehouse = readWarehouse.execute(db).get();
            if (replyReadWarehouse.sourcePartitionId!=currentPartition) isLocalTransaction = false;

//        System.out.println("got reply " + replyReadWarehouse);
            warehouse[0] = jTPCCConnection.getObjectFromReply(Warehouse.class, replyReadWarehouse);
            payment.w_name = warehouse[0].w_name;
            payment.w_street_1 = warehouse[0].w_street_1;
            payment.w_street_2 = warehouse[0].w_street_2;
            payment.w_city = warehouse[0].w_city;
            payment.w_state = warehouse[0].w_state;
            payment.w_zip = warehouse[0].w_zip;

            // If C_LAST is given instead of C_ID (60%), determine the C_ID.
            Message replyReadCustomer;
            if (payment.c_last != null) {
                ObjId c_objId = jTPCCConnection.getCustomerObjId(payment.c_last, payment.d_id, payment.w_id);
                DSqlCommand readCustomerByName = new DSqlCommand(GenericCommand.READ_BATCH, c_objId);
                replyReadCustomer = readCustomerByName.execute(db).get();
                if (replyReadCustomer.sourcePartitionId!=currentPartition) isLocalTransaction = false;
            } else {
                ObjId c_objId = jTPCCConnection.getCustomerObjId(payment.c_id, payment.d_id, payment.w_id);
                DSqlCommand readCustomerById = new DSqlCommand(GenericCommand.READ, c_objId);
                replyReadCustomer = readCustomerById.execute(db).get();
                if (replyReadCustomer.sourcePartitionId!=currentPartition) isLocalTransaction = false;
            }

//            System.out.println("got reply " + replyReadCustomer);
            Customer customer;
            if (payment.c_last != null) {
                ArrayList<Customer> customers = (ArrayList<Customer>) jTPCCConnection.getObjectsFromReply(Customer.class, replyReadCustomer);
                if (customers.size() == 0) {
                    throw new RuntimeException("NO CUSTOMER WITH GIVEN LASTNAME FOUND");
//                    return CompletableFuture.supplyAsync(() -> null);
                }
                customers.sort((o1, o2) -> o1.c_first.compareToIgnoreCase(o2.c_first));
                int count = customers.size();
                customer = customers.get((customers.size() + 1) / 2 - 1);
            } else {
                customer = jTPCCConnection.getObjectFromReply(Customer.class, replyReadCustomer);
            }
            payment.c_first = customer.c_first;
            payment.c_middle = customer.c_middle;
            if (payment.c_last == null)
                payment.c_last = customer.c_last;
            payment.c_street_1 = customer.c_street_1;
            payment.c_street_2 = customer.c_street_2;
            payment.c_city = customer.c_city;
            payment.c_state = customer.c_state;
            payment.c_zip = customer.c_zip;
            payment.c_phone = customer.c_phone;
            payment.c_since = customer.c_since;
            payment.c_credit = customer.c_credit;
            payment.c_credit_lim = customer.c_credit_lim;
            payment.c_discount = customer.c_discount;
            payment.c_balance = customer.c_balance;
            payment.c_data = new String("");

            // Update the CUSTOMER.
            payment.c_balance -= payment.h_amount;
            Message replyUpdateCustomer;
            if (payment.c_credit.equals("GC")) {
                // Customer with good credit, don't update C_DATA.
                DSqlCommand updateCustomer = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                        customer.getId(), new String[]{"c_balance", "+" + payment.h_amount,
                        "c_ytd_payment", "+" + payment.h_amount, "c_payment_cnt", "+1"});
                replyUpdateCustomer = updateCustomer.execute(db).get();
                if (replyUpdateCustomer.sourcePartitionId!=currentPartition) isLocalTransaction = false;
            } else {
                // Customer with bad credit, need to do the C_DATA work.
                payment.c_data = customer.c_data;
                StringBuffer sbData = new StringBuffer();
                Formatter fmtData = new Formatter(sbData);
                fmtData.format("C_ID=%d C_D_ID=%d C_W_ID=%d " +
                                "D_ID=%d W_ID=%d H_AMOUNT=%.2f   ",
                        payment.c_id, payment.c_d_id, payment.c_w_id,
                        payment.d_id, payment.w_id, payment.h_amount);
                sbData.append(payment.c_data);
                if (sbData.length() > 500)
                    sbData.setLength(500);
                payment.c_data = sbData.toString();
                DSqlCommand updateCustomer = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                        customer.getId(), new String[]{"c_balance", "+" + payment.h_amount,
                        "c_ytd_payment", "+" + payment.h_amount, "c_payment_cnt", "+1", "c_data", payment.c_data});
                replyUpdateCustomer = updateCustomer.execute(db).get();
                if (replyUpdateCustomer.sourcePartitionId!=currentPartition) isLocalTransaction = false;
            }

//            System.out.println("got reply " + replyUpdateCustomer);

            // Insert the HISORY row.
            if (payment.w_name.length() > 10) payment.w_name = payment.w_name.substring(0, 10);
            if (payment.d_name.length() > 10) payment.d_name = payment.d_name.substring(0, 10);
            String h_data = payment.w_name + "    " + payment.d_name;
            ObjId h_objId = Base.genObjId(Base.MODEL.HISTORY, new String[]{
                    "h_c_id", String.valueOf(payment.c_d_id),
                    "h_c_d_id", String.valueOf(payment.c_d_id),
                    "h_c_w_id", String.valueOf(payment.c_w_id)
            });
            History history = new History(payment.c_d_id, payment.c_d_id, payment.c_w_id);
            history.h_d_id = payment.d_id;
            history.h_w_id = payment.w_id;
            history.h_date = System.currentTimeMillis();
            history.h_amount = payment.h_amount;
            history.h_data = h_data;
            DSqlCommand createHistory = new DSqlCommand(GenericCommand.CREATE, h_objId, history.toHashMap());
            Message replyCreateHistory = createHistory.execute(db).get();
            if (replyCreateHistory.sourcePartitionId!=currentPartition) isLocalTransaction = false;

//            System.out.println("got reply " + replyCreateHistory);
        } catch (Exception e) {
            payment.c_data="EXCEPTIONS";
//            System.out.println(e.getMessage());
        }

    }

    public void generatePayment(jTPCCRandom rnd, long due) {
        transType = TT_PAYMENT;
        transDue = due;
        transStart = 0;
        transEnd = 0;
        transRbk = false;
        transError = null;

        newOrder = null;
        payment = new PaymentData();
        orderStatus = null;
        stockLevel = null;
        delivery = null;
        deliveryBG = null;

        payment.w_id = terminalWarehouse;    // 2.5.1.1
        payment.d_id = rnd.nextInt(1, jTPCCConfig.configDistPerWhse);   // 2.5.1.2
        payment.c_w_id = payment.w_id;
        payment.c_d_id = payment.d_id;
        if (rnd.nextInt(1, 100) > 85) {
            payment.c_d_id = rnd.nextInt(1, 10);
            while (payment.c_w_id == payment.w_id && numWarehouses > 1)
                payment.c_w_id = rnd.nextInt(1, numWarehouses);
        }
        if (rnd.nextInt(1, 100) <= 60) {
            payment.c_last = rnd.getCLast();
            payment.c_id = 0;
        } else {
            payment.c_last = null;
            payment.c_id = rnd.getCustomerID();
        }

        // 2.5.1.3
        payment.h_amount = ((double) rnd.nextLong(100, 500000)) / 100.0;
    }

    /* **********************************************************************
    * **********************************************************************
    * ***** ORDER_STATUS related methods and subclass. *********************
    * **********************************************************************
    * *********************************************************************/

    private void executeOrderStatus(jTPCCConnection db) throws Exception {
        // If C_LAST is given instead of C_ID (60%), determine the C_ID.
        // If C_LAST is given instead of C_ID (60%), determine the C_ID.
        CompletableFuture<Message> command;
        final Order[] order = new Order[1];
        int currentPartition = -1;
        final int[] ol_idx = {0};
        try {
            if (orderStatus.c_last != null) {
                ObjId c_objId = jTPCCConnection.getCustomerObjId(orderStatus.c_last, orderStatus.d_id, orderStatus.w_id);
                DSqlCommand readCustomerByName = new DSqlCommand(GenericCommand.READ_BATCH, c_objId);
                command = readCustomerByName.execute(db);
            } else {
                ObjId c_objId = jTPCCConnection.getCustomerObjId(orderStatus.c_id, orderStatus.d_id, orderStatus.w_id);
                DSqlCommand readCustomerById = new DSqlCommand(GenericCommand.READ, c_objId);
                command = readCustomerById.execute(db);
            }
            Message replyReadCustomer = command.get();
            currentPartition = replyReadCustomer.sourcePartitionId;
            if (replyReadCustomer.sourcePartitionId!=currentPartition) isLocalTransaction = false;
//            System.out.println("got reply " + replyReadCustomer);
            Customer customer;
            if (orderStatus.c_last != null) {
                ArrayList<Customer> customers = (ArrayList<Customer>) jTPCCConnection.getObjectsFromReply(Customer.class, replyReadCustomer);
                if (customers.size() == 0) {
                    throw new RuntimeException("NO CUSTOMER WITH GIVEN LASTNAME FOUND");
//                    return CompletableFuture.supplyAsync(() -> null);
                }
                customers.sort((o1, o2) -> o1.c_first.compareToIgnoreCase(o2.c_first));
                int count = customers.size();
                customer = customers.get((customers.size() + 1) / 2 - 1);
            } else {
                customer = jTPCCConnection.getObjectFromReply(Customer.class, replyReadCustomer);
            }
            orderStatus.c_id = customer.c_id;
            orderStatus.c_first = customer.c_first;
            orderStatus.c_middle = customer.c_middle;
            if (orderStatus.c_last == null)
                orderStatus.c_last = customer.c_last;
            orderStatus.c_balance = customer.c_balance;

            ObjId o_objId = jTPCCConnection.getOrderIdWithCustomerId(orderStatus.w_id, orderStatus.d_id, orderStatus.c_id);
            DSqlCommand readOrder = new DSqlCommand(GenericCommand.READ_BATCH, o_objId);
            Message replyReadOrder = readOrder.execute(db).get();
            if (replyReadOrder.sourcePartitionId!=currentPartition) isLocalTransaction = false;

            ArrayList<Order> orders = (ArrayList<Order>) jTPCCConnection.getObjectsFromReply(Order.class, replyReadOrder);
            Message replyReadOrderLine = null;
            if (orders.size() > 0) {
                // find the newest order for the customer
                // retrieve the carrier & order date for the most recent order.
                orders.sort((o1, o2) -> o2.o_id - o1.o_id);
                order[0] = orders.get(0);

                orderStatus.o_id = order[0].o_id;
                orderStatus.o_entry_d = String.valueOf(order[0].o_entry_d);
                orderStatus.o_carrier_id = order[0].o_carrier_id;

                // retrieve the order lines for the most recent order
                ObjId ol_objId = Base.genObjId(Base.MODEL.ORDERLINE, new String[]{
                        "ol_o_id", String.valueOf(orderStatus.o_id),
                        "ol_d_id", String.valueOf(orderStatus.d_id),
                        "ol_w_id", String.valueOf(orderStatus.w_id)
                });
                DSqlCommand readOrderLine = new DSqlCommand(GenericCommand.READ_BATCH, ol_objId);
                replyReadOrderLine = readOrderLine.execute(db).get();
                if (replyReadOrderLine.sourcePartitionId!=currentPartition) isLocalTransaction = false;
            } else {
                orderStatus.o_carrier_id = -1;
                throw new Exception("Last Order for" +
                        " W_ID=" + orderStatus.w_id +
                        " D_ID=" + orderStatus.d_id +
                        " C_ID=" + orderStatus.c_id + " not found");
            }

//            System.out.println("got reply " + replyReadOrderLine);

            ArrayList<OrderLine> orderLines = (ArrayList<OrderLine>) jTPCCConnection.getObjectsFromReply(OrderLine.class, replyReadOrderLine);

            for (OrderLine orderLine : orderLines) {
                Timestamp ol_delivery_d;

                orderStatus.ol_i_id[ol_idx[0]] = orderLine.ol_i_id;
                orderStatus.ol_supply_w_id[ol_idx[0]] = orderLine.ol_supply_w_id;
                orderStatus.ol_quantity[ol_idx[0]] = orderLine.ol_quantity;
                orderStatus.ol_amount[ol_idx[0]] = orderLine.ol_amount;
                ol_delivery_d = new Timestamp(orderLine.ol_delivery_d);
                if (ol_delivery_d != null)
                    orderStatus.ol_delivery_d[ol_idx[0]] = ol_delivery_d.toString();
                else
                    orderStatus.ol_delivery_d[ol_idx[0]] = null;
                ol_idx[0]++;
            }
            while (ol_idx[0] < 15) {
                orderStatus.ol_i_id[ol_idx[0]] = 0;
                orderStatus.ol_supply_w_id[ol_idx[0]] = 0;
                orderStatus.ol_quantity[ol_idx[0]] = 0;
                orderStatus.ol_amount[ol_idx[0]] = 0.0;
                orderStatus.ol_delivery_d[ol_idx[0]] = null;
                ol_idx[0]++;
            }
        } catch (Exception e) {
//            System.out.println(e.getMessage());
        }
    }

    public void generateOrderStatus(jTPCCRandom rnd, long due) {
        transType = TT_ORDER_STATUS;
        transDue = due;
        transStart = 0;
        transEnd = 0;
        transRbk = false;
        transError = null;

        newOrder = null;
        payment = null;
        orderStatus = new OrderStatusData();
        stockLevel = null;
        delivery = null;
        deliveryBG = null;

        orderStatus.w_id = terminalWarehouse;
        orderStatus.d_id = rnd.nextInt(1, jTPCCConfig.configDistPerWhse);
        if (rnd.nextInt(1, 100) <= 60) {
            orderStatus.c_id = 0;
            orderStatus.c_last = rnd.getCLast();
        } else {
            orderStatus.c_id = rnd.getCustomerID();
            orderStatus.c_last = null;
        }
    }

    /* **********************************************************************
     * **********************************************************************
     * ***** STOCK_LEVEL related methods and subclass. **********************
     * **********************************************************************
     * *********************************************************************/
    private void executeStockLevel(jTPCCConnection db) throws Exception {
        ObjId d_objId = new ObjId(Base.genObjId(Base.MODEL.DISTRICT,
                new String[]{"d_id", String.valueOf(stockLevel.d_id), "d_w_id", String.valueOf(stockLevel.w_id)}));
        DSqlCommand readDistrict = new DSqlCommand(GenericCommand.READ, d_objId);
        Message replyReadDistrict = readDistrict.execute(db).get();
        int currentPartition = replyReadDistrict.sourcePartitionId;
        if (replyReadDistrict.sourcePartitionId!=currentPartition) isLocalTransaction = false;

        District district = jTPCCConnection.getObjectFromReply(District.class, replyReadDistrict);
        ObjId ol_oid = new ObjId(Base.MODEL.ORDERLINE.getName() + ":" + "ol_d_id=" + String.valueOf(stockLevel.d_id) + ":ol_w_id=" + String.valueOf(stockLevel.w_id)
                + "#ol_o_id#" + (district.d_next_o_id - 20) + "#" + district.d_next_o_id);

        DSqlCommand readOrderLines = new DSqlCommand(GenericCommand.READ_BATCH, ol_oid);
        Message replyReadOrderLines = readOrderLines.execute(db).get();
        if (replyReadOrderLines.sourcePartitionId!=currentPartition) isLocalTransaction = false;
        ArrayList<OrderLine> orderLines = (ArrayList<OrderLine>) jTPCCConnection.getObjectsFromReply(OrderLine.class, replyReadOrderLines);
        Set<Integer> olNumbers = new HashSet<Integer>();
        orderLines.stream().forEach(orderLine -> olNumbers.add(orderLine.ol_i_id));

        Set<ObjId> objIds = new HashSet<ObjId>();
        olNumbers.stream().forEach(ol_i_id -> {
            objIds.add(Base.genObjId(Base.MODEL.STOCK, new String[]{
                    "s_w_id", String.valueOf(stockLevel.w_id),
                    "s_i_id", String.valueOf(ol_i_id)
            }));
        });

        Command command = new Command(GenericCommand.READ_BATCH, objIds);
        Message replyReadStocks = db.readBatch(command).get();
        if (replyReadStocks.sourcePartitionId!=currentPartition) isLocalTransaction = false;
    }

    public void generateStockLevel(jTPCCRandom rnd, long due) {
        transType = TT_STOCK_LEVEL;
        transDue = due;
        transStart = 0;
        transEnd = 0;
        transRbk = false;
        transError = null;

        newOrder = null;
        payment = null;
        orderStatus = null;
        stockLevel = new StockLevelData();
        delivery = null;
        deliveryBG = null;

        stockLevel.w_id = terminalWarehouse;
        stockLevel.d_id = terminalDistrict;
        stockLevel.threshold = rnd.nextInt(10, 20);
    }


    /* **********************************************************************
     * **********************************************************************
     * ***** DELIVERY related methods and subclass. *************************
     * **********************************************************************
     * *********************************************************************/
    private void executeDelivery(jTPCCConnection db) throws Exception {
        long now = System.currentTimeMillis();

	/*
     * The DELIVERY transaction is different from all the others.
	 * The foreground transaction, experienced by the user, does
	 * not perform any interaction with the database. It only queues
	 * a request to perform such a transaction in the background
	 * (DeliveryBG). We store that TData object in the delivery
	 * part for the caller to pick up and queue/execute.
	 */
        delivery.deliveryBG = new jTPCCTDataSync();
        delivery.deliveryBG.generateDeliveryBG(delivery.w_id, now,
                new Timestamp(now).toString(), this);
        delivery.execution_status = new String("Delivery has been queued");
    }

    private void executeDeliveryBG(jTPCCConnection db) throws Exception {
        int rc;
        int d_id;
        int o_id;
        int c_id;
        double sum_ol_amount;
        long now = System.currentTimeMillis();
        int currentPartition = -1;
        try {
            for (d_id = 1; d_id <= jTPCCConfig.configDistPerWhse; d_id++) {
                /*
                 * Try to find the oldest undelivered order for this
                 * DISTRICT. There may not be one, which is a case
                 * that needs to be reportd.
                 */
                o_id = -1;
                while (o_id < 0) {
                    ObjId no_objId = new ObjId(Base.MODEL.NEWORDER.getName() + ":" + "no_d_id=" + String.valueOf(d_id) + ":no_w_id=" + String.valueOf(deliveryBG.w_id)
                            + "#no_o_id#min");
                    DSqlCommand readNO = new DSqlCommand(GenericCommand.READ_BATCH, no_objId);
                    Message replyReadNewOrder = readNO.execute(db).get();
                    if (currentPartition==-1) currentPartition = replyReadNewOrder.sourcePartitionId;
                    if (replyReadNewOrder.sourcePartitionId!=currentPartition) isLocalTransaction = false;
                    ArrayList<NewOrder> newOrders = (ArrayList<NewOrder>) jTPCCConnection.getObjectsFromReply(NewOrder.class, replyReadNewOrder);
                    if (newOrders.size() == 0) {
                        break;
                    }
                    o_id = newOrders.get(0).no_o_id;
                    ObjId no_objId_delete = newOrders.get(0).getId();
                    DSqlCommand deleteNO = new DSqlCommand(GenericCommand.DELETE, no_objId_delete);
                    Message replyDeleteNewOrder = deleteNO.execute(db).get();
                    if (replyDeleteNewOrder.sourcePartitionId!=currentPartition) isLocalTransaction = false;

                    if (!replyDeleteNewOrder.toString().equals("[OK]")) {
                        /*
                         * Failed to delete the NEW_ORDER row. This is not
                         * an error since for concurrency reasons we did
                         * not select FOR UPDATE above. It is possible that
                         * another, concurrent DELIVERY_BG transaction just
                         * deleted this row and is working on it now. We
                         * simply got back and try to get the next one.
                         * This logic only works in READ_COMMITTED isolation
                         * level and will cause SQLExceptions in anything
                         * higher than that.
                         */
                        o_id = -1;
                    }
                    if (o_id < 0) {
                        // No undelivered NEW_ORDER found for this DISTRICT.
                        continue;
                    }
                    /*
                     * We found out oldest undelivered order for this DISTRICT
                     * and the NEW_ORDER line has been deleted. Process the
                     * rest of the DELIVERY_BG.
                     */
                    ObjId o_objId = jTPCCConnection.getOrderIdWithOrderId(deliveryBG.w_id, d_id, o_id);
                    DSqlCommand updateOrder = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                            o_objId, new String[]{"o_carrier_id", String.valueOf(deliveryBG.o_carrier_id)});
                    Message replyUpdateOrder = updateOrder.execute(db).get();
                    if (replyUpdateOrder.sourcePartitionId!=currentPartition) isLocalTransaction = false;
                    Order order = jTPCCConnection.getObjectFromReply(Order.class, replyUpdateOrder);
                    if (order == null) {
                        throw new Exception("ORDER in DELIVERY_BG for" +
                                " O_W_ID=" + deliveryBG.w_id +
                                " O_D_ID=" + d_id +
                                " O_ID=" + o_id + " not found");
                    }
                    // Get the o_c_id from the ORDER.
                    c_id = order.o_c_id;

                    // Update ORDER_LINE setting the ol_delivery_d.
                    ObjId ol_objId = jTPCCConnection.getOrderLineObjId(o_id, d_id, deliveryBG.w_id);
                    DSqlCommand updateOrderLine = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE_BATCH,
                            ol_objId, new String[]{"ol_delivery_d", String.valueOf(System.currentTimeMillis())});
                    Message replyUpdateOrderLine = updateOrderLine.execute(db).get();
                    if (replyUpdateOrderLine.sourcePartitionId!=currentPartition) isLocalTransaction = false;

                    // Select the sum(ol_amount) from ORDER_LINE.
                    DSqlCommand readAllOrderLine = new DSqlCommand(GenericCommand.READ_BATCH, ol_objId);
                    Message replyReadAllOrderLine= updateOrderLine.execute(db).get();
                    if (replyReadAllOrderLine.sourcePartitionId!=currentPartition) isLocalTransaction = false;
                    List<OrderLine> ols = jTPCCConnection.getObjectsFromReply(OrderLine.class, replyReadAllOrderLine);
                    final double[] total = {0};
                    ols.stream().forEach(ol -> total[0] = total[0] + ol.ol_amount);
                    sum_ol_amount = total[0];

                    // Update the CUSTOMER.
                    ObjId c_objId = jTPCCConnection.getCustomerObjId(c_id, d_id, deliveryBG.w_id);
                    DSqlCommand updateCustomer = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE,
                            c_objId, new String[]{"c_balance", "+" + total[0]});
                    Message replyUpdateCustomer = updateCustomer.execute(db).get();
                    if (replyUpdateCustomer.sourcePartitionId!=currentPartition) isLocalTransaction = false;

                    // Recored the delivered O_ID in the DELIVERY_BG
                    deliveryBG.delivered_o_id[d_id - 1] = o_id;
                }
            }
        } catch (Exception e) {
//            System.out.println(e.getMessage());
        }

    }

    public void generateDelivery(jTPCCRandom rnd, long due) {
        transType = TT_DELIVERY;
        transDue = due;
        transStart = 0;
        transEnd = 0;
        transRbk = false;
        transError = null;

        newOrder = null;
        payment = null;
        orderStatus = null;
        stockLevel = null;
        delivery = new DeliveryData();
        deliveryBG = null;

        delivery.w_id = terminalWarehouse;
        delivery.o_carrier_id = rnd.nextInt(1, 10);
        delivery.execution_status = null;
        delivery.deliveryBG = null;
    }

    private void generateDeliveryBG(int w_id, long due, String ol_delivery_d,
                                    jTPCCTDataSync parent) {
    /*
     * The DELIVERY_BG part is created as a result of executing the
	 * foreground part of the DELIVERY transaction. Because of that
	 * it inherits certain information from it.
	 */
        numWarehouses = parent.numWarehouses;
        terminalWarehouse = parent.terminalWarehouse;
        terminalDistrict = parent.terminalDistrict;

        transType = TT_DELIVERY_BG;
        transDue = due;
        transStart = 0;
        transEnd = 0;
        transRbk = false;
        transError = null;

        newOrder = null;
        payment = null;
        orderStatus = null;
        stockLevel = null;
        delivery = null;
        deliveryBG = new DeliveryBGData();

        deliveryBG.w_id = parent.delivery.w_id;
        deliveryBG.o_carrier_id = parent.delivery.o_carrier_id;
        deliveryBG.ol_delivery_d = ol_delivery_d;

        deliveryBG.delivered_o_id = new int[10];
        for (int i = 0; i < 10; i++)
            deliveryBG.delivered_o_id[i] = -1;
    }

    public void traceScreen()
            throws Exception {
        StringBuffer sb = new StringBuffer();
        Formatter fmt = new Formatter(sb);

        StringBuffer screenSb[] = new StringBuffer[23];
        Formatter screenFmt[] = new Formatter[23];
        for (int i = 0; i < 23; i++) {
            screenSb[i] = new StringBuffer();
            screenFmt[i] = new Formatter(screenSb[i]);
        }

        if (transType < TT_NEW_ORDER || transType > TT_DONE)
            throw new Exception("Unknown transType " + transType);

        synchronized (traceLock) {
            fmt.format("==== %s %s ==== Terminal %d,%d =================================================",
                    transTypeNames[transType],
                    (transEnd == 0) ? "INPUT" : "OUTPUT",
                    terminalWarehouse, terminalDistrict);
            sb.setLength(79);
            log.debug(sb.toString());
            sb.setLength(0);

            fmt.format("---- Due:   %s", (transDue == 0) ? "N/A" :
                    new Timestamp(transDue).toString());
            log.debug(sb.toString());
            sb.setLength(0);

            fmt.format("---- Start: %s", (transStart == 0) ? "N/A" :
                    new Timestamp(transStart).toString());
            log.debug(sb.toString());
            sb.setLength(0);

            fmt.format("---- End:   %s", (transEnd == 0) ? "N/A" :
                    new Timestamp(transEnd).toString());
            log.debug(sb.toString());
            sb.setLength(0);

            if (transError != null) {
                fmt.format("#### ERROR: %s", transError);
                log.debug(sb.toString());
                sb.setLength(0);
            }

            log.debug("-------------------------------------------------------------------------------");

            switch (transType) {
                case TT_NEW_ORDER:
                    traceNewOrder(screenFmt);
                    break;

                case TT_PAYMENT:
                    tracePayment(screenFmt);
                    break;

                case TT_ORDER_STATUS:
                    traceOrderStatus(screenFmt);
                    break;

                case TT_STOCK_LEVEL:
                    traceStockLevel(screenFmt);
                    break;

                case TT_DELIVERY:
                    traceDelivery(screenFmt);
                    break;

                case TT_DELIVERY_BG:
                    traceDeliveryBG(screenFmt);
                    break;

                default:
                    throw new Exception("Unknown transType " + transType);
            }

            for (int i = 0; i < 23; i++) {
                if (screenSb[i].length() > 79)
                    screenSb[i].setLength(79);
                log.debug(screenSb[i].toString());
            }

            log.debug("-------------------------------------------------------------------------------");
            log.debug("");
        }
    }

    public String resultLine(long sessionStart) {
        String line;

        resultFmt.format("%d,%d,%d,%s,%d,%d,%d\n",
                transEnd - sessionStart,
                transEnd - transDue,
                transEnd - transStart,
                transTypeNames[transType],
                (transRbk) ? 1 : 0,
                (transType == TT_DELIVERY_BG) ? getSkippedDeliveries() : 0,
                (transError == null) ? 0 : 1);
        line = resultSB.toString();
        resultSB.setLength(0);
        return line;
    }

    private void traceNewOrder(Formatter fmt[]) {
        fmt[0].format("                                    New Order");

        if (transEnd == 0) {
            // NEW_ORDER INPUT screen
            fmt[1].format("Warehouse: %6d  District: %2d                       Date:",
                    newOrder.w_id, newOrder.d_id);
            fmt[2].format("Customer:    %4d  Name:                    Credit:      %%Disc:",
                    newOrder.c_id);
            fmt[3].format("Order Number:            Number of Lines:           W_tax:         D_tax:");

            fmt[5].format("Supp_W   Item_Id  Item Name                  Qty  Stock  B/G  Price    Amount");

            for (int i = 0; i < 15; i++) {
                if (newOrder.ol_i_id[i] != 0)
                    fmt[6 + i].format("%6d   %6d                              %2d",
                            newOrder.ol_supply_w_id[i],
                            newOrder.ol_i_id[i], newOrder.ol_quantity[i]);
                else
                    fmt[6 + i].format("______   ______                              __");
            }

            fmt[21].format("Execution Status:                                             Total:  $");
        } else {
            // NEW_ORDER OUTPUT screen
            fmt[1].format("Warehouse: %6d  District: %2d                       Date: %19.19s",
                    newOrder.w_id, newOrder.d_id, newOrder.o_entry_d);
            fmt[2].format("Customer:    %4d  Name: %-16.16s   Credit: %2.2s   %%Disc: %5.2f",
                    newOrder.c_id, newOrder.c_last,
                    newOrder.c_credit, newOrder.c_discount * 100.0);
            fmt[3].format("Order Number:  %8d  Number of Lines: %2d        W_tax: %5.2f   D_tax: %5.2f",
                    newOrder.o_id, newOrder.o_ol_cnt,
                    newOrder.w_tax * 100.0, newOrder.d_tax * 100.0);

            fmt[5].format("Supp_W   Item_Id  Item Name                  Qty  Stock  B/G  Price    Amount");

            for (int i = 0; i < 15; i++) {
                if (newOrder.ol_i_id[i] != 0)
                    fmt[6 + i].format("%6d   %6d   %-24.24s   %2d    %3d    %1.1s   $%6.2f  $%7.2f",
                            newOrder.ol_supply_w_id[i],
                            newOrder.ol_i_id[i], newOrder.i_name[i],
                            newOrder.ol_quantity[i],
                            newOrder.s_quantity[i],
                            newOrder.brand_generic[i],
                            newOrder.i_price[i],
                            newOrder.ol_amount[i]);
            }

            fmt[21].format("Execution Status: %-24.24s                    Total:  $%8.2f",
                    newOrder.execution_status, newOrder.total_amount);
        }
    }

    private void tracePayment(Formatter fmt[]) {
        fmt[0].format("                                     Payment");

        if (transEnd == 0) {
            // PAYMENT INPUT screen
            fmt[1].format("Date: ");
            fmt[3].format("Warehouse: %6d                         District: %2d",
                    payment.w_id, payment.d_id);

            if (payment.c_last == null) {
                fmt[8].format("Customer: %4d  Cust-Warehouse: %6d  Cust-District: %2d",
                        payment.c_id, payment.c_w_id, payment.c_d_id);
                fmt[9].format("Name:                       ________________       Since:");
            } else {
                fmt[8].format("Customer: ____  Cust-Warehouse: %6d  Cust-District: %2d",
                        payment.c_w_id, payment.c_d_id);
                fmt[9].format("Name:                       %-16.16s       Since:",
                        payment.c_last);
            }
            fmt[10].format("                                                   Credit:");
            fmt[11].format("                                                   %%Disc:");
            fmt[12].format("                                                   Phone:");

            fmt[14].format("Amount Paid:          $%7.2f        New Cust-Balance:",
                    payment.h_amount);
            fmt[15].format("Credit Limit:");
            fmt[17].format("Cust-Data:");
        } else {
            // PAYMENT OUTPUT screen
            fmt[1].format("Date: %-19.19s", payment.h_date);
            fmt[3].format("Warehouse: %6d                         District: %2d",
                    payment.w_id, payment.d_id);
            fmt[4].format("%-20.20s                      %-20.20s",
                    payment.w_street_1, payment.d_street_1);
            fmt[5].format("%-20.20s                      %-20.20s",
                    payment.w_street_2, payment.d_street_2);
            fmt[6].format("%-20.20s %2.2s %5.5s-%4.4s        %-20.20s %2.2s %5.5s-%4.4s",
                    payment.w_city, payment.w_state,
                    String.valueOf(payment.w_zip), String.valueOf(payment.w_zip),
                    payment.d_city, payment.d_state,
                    String.valueOf(payment.d_zip), String.valueOf(payment.d_zip));
            log.debug("w_zip=" + payment.w_zip + " d_zip=" + payment.d_zip);

            fmt[8].format("Customer: %4d  Cust-Warehouse: %6d  Cust-District: %2d",
                    payment.c_id, payment.c_w_id, payment.c_d_id);
            fmt[9].format("Name:   %-16.16s %2.2s %-16.16s       Since:  %-10.10s",
                    payment.c_first, payment.c_middle, payment.c_last,
                    payment.c_since);
            fmt[10].format("        %-20.20s                       Credit: %2s",
                    payment.c_street_1, payment.c_credit);
            fmt[11].format("        %-20.20s                       %%Disc:  %5.2f",
                    payment.c_street_2, payment.c_discount * 100.0);
            fmt[12].format("        %-20.20s %2.2s %5.5s-%4.4s         Phone:  %6.6s-%3.3s-%3.3s-%4.4s",
                    payment.c_city, payment.c_state,
                    String.valueOf(payment.c_zip), String.valueOf(payment.c_zip),
                    payment.c_phone, payment.c_phone,
                    payment.c_phone, payment.c_phone);

            fmt[14].format("Amount Paid:          $%7.2f        New Cust-Balance: $%14.2f",
                    payment.h_amount, payment.c_balance);
            fmt[15].format("Credit Limit:   $%13.2f", payment.c_credit_lim);
            if (payment.c_data.length() >= 200) {
                fmt[17].format("Cust-Data: %-50.50s", payment.c_data.substring(0, 50));
                fmt[18].format("           %-50.50s", payment.c_data.substring(50, 100));
                fmt[19].format("           %-50.50s", payment.c_data.substring(100, 150));
                fmt[20].format("           %-50.50s", payment.c_data.substring(150, 200));
            } else {
                fmt[17].format("Cust-Data:");
            }
        }
    }

    private void traceOrderStatus(Formatter fmt[]) {
        fmt[0].format("                                  Order Status");

        if (transEnd == 0) {
            // ORDER_STATUS INPUT screen
            fmt[1].format("Warehouse: %6d   District: %2d",
                    orderStatus.w_id, orderStatus.d_id);
            if (orderStatus.c_last == null)
                fmt[2].format("Customer: %4d   Name:                     ________________",
                        orderStatus.c_id);
            else
                fmt[2].format("Customer: ____   Name:                     %-16.16s",
                        orderStatus.c_last);
            fmt[3].format("Cust-Balance:");

            fmt[5].format("Order-Number:            Entry-Date:                       Carrier-Number:");
            fmt[6].format("Suppy-W      Item-Id     Qty    Amount        Delivery-Date");
        } else {
            // ORDER_STATUS OUTPUT screen
            fmt[1].format("Warehouse: %6d   District: %2d",
                    orderStatus.w_id, orderStatus.d_id);
            fmt[2].format("Customer: %4d   Name: %-16.16s %2.2s %-16.16s",
                    orderStatus.c_id, orderStatus.c_first,
                    orderStatus.c_middle, orderStatus.c_last);
            fmt[3].format("Cust-Balance: $%13.2f", orderStatus.c_balance);

            if (orderStatus.o_carrier_id >= 0)
                fmt[5].format("Order-Number: %8d   Entry-Date: %-19.19s   Carrier-Number: %2d",
                        orderStatus.o_id, orderStatus.o_entry_d, orderStatus.o_carrier_id);
            else
                fmt[5].format("Order-Number: %8d   Entry-Date: %-19.19s   Carrier-Number:",
                        orderStatus.o_id, orderStatus.o_entry_d);
            fmt[6].format("Suppy-W      Item-Id     Qty    Amount        Delivery-Date");
            for (int i = 0; i < 15 && orderStatus.ol_i_id[i] > 0; i++) {
                fmt[7 + i].format(" %6d      %6d     %3d     $%8.2f     %-10.10s",
                        orderStatus.ol_supply_w_id[i],
                        orderStatus.ol_i_id[i],
                        orderStatus.ol_quantity[i],
                        orderStatus.ol_amount[i],
                        (orderStatus.ol_delivery_d[i] == null) ? "" :
                                orderStatus.ol_delivery_d[i]);
            }
        }
    }

    private void traceStockLevel(Formatter fmt[]) {
        fmt[0].format("                                  Stock-Level");

        fmt[1].format("Warehouse: %6d   District: %2d",
                stockLevel.w_id, stockLevel.d_id);
        fmt[3].format("Stock Level Threshold: %2d",
                stockLevel.threshold);

        if (transEnd == 0)
            fmt[5].format("Low Stock:");
        else
            fmt[5].format("Low Stock: %3d",
                    stockLevel.low_stock);
    }

    private void traceDelivery(Formatter fmt[]) {
        fmt[0].format("                                     Delivery");
        fmt[1].format("Warehouse: %6d", delivery.w_id);
        fmt[3].format("Carrier Number: %2d", delivery.o_carrier_id);
        if (transEnd == 0) {
            fmt[5].format("Execution Status: ");
        } else {
            fmt[5].format("Execution Status: %s", delivery.execution_status);
        }
    }

    private void traceDeliveryBG(Formatter fmt[]) {
        fmt[0].format("                                    DeliveryBG");
        fmt[1].format("Warehouse: %6d", deliveryBG.w_id);
        fmt[2].format("Carrier Number: %2d", deliveryBG.o_carrier_id);
        fmt[3].format("Delivery Date: %-19.19s", deliveryBG.ol_delivery_d);

        if (transEnd != 0) {
            for (int d_id = 1; d_id <= 10; d_id++) {
                fmt[4 + d_id].format("District %02d: delivered O_ID: %8d",
                        d_id, deliveryBG.delivered_o_id[d_id - 1]);
            }
        }
    }

    public jTPCCTDataSync getDeliveryBG()
            throws Exception {
        if (transType != TT_DELIVERY)
            throw new Exception("Not a DELIVERY");
        if (delivery.deliveryBG == null)
            throw new Exception("DELIVERY foreground not executed yet " +
                    "or background part already consumed");

        jTPCCTDataSync result = delivery.deliveryBG;
        delivery.deliveryBG = null;
        return result;
    }

    private class NewOrderData {
        /* terminal input data */
        public int w_id;
        public int d_id;
        public int c_id;

        public int ol_supply_w_id[] = new int[15];
        public int ol_i_id[] = new int[15];
        public int ol_quantity[] = new int[15];

        /* terminal output data */
        public String c_last;
        public String c_credit;
        public double c_discount;
        public double w_tax;
        public double d_tax;
        public int o_ol_cnt;
        public int o_id;
        public String o_entry_d;
        public double total_amount;
        public String execution_status;

        public String i_name[] = new String[15];
        public int s_quantity[] = new int[15];
        public String brand_generic[] = new String[15];
        public double i_price[] = new double[15];
        public double ol_amount[] = new double[15];
    }

    private class PaymentData {
        /* terminal input data */
        public int w_id;
        public int d_id;
        public int c_id;
        public int c_d_id;
        public int c_w_id;
        public String c_last;
        public double h_amount;

        /* terminal output data */
        public String w_name;
        public String w_street_1;
        public String w_street_2;
        public String w_city;
        public String w_state;
        public int w_zip;
        public String d_name;
        public String d_street_1;
        public String d_street_2;
        public String d_city;
        public String d_state;
        public int d_zip;
        public String c_first;
        public String c_middle;
        public String c_street_1;
        public String c_street_2;
        public String c_city;
        public String c_state;
        public int c_zip;
        public String c_phone;
        public long c_since;
        public String c_credit;
        public double c_credit_lim;
        public double c_discount;
        public double c_balance;
        public String c_data;
        public String h_date;
    }

    private class OrderStatusData {
        /* terminal input data */
        public int w_id;
        public int d_id;
        public int c_id;
        public String c_last;

        /* terminal output data */
        public String c_first;
        public String c_middle;
        public double c_balance;
        public int o_id;
        public String o_entry_d;
        public int o_carrier_id;

        public int ol_supply_w_id[] = new int[15];
        public int ol_i_id[] = new int[15];
        public int ol_quantity[] = new int[15];
        public double ol_amount[] = new double[15];
        public String ol_delivery_d[] = new String[15];
    }

    private class StockLevelData {
        /* terminal input data */
        public int w_id;
        public int d_id;
        public int threshold;

        /* terminal output data */
        public int low_stock;
    }

    private class DeliveryData {
        /* terminal input data */
        public int w_id;
        public int o_carrier_id;

        /* terminal output data */
        public String execution_status;

        /*
         * executeDelivery() will store the background request
         * here for the caller to pick up and process as needed.
         */
        public jTPCCTDataSync deliveryBG;
    }

    private class DeliveryBGData {
        /* DELIVERY_BG data */
        public int w_id;
        public int o_carrier_id;
        public String ol_delivery_d;

        public int delivered_o_id[];
    }

    private class Timeline {
        private List<Milestone> milestones = new ArrayList<>();
        private String transaction;

        public void setTransaction(String transaction) {
            this.transaction = transaction;
        }


        private void addMileStone(String event, long timestamp) {
            this.milestones.add(new Milestone(event, timestamp));
        }

        @Override
        public String toString() {
            StringBuffer ret = new StringBuffer(this.transaction + " :: " + (milestones.get(milestones.size() - 1).timestamp - milestones.get(0).timestamp) + "\n");
            for (int i = 1; i < milestones.size(); i++) {
                ret.append(milestones.get(i).event + " : " + (milestones.get(i).timestamp - milestones.get(i - 1).timestamp) + "\n");
            }
            return ret.toString();
        }

        private class Milestone {
            public long timestamp;
            public String event;

            public Milestone(String event, long timestamp) {
                this.event = event;
                this.timestamp = timestamp;
            }
        }
    }
}
