/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.dsqldb;

import ch.usi.dslab.lel.aerie.*;
import com.beust.jcommander.JCommander;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.client.PromiseClient;
import ch.usi.dslab.lel.dsqldb.tables.Base;
import ch.usi.dslab.lel.dsqldb.tables.Customer;
import ch.usi.dslab.lel.dsqldb.tables.District;
import ch.usi.dslab.lel.dsqldb.tables.History;
import ch.usi.dslab.lel.dsqldb.tables.Item;
import ch.usi.dslab.lel.dsqldb.tables.NewOrder;
import ch.usi.dslab.lel.dsqldb.tables.Order;
import ch.usi.dslab.lel.dsqldb.tables.OrderLine;
import ch.usi.dslab.lel.dsqldb.tables.Stock;
import ch.usi.dslab.lel.dsqldb.tables.Warehouse;
import ch.usi.dslab.lel.dsqldb.tpcc.jTPCC;
import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCConfig;
import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCDataGenerator;
import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCUtil;
import ch.usi.dslab.lel.dsqldb.types.Timestamp;

/**
 * Created by longle on 17/02/16.
 */

public class DSqlClientTPCC {
    private static final Logger log = LogManager.getLogger(DSqlClient.class.toString());
    private static java.util.Date now = null;
    private static java.util.Date startDate = null;
    private static java.util.Date endDate = null;
    private static Random gen;
    private static long lastTimeMS = 0;
    public PromiseClient dbAsyncClient;
    private Semaphore sendPermits;
    private ExecutorService executorService;
    private jTPCC tpcc;

    // fully start client
    public DSqlClientTPCC(int clientId, String systemConfigFile, String partitionsConfigFile, int numPermits) {
        dbAsyncClient = new PromiseClient(clientId, systemConfigFile, partitionsConfigFile, new DSqlApplicationProcedure(), new CacheMultimap());
        sendPermits = new Semaphore(numPermits);
        executorService = Executors.newFixedThreadPool(numPermits);
    }


    public static void main(String[] args) {
        if (args.length >= 5) {
            int clientId = Integer.parseInt(args[0]);
            String systemConfigFile = args[1];
            String partitionsConfigFile = args[2];
            int numPermits = Integer.parseInt(args[3]);
            String dataFile = String.valueOf(args[4]);
            String terminalNum = String.valueOf(args[5]);
            String transactionNum = String.valueOf(args[6]);
            DSqlClientTPCC appcli = new DSqlClientTPCC(clientId, systemConfigFile, partitionsConfigFile, numPermits);
            System.out.println("Client " + clientId + " is starting");
            if (args.length >= 8) {
                appcli.streamDataToCache(dataFile, terminalNum, transactionNum);
                String gathererHost = args[7];
                int gathererPort = Integer.parseInt(args[8]);
                String fileDirectory = args[9];
                int gathererDuration = Integer.parseInt(args[10]);
                int warmup = Integer.parseInt(args[11]);
//                appcli.dbAsyncClient.initMonitoring(gathererHost, gathererPort, fileDirectory, gathererDuration, warmup);
                appcli.runTPCCTrans();
            } else {
//                appcli.generateData();
                appcli.streamDataToCache(dataFile, terminalNum, transactionNum);
                appcli.runInteractive();
            }
        } else {
            System.out.println("USAGE1: AppClient | clientId | systemConfigFile | partitionConfigFile | numPermit | dataFile | terminalNum | transNum ");
            System.out.println("USAGE2: AppClient | clientId | systemConfigFile | partitionConfigFile | numPermit | dataFile | terminalNum | transNum | monitorHost | monitorPort | monitorFileDir | monitorDuration | monitorWarmup");
            System.exit(1);
        }
    }

    public void runInteractive() {
        System.out.println("input format: c(reate) / r(ead) / rb (readBatch) / u(pdate) / up (updateBatch)/ d(elete) oid [<key value>] (or end to finish)");
        Scanner scan = new Scanner(System.in);
        String input;
        input = scan.nextLine();
        DSqlCommandLineParser parser = new DSqlCommandLineParser();
        while (!input.equalsIgnoreCase("end")) {
            String[] params = input.split(" ");
            new JCommander(parser, params);

            String opStr = parser.getCommand();
            String[] ids = parser.getIds();

            ObjId oid1 = Base.genObjId(parser.getModel(), ids);
            DSqlCommand command = null;
            if (opStr.equalsIgnoreCase("r")) {
                command = new DSqlCommand(GenericCommand.READ, oid1);
            } else if (opStr.equalsIgnoreCase("rb")) {
                command = new DSqlCommand(GenericCommand.READ_BATCH, oid1);
            } else if (opStr.equalsIgnoreCase("c")) {
                command = new DSqlCommand(GenericCommand.CREATE, oid1, parser.getAttributes());
            } else if (opStr.equalsIgnoreCase("u")) {
                command = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE, oid1, parser.getAttributes());
            } else if (opStr.equalsIgnoreCase("ub")) {
                command = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE_BATCH, oid1, parser.getAttributes());
            } else if (opStr.equalsIgnoreCase("d")) {
                command = new DSqlCommand(GenericCommand.DELETE, oid1);
            } else if (opStr.equalsIgnoreCase("q")) {
                // testing query object with field bound
                ObjId objId = new ObjId("Customer:c_d_id=1:c_w_id=1#c_id#4#8");
                command = new DSqlCommand(GenericCommand.READ_BATCH, objId);
            } else if (opStr.equalsIgnoreCase("ssmr")) {
                // testing query object with field bound
                ObjId objId = new ObjId("Customer:c_d_id=1:c_w_id=1");
                Command ssmrCmd = new Command(DSqlCommand.DSqlCommandType.READ_SSMR, objId);
                dbAsyncClient.sendSSMRCommand(ssmrCmd).thenAccept(reply -> {
                    System.out.println("got reply " + reply);
                });
                input = scan.nextLine();
                continue;
            } else {
                System.out.println("Incorrect command syntax!!");
                input = scan.nextLine();
                continue;
            }

            assert command != null;
            command.execute(dbAsyncClient).thenAccept(reply -> {
                System.out.println("got reply " + reply);
            });
            input = scan.nextLine();
        }
        scan.close();
    }

    public void loadDataToCache(String dataFile, String terminalNum, String transactionNum) {
        System.out.println("[CLIENT] loading sample data...");
        long start = System.currentTimeMillis();
        Map<String, Object> config = jTPCCDataGenerator.loadCSVData(dataFile);
        System.out.println("[CLIENT] File read, takes " + (System.currentTimeMillis() - start));
        tpcc = new jTPCC(this.dbAsyncClient, terminalNum, transactionNum);
        jTPCCConfig.configDistPerWhse = (Integer.parseInt(String.valueOf(config.get("districtCount"))));
        jTPCCConfig.configWhseCount = (Integer.parseInt(String.valueOf(config.get("warehouseCount"))));
        jTPCCConfig.configCustPerDist = (Integer.parseInt(String.valueOf(config.get("customerCount"))));
        jTPCCConfig.configItemCount = (Integer.parseInt(String.valueOf(config.get("itemCount"))));
        for (Base obj : (Set<Base>) config.get("objs")) {
            ObjId objId = obj.getId();
            CacheHashMap.PRObjectLocation loc = new CacheHashMap.PRObjectLocation(objId, DSqlApplicationProcedure.mapIdToPartition(objId));
            this.dbAsyncClient.setCache(objId, loc);
        }
        System.out.println("[CLIENT] Data loaded, takes " + (System.currentTimeMillis() - start));
    }

    public void streamDataToCache(String dataFile, String terminalNum, String transactionNum) {
        System.out.println("[CLIENT] loading sample data...");
        long start = System.currentTimeMillis();
        jTPCCDataGenerator.loadCSVData(dataFile, line -> {
            String[] tmp = line.split(",");
            ObjId objId = null;
            if (tmp.length == 2) {
                if (tmp[0].equals("warehouseCount"))
                    jTPCCConfig.configWhseCount = (Integer.parseInt(tmp[1]));
                if (tmp[0].equals("districtCount"))
                    jTPCCConfig.configDistPerWhse = (Integer.parseInt(tmp[1]));
                if (tmp[0].equals("customerCount"))
                    jTPCCConfig.configCustPerDist = (Integer.parseInt(tmp[1]));
                if (tmp[0].equals("itemCount"))
                    jTPCCConfig.configItemCount = (Integer.parseInt(tmp[1]));
            } else {
                switch (tmp[0]) {
                    case "Header":
                        break;
                    default:
                        objId = new ObjId(tmp[1]);
                        break;
                }
            }
            if (objId != null) {
                CacheHashMap.PRObjectLocation loc = new CacheHashMap.PRObjectLocation(objId, DSqlApplicationProcedure.mapIdToPartition(objId));
                this.dbAsyncClient.setCache(objId, loc);
            }
        });
        System.out.println("[CLIENT] Data loaded, takes " + (System.currentTimeMillis() - start));
        tpcc = new jTPCC(this.dbAsyncClient, terminalNum, transactionNum);
    }

    public void runTPCCTrans() {
        tpcc.run();
    }

    void getPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void generateData() {
        // seed the random number generator
        gen = new Random(System.currentTimeMillis());

        //######################### MAINLINE ######################################
        startDate = new java.util.Date();
        System.out.println("");
        System.out.println("------------- LoadData StartTime = " + startDate + "-------------");

        long startTimeMS = new java.util.Date().getTime();
        lastTimeMS = startTimeMS;

        final AtomicInteger totalRows = new AtomicInteger();
        executorService.execute(() -> {
            loadWhse(tpcc.configWhseCount).thenAccept(whCount -> {
                System.out.println("created warehouse: " + whCount);
                totalRows.addAndGet(whCount);
            }).thenCompose(count -> loadItem(tpcc.configItemCount)).thenAccept(itemCount -> {
                System.out.println("created items: " + itemCount);
                totalRows.addAndGet(itemCount);
            }).thenCompose(count -> loadStock(tpcc.configWhseCount, tpcc.configItemCount)).thenAccept(stockCount -> {
                System.out.println("created stocks: " + stockCount);
                totalRows.addAndGet(stockCount);
            }).thenCompose(count -> loadDist(tpcc.configWhseCount, tpcc.configDistPerWhse)).thenAccept(districtCount -> {
                System.out.println("created districts: " + districtCount);
                totalRows.addAndGet(districtCount);
            }).thenCompose(count -> loadCust(tpcc.configWhseCount, tpcc.configDistPerWhse, tpcc.configCustPerDist)).thenAccept(customerCount -> {
                System.out.println("created customers: " + customerCount);
                totalRows.addAndGet(customerCount);
            }).thenCompose(count -> loadOrder(tpcc.configWhseCount, tpcc.configDistPerWhse, tpcc.configCustPerDist)).thenAccept(orderCount -> {
                System.out.println("created orders: " + orderCount);
                totalRows.addAndGet(orderCount);
            }).thenAccept(reply -> {
                System.out.println("Created total " + totalRows.get() + " records");
                long runTimeMS = (new java.util.Date().getTime()) + 1 - startTimeMS;
                endDate = new java.util.Date();
                System.out.println("");
                System.out.println("------------- LoadJDBC Statistics --------------------");
                System.out.println("     Start Time = " + startDate);
                System.out.println("       End Time = " + endDate);
                System.out.println("       Run Time = " + (int) runTimeMS / 1000 + " Seconds");
                System.out.println("    Rows Loaded = " + totalRows + " Rows");
                System.out.println("Rows Per Second = " + (totalRows.get() / ((runTimeMS / 1000) != 0 ? (runTimeMS / 1000) : 1)) + " Rows/Sec");
                System.out.println("------------------------------------------------------");
                runTPCCTrans();
            });
        });


    }

    private CompletableFuture<Integer> loadWhse(int warehouseCount) {
        now = new java.util.Date();
        System.out.println("Start Whse Load for " + warehouseCount + " Whses @ " + now + " ...");
        List<CompletableFuture<Message>> cmds = new ArrayList<>();
        for (int i = 1; i <= warehouseCount; i++) {
            ObjId oid1 = Base.genObjId(Base.MODEL.WAREHOUSE, new String[]{"w_id", String.valueOf(i)});
            Warehouse warehouse = new Warehouse(i);
            warehouse.w_ytd = 300000;

            // random within [0.0000 .. 0.2000]
            warehouse.w_tax = (float) ((jTPCCUtil.randomNumber(0, 2000, gen)) / 10000.0);

            warehouse.w_name = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(6, 10, gen));
            warehouse.w_street_1 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
            warehouse.w_street_2 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
            warehouse.w_city = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
            warehouse.w_state = jTPCCUtil.randomStr(3).toUpperCase();
            warehouse.w_zip = 123456;

            DSqlCommand command = new DSqlCommand(GenericCommand.CREATE, oid1, warehouse.toHashMap());
            cmds.add(command.execute(dbAsyncClient));
        }
        return CompletableFuture.allOf(cmds.toArray(new CompletableFuture[cmds.size()])).thenApply(reply -> cmds.size());
    }

    private CompletableFuture<Integer> loadItem(int itemCount) {
        int randPct;
        int len;
        int startORIGINAL;

//        AtomicInteger createCount = new AtomicInteger(0);
        now = new java.util.Date();
        System.out.println("Start Item Load for " + itemCount + " Items @ " + now + " ...");
        List<CompletableFuture<Message>> cmds = new ArrayList<>();
        for (int i = 1; i <= itemCount; i++) {
//            getPermit();
            ObjId oid1 = Base.genObjId(Base.MODEL.ITEM, new String[]{"i_id", String.valueOf(i)});
            Item item = new Item(i);
            item.i_name = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(14, 24, gen));
            item.i_price = (float) (jTPCCUtil.randomNumber(100, 10000, gen) / 100.0);

            // i_data
            randPct = jTPCCUtil.randomNumber(1, 100, gen);
            len = jTPCCUtil.randomNumber(26, 50, gen);
            if (randPct > 10) {
                // 90% of time i_data isa random string of length [26 .. 50]
                item.i_data = jTPCCUtil.randomStr(len);
            } else {
                // 10% of time i_data has "ORIGINAL" crammed somewhere in middle
                startORIGINAL = jTPCCUtil.randomNumber(2, (len - 8), gen);
                item.i_data = jTPCCUtil.randomStr(startORIGINAL - 1) + "ORIGINAL" + jTPCCUtil.randomStr(len - startORIGINAL - 9);
            }

            item.i_im_id = jTPCCUtil.randomNumber(1, 10000, gen);

            DSqlCommand command = new DSqlCommand(GenericCommand.CREATE, oid1, item.toHashMap());
            cmds.add(command.execute(dbAsyncClient));
        } // end for
        return CompletableFuture.allOf(cmds.toArray(new CompletableFuture[cmds.size()])).thenApply(reply -> cmds.size());
    }

    private CompletableFuture<Integer> loadStock(int warehouseCount, int itemCount) {

        final int t = (warehouseCount * itemCount);
        int randPct = 0;
        int len = 0;
        int startORIGINAL = 0;
        List<CompletableFuture<Message>> cmds = new ArrayList<>();
        now = new java.util.Date();
        System.out.println("Start Stock Load for " + t + " units @ " + now + " ...");
        for (int i = 1; i <= itemCount; i++) {
            for (int w = 1; w <= warehouseCount; w++) {
//                getPermit();
                //TODO: handle 2 primary keys
                ObjId oid1 = Base.genObjId(Base.MODEL.STOCK, new String[]{"s_w_id", String.valueOf(w), "s_i_id", String.valueOf(i)});
                Stock stock = new Stock(w, i);
                stock.s_i_id = i;
                stock.s_w_id = w;
                stock.s_quantity = jTPCCUtil.randomNumber(10, 100, gen);
                stock.s_ytd = 0;
                stock.s_order_cnt = 0;
                stock.s_remote_cnt = 0;

                // s_data
                randPct = jTPCCUtil.randomNumber(1, 100, gen);
                len = jTPCCUtil.randomNumber(26, 50, gen);
                if (randPct > 10) {
                    // 90% of time i_data isa random string of length [26 .. 50]
                    stock.s_data = jTPCCUtil.randomStr(len);
                } else {
                    // 10% of time i_data has "ORIGINAL" crammed somewhere in middle
                    startORIGINAL = jTPCCUtil.randomNumber(2, (len - 8), gen);
                    stock.s_data = jTPCCUtil.randomStr(startORIGINAL - 1) + "ORIGINAL" + jTPCCUtil.randomStr(len - startORIGINAL - 9);
                }

                stock.s_dist_01 = jTPCCUtil.randomStr(24);
                stock.s_dist_02 = jTPCCUtil.randomStr(24);
                stock.s_dist_03 = jTPCCUtil.randomStr(24);
                stock.s_dist_04 = jTPCCUtil.randomStr(24);
                stock.s_dist_05 = jTPCCUtil.randomStr(24);
                stock.s_dist_06 = jTPCCUtil.randomStr(24);
                stock.s_dist_07 = jTPCCUtil.randomStr(24);
                stock.s_dist_08 = jTPCCUtil.randomStr(24);
                stock.s_dist_09 = jTPCCUtil.randomStr(24);
                stock.s_dist_10 = jTPCCUtil.randomStr(24);


                DSqlCommand command = new DSqlCommand(GenericCommand.CREATE, oid1, stock.toHashMap());
                cmds.add(command.execute(dbAsyncClient));
            } // end for [w]
        } // end for [i]
        return CompletableFuture.allOf(cmds.toArray(new CompletableFuture[cmds.size()])).thenApply(reply -> cmds.size());
    } // end loadStock()


    private CompletableFuture<Integer> loadDist(int warehouseCount, int distWhseCount) {
        final int t = warehouseCount * distWhseCount;
        now = new java.util.Date();
        System.out.println("Start District Data for " + t + " Dists @ " + now + " ...");
        List<CompletableFuture<Message>> cmds = new ArrayList<>();
        for (int w = 1; w <= warehouseCount; w++) {
            for (int d = 1; d <= distWhseCount; d++) {
//                getPermit();
                //TODO: handle 2 primary keys
                ObjId oid1 = Base.genObjId(Base.MODEL.DISTRICT, new String[]{"d_id", String.valueOf(d), "d_w_id", String.valueOf(w)});
                District district = new District(d, w);
                district.d_id = d;
                district.d_w_id = w;
                district.d_ytd = 30000;

                // random within [0.0000 .. 0.2000]
                district.d_tax = (float) ((jTPCCUtil.randomNumber(0, 2000, gen)) / 10000.0);

                district.d_next_o_id = 3001;
                district.d_name = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(6, 10, gen));
                district.d_street_1 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                district.d_street_2 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                district.d_city = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                district.d_state = jTPCCUtil.randomStr(3).toUpperCase();
                district.d_zip = 12345678;

                DSqlCommand command = new DSqlCommand(GenericCommand.CREATE, oid1, district.toHashMap());
                cmds.add(command.execute(dbAsyncClient));
            } // end for [d]
        } // end for [w]
        return CompletableFuture.allOf(cmds.toArray(new CompletableFuture[cmds.size()])).thenApply(reply -> cmds.size());
    }

    private CompletableFuture<Integer> loadCust(int warehouseCount, int distWhseCount, int custDistCount) {

        final int t = (warehouseCount * distWhseCount * custDistCount);
        Timestamp sysdate = null;
        now = new java.util.Date();
        List<CompletableFuture<Message>> cmds = new ArrayList<>();
        System.out.println("Start Cust-Hist Load for " + t + " Cust-Hists @ " + now + " ...");

        for (int w = 1; w <= warehouseCount; w++) {
            for (int d = 1; d <= distWhseCount; d++) {
                for (int c = 1; c <= custDistCount; c++) {
                    sysdate = new Timestamp(System.currentTimeMillis());
                    String c_last = jTPCCUtil.getLastName(gen);
                    Customer customer = new Customer(c, d, w, c_last);
                    ObjId oidCust = Base.genObjId(Base.MODEL.CUSTOMER, new String[]{"c_id", String.valueOf(c), "c_d_id", String.valueOf(d), "c_w_id", String.valueOf(w), "c_last", String.valueOf(c_last)});

                    // discount is random between [0.0000 ... 0.5000]
                    customer.c_discount = (float) (jTPCCUtil.randomNumber(1, 5000, gen) / 10000.0);

                    if (jTPCCUtil.randomNumber(1, 100, gen) <= 90) {
                        customer.c_credit = "BC";   // 10% Bad Credit
                    } else {
                        customer.c_credit = "GC";   // 90% Good Credit
                    }

                    customer.c_last = c_last;
                    customer.c_first = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(8, 16, gen));
                    customer.c_credit_lim = 50000;

                    customer.c_balance = -10;
                    customer.c_ytd_payment = 10;
                    customer.c_payment_cnt = 1;
                    customer.c_delivery_cnt = 0;

                    customer.c_street_1 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                    customer.c_street_2 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                    customer.c_city = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                    customer.c_state = jTPCCUtil.randomStr(3).toUpperCase();
                    customer.c_zip = 123456789;

                    customer.c_phone = "(732)744-1700";

                    customer.c_since = sysdate.getTime();
                    customer.c_middle = "OE";
                    customer.c_data = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(300, 500, gen));

                    ObjId oidHist = Base.genObjId(Base.MODEL.HISTORY, new String[]{"h_c_id", String.valueOf(c), "h_c_d_id", String.valueOf(d), "h_c_w_id", String.valueOf(w)});
                    History history = new History(c, d, w);
                    history.h_d_id = d;
                    history.h_w_id = w;
                    history.h_date = sysdate.getTime();
                    history.h_amount = 10;
                    history.h_data = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 24, gen));

                    DSqlCommand commandCust = new DSqlCommand(GenericCommand.CREATE, oidCust, customer.toHashMap());
                    DSqlCommand commandHist = new DSqlCommand(GenericCommand.CREATE, oidHist, history.toHashMap());
                    cmds.add(commandCust.execute(dbAsyncClient));
                    cmds.add(commandHist.execute(dbAsyncClient));
                } // end for [c]
            } // end for [d]
        } // end for [w]
        return CompletableFuture.allOf(cmds.toArray(new CompletableFuture[cmds.size()])).thenApply(reply -> cmds.size());
    } // end loadCust()

    private CompletableFuture<Integer> loadOrder(int warehouseCount, int distWhseCount, int custDistCount) {

        AtomicInteger createCount = new AtomicInteger(0);
        AtomicInteger createNewOrderCount = new AtomicInteger(0);
        AtomicInteger totalCount = new AtomicInteger(0);
        final int t = (warehouseCount * distWhseCount * custDistCount);

        now = new java.util.Date();
        List<CompletableFuture<Message>> cmds = new ArrayList<>();

        System.out.println("whse=" + warehouseCount + ", dist=" + distWhseCount + ", cust=" + custDistCount);
        System.out.println("Start Order-Line-New Load for approx " + t + " rows @ " + now + " ...");
        List<Order> orderList = new ArrayList<>();
        for (int w = 1; w <= warehouseCount; w++) {
            for (int d = 1; d <= distWhseCount; d++) {
                for (int c = 1; c <= custDistCount; c++) {
//                    getPermit();
                    int o_c_id = jTPCCUtil.randomNumber(1, custDistCount, gen);
                    final Order oorder = new Order(c, d, w, o_c_id);
                    ObjId oidOrder = Base.genObjId(Base.MODEL.ORDER, new String[]{"o_id", String.valueOf(c), "o_d_id", String.valueOf(d), "o_w_id", String.valueOf(w), "o_c_id", String.valueOf(o_c_id)});
                    oorder.o_c_id = o_c_id;
                    oorder.o_carrier_id = jTPCCUtil.randomNumber(1, 10, gen);
                    oorder.o_ol_cnt = jTPCCUtil.randomNumber(5, 15, gen);
                    oorder.o_all_local = 1;
                    oorder.o_entry_d = System.currentTimeMillis();
                    orderList.add(oorder);

                    totalCount.getAndIncrement();
                    DSqlCommand commandOrder = new DSqlCommand(GenericCommand.CREATE, oidOrder, oorder.toHashMap());
                    cmds.add(commandOrder.execute(dbAsyncClient));
                    // 900 rows in the NEW-ORDER table corresponding to the last
                    // 900 rows in the ORDER table for that district (i.e., with
                    // NO_O_ID between 2,101 and 3,000) (70%)
                    if (oorder.o_id > custDistCount * 0.7) {
                        NewOrder new_order = new NewOrder(oorder.o_id, oorder.o_d_id, oorder.o_w_id);
                        ObjId oidNewOrder = Base.genObjId(Base.MODEL.NEWORDER, new String[]{"no_o_id", String.valueOf(oorder.o_id), "no_d_id", String.valueOf(oorder.o_d_id), "no_w_id", String.valueOf(oorder.o_w_id)});
                        DSqlCommand commandNewOrder = new DSqlCommand(GenericCommand.CREATE, oidNewOrder, new_order.toHashMap());
                        cmds.add(commandNewOrder.execute(dbAsyncClient));
                    }

                    if (createCount.incrementAndGet() == t) {
                        int a = 0;
                        for (Order tmp : orderList) {
                            a += tmp.o_ol_cnt;
                        }
                        final int totalOderLine = a;
                        for (Order order : orderList) {
                            for (int l = 1; l <= order.o_ol_cnt; l++) {
                                OrderLine order_line = new OrderLine(l, order.o_id, order.o_d_id, order.o_w_id);
                                ObjId oidOrderLine = Base.genObjId(Base.MODEL.ORDERLINE, new String[]{"ol_number", String.valueOf(l), "ol_o_id", String.valueOf(order.o_id), "ol_d_id", String.valueOf(order.o_d_id), "ol_w_id", String.valueOf(order.o_w_id)});
                                order_line.ol_i_id = jTPCCUtil.randomNumber(1, 100000, gen);
                                order_line.ol_delivery_d = order.o_entry_d;

                                if (order_line.ol_o_id < custDistCount * 0.7) {
                                    order_line.ol_amount = 0;
                                } else {
                                    // random within [0.01 .. 9,999.99]
                                    order_line.ol_amount = (float) (jTPCCUtil.randomNumber(1, 999999, gen) / 100.0);
                                }

                                order_line.ol_supply_w_id = jTPCCUtil.randomNumber(1, tpcc.configWhseCount, gen);
                                order_line.ol_quantity = 5;
                                order_line.ol_dist_info = jTPCCUtil.randomStr(24);
                                DSqlCommand commandOrderLine = new DSqlCommand(GenericCommand.CREATE, oidOrderLine, order_line.toHashMap());
                                cmds.add(commandOrderLine.execute(dbAsyncClient));
                            } // end for [l]
                        }
                    }
                } // end for [c]
            } // end for [d]
        } // end for [w]
        return CompletableFuture.allOf(cmds.toArray(new CompletableFuture[cmds.size()])).thenApply(reply -> cmds.size());
    }

    void addPermit() {
        if (sendPermits != null)
            sendPermits.release();
    }
}