/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.dsqldb;

import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.aerie.client.EvenCallBack;
import ch.usi.dslab.lel.dsqldb.jtpcc.jTPCCInterface;
import com.beust.jcommander.JCommander;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.CacheHashMap;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.client.PromiseClient;
import ch.usi.dslab.lel.dsqldb.jtpcc.jTPCC;
import ch.usi.dslab.lel.dsqldb.jtpcc.jTPCCConfig;
import ch.usi.dslab.lel.dsqldb.jtpcc.jTPCCConnection;
import ch.usi.dslab.lel.dsqldb.tables.Base;
import ch.usi.dslab.lel.dsqldb.tables.Customer;
import ch.usi.dslab.lel.dsqldb.tables.District;
import ch.usi.dslab.lel.dsqldb.tables.History;
import ch.usi.dslab.lel.dsqldb.tables.Item;
import ch.usi.dslab.lel.dsqldb.tables.NewOrder;
import ch.usi.dslab.lel.dsqldb.tables.Order;
import ch.usi.dslab.lel.dsqldb.tables.OrderLine;
import ch.usi.dslab.lel.dsqldb.tables.Stock;
import ch.usi.dslab.lel.dsqldb.tables.Warehouse;
import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCDataGenerator;
//import ch.usi.dslab.lel.dsqldb.tpcc.jTPCC;
//import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCConfig;
//import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCDataGenerator;
//import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCUtil;
//import ch.usi.dslab.lel.dsqldb.types.Timestamp;

/**
 * Created by longle on 17/02/16.
 */

public class DSqlClient {
    private static final Logger log = LogManager.getLogger(DSqlClient.class.toString());
    private static java.util.Date now = null;
    private static java.util.Date startDate = null;
    private static java.util.Date endDate = null;
    private static Random gen;
    private static long lastTimeMS = 0;
    //    private PromiseClient dbAsyncClient;
    private jTPCCConnection dbAsyncClient;
    private Semaphore sendPermits;
    private ExecutorService executorService;
    //    private jTPCC tpcc;
    private jTPCCInterface jtpcc;

    String gathererHost;
    int gathererPort;
    String fileDirectory;
    int gathererDuration;
    int warmupTime;

    // fully start client
    public DSqlClient(int clientId, String systemConfigFile, String partitionsConfigFile, int numPermits) {
        dbAsyncClient = new jTPCCConnection(clientId, systemConfigFile, partitionsConfigFile, new DSqlApplicationProcedure());
        sendPermits = new Semaphore(numPermits);
        executorService = Executors.newFixedThreadPool(numPermits);
    }


    public static void main(String[] args) {
        if (args.length >= 5) {
            int clientId = Integer.parseInt(args[0]);
            String systemConfigFile = args[1];
            String partitionsConfigFile = args[2];
            int numPermits = Integer.parseInt(args[3]);
            String dataFile = String.valueOf(args[4]);
            String terminalNum = String.valueOf(args[5]);
            String transactionNum = String.valueOf(args[6]);
            DSqlClient appcli = new DSqlClient(clientId, systemConfigFile, partitionsConfigFile, numPermits);
            System.out.println("Client " + clientId + " is starting");
            if (args.length >= 8) {
                String gathererHost = args[7];
                int gathererPort = Integer.parseInt(args[8]);
                String fileDirectory = args[9];
                int gathererDuration = Integer.parseInt(args[10]);
                int warmup = Integer.parseInt(args[11]);
                appcli.gathererHost = gathererHost;
                appcli.gathererPort = gathererPort;
                appcli.fileDirectory = fileDirectory;
                appcli.gathererDuration = gathererDuration;
                appcli.warmupTime = warmup;
                appcli.streamDataToCache(dataFile, terminalNum, transactionNum);
                appcli.runTPCCTrans();
            } else {
//                appcli.generateData();
                appcli.streamDataToCache(dataFile, terminalNum, transactionNum);
                appcli.runInteractive();
            }
        } else {
            System.out.println("USAGE1: AppClient | clientId | systemConfigFile | partitionConfigFile | numPermit | dataFile | terminalNum | transNum ");
            System.out.println("USAGE2: AppClient | clientId | systemConfigFile | partitionConfigFile | numPermit | dataFile | terminalNum | transNum | monitorHost | monitorPort | monitorFileDir | monitorDuration | monitorWarmup");
            System.exit(1);
        }
    }

    public void runInteractive() {
        System.out.println("input format: c(reate) / r(ead) / rb (readBatch) / u(pdate) / up (updateBatch)/ d(elete) oid [<key value>] (or end to finish)");
        Scanner scan = new Scanner(System.in);
        String input;
        input = scan.nextLine();
        DSqlCommandLineParser parser = new DSqlCommandLineParser();
        while (!input.equalsIgnoreCase("end")) {
            String[] params = input.split(" ");
            new JCommander(parser, params);

            String opStr = parser.getCommand();
            String[] ids = parser.getIds();

            ObjId oid1 = Base.genObjId(parser.getModel(), ids);
            DSqlCommand command = null;
            if (opStr.equalsIgnoreCase("r")) {
                command = new DSqlCommand(GenericCommand.READ, oid1);
            } else if (opStr.equalsIgnoreCase("rb")) {
                command = new DSqlCommand(GenericCommand.READ_BATCH, oid1);
            } else if (opStr.equalsIgnoreCase("c")) {
                command = new DSqlCommand(GenericCommand.CREATE, oid1, parser.getAttributes());
            } else if (opStr.equalsIgnoreCase("u")) {
                command = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE, oid1, parser.getAttributes());
            } else if (opStr.equalsIgnoreCase("ub")) {
                command = new DSqlCommand(DSqlCommand.DSqlCommandType.UPDATE_BATCH, oid1, parser.getAttributes());
            } else if (opStr.equalsIgnoreCase("d")) {
                command = new DSqlCommand(GenericCommand.DELETE, oid1);
            } else if (opStr.equalsIgnoreCase("q")) {
                // testing query object with field bound
                ObjId objId = new ObjId("Customer:c_d_id=1:c_w_id=1#c_id#4#8");
                command = new DSqlCommand(GenericCommand.READ_BATCH, objId);
            } else if (opStr.equalsIgnoreCase("ssmr")) {
                // testing query object with field bound
                ObjId objId = new ObjId("Customer:c_d_id=1:c_w_id=1");
                Command ssmrCmd = new Command(DSqlCommand.DSqlCommandType.READ_SSMR, objId);
                dbAsyncClient.sendSSMRCommand(ssmrCmd).thenAccept(reply -> {
                    System.out.println("got reply " + reply);
                });
                input = scan.nextLine();
                continue;
            } else {
                System.out.println("Incorrect command syntax!!");
                input = scan.nextLine();
                continue;
            }

            assert command != null;
            command.execute(dbAsyncClient).thenAccept(reply -> {
                System.out.println("got reply " + reply);
            });
            input = scan.nextLine();
        }
        scan.close();
    }


    public void streamDataToCache(String dataFile, String terminalNum, String transactionNum) {
        System.out.println("CLIENT " + this.dbAsyncClient.getId() + " loading sample data...");
        long start = System.currentTimeMillis();
        jTPCCDataGenerator.loadCSVData(dataFile, line -> {
            String[] tmp = line.split(",");
            ObjId objId = null;
            if (tmp.length == 2) {
                if (tmp[0].equals("warehouseCount"))
                    jTPCCConfig.configWhseCount = (Integer.parseInt(tmp[1]));
                if (tmp[0].equals("districtCount"))
                    jTPCCConfig.configDistPerWhse = (Integer.parseInt(tmp[1]));
                if (tmp[0].equals("customerCount"))
                    jTPCCConfig.configCustPerDist = (Integer.parseInt(tmp[1]));
                if (tmp[0].equals("itemCount"))
                    jTPCCConfig.configItemCount = (Integer.parseInt(tmp[1]));
            } else {
                switch (tmp[0]) {
                    case "Header":
                        break;
                    default:
                        objId = new ObjId(tmp[1]);
                        break;
                }
            }
            if (objId != null) {
                CacheHashMap.PRObjectLocation loc = new CacheHashMap.PRObjectLocation(objId, DSqlApplicationProcedure.mapIdToPartition(objId));
                this.dbAsyncClient.setCache(objId, loc);
            }
        });
        System.out.println("CLIENT-" + this.dbAsyncClient.getId() + " Data loaded, takes " + (System.currentTimeMillis() - start));


        jtpcc = new jTPCC(this.dbAsyncClient.getId(), this.dbAsyncClient, terminalNum, transactionNum);
        jtpcc.initMonitoring(gathererHost, gathererPort, fileDirectory, gathererDuration, warmupTime);

        moveMonitor = new ThroughputPassiveMonitor(this.dbAsyncClient.getId(), "client_move_rate", true);
        retryMonitor = new ThroughputPassiveMonitor(this.dbAsyncClient.getId(), "client_retry_rate", true);
        queryMonintor = new ThroughputPassiveMonitor(this.dbAsyncClient.getId(), "client_query_rate", true);
        localCommandMonitor = new ThroughputPassiveMonitor(this.dbAsyncClient.getId(), "client_local_command_rate", true);
        globalCommandMonitor = new ThroughputPassiveMonitor(this.dbAsyncClient.getId(), "client_global_command_rate", true);

        this.dbAsyncClient.registerMonitoringEvent(
                new MonitorCallBack(queryMonintor),
                new MonitorCallBack(moveMonitor),
                new MonitorCallBack(retryMonitor),
                new MonitorCallBack(localCommandMonitor),
                new MonitorCallBack(globalCommandMonitor));

    }

    public void runTPCCTrans() {
        try {
            jtpcc.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class MonitorCallBack implements EvenCallBack {
        ThroughputPassiveMonitor monitor;

        public MonitorCallBack(ThroughputPassiveMonitor monitor) {
            this.monitor = monitor;
        }

        @Override
        public void callback(Object data) {
            monitor.incrementCount((Integer) data);
        }
    }

    ThroughputPassiveMonitor moveMonitor;
    ThroughputPassiveMonitor queryMonintor;
    ThroughputPassiveMonitor retryMonitor;
    ThroughputPassiveMonitor localCommandMonitor;
    ThroughputPassiveMonitor globalCommandMonitor;

}