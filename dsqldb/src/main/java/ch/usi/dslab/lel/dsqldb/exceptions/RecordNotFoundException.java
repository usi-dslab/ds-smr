package ch.usi.dslab.lel.dsqldb.exceptions;

/**
 * Author: longle, created on 01/04/16.
 */
public class RecordNotFoundException extends Exception {
    public RecordNotFoundException(String message) {
        super(message);
    }
}
