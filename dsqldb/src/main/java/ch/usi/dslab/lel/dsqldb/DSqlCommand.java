package ch.usi.dslab.lel.dsqldb;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.CommandType;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.client.PromiseClient;

/**
 * Author: longle, created on 07/03/16.
 */
public class DSqlCommand {
    Map<String, Object> attributes;
    private Command command;
    private CommandType commandType;
    private ObjId objId;

    public DSqlCommand(CommandType type, ObjId objId, Object... params) {
        this.commandType = type;
        this.objId = objId;
        attributes = new HashMap<>();
        for (int i = 0; i < params.length; ++i) {
            attributes.put(String.valueOf(params[i]), params[++i]);
        }
    }

    public DSqlCommand(CommandType type, ObjId objId, Map<String, Object> attributes) {
        this.commandType = type;
        this.attributes = attributes;
        this.objId = objId;
    }

//    public boolean isRangeCommand() {
//        Base.MODEL modelObj = Base.getModelFromObjId(objId);
//        String model = "ch.usi.dslab.lel.dsqldb.tables." + modelObj.getName();
//        Boolean ret = true;
//        try {
//            Method method = Class.forName(model).getMethod("getPrimaryKeys");
//            String[] keys = (String[]) method.invoke(Class.forName(model).newInstance());
//            ArrayList<String> extractedKeys = new ArrayList<>(Arrays.asList(Base.extractAttributes(objId)));
//            for (String key : keys) {
//                if (!extractedKeys.contains(key)) ret = false;
//            }
//        } catch (IllegalAccessException | NoSuchMethodException | ClassNotFoundException | InvocationTargetException | InstantiationException e) {
//            e.printStackTrace();
//        }
//        return !ret;
//    }

    public Command getCommand() {
        if (commandType instanceof GenericCommand) {
            switch ((GenericCommand) commandType) {
                case CREATE:
                    DSqlCommandPayload payload = new DSqlCommandPayload(attributes);
                    command = new Command(commandType, objId, payload);
                    break;
                case READ_BATCH:
                    command = new Command(commandType, objId);
                    command.setRangeCommand(true);
                    break;
                case READ:
                case DELETE:
                    command = new Command(commandType, objId);
                    break;
            }
        } else {
            DSqlCommandPayload payload;
            switch ((DSqlCommandType) commandType) {
                case UPDATE:
                    payload = new DSqlCommandPayload(attributes);
                    command = new Command(commandType, objId, payload);
                    break;
                case UPDATE_BATCH:
                    payload = new DSqlCommandPayload(attributes);
                    command = new Command(commandType, objId, payload);
                    command.setRangeCommand(true);
                    break;
            }
        }
        return command;
    }

    public CompletableFuture<Message> execute(PromiseClient aerieAsyncClient) {
        Command command = getCommand();
        if (commandType instanceof GenericCommand) {
            switch ((GenericCommand) commandType) {
                case CREATE:
                    return aerieAsyncClient.create(command);
                case READ:
                    return aerieAsyncClient.read(command);
                case READ_BATCH:
                    return aerieAsyncClient.readBatch(command);
                case DELETE:
                    return aerieAsyncClient.delete(command);

            }
        } else {
            switch ((DSqlCommandType) commandType) {
                case UPDATE:
                    return aerieAsyncClient.update(command);
                case UPDATE_BATCH:
                    return aerieAsyncClient.updateBatch(command);
            }
        }
        return null;
    }

    public enum DSqlCommandType implements CommandType {
        UPDATE, UPDATE_BATCH, READ_SSMR
    }

}
