/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.dsqldb.utils;

import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.dsqldb.tables.Base;

/**
 * Created by longle on 18/02/16.
 */
public class Util {
    /*
        int to bit string, with fixed length
     */
    public static String intToBitsString(int number, int length) {
        StringBuilder result = new StringBuilder();

        for (int i = length - 1; i >= 0; i--) {
            int mask = 1 << i;
            result.append((number & mask) != 0 ? "1" : "0");
        }
//        result.replace(result.length() - 1, result.length(), "");

        return result.toString();
    }

}
