package ch.usi.dslab.lel.dsqldb;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

import ch.usi.dslab.lel.dsqldb.tables.Base;

/**
 * Author: longle, created on 14/03/16.
 */
public class DSqlCommandLineParser {

    @Parameter(names = {"--model", "-m"}, description = "Model to process")
    private Base.MODEL model;

    @Parameter(names = {"--id", "-id"}, description = "Id of object, or primary key", arity = 2)
    private List<String> ids;

    @Parameter(names = {"-c", "--command"}, description = "Command to run")
    private String command;

    @Parameter(names = {"-a", "--attributes"}, description = "Attributes [key value]", arity = 2)
    private List<String> attributes;

    public String[] getIds() {
        return ids.toArray(new String[ids.size()]);
    }

    public String getCommand() {
        return command;
    }

    public String[] getAttributes() {
        return attributes.toArray(new String[attributes.size()]);
    }

    public Base.MODEL getModel() {
        return model;
    }
}
