package ch.usi.dslab.lel.dsqldb.jtpcc;/*
 * jTPCCTerminal - Terminal emulator code for jTPCC (transactions)
 *
 * Copyright (C) 2003, Raul Barbosa
 * Copyright (C) 2004-2016, Denis Lussier
 * Copyright (C) 2016, Jan Wieck
 *
 */

import org.apache.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;

public class jTPCCTerminal extends jTPCCConfig implements Runnable {
    private static Logger log = Logger.getLogger(jTPCCTerminal.class);

    private String terminalName;
    private int terminalWarehouseID, terminalDistrictID;
    private boolean terminalWarehouseFixed;
    private int paymentWeight, orderStatusWeight, deliveryWeight, stockLevelWeight, limPerMin_Terminal;
    private jTPCC parent;
    private jTPCCRandom rnd;

    private int transactionCount = 1;
    private int numTransactions;
    private int numWarehouses;
    private int newOrderCounter;
    private long totalTnxs = 1;
    private StringBuffer query = null;
    private int result = 0;
    private boolean stopRunningSignal = false;

    long terminalStartTime = 0;
    long transactionEnd = 0;

    jTPCCConnection dbConn = null;


    int dbType = 0;

    public jTPCCTerminal
            (String terminalName, int terminalWarehouseID, int terminalDistrictID,
             int numTransactions, boolean terminalWarehouseFixed,
             int paymentWeight, int orderStatusWeight,
             int deliveryWeight, int stockLevelWeight, int numWarehouses, int limPerMin_Terminal, jTPCC parent) throws SQLException {
        this.terminalName = terminalName;

        this.terminalWarehouseID = terminalWarehouseID;
        this.terminalDistrictID = terminalDistrictID;
        this.terminalWarehouseFixed = terminalWarehouseFixed;
        this.parent = parent;
        this.rnd = parent.getRnd().newRandom();
        this.numTransactions = numTransactions;
        this.paymentWeight = paymentWeight;
        this.orderStatusWeight = orderStatusWeight;
        this.deliveryWeight = deliveryWeight;
        this.stockLevelWeight = stockLevelWeight;
        this.numWarehouses = numWarehouses;
        this.newOrderCounter = 0;
        this.limPerMin_Terminal = limPerMin_Terminal;

        this.dbConn = parent.dbConn;

        terminalMessage("");
        terminalMessage("Terminal \'" + terminalName + "\' has WarehouseID=" + terminalWarehouseID + " and DistrictID=" + terminalDistrictID + ".");
        terminalStartTime = System.currentTimeMillis();
    }

    public void run() {
        executeTransactions(numTransactions);
        try {
            printMessage("");
            printMessage("Closing statement and connection...");
        } catch (Exception e) {
            printMessage("");
            printMessage("An error occurred!");
            logException(e);
        }

        printMessage("");
        printMessage("Terminal \'" + terminalName + "\' finished after " + (transactionCount - 1) + " transaction(s).");

        parent.signalTerminalEnded(this, newOrderCounter);
    }

    public void stopRunningWhenPossible() {
        stopRunningSignal = true;
        printMessage("");
        printMessage("Terminal received stop signal!");
        printMessage("Finishing current transaction before exit...");
    }

    private void executeTransactions(int numTransactions) {
        boolean stopRunning = false;
        boolean isLocalTrans;
        if (numTransactions != -1)
            printMessage("Executing " + numTransactions + " transactions...");
        else
            printMessage("Executing for a limited time...");

        for (int i = 0; (i < numTransactions || numTransactions == -1) && !stopRunning; i++) {
            transactionCount++;

            long transactionType = rnd.nextLong(1, 100);
            int skippedDeliveries = 0, newOrder = 0;
            String transactionTypeName;

            long transactionStart = System.currentTimeMillis();
            log.info("Terminal " + terminalName + " start execute transaction of type " + transactionType);
        /*
         * TPC/C specifies that each terminal has a fixed
	     * "home" warehouse. However, since this implementation
	     * does not simulate "terminals", but rather simulates
	     * "application threads", that association is no longer
	     * valid. In the case of having less clients than
	     * warehouses (which should be the normal case), it
	     * leaves the warehouses without a client without any
	     * significant traffic, changing the overall database
	     * access pattern significantly.
	     */
            if (!terminalWarehouseFixed)
                terminalWarehouseID = rnd.nextInt(1, numWarehouses);

            if (transactionType <= paymentWeight) {
                jTPCCTDataSync term = new jTPCCTDataSync();
                term.setNumWarehouses(numWarehouses);
                term.setWarehouse(terminalWarehouseID);
                term.setDistrict(terminalDistrictID);
                try {
                    term.generatePayment(rnd, 0);
                    term.traceScreen();
                    term.execute(dbConn);
                    parent.resultAppend(term);
                    term.traceScreen();
                } catch (Exception e) {
                    log.fatal(e.getMessage());
                    e.printStackTrace();
                    System.exit(4);
                }
                transactionTypeName = "Payment";
                isLocalTrans = term.isLocalTransaction;
            } else if (transactionType <= paymentWeight + stockLevelWeight) {
                jTPCCTDataSync term = new jTPCCTDataSync();
                term.setNumWarehouses(numWarehouses);
                term.setWarehouse(terminalWarehouseID);
                term.setDistrict(terminalDistrictID);
                try {
                    term.generateStockLevel(rnd, 0);
                    term.traceScreen();
                    term.execute(dbConn);
                    parent.resultAppend(term);
                    term.traceScreen();
                } catch (Exception e) {
                    log.fatal(e.getMessage());
                    e.printStackTrace();
                    System.exit(4);
                }
                transactionTypeName = "Stock-Level";
                isLocalTrans = term.isLocalTransaction;
            } else if (transactionType <= paymentWeight + stockLevelWeight + orderStatusWeight) {
                jTPCCTDataSync term = new jTPCCTDataSync();
                term.setNumWarehouses(numWarehouses);
                term.setWarehouse(terminalWarehouseID);
                term.setDistrict(terminalDistrictID);
                try {
                    term.generateOrderStatus(rnd, 0);
                    term.traceScreen();
                    term.execute(dbConn);
                    parent.resultAppend(term);
                    term.traceScreen();
                } catch (Exception e) {
                    log.fatal(e.getMessage());
                    e.printStackTrace();
                    System.exit(4);
                }
                transactionTypeName = "Order-Status";
                isLocalTrans = term.isLocalTransaction;
            } else if (transactionType <= paymentWeight + stockLevelWeight + orderStatusWeight + deliveryWeight) {
                jTPCCTDataSync term = new jTPCCTDataSync();
                term.setNumWarehouses(numWarehouses);
                term.setWarehouse(terminalWarehouseID);
                term.setDistrict(terminalDistrictID);
                try {
                    term.generateDelivery(rnd, 0);
                    term.traceScreen();
                    term.execute(dbConn);
                    parent.resultAppend(term);
                    term.traceScreen();

		    /*
             * The old style driver does not have a delivery
		     * background queue, so we have to execute that
		     * part here as well.
		     */
                    jTPCCTDataSync bg = term.getDeliveryBG();
                    bg.traceScreen();
                    bg.execute(dbConn);
                    parent.resultAppend(bg);
                    bg.traceScreen();

                    skippedDeliveries = bg.getSkippedDeliveries();
                } catch (Exception e) {
                    log.fatal(e.getMessage());
                    e.printStackTrace();
                    System.exit(4);
                }
                transactionTypeName = "Delivery";
                isLocalTrans = term.isLocalTransaction;
            } else {
                jTPCCTDataSync term = new jTPCCTDataSync();
                term.setNumWarehouses(numWarehouses);
                term.setWarehouse(terminalWarehouseID);
                term.setDistrict(terminalDistrictID);
                try {
                    term.generateNewOrder(rnd, 0);
                    term.traceScreen();
                    term.execute(dbConn);
                    parent.resultAppend(term);
                    term.traceScreen();
                } catch (Exception e) {
                    log.fatal(e.getMessage());
                    e.printStackTrace();
                    System.exit(4);
                }
                transactionTypeName = "New-Order";
                newOrderCounter++;
                newOrder = 1;
                isLocalTrans = term.isLocalTransaction;
            }


            long transactionEnd = System.currentTimeMillis();
            log.info("Terminal " + terminalName + " executed transaction #" + transactionCount + " - " + transactionTypeName + " in " + (transactionEnd - transactionStart) + " ms");

            if (!transactionTypeName.equals("Delivery")) {
                parent.signalTerminalEndedTransaction(this.terminalName, transactionTypeName, isLocalTrans, transactionEnd - transactionStart, null, newOrder);
            } else {
                parent.signalTerminalEndedTransaction(this.terminalName, transactionTypeName, isLocalTrans, transactionEnd - transactionStart, (skippedDeliveries == 0 ? "None" : "" + skippedDeliveries + " delivery(ies) skipped."), newOrder);
            }

            if (limPerMin_Terminal > 0) {
                long elapse = transactionEnd - transactionStart;
                long timePerTx = 60000 / limPerMin_Terminal;

                if (elapse < timePerTx) {
                    try {
                        long sleepTime = timePerTx - elapse;
                        Thread.sleep((sleepTime));
                    } catch (Exception e) {
                    }
                }
            }
            if (stopRunningSignal) stopRunning = true;
        }
    }


    private void error(String type) {
        log.error(terminalName + ", TERMINAL=" + terminalName + "  TYPE=" + type + "  COUNT=" + transactionCount);
        System.out.println(terminalName + ", TERMINAL=" + terminalName + "  TYPE=" + type + "  COUNT=" + transactionCount);
    }


    private void logException(Exception e) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        printWriter.close();
        log.error(stringWriter.toString());
    }


    private void terminalMessage(String message) {
        log.debug(terminalName + ", " + message);
    }


    private void printMessage(String message) {
        log.debug(terminalName + ", " + message);
    }


    void transRollback() {
//        try {
//            conn.rollback();
//        } catch (SQLException se) {
//            log.error(se.getMessage());
//        }
    }


    void transCommit() {
//        try {
//            conn.commit();
//        } catch (SQLException se) {
//            log.error(se.getMessage());
//            transRollback();
//        }
    } // end transCommit()


}
