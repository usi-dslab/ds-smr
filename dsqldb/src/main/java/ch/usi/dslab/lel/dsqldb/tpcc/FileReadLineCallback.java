package ch.usi.dslab.lel.dsqldb.tpcc;

/**
 * Author: longle, created on 02/05/16.
 */
public interface FileReadLineCallback {
    void callback(String line);
}
