package ch.usi.dslab.lel.dsqldb.tpcc;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Stream;

import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.dsqldb.tables.Base;
import ch.usi.dslab.lel.dsqldb.tables.Customer;
import ch.usi.dslab.lel.dsqldb.tables.District;
import ch.usi.dslab.lel.dsqldb.tables.History;
import ch.usi.dslab.lel.dsqldb.tables.Item;
import ch.usi.dslab.lel.dsqldb.tables.NewOrder;
import ch.usi.dslab.lel.dsqldb.tables.Order;
import ch.usi.dslab.lel.dsqldb.tables.OrderLine;
import ch.usi.dslab.lel.dsqldb.tables.Stock;
import ch.usi.dslab.lel.dsqldb.tables.Warehouse;

/**
 * Author: longle, created on 08/04/16.
 */
public class jTPCCDataGenerator {
    int warehouseCount, districtCount, customerCount, itemCount;
    String filePath;
    Random gen = new Random(System.currentTimeMillis());

    public jTPCCDataGenerator(String filePath, int warehouseCount, int districtCount, int customerCount, int itemCount) {
        this.warehouseCount = warehouseCount;
        this.districtCount = districtCount;
        this.customerCount = customerCount;
        this.itemCount = itemCount;
        this.filePath = filePath;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: jTPCCDataGenerator <filePath>");
            System.exit(0);
        }
        String filePath = String.valueOf(args[0]);
//        jTPCCDataGenerator.loadData(filePath);
        System.out.println("Creating dataset for jTPCC. Enter required information");
        Scanner scan = new Scanner(System.in);
        String input;
        System.out.print("Number of warehouses:");
        input = scan.nextLine();
        int warehouseCount = Integer.parseInt(input);
        System.out.print("Number of district per warehouse:");
        input = scan.nextLine();
        int districtCount = Integer.parseInt(input);
        System.out.print("Number of customer per district:");
        input = scan.nextLine();
        int customerCount = Integer.parseInt(input);
        System.out.print("Number of item:");
        input = scan.nextLine();
        int itemCount = Integer.parseInt(input);
        jTPCCDataGenerator generator = new jTPCCDataGenerator(filePath, warehouseCount, districtCount, customerCount, itemCount);
//        generator.generateJSONData();
        generator.generateCSVData();
    }

    private void generateCSVData() {
        List<String> lines = new ArrayList<>();
        lines.add("warehouseCount," + warehouseCount);
        lines.add("districtCount," + districtCount);
        lines.add("customerCount," + customerCount);
        lines.add("itemCount," + itemCount);
        List<Warehouse> warehouses = genWarehouses();
        List<Item> items = genItems();
        List<Stock> stocks = genStocks();
        List<District> districts = genDistricts();
        Map<String, List> cust_hists = genCustomersAndOrderHistory();
        List<Customer> customers = cust_hists.get("customers");
        List<History> orderHistories = cust_hists.get("orderHistories");
        Map<String, List> orderList = genOrders();
        List<Order> orders = orderList.get("orders");
        List<OrderLine> orderLines = orderList.get("orderLines");
        List<NewOrder> newOrders = orderList.get("newOrders");

        lines.add(warehouses.get(0).toCSVHeader());
        lines.add(items.get(0).toCSVHeader());
        lines.add(stocks.get(0).toCSVHeader());
        lines.add(districts.get(0).toCSVHeader());
        lines.add(customers.get(0).toCSVHeader());
        lines.add(orderHistories.get(0).toCSVHeader());
        lines.add(orders.get(0).toCSVHeader());
        lines.add(orderLines.get(0).toCSVHeader());
        lines.add(newOrders.get(0).toCSVHeader());

        warehouses.stream().forEach(warehouse -> lines.add(warehouse.toCSVString()));
        items.stream().forEach(item -> lines.add(item.toCSVString()));
        stocks.stream().forEach(stock -> lines.add(stock.toCSVString()));
        districts.stream().forEach(district -> lines.add(district.toCSVString()));
        customers.stream().forEach(customer -> lines.add(customer.toCSVString()));
        orderHistories.stream().forEach(orderHistory -> lines.add(orderHistory.toCSVString()));
        orders.stream().forEach(order -> lines.add(order.toCSVString()));
        orderLines.stream().forEach(orderLine -> lines.add(orderLine.toCSVString()));
        newOrders.stream().forEach(newOrder -> lines.add(newOrder.toCSVString()));


        try {
            String fileName = "w_" + this.warehouseCount + "_d_" + this.districtCount + "_i_" + this.itemCount + "_c_" + this.customerCount + ".data";
            Path file = Paths.get(filePath + fileName);
            Files.write(file, lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateJSONData() {
        JSONObject config = new JSONObject();
        config.put("warehouseCount", warehouseCount);
        config.put("districtCount", districtCount);
        config.put("customerCount", customerCount);
        config.put("itemCount", itemCount);
        JSONArray warehouses = genJSONWarehouses();
        config.put("warehouses", warehouses);
        JSONArray items = genJSONItems();
        config.put("items", items);
        JSONArray stocks = genJSONStocks();
        config.put("stocks", stocks);
        JSONArray districts = genJSONDistricts();
        config.put("districts", districts);

        Map<String, JSONArray> cust_hists = genJSONCustomersAndOrderHistory();
        JSONArray customers = cust_hists.get("customers");
        config.put("customers", customers);
        JSONArray orderHistories = cust_hists.get("orderHistories");
        config.put("orderHistories", orderHistories);

        Map<String, JSONArray> orderList = genJSONOrders();
        JSONArray orders = orderList.get("orders");
        config.put("orders", orders);
        JSONArray orderLines = orderList.get("orderLines");
        config.put("orderLines", orderLines);
        JSONArray newOrders = orderList.get("newOrders");
        config.put("newOrders", newOrders);

        try {
            String fileName = "w_" + this.warehouseCount + "_d_" + this.districtCount + "_i_" + this.itemCount + "_c_" + this.customerCount + ".data";
            Path file = Paths.get(filePath + fileName);
            ArrayList<String> str = new ArrayList<>();
            str.add(config.toJSONString());
            Files.write(file, str);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try {
//            String fileName = "w_" + this.warehouseCount + "_d_" + this.districtCount + "_i_" + this.itemCount + "_c_" + this.customerCount + ".json";
//            FileWriter file = new FileWriter(filePath + fileName);
//            file.write(config.toJSONString());
//            file.flush();
//            file.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private List<Warehouse> genWarehouses() {
        List<Warehouse> warehouses = new ArrayList<>();
        for (int w = 1; w <= warehouseCount; w++) {
            ObjId oid1 = Base.genObjId(Base.MODEL.WAREHOUSE, new String[]{"w_id", String.valueOf(w)});
            Warehouse warehouse = new Warehouse(w);
            warehouse.w_ytd = 300000;

            // random within [0.0000 .. 0.2000]
            warehouse.w_tax = (float) ((jTPCCUtil.randomNumber(0, 2000, gen)) / 10000.0);

            warehouse.w_name = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(6, 10, gen));
            warehouse.w_street_1 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
            warehouse.w_street_2 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
            warehouse.w_city = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
            warehouse.w_state = jTPCCUtil.randomStr(3).toUpperCase();
            warehouse.w_zip = 123456;
            warehouses.add(warehouse);
        }
        return warehouses;
    }

    private JSONArray genJSONWarehouses() {
        JSONArray warehouses = new JSONArray();
        genWarehouses().stream().forEach(warehouse -> warehouses.add(warehouse.toJSON()));
        return warehouses;
    }

    private List<Item> genItems() {
        int randPct;
        int len;
        int startORIGINAL;
        List<Item> items = new ArrayList<>();

        for (int i = 1; i <= itemCount; i++) {
            ObjId oid1 = Base.genObjId(Base.MODEL.ITEM, new String[]{"i_id", String.valueOf(i)});
            Item item = new Item(i);
            item.i_name = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(14, 24, gen));
            item.i_price = (float) (jTPCCUtil.randomNumber(100, 10000, gen) / 100.0);

            // i_data
            randPct = jTPCCUtil.randomNumber(1, 100, gen);
            len = jTPCCUtil.randomNumber(26, 50, gen);
            if (randPct > 10) {
                // 90% of time i_data isa random string of length [26 .. 50]
                item.i_data = jTPCCUtil.randomStr(len);
            } else {
                // 10% of time i_data has "ORIGINAL" crammed somewhere in middle
                startORIGINAL = jTPCCUtil.randomNumber(2, (len - 8), gen);
                item.i_data = jTPCCUtil.randomStr(startORIGINAL - 1) + "ORIGINAL" + jTPCCUtil.randomStr(len - startORIGINAL - 9);
            }

            item.i_im_id = jTPCCUtil.randomNumber(1, 10000, gen);
            items.add(item);
        } // end for
        return items;
    }

    private JSONArray genJSONItems() {
        JSONArray items = new JSONArray();
        genItems().stream().forEach(item -> items.add(item.toJSON()));
        return items;
    }

    private List<Stock> genStocks() {
        int randPct = 0;
        int len = 0;
        int startORIGINAL = 0;
        List<Stock> stocks = new JSONArray();
        for (int i = 1; i <= itemCount; i++) {
            for (int w = 1; w <= warehouseCount; w++) {
//                getPermit();
                //TODO: handle 2 primary keys
                ObjId oid1 = Base.genObjId(Base.MODEL.STOCK, new String[]{"s_w_id", String.valueOf(w), "s_i_id", String.valueOf(i)});
                Stock stock = new Stock(w, i);
                stock.s_i_id = i;
                stock.s_w_id = w;
                stock.s_quantity = jTPCCUtil.randomNumber(10, 100, gen);
                stock.s_ytd = 0;
                stock.s_order_cnt = 0;
                stock.s_remote_cnt = 0;

                // s_data
                randPct = jTPCCUtil.randomNumber(1, 100, gen);
                len = jTPCCUtil.randomNumber(26, 50, gen);
                if (randPct > 10) {
                    // 90% of time i_data isa random string of length [26 .. 50]
                    stock.s_data = jTPCCUtil.randomStr(len);
                } else {
                    // 10% of time i_data has "ORIGINAL" crammed somewhere in middle
                    startORIGINAL = jTPCCUtil.randomNumber(2, (len - 8), gen);
                    stock.s_data = jTPCCUtil.randomStr(startORIGINAL - 1) + "ORIGINAL" + jTPCCUtil.randomStr(len - startORIGINAL - 9);
                }

                stock.s_dist_01 = jTPCCUtil.randomStr(24);
                stock.s_dist_02 = jTPCCUtil.randomStr(24);
                stock.s_dist_03 = jTPCCUtil.randomStr(24);
                stock.s_dist_04 = jTPCCUtil.randomStr(24);
                stock.s_dist_05 = jTPCCUtil.randomStr(24);
                stock.s_dist_06 = jTPCCUtil.randomStr(24);
                stock.s_dist_07 = jTPCCUtil.randomStr(24);
                stock.s_dist_08 = jTPCCUtil.randomStr(24);
                stock.s_dist_09 = jTPCCUtil.randomStr(24);
                stock.s_dist_10 = jTPCCUtil.randomStr(24);

                stocks.add(stock);
            } // end for [w]
        } // end for [i]
        return stocks;
    }

    private JSONArray genJSONStocks() {
        JSONArray stocks = new JSONArray();
        genStocks().stream().forEach(stock -> stocks.add(stock.toJSON()));
        return stocks;
    }

    private List<District> genDistricts() {
        List<District> districts = new ArrayList<>();
        final int t = warehouseCount * districtCount;
        for (int w = 1; w <= warehouseCount; w++) {
            for (int d = 1; d <= districtCount; d++) {
                ObjId oid1 = Base.genObjId(Base.MODEL.DISTRICT, new String[]{"d_id", String.valueOf(d), "d_w_id", String.valueOf(w)});
                District district = new District(d, w);
                district.d_id = d;
                district.d_w_id = w;
                district.d_ytd = 30000;

                // random within [0.0000 .. 0.2000]
                district.d_tax = (float) ((jTPCCUtil.randomNumber(0, 2000, gen)) / 10000.0);

                district.d_next_o_id = customerCount+1;
                district.d_name = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(6, 10, gen));
                district.d_street_1 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                district.d_street_2 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                district.d_city = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                district.d_state = jTPCCUtil.randomStr(3).toUpperCase();
                district.d_zip = 12345678;

                districts.add(district);
            } // end for [d]
        } // end for [w]
        return districts;
    }

    private JSONArray genJSONDistricts() {
        JSONArray districts = new JSONArray();
        genDistricts().stream().forEach(district -> districts.add(district.toJSON()));
        return districts;
    }

    private Map<String, List> genCustomersAndOrderHistory() {
        Map<String, List> ret = new HashMap<>();
        List<Customer> customers = new ArrayList();
        List<History> orderHistories = new ArrayList();

        for (int w = 1; w <= warehouseCount; w++) {
            for (int d = 1; d <= districtCount; d++) {
                for (int c = 1; c <= customerCount; c++) {
                    long sysdate = System.currentTimeMillis();
                    String c_last = jTPCCUtil.getLastName(gen);
                    Customer customer = new Customer(c, d, w, c_last);
                    ObjId oidCust = Base.genObjId(Base.MODEL.CUSTOMER, new String[]{"c_id", String.valueOf(c), "c_d_id", String.valueOf(d), "c_w_id", String.valueOf(w), "c_last", String.valueOf(c_last)});

                    // discount is random between [0.0000 ... 0.5000]
                    customer.c_discount = (float) (jTPCCUtil.randomNumber(1, 5000, gen) / 10000.0);

                    if (jTPCCUtil.randomNumber(1, 100, gen) <= 90) {
                        customer.c_credit = "BC";   // 10% Bad Credit
                    } else {
                        customer.c_credit = "GC";   // 90% Good Credit
                    }

                    customer.c_last = c_last;
                    customer.c_first = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(8, 16, gen));
                    customer.c_credit_lim = 50000;

                    customer.c_balance = -10;
                    customer.c_ytd_payment = 10;
                    customer.c_payment_cnt = 1;
                    customer.c_delivery_cnt = 0;

                    customer.c_street_1 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                    customer.c_street_2 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                    customer.c_city = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
                    customer.c_state = jTPCCUtil.randomStr(3).toUpperCase();
                    customer.c_zip = 123456789;

                    customer.c_phone = "(732)744-1700";

                    customer.c_since = sysdate;
                    customer.c_middle = "OE";
                    customer.c_data = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(300, 500, gen));

                    ObjId oidHist = Base.genObjId(Base.MODEL.HISTORY, new String[]{"h_c_id", String.valueOf(c), "h_c_d_id", String.valueOf(d), "h_c_w_id", String.valueOf(w)});
                    History history = new History(c, d, w);
                    history.h_d_id = d;
                    history.h_w_id = w;
                    history.h_date = sysdate;
                    history.h_amount = 10;
                    history.h_data = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 24, gen));

                    customers.add(customer);
                    orderHistories.add(history);
                } // end for [c]
            } // end for [d]
        } // end for [w]
        ret.put("customers", customers);
        ret.put("orderHistories", orderHistories);
        return ret;
    }

    private Map<String, JSONArray> genJSONCustomersAndOrderHistory() {
        Map<String, List> objs = genCustomersAndOrderHistory();
        Map<String, JSONArray> ret = new HashMap<>();
        JSONArray cus = new JSONArray();
        JSONArray his = new JSONArray();
        ((List<Customer>) objs.get("customers")).stream().forEach(o -> {
            cus.add(o.toJSON());
        });
        ((List<History>) objs.get("orderHistories")).stream().forEach(o -> {
            his.add(o.toJSON());
        });
        ret.put("customers", cus);
        ret.put("orderHistories", his);
        return ret;
    }

    private Map<String, JSONArray> genJSONOrders() {
        Map<String, JSONArray> ret = new HashMap<>();
        Map<String, List> objs = genOrders();
        JSONArray orders = new JSONArray();
        JSONArray newOrders = new JSONArray();
        JSONArray orderLines = new JSONArray();
        ((List<Order>) objs.get("orders")).stream().forEach(o -> {
            orders.add(o.toJSON());
        });
        ((List<NewOrder>) objs.get("newOrders")).stream().forEach(o -> {
            newOrders.add(o.toJSON());
        });
        ((List<OrderLine>) objs.get("orderLines")).stream().forEach(o -> {
            orderLines.add(o.toJSON());
        });
        ret.put("orders", orders);
        ret.put("newOrders", newOrders);
        ret.put("orderLines", orderLines);
        return ret;
    }

    public Map<String, List> genOrders() {
        Map<String, List> ret = new HashMap<>();
        List<Order> orders = new ArrayList<>();
        List<NewOrder> newOrders = new ArrayList<>();
        List<OrderLine> orderLines = new ArrayList<>();
        int createCount = 0;
        final int t = (warehouseCount * districtCount * customerCount);
        List<Order> orderList = new ArrayList<>();
        for (int w = 1; w <= warehouseCount; w++) {
            for (int d = 1; d <= districtCount; d++) {
                for (int c = 1; c <= customerCount; c++) {
                    int o_c_id = jTPCCUtil.randomNumber(1, customerCount, gen);
                    final Order oorder = new Order(c, d, w, o_c_id);
                    oorder.o_c_id = o_c_id;
                    oorder.o_carrier_id = jTPCCUtil.randomNumber(1, 10, gen);
                    oorder.o_ol_cnt = jTPCCUtil.randomNumber(5, 15, gen);
                    oorder.o_all_local = 1;
                    oorder.o_entry_d = System.currentTimeMillis();
                    orders.add(oorder);
                    orderList.add(oorder);

                    // 900 rows in the NEW-ORDER table corresponding to the last
                    // 900 rows in the ORDER table for that district (i.e., with
                    // NO_O_ID between 2,101 and 3,000) (70%)
                    if (oorder.o_id > customerCount * 0.7) {
                        NewOrder new_order = new NewOrder(oorder.o_id, oorder.o_d_id, oorder.o_w_id);
                        newOrders.add(new_order);
                    }

                    if (++createCount == t) {
                        int a = 0;
                        for (Order tmp : orderList) {
                            a += tmp.o_ol_cnt;
                        }
                        final int totalOderLine = a;
                        for (Order order : orderList) {
                            for (int l = 1; l <= order.o_ol_cnt; l++) {
                                OrderLine order_line = new OrderLine(l, order.o_id, order.o_d_id, order.o_w_id);
                                order_line.ol_i_id = jTPCCUtil.randomNumber(1, itemCount, gen);
                                order_line.ol_delivery_d = order.o_entry_d;

                                if (order_line.ol_o_id < customerCount * 0.7) {
                                    order_line.ol_amount = 0;
                                } else {
                                    // random within [0.01 .. 9,999.99]
                                    order_line.ol_amount = (float) (jTPCCUtil.randomNumber(1, 999999, gen) / 100.0);
                                }

                                order_line.ol_supply_w_id = jTPCCUtil.randomNumber(1, warehouseCount, gen);
                                order_line.ol_quantity = 5;
                                order_line.ol_dist_info = jTPCCUtil.randomStr(24);
                                orderLines.add(order_line);
                            } // end for [l]
                        }
                    }
                } // end for [c]
            } // end for [d]
        } // end for [w]
        ret.put("orders", orders);
        ret.put("newOrders", newOrders);
        ret.put("orderLines", orderLines);
        return ret;
    }

    public static void loadCSVData(String filePath, FileReadLineCallback callback) {
        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(callback::callback);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Object> loadCSVData(String filePath) {
        Map<String, Object> ret = new HashMap<>();
        Set<Base> allObj = new HashSet<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            for (String line; (line = br.readLine()) != null; ) {
                String[] tmp = line.split(",");
                if (tmp.length == 2) {
                    if (tmp[0].equals("warehouseCount")) ret.put("warehouseCount", tmp[1]);
                    if (tmp[0].equals("districtCount")) ret.put("districtCount", tmp[1]);
                    if (tmp[0].equals("customerCount")) ret.put("customerCount", tmp[1]);
                    if (tmp[0].equals("itemCount")) ret.put("itemCount", tmp[1]);
                } else {
                    switch (tmp[0]) {
                        case "Header":
                            ret.put(tmp[1], Arrays.copyOfRange(tmp, 1, tmp.length));
                            break;
                        case "Warehouse":
                            Warehouse w = new Warehouse();
                            w = w.fromCSV((String[]) ret.get("Warehouse"), tmp);
                            allObj.add(w);
                            break;
                        case "Item":
                            Item i = new Item();
                            i = i.fromCSV((String[]) ret.get("Item"), tmp);
                            allObj.add(i);
                            break;
                        case "Customer":
                            Customer c = new Customer();
                            c = c.fromCSV((String[]) ret.get("Customer"), tmp);
                            allObj.add(c);
                            break;
                        case "Stock":
                            Stock s = new Stock();
                            s = s.fromCSV((String[]) ret.get("Stock"), tmp);
                            allObj.add(s);
                            break;
                        case "District":
                            District d = new District();
                            d = d.fromCSV((String[]) ret.get("District"), tmp);
                            allObj.add(d);
                            break;
                        case "History":
                            History h = new History();
                            h = h.fromCSV((String[]) ret.get("History"), tmp);
                            allObj.add(h);
                            break;
                        case "Order":
                            Order o = new Order();
                            o = o.fromCSV((String[]) ret.get("Order"), tmp);
                            allObj.add(o);
                            break;
                        case "NewOrder":
                            NewOrder no = new NewOrder();
                            no = no.fromCSV((String[]) ret.get("NewOrder"), tmp);
                            allObj.add(no);
                            break;
                        case "OrderLine":
                            OrderLine ol = new OrderLine();
                            ol = ol.fromCSV((String[]) ret.get("OrderLine"), tmp);
                            allObj.add(ol);
                            break;
                    }
                }
            }
            ret.put("objs", allObj);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }


    public static Map<String, Object> loadJSONData(String filePath) {
        Map<String, Object> ret = new HashMap<>();
        try {
            Set<Base> allObjs = new HashSet<>();
            JSONParser parser = new JSONParser();
            JSONObject config = (JSONObject) parser.parse(new FileReader(filePath));
            ret.put("warehouseCount", config.get("warehouseCount"));
            ret.put("districtCount", config.get("districtCount"));
            ret.put("customerCount", config.get("customerCount"));
            ret.put("itemCount", config.get("itemCount"));
            ret.put("objs", allObjs);

            JSONArray warehouses = (JSONArray) config.get("warehouses");
            for (Object obj : warehouses) {
                JSONObject json = (JSONObject) obj;
                Warehouse w = new Warehouse();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

            JSONArray items = (JSONArray) config.get("items");
            for (Object obj : items) {
                JSONObject json = (JSONObject) obj;
                Item w = new Item();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

            JSONArray customers = (JSONArray) config.get("customers");
            for (Object obj : customers) {
                JSONObject json = (JSONObject) obj;
                Customer w = new Customer();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

            JSONArray stocks = (JSONArray) config.get("stocks");
            for (Object obj : stocks) {
                JSONObject json = (JSONObject) obj;
                Stock w = new Stock();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

            JSONArray districts = (JSONArray) config.get("districts");
            for (Object obj : districts) {
                JSONObject json = (JSONObject) obj;
                District w = new District();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

            JSONArray orderHistories = (JSONArray) config.get("orderHistories");
            for (Object obj : orderHistories) {
                JSONObject json = (JSONObject) obj;
                History w = new History();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

            JSONArray orders = (JSONArray) config.get("orders");
            for (Object obj : orders) {
                JSONObject json = (JSONObject) obj;
                Order w = new Order();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

            JSONArray orderLines = (JSONArray) config.get("orderLines");
            for (Object obj : orderLines) {
                JSONObject json = (JSONObject) obj;
                OrderLine w = new OrderLine();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

            JSONArray newOrders = (JSONArray) config.get("newOrders");
            for (Object obj : newOrders) {
                JSONObject json = (JSONObject) obj;
                NewOrder w = new NewOrder();
                w = w.fromJSON(json);
                w.id = new ObjId(String.valueOf(json.get("objId")));
                allObjs.add(w);
            }

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
