package ch.usi.dslab.lel.dsqldb;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ch.usi.dslab.lel.dsqldb.tables.Base;

/**
 * Author: longle, created on 11/03/16.
 */
public class DSqlCommandPayload {
    public Map<String, Object> attributes = new ConcurrentHashMap<>();

    public DSqlCommandPayload() {

    }

    public DSqlCommandPayload(Map attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer("[");
        this.attributes.keySet().stream().forEach(key -> str.append(key + ":" + this.attributes.get(key) + ", "));
        str.append("]");
        return str.toString();
    }
}
