package ch.usi.dslab.lel.dsqldb.tpcc;/*
 * jTPCC - Open Source Java implementation of a TPC-C like benchmark
 *
 * Copyright (C) 2003, Raul Barbosa
 * Copyright (C) 2004-2016, Denis Lussier
 * Copyright (C) 2016, Jan Wieck
 *
 */

import ch.usi.dslab.lel.dsqldb.jtpcc.jTPCCInterface;
import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Formatter;
import java.util.Properties;
import java.util.Random;

import ch.usi.dslab.lel.aerie.client.PromiseClient;


public class jTPCC extends jTPCCConfig implements jTPCCInterface {
    private static Logger log = Logger.getLogger(jTPCC.class);
    public PromiseClient dbAsyncClient;
    private int currentlyDisplayedTerminal;
    private jTPCCTerminal[] terminals;
    private String[] terminalNames;
    private boolean terminalsBlockingExit = false;
    private Random random;
    private long terminalsStarted = 0, sessionCount = 0, transactionCount;
    private long newOrderCounter, sessionStartTimestamp, sessionEndTimestamp, sessionNextTimestamp = 0, sessionNextKounter = 0;
    private long sessionEndTargetTime = -1, fastNewOrderCounter, recentTpmC = 0, recentTpmTotal = 0;
    private boolean signalTerminalsRequestEndSent = false, databaseDriverLoaded = false;
    private FileOutputStream fileOutputStream;
    private PrintStream printStreamReport;
    private String sessionStart, sessionEnd;
    private int limPerMin_Terminal;
    private double tpmC;
    private String transactionNum;
    private String terminalNum;

    public jTPCC(PromiseClient dbAsyncClient, String terminalNum, String transactionNum) {
        this.dbAsyncClient = dbAsyncClient;
        this.terminalNum = terminalNum;
        this.transactionNum = transactionNum;
    }

    @Override
    public void initMonitoring(String gathererHost, int gathererPort, String fileDirectory, int gathererDuration, int warmupTime) {

    }

    public void run() {
        // load the ini file
//        Properties ini = new Properties();
//        try {
//            ini.load(new FileInputStream(System.getProperty("prop")));
//        } catch (IOException e) {
//            errorMessage("Term-00, could not load properties file");
//        }


        log.info("Term-00, ");
        log.info("Term-00, +-------------------------------------------------------------+");
        log.info("Term-00,      BenchmarkSQL v" + JTPCCVERSION);
        log.info("Term-00, +-------------------------------------------------------------+");
        log.info("Term-00, ");


        log.info("Term-00, ");
//        String iTerminals = getProp(ini, "terminals");
        String iTerminals = this.terminalNum; //10

//        String iRunTxnsPerTerminal = ini.getProperty("runTxnsPerTerminal");
        String iRunTxnsPerTerminal = this.transactionNum; // 100
//        String iRunMins = ini.getProperty("runMins");
        String iRunMins = "0";
        if (Integer.parseInt(iRunTxnsPerTerminal) == 0 && Integer.parseInt(iRunMins) != 0) {
            log.info("Term-00, runMins" + "=" + iRunMins);
        } else if (Integer.parseInt(iRunTxnsPerTerminal) != 0 && Integer.parseInt(iRunMins) == 0) {
            log.info("Term-00, runTxnsPerTerminal" + "=" + iRunTxnsPerTerminal);
        } else {
            errorMessage("Term-00, Must indicate either transactions per terminal or number of run minutes!");
        }
        ;
//        String limPerMin = getProp(ini, "limitTxnsPerMin");
        String limPerMin = "0";
        log.info("Term-00, ");
//        String iNewOrderWeight = getProp(ini, "newOrderWeight");
//        String iPaymentWeight = getProp(ini, "paymentWeight");
//        String iOrderStatusWeight = getProp(ini, "orderStatusWeight");
//        String iDeliveryWeight = getProp(ini, "deliveryWeight");
//        String iStockLevelWeight = getProp(ini, "stockLevelWeight");

        String iNewOrderWeight = "45"; //45
        String iPaymentWeight = "43"; //43
        String iOrderStatusWeight = "4"; //4 /err
        String iDeliveryWeight = "4"; //4
        String iStockLevelWeight = "4"; //4

        log.info("Term-00, ");

        if (Integer.parseInt(limPerMin) != 0) {
            limPerMin_Terminal = Integer.parseInt(limPerMin) / Integer.parseInt(iTerminals);
        } else {
            limPerMin_Terminal = -1;
        }


        boolean iRunMinsBool = false;


        this.random = new Random(System.currentTimeMillis());

        fastNewOrderCounter = 0;
        updateStatusLine();


        boolean limitIsTime = iRunMinsBool;
        int numTerminals = -1, transactionsPerTerminal = -1, numWarehouses = -1;
        int newOrderWeightValue = -1, paymentWeightValue = -1, orderStatusWeightValue = -1, deliveryWeightValue = -1, stockLevelWeightValue = -1;
        long executionTimeMillis = -1;
//
        try {
            if (Integer.parseInt(iRunMins) != 0 && Integer.parseInt(iRunTxnsPerTerminal) == 0) {
                iRunMinsBool = true;
            } else if (Integer.parseInt(iRunMins) == 0 && Integer.parseInt(iRunTxnsPerTerminal) != 0) {
                iRunMinsBool = false;
            } else {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e1) {
            errorMessage("Must indicate either transactions per terminal or number of run minutes!");
        }

        try {
            numWarehouses = jTPCCConfig.configWhseCount;
            if (numWarehouses <= 0)
                throw new NumberFormatException();
        } catch (NumberFormatException e1) {
            errorMessage("Invalid number of warehouses!");
        }

        try {
            numTerminals = Integer.parseInt(iTerminals);
            if (numTerminals <= 0 || numTerminals > jTPCCConfig.configDistPerWhse * numWarehouses)
                throw new NumberFormatException();
        } catch (NumberFormatException e1) {
            errorMessage("Invalid number of terminals!");
        }


        if (Long.parseLong(iRunMins) != 0 && Integer.parseInt(iRunTxnsPerTerminal) == 0) {
            try {
                executionTimeMillis = Long.parseLong(iRunMins) * 60000;
                if (executionTimeMillis <= 0)
                    throw new NumberFormatException();
            } catch (NumberFormatException e1) {
                errorMessage("Invalid number of minutes!");
            }
        } else {
            try {
                transactionsPerTerminal = Integer.parseInt(iRunTxnsPerTerminal);
                if (transactionsPerTerminal <= 0)
                    throw new NumberFormatException();
            } catch (NumberFormatException e1) {
                errorMessage("Invalid number of transactions per terminal!");
            }
        }

        try {
            newOrderWeightValue = Integer.parseInt(iNewOrderWeight);
            paymentWeightValue = Integer.parseInt(iPaymentWeight);
            orderStatusWeightValue = Integer.parseInt(iOrderStatusWeight);
            deliveryWeightValue = Integer.parseInt(iDeliveryWeight);
            stockLevelWeightValue = Integer.parseInt(iStockLevelWeight);

            if (newOrderWeightValue < 0 || paymentWeightValue < 0 || orderStatusWeightValue < 0 || deliveryWeightValue < 0 || stockLevelWeightValue < 0)
                throw new NumberFormatException();
            else if (newOrderWeightValue == 0 && paymentWeightValue == 0 && orderStatusWeightValue == 0 && deliveryWeightValue == 0 && stockLevelWeightValue == 0)
                throw new NumberFormatException();
        } catch (NumberFormatException e1) {
            errorMessage("Invalid number in mix percentage!");
        }

        if (newOrderWeightValue + paymentWeightValue + orderStatusWeightValue + deliveryWeightValue + stockLevelWeightValue > 100) {
            errorMessage("Sum of mix percentage parameters exceeds 100%!");
        }

        newOrderCounter = 0;
        printMessage("Session started!");
        if (!limitIsTime)
            printMessage("Creating " + numTerminals + " terminal(s) with " + transactionsPerTerminal + " transaction(s) per terminal...");
        else
            printMessage("Creating " + numTerminals + " terminal(s) with " + (executionTimeMillis / 60000) + " minute(s) of execution...");
        printMessage("Transaction Weights: " + newOrderWeightValue + "% New-Order, " + paymentWeightValue + "% Payment, " + orderStatusWeightValue + "% Order-Status, " + deliveryWeightValue + "% Delivery, " + stockLevelWeightValue + "% Stock-Level");

        printMessage("Number of Terminals\t" + numTerminals);

        terminals = new jTPCCTerminal[numTerminals];
        terminalNames = new String[numTerminals];
        terminalsStarted = numTerminals;
        try {
            int[][] usedTerminals = new int[numWarehouses][jTPCCConfig.configDistPerWhse];
            for (int i = 0; i < numWarehouses; i++)
                for (int j = 0; j < jTPCCConfig.configDistPerWhse; j++)
                    usedTerminals[i][j] = 0;

            for (int i = 0; i < numTerminals; i++) {
                int terminalWarehouseID;
                int terminalDistrictID;
                do {
                    terminalWarehouseID = (int) randomNumber(1, numWarehouses);
                    terminalDistrictID = (int) randomNumber(1, jTPCCConfig.configDistPerWhse);
                }
                while (usedTerminals[terminalWarehouseID - 1][terminalDistrictID - 1] == 1);
                usedTerminals[terminalWarehouseID - 1][terminalDistrictID - 1] = 1;

                String terminalName = "Term-" + (i >= 9 ? "" + (i + 1) : "0" + (i + 1));
                jTPCCTerminal terminal = new jTPCCTerminal
                        (terminalName, terminalWarehouseID, terminalDistrictID,
                                transactionsPerTerminal, paymentWeightValue, orderStatusWeightValue,
                                deliveryWeightValue, stockLevelWeightValue, numWarehouses, limPerMin_Terminal, this);

                terminals[i] = terminal;
                terminalNames[i] = terminalName;
                printMessage(terminalName + "\t" + terminalWarehouseID);
            }

            sessionEndTargetTime = executionTimeMillis;
            signalTerminalsRequestEndSent = false;


            printMessage("Transaction\tWeight");
            printMessage("% New-Order\t" + newOrderWeightValue);
            printMessage("% Payment\t" + paymentWeightValue);
            printMessage("% Order-Status\t" + orderStatusWeightValue);
            printMessage("% Delivery\t" + deliveryWeightValue);
            printMessage("% Stock-Level\t" + stockLevelWeightValue);

            printMessage("Transaction Number\tTerminal\tType\tExecution Time (ms)\t\tComment");

            printMessage("Created " + numTerminals + " terminal(s) successfully!");


            //^Create Terminals, Start Transactions v //

            sessionStart = getCurrentTime();
            sessionStartTimestamp = System.currentTimeMillis();
            sessionNextTimestamp = sessionStartTimestamp;
            if (sessionEndTargetTime != -1)
                sessionEndTargetTime += sessionStartTimestamp;

            synchronized (terminals) {
                printMessage("Starting all terminals...");
                transactionCount = 1;
                for (int i = 0; i < terminals.length; i++)
                    (new Thread(terminals[i])).start();

            }

            printMessage("All terminals started executing " + sessionStart);

        } catch (Exception e1) {
            errorMessage("This session ended with errors!");
            printStreamReport.close();
        }

        updateStatusLine();
    }

    private String getProp(Properties p, String pName) {
        String prop = p.getProperty(pName);
        log.info("Term-00, " + pName + "=" + prop);
        return (prop);
    }

    private void signalTerminalsRequestEnd(boolean timeTriggered) {
        synchronized (terminals) {
            if (!signalTerminalsRequestEndSent) {
                if (timeTriggered)
                    printMessage("The time limit has been reached.");
                printMessage("Signalling all terminals to stop...");
                signalTerminalsRequestEndSent = true;

                for (int i = 0; i < terminals.length; i++)
                    if (terminals[i] != null)
                        terminals[i].stopRunningWhenPossible();

                printMessage("Waiting for all active transactions to end...");
            }
        }
    }

    public void signalTerminalEnded(jTPCCTerminal terminal, long countNewOrdersExecuted) {
        synchronized (terminals) {
            boolean found = false;
            terminalsStarted--;
            for (int i = 0; i < terminals.length && !found; i++) {
                if (terminals[i] == terminal) {
                    terminals[i] = null;
                    terminalNames[i] = "(" + terminalNames[i] + ")";
                    newOrderCounter += countNewOrdersExecuted;
                    found = true;
                }
            }
        }

        if (terminalsStarted == 0) {
            sessionEnd = getCurrentTime();
            sessionEndTimestamp = System.currentTimeMillis();
            sessionEndTargetTime = -1;
            printMessage("All terminals finished executing " + sessionEnd);
            endReport();
            terminalsBlockingExit = false;
            printMessage("Session finished!");
        }
    }

    public void signalTerminalEndedTransaction(int transactionNum, String terminalName, String transactionType, long executionTime, String comment, int newOrder) {
//        System.out.println("transactionCount " + transactionCount);
        transactionCount++;
        fastNewOrderCounter += newOrder;
        if (transactionNum == 1) {
            sessionStartTimestamp = System.currentTimeMillis();
        }
        if (transactionNum >= 1) {
            if (sessionEndTargetTime != -1 && System.currentTimeMillis() > sessionEndTargetTime) {
                signalTerminalsRequestEnd(true);
            }
            updateStatusLine();
        }
    }

    private void endReport() {
        long currTimeMillis = System.currentTimeMillis();
        long freeMem = Runtime.getRuntime().freeMemory() / (1024 * 1024);
        long totalMem = Runtime.getRuntime().totalMemory() / (1024 * 1024);
        double tpmC = (6000000 * fastNewOrderCounter / (currTimeMillis - sessionStartTimestamp)) / 100.0;
        double tpmTotal = (6000000 * transactionCount / (currTimeMillis - sessionStartTimestamp)) / 100.0;


        System.out.println("");
        log.info("Term-00, ");
        log.info("Term-00, ");
        log.info("Term-00, Measured tpmC (NewOrders) = " + tpmC);
        log.info("Term-00, Measured tpmTOTAL = " + tpmTotal);
        log.info("Term-00, Session Start     = " + sessionStart);
        log.info("Term-00, Session End       = " + sessionEnd);
        log.info("Term-00, Transaction Count = " + (transactionCount - 1));

    }

    private void printMessage(String message) {
        log.info("Term-00, " + message);
    }

    private void errorMessage(String message) {
        log.error("Term-00, " + message);
    }

    private void exit() {
        System.exit(0);
    }

    private long randomNumber(long min, long max) {
        return (long) (random.nextDouble() * (max - min + 1) + min);
    }

    private String getCurrentTime() {
        return dateFormat.format(new java.util.Date());
    }

    private String getFileNameSuffix() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return dateFormat.format(new java.util.Date());
    }

    synchronized private void updateStatusLine() {
        long currTimeMillis = System.currentTimeMillis();

        if (currTimeMillis > sessionNextTimestamp) {
            StringBuilder informativeText = new StringBuilder("");
            Formatter fmt = new Formatter(informativeText);
            if (currTimeMillis - sessionStartTimestamp == 0)
                sessionStartTimestamp = currTimeMillis - 1;
            double tpmC = (6000000 * fastNewOrderCounter / (currTimeMillis - sessionStartTimestamp)) / 100.0;
            double tpmTotal = (6000000 * transactionCount / (currTimeMillis - sessionStartTimestamp)) / 100.0;

            sessionNextTimestamp += 1000;  /* update this every seconds */

//            fmt.format("Term-00, Running Average tpmTOTAL: %.2f", tpmTotal);

	    /* XXX What is the meaning of these numbers? */
            recentTpmC = (fastNewOrderCounter - sessionNextKounter) * 12;
            recentTpmTotal = (transactionCount - sessionNextKounter) * 12;
            sessionNextKounter = fastNewOrderCounter;
//            fmt.format("    Current tpmTOTAL: %d", recentTpmTotal);

            long freeMem = Runtime.getRuntime().freeMemory() / (1024 * 1024);
            long totalMem = Runtime.getRuntime().totalMemory() / (1024 * 1024);
//            fmt.format("    Memory Usage: %dMB / %dMB          ", (totalMem - freeMem), totalMem);

//            System.out.print(informativeText);
//            for (int count = 0; count < 1 + informativeText.length(); count++)
//                System.out.print("\b");
        }
    }
}
