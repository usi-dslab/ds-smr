package ch.usi.dslab.lel.dsqldb.tpcc;/*
 * jTPCCConfig - Basic configuration parameters for jTPCC
 *
 * Copyright (C) 2003, Raul Barbosa
 * Copyright (C) 2004-2016, Denis Lussier
 * Copyright (C) 2016, Jan Wieck
 *
 */

import java.text.SimpleDateFormat;

public class jTPCCConfig {
    public static String JTPCCVERSION = "4.1.1";

    public static final int NEW_ORDER = 1, PAYMENT = 2, ORDER_STATUS = 3, DELIVERY = 4, STOCK_LEVEL = 5;

    public static String[] nameTokens = {"BAR", "OUGHT", "ABLE", "PRI", "PRES", "ESE", "ANTI", "CALLY", "ATION", "EING"};

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static int configCommitCount = 10000;  // commit every n records in LoadData

    public static int configWhseCount = 1;
    public static int configItemCount = 10; // tpc-c std = 100,000
    public static int configDistPerWhse = 10;     // tpc-c std = 10
    public static int configCustPerDist = 10;   // tpc-c std = 3,000
}
