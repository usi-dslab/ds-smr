/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.dsqldb;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.PRObject;
import ch.usi.dslab.lel.aerie.StateMachine;
import ch.usi.dslab.lel.dsqldb.DSqlCommand.DSqlCommandType;
import ch.usi.dslab.lel.dsqldb.tables.Base;
import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCDataGenerator;

/**
 * Created by longle on 17/02/16.
 */
public class DSqlServer extends StateMachine {
    public static final Logger log = LogManager.getLogger(DSqlServer.class);

    public DSqlServer(int serverId, int partitionId, String systemConfig, String partitionsConfig) {
        super(serverId, partitionId, systemConfig, partitionsConfig, new DSqlApplicationProcedure());
    }

    public static void main(String[] args) {
        if (args.length == 5) {
            int serverId = Integer.parseInt(args[0]);
            int partitionId = Integer.parseInt(args[1]);
            String systemConfigFile = args[2];
            String partitionsConfigFile = args[3];
            String dataFile = args[4];
            DSqlServer chitterServer = new DSqlServer(serverId, partitionId, systemConfigFile, partitionsConfigFile);
            chitterServer.streamDataToCache(dataFile);
            chitterServer.runStateMachine();
        } else if (args.length == 10) {
            int serverId = Integer.parseInt(args[0]);
            int partitionId = Integer.parseInt(args[1]);
            String systemConfigFile = args[2];
            String partitionsConfigFile = args[3];
            String dataFile = args[4];
            String gathererHost = args[5];
            int gathererPort = Integer.parseInt(args[6]);
            String fileDirectory = args[7];
            int gathererDuration = Integer.parseInt(args[8]);
            int warmup = Integer.parseInt(args[9]);
            System.out.println("Server "+ serverId+" is starting");
            DSqlServer chitterServer = new DSqlServer(serverId, partitionId, systemConfigFile, partitionsConfigFile);
            chitterServer.streamDataToCache(dataFile);
            chitterServer.setupMonitoring(gathererHost, gathererPort, fileDirectory, gathererDuration, warmup);
            chitterServer.runStateMachine();
        } else {
            System.err.println("usage: serverId partitionId systemConfigFile partitioningFile dataFile");
            System.err.println("usage: serverId partitionId systemConfigFile partitioningFile dataFile monitorHost monitorPort monitorLogDir monitorDuration monitorWarmup");
            System.exit(1);
        }

    }

    private void loadDataToCache(String dataFile) {
        long start = System.currentTimeMillis();
        System.out.println("[SERVER" + this.partitionId + "] Loading sample data...");
        Map<String, Object> config = jTPCCDataGenerator.loadCSVData(dataFile);
        System.out.println("[SERVER" + this.partitionId + "] File read, takes " + (System.currentTimeMillis() - start));
        for (Base obj : (Set<Base>) config.get("objs")) {
            ObjId objId = obj.getId();
            int dest = DSqlApplicationProcedure.mapIdToPartition(objId);
            if (this.partitionId == dest) {
                DSqlCommandPayload payload = new DSqlCommandPayload(obj.toHashMap());
                createObject(objId, payload);
            }
        }
        System.out.println("[SERVER" + this.partitionId + "] Data loaded, takes " + (System.currentTimeMillis() - start));
    }

    public void streamDataToCache(String dataFile) {
        System.out.println("[SERVER" + this.partitionId + "] loading sample data...");
        long start = System.currentTimeMillis();
        Map<String, String[]> ret = new HashMap<>();
        jTPCCDataGenerator.loadCSVData(dataFile, line -> {
            String[] tmp = line.split(",");
            ObjId objId = null;
            Map<String, Object> obj = null;
            if (tmp.length == 2) {

            } else {
                switch (tmp[0]) {
                    case "Header":
                        ret.put(tmp[1], Arrays.copyOfRange(tmp, 1, tmp.length));
                        break;
                    default:
                        objId = new ObjId(tmp[1]);
                        obj = Base.csvToHashMap(ret.get(tmp[0]), tmp);
                        break;
                }
            }
            if (objId != null) {
                int dest = DSqlApplicationProcedure.mapIdToPartition(objId);
                if (this.partitionId == dest) {
                    DSqlCommandPayload payload = new DSqlCommandPayload(obj);
                    createObject(objId, payload);
                }
            }
        });
        System.out.println("[SERVER" + this.partitionId + "] Data loaded, takes " + (System.currentTimeMillis() - start));
    }

    @Override
    public Message executeSSMRCommand(Command command) {
        return executeCommand(command);
//        command.rewind();
//        log.debug("Processing command " + command.getId().toString());
//        DSqlCommandType op = (DSqlCommandType) command.getNext();
//        return new Message("OK");
    }

    @Override
    public Message executeCommand(Command command) {
        command.rewind();
        // Command format : | byte op | ObjId o1 | int value |
        log.debug("Processing command " + command.getId().toString());
        DSqlCommandType op = (DSqlCommandType) command.getNext();
        Base baseModel = null;
        switch (op) {
            case READ_SSMR:
                ObjId objId1 = (ObjId) command.getNext();
                baseModel = (Base) PRObject.getObjectById(objId1);
                return new Message(baseModel);
            case UPDATE:
            case UPDATE_BATCH: {
                ArrayList<ObjId> objIds = (ArrayList) command.getNext();
                DSqlCommandPayload payload = (DSqlCommandPayload) command.getNext();
                Set<Base> objs = new HashSet<>();
                Set<ObjId> missingObjs = new HashSet<>();
                for (ObjId objId : objIds) {
                    baseModel = (Base) PRObject.getObjectById(objId);
                    if (baseModel == null) missingObjs.add(objId);
                    else objs.add(baseModel);
                }
                if (missingObjs.size() > 0) return new Message("RETRY", missingObjs);
                for (Base obj : objs) {
                    for (String attr : payload.attributes.keySet()) {
                        Base.set(obj, attr, payload.attributes.get(attr));
                    }
                }
                return new Message(objs);
            }
        }
        return new Message(baseModel);
    }

    @Override
    public PRObject createObject(Object id, Object payloadObj) {
        ObjId objId = (ObjId) id;
        DSqlCommandPayload payload = (DSqlCommandPayload) payloadObj;
        Base.MODEL modelObj = Base.getModelFromObjId(objId);
        log.debug("Creating object " + modelObj.getName() + " with id=" + objId.value);
        String model = "ch.usi.dslab.lel.dsqldb.tables." + modelObj.getName();

        try {
            Class<?>[] params = Base.getConstructorParams();
            Object xyz = Class.forName(model).getConstructor(params).newInstance(objId);
            for (String attr : payload.attributes.keySet()) {
                Base.set(xyz, attr, payload.attributes.get(attr));
            }
            return (PRObject) xyz;
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | ClassNotFoundException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
