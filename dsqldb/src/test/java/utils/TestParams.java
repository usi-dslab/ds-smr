package utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Author: longle, created on 08/03/16.
 */
public class TestParams {

    void takeStringArray(Object... strings) {
        System.out.println(strings);
    }

    @Test

    public void testParams() {
        takeStringArray("A", "B", "C");
        String[] test = {"2", "3"};
        List<Object> args = new ArrayList<>();
        args.add("1");
        args.addAll(Arrays.asList(test));
        takeStringArray(args.toArray());
    }
}
