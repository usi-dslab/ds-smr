package utils;

import com.github.underscore.$;
import com.github.underscore.Predicate;

import org.junit.Assert;
import org.junit.Test;

import static java.util.Arrays.asList;

/**
 * Author: longle, created on 03/03/16.
 */
public class TestUnderscore {
    @Test
    public void testFind() {
        int a = $.find(asList(1, 2, 3, 4, 5, 6),
                new Predicate<Integer>() {
                    public Boolean apply(Integer item) {
                        return item % 2 == 0;
                    }
                }).get();
        Assert.assertEquals(a, 2);
    }
}
