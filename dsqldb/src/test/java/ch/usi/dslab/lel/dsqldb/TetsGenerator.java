package ch.usi.dslab.lel.dsqldb;

import org.junit.Test;

import java.util.Map;

import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCDataGenerator;

/**
 * Author: longle, created on 28/04/16.
 */
public class TetsGenerator {
    @Test
    public void testLoadCSVData() {
        long now = System.currentTimeMillis();
        Map<String, Object> data = jTPCCDataGenerator.loadCSVData("/Users/longle/Dropbox/Workspace/PhD/DS-SMR/bin/dsqldb/w_1_d_10_i_100000_c_3000.data");
        long end = System.currentTimeMillis();

        System.out.println("loadCSV:" + (end - now));

    }
}
