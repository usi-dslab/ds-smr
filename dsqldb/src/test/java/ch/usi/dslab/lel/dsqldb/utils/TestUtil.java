package ch.usi.dslab.lel.dsqldb.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Author: longle, created on 14/03/16.
 */
public class TestUtil {
    @Test
    public void intToBitsString() {
        Assert.assertEquals(Util.intToBitsString(12,8), "00001100");
        Assert.assertEquals(Util.intToBitsString(1,4), "0001");
        Assert.assertEquals(Util.intToBitsString(123456789,28), "0111010110111100110100010101");
    }
}
