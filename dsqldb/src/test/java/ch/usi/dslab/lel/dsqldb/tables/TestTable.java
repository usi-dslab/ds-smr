/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.dsqldb.tables;


/**
 * Created by longle on 18/02/16.
 */
public class TestTable {
//    @Test
//    public void Constructor() {
//        Model customerObj = new Model(Table.CUSTOMER, 10);
//        Assert.assertEquals(customerObj.getIds(), new ObjId(10, Table.CUSTOMER.getValue(), 0));
//        customerObj.setAttribute("c_first", "Long");
//        Assert.assertEquals(customerObj.getAttribute("c_first"), "Long");
//    }
}
