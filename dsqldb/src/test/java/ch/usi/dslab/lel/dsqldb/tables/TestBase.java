package ch.usi.dslab.lel.dsqldb.tables;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.Random;

import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.dsqldb.tpcc.jTPCCUtil;

/**
 * Author: longle, created on 11/03/16.
 */
public class TestBase {
    @Test
    public void toHashMap() {
        Random gen = new Random(System.currentTimeMillis());
        Warehouse warehouse = new Warehouse();
        warehouse.w_id = 1;
        warehouse.w_ytd = 300000;

        // random within [0.0000 .. 0.2000]
        warehouse.w_tax = (float) ((jTPCCUtil.randomNumber(0, 2000, gen)) / 10000.0);

        warehouse.w_name = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(6, 10, gen));
        warehouse.w_street_1 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
        warehouse.w_street_2 = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
        warehouse.w_city = jTPCCUtil.randomStr(jTPCCUtil.randomNumber(10, 20, gen));
        warehouse.w_state = jTPCCUtil.randomStr(3).toUpperCase();
        warehouse.w_zip = 123456789;
        Map attr = warehouse.toHashMap();
//        Assert.assertEquals(attr.get("w_zip"), "123456789");
    }

    @Test
    public void setAttr() {
//        Warehouse w = new Warehouse(1);
//        Base.set(w, "w_name", "long");
//        Base.set(w, "w_street_1", "long");
//        Base.set(w, "w_tax", 1.2);
//        Base.set(w, "w_id", 1);
//        Base.set(w, "w_ytd", "1");
//        Base.set(w, "w_ytd", "+1");
//        Base.set(w, "w_tax", "+1.0");
//        Assert.assertEquals(w.w_ytd, 2);
//        Assert.assertEquals(w.w_tax, 2.2);
    }

    @Test
    public void extractAttribute() {
        ObjId objId = new ObjId("OrderLine:ol_number#13:ol_w_id#1:ol_d_id#3:ol_o_id#10");
//        String[] keys = Base.extractAttributes(objId);
    }

    @Test
    public void parseObjId() {
//        Map<String, Object> parse = Base.parseObjId(new ObjId("Customer:c_id#1:c_d_id#1:c_w_id#1:c_last#EINGPRIEING"));

    }

    @Test
    public void interpret() {
        System.out.print(Base.interpret("123 long"));
        System.out.print(Base.interpret("6.000000284984708E-4").getClass());
        System.out.print(Base.interpret("-1 3 1 3 1 4592.97 |-1 3 1 3 1 4").getClass());
    }

    @Test
    public void fromJSON() {
        JSONObject json = new JSONObject();
        json.put("objId", "Warehouse:w_id#1");
        json.put("w_zip", 123456);
        json.put("w_id", 1);
        json.put("w_street_2", "erLbMNrfRHKwmAUL");
        json.put("w_state", "ON");
        json.put("w_street_1", "OUfOdYWDx");
        json.put("w_ytd", 300000.0);
        json.put("objId", "Warehouse:w_id#1");
        json.put("w_city", "NfJCxBAMsaEjEl");
        json.put("w_tax", 0.03880000114440918);
        json.put("w_name", "tlobqy");
        Warehouse w = new Warehouse();
        w.fromJSON(json);
        System.out.println(w);

    }
}
