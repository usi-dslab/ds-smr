/*

 Netwrapper - A library for easy networking in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Netwrapper.
 
 Netwrapper is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

package ch.usi.dslab.bezerra.netwrapper.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.netwrapper.Message;

public class TCPConnection {
   Logger logger = LogManager.getLogger(TCPConnection.class.getName());
   SocketChannel channel;
   ByteBuffer    receiveBuffer = null;
   Queue<ByteBuffer> sendQueue = new ConcurrentLinkedQueue<ByteBuffer>();
   SelectionKey writeSelectionKey = null;
   
   static final int RECV_BUFFER_SIZE = 262144;
   
   
   
   
   /*
    *    CONSTRUCTORS
    */
   
   public TCPConnection(SocketChannel existingChannel, int bufferSize) {
      try {
         this.channel  = existingChannel;
         this.channel.setOption(StandardSocketOptions.TCP_NODELAY, true);
         receiveBuffer = ByteBuffer.allocate(bufferSize);
      } catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   public TCPConnection(String address, int port, int bufferSize) throws IOException {
      logger.debug(String.format("Connecting to %s:%d", address, port));
      channel = SocketChannel.open();
      channel.connect(new InetSocketAddress(address, port));
      channel.configureBlocking(false);
      channel.setOption(StandardSocketOptions.TCP_NODELAY, true);
      logger.debug(String.format("Connected to %s:%d", address, port));
      receiveBuffer = ByteBuffer.allocate(bufferSize);
   }
   
   public TCPConnection(SocketChannel existingChannel) {
      this(existingChannel, RECV_BUFFER_SIZE);
   }
   
   public TCPConnection(String address, int port) throws IOException {
      this(address, port, RECV_BUFFER_SIZE);
   }
   

   
   
   /*
    *    METHODS
    */   
   
   public void sendBusyWait(Message message) {
      ByteBuffer serializedMessage = message.getByteBufferWithLengthHeader();

      try {
         while (serializedMessage.hasRemaining())
            channel.write(serializedMessage);
      } catch (IOException e) {
         e.printStackTrace();
         logger.fatal("!!! - sender apparentely disconnected from remote tcp receiver; exiting...");
         System.exit(1);
      }
   }
   
   public Message receiveBusyWait() {
      byte[] message = null;      
      
      if (receiveBuffer == null)
         receiveBuffer = ByteBuffer.allocate(RECV_BUFFER_SIZE);
      
      try {
         int remaining = 4;
         receiveBuffer.clear();
         receiveBuffer.limit(4);
         while (remaining > 0) {
            remaining -= channel.read(receiveBuffer);
         }
         receiveBuffer.flip();
         remaining = receiveBuffer.getInt();
         message = new byte[remaining];
         receiveBuffer.clear();
         receiveBuffer.limit(remaining);
         while (remaining > 0) {
            remaining -= channel.read(receiveBuffer);
         }
         receiveBuffer.flip();
         receiveBuffer.get(message);
      } catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
      
      return Message.createFromBytes(message);
   }
}
