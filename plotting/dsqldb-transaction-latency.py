import matplotlib.pyplot as plt
import numpy as np
import common

graph = "payment-latency"
graph = "new-order-latency"
graph = "local-command-rate"
filename = common.script_dir() + "/data/" + graph + ".log"
lat = np.loadtxt(filename, skiprows=0, dtype=long)

plt.plot(lat, label="latency")
plt.title(graph)
plt.ylabel("ms")
plt.legend()
plt.grid(True)
# plt.savefig(OUT_DIR + FOLDER + ".png", bbox_inches='tight')
plt.show()
