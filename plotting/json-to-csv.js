var fs = require('fs');
var data = require('./data/users_5000_partitions_4.json');
var out = 'data.csv';
var str = "Source,Target,Type,Id,Label,Weight,Partition\n";
console.log(data.allUsers.length);
var count = 0;
for (var i = 0; i < data.allUsers.length; i++) {
    var user = data.allUsers[i];
    for (var j = 0; j < user.followers.length; j++) {
        count++;
        str +=
            user.userId + "," + user.followers[j] + "," + "Directed" + "," + count + ",,1.0,"
            + user.partitionId + "\n"
    }
}

fs.writeFile(out, str, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log("file saved to " + out);
    }
});
