import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import datetime

days, impressions = np.loadtxt("./data/page-impressions.csv", unpack=True,
                               converters={1: mdates.strpdate2num('%Y-%m-%d')})

plt.plot_date(x=days, y=impressions, fmt="r-")
plt.title("Page impressions on example.com")
plt.ylabel("Page impressions")
plt.grid(True)
plt.show()
