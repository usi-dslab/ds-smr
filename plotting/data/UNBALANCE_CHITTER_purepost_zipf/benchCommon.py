#!/usr/bin/python

import math
import os
import re
import sys
import shlex
import threading
import subprocess
import socket
from os.path import expanduser
import inspect

def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

localhost = False
if socket.gethostname() in ['longs-mbp.mobile.usilu.net', "Longs-MBP.fritz.box", "Longs-MacBook-Pro.local"]:
    HOME = expanduser("~/Dropbox/Workspace/PhD/DS-SMR/chitter")
    localhost = True
else:
    HOME = expanduser("/home/long/apps/DS-SMR/chitter")

# parameters
GLOBAL_HOME = os.path.normpath(script_dir() + '/../../')

BIN_HOME = os.path.normpath(GLOBAL_HOME + '/bin')

CHITTER_HOME = os.path.normpath(GLOBAL_HOME + '/chitter')
CHITTER_CP = os.path.normpath(CHITTER_HOME + '/target/classes')
CHITTER_CLASS_SERVER = os.path.normpath('ch.usi.dslab.lel.chitter.ChitterServer')
CHITTER_CLASS_ORACLE = os.path.normpath('ch.usi.dslab.lel.chitter.ChitterOracle')
CHITTER_CLASS_CLIENT = os.path.normpath('ch.usi.dslab.lel.chitter.ChitterClientPromise')
CHITTER_CLASS_TEST_RUNNER = os.path.normpath('ch.usi.dslab.lel.chitter.benchmarks.TestRunnerPromise')
# CHITTER_CLASS_TEST_RUNNER = os.path.normpath('ch.usi.dslab.lel.chitter.benchmarks.TestRunner')



DSSMR_HOME = os.path.normpath(GLOBAL_HOME + '/aerie')
DSSMR_CP = os.path.normpath(DSSMR_HOME + '/target/classes')

LIBMCAD_HOME = os.path.normpath(GLOBAL_HOME + '/libmcad')
LIBMCAD_CP = os.path.normpath(LIBMCAD_HOME + '/target/classes')
LIBMCAD_CLASS_RIDGE = 'ch.usi.dslab.bezerra.mcad.ridge.RidgeEnsembleNode'

NETWRAPPER_HOME = os.path.normpath(GLOBAL_HOME + '/netwrapper')
NETWRAPPER_CP = os.path.normpath(NETWRAPPER_HOME + '/target/classes')

RIDGE_HOME = os.path.normpath(GLOBAL_HOME + '/ridge')
RIDGE_CP = os.path.normpath(RIDGE_HOME + '/target/classes')

SENSE_HOME = os.path.normpath(GLOBAL_HOME + '/sense')
SENSE_CP = os.path.normpath(SENSE_HOME + '/target/classes')
SENSE_CLASS_GATHERER = "ch.usi.dslab.bezerra.sense.DataGatherer"
SENSE_GATHERER_PORT = 60000
SENSE_WARMUP_TIME = 20000
SENSE_DURATION = 90

SYSTEM_CONFIG_DIR = os.path.normpath(BIN_HOME + '/systemConfigs')

DEPENDENCIES_DIR = os.path.normpath(GLOBAL_HOME + '/dependencies/*')

_class_path = [os.path.normpath(GLOBAL_HOME + '/dependencies/guava-19.0.jar'), CHITTER_CP, DSSMR_CP,
               LIBMCAD_CP, NETWRAPPER_CP, RIDGE_CP, SENSE_CP, DEPENDENCIES_DIR]

if localhost:
    LOG4J = os.path.normpath(BIN_HOME + '/dsqldb/log4jDebug.xml')
else:
    LOG4J = os.path.normpath(HOME + '/resources_override/log4j.xml')

JAVA_BIN = 'java -XX:+UseG1GC -Xmx8g -Dlog4j.configuration=file:' + LOG4J
JAVA_CLASSPATH = '-cp \'' + ':'.join([str(val) for val in _class_path])+'\''


# HOME = expanduser("~/Dropbox/Workspace/PhD/CosmmusProject/chitter")
# localhost = True
# ===================================================
# definitions

benchCommonPath = os.path.dirname(os.path.realpath(__file__)) + "/benchCommon.py"

# available machines
def noderange(first, last):
    return ["node" + str(val) for val in [node for node in range(first, last + 1) if node not in deadNodes]]

# availableNodes = noderange(1,40) + noderange(66,72) + noderange(74,75)
deadNodes = [21, 63, 80, 68, 73, 71, 41, 75, 78]
localhosts = []

for i in range(1, 50):
    localhosts.append("localhost")
availableNodes = localhosts if localhost else noderange(1, 60)

# single experiment
onceRunner = HOME + "/resources_override/runAllOnce.py"
cleaner = HOME + "/resources_override/cleanUp.py"
socialNetworkDir = HOME + "/resources_override/socialNetworksInterestLinked/"
clockSynchronizer = HOME + "/resources_override/clockSynchronizer.py"
continousClockSynchronizer = HOME + "/resources_override/continuousClockSynchronizer.py"
systemParamSetter = HOME + "/resources_override/systemParamSetter.py"

# parameters
if localhost:
    javabin = "java -XX:+UseG1GC -Xmx8g -Dlog4j.configuration=file:" + HOME + "/resources_override/log4jLocal.xml"
else:
    javabin = "java -XX:+UseG1GC -Xmx8g -Dlog4j.configuration=file:" + HOME + "/resources_override/log4j.xml"
javacp = JAVA_CLASSPATH
# javaRunnerClass = "ch.usi.dslab.lel.chitter.benchmarks.TestRunnerPromise"
# javaRunnerClass = "ch.usi.dslab.lel.chitter.benchmarks.TestRunner"
# javachitterServerClass = "ch.usi.dslab.lel.chitter.ChitterServer"
# javachitterOracleClass = "ch.usi.dslab.lel.chitter.ChitterOracle"
# duration = "90"

######################
# default workload
workloadName = "mix"
# weightPost = 7.5
weightPost = 0
# weightFollow = 3.75
weightFollow = 0
# weightUnfollow = 3.75
weightUnfollow = 0
# weightGetTimeline = 85
weightGetTimeline = 100
######################

# CLIENTS
numPermits = 1
numUsers = 10000
clientDeployer = HOME + "/resources_override/deployTestRunners.py"

# chitter
replicasPerPartition = 2
ensembleSize = 3
ridgeProcessPerNode = 2
serverPerNode = 2

sysConfigFile = HOME + "/resources_override/generatedSysConfig.json"
partitionsFile = HOME + "/resources_override/generatedPartitionsConfig.json"
chitterServerDeployer = HOME + "/resources_override/deployServerChitter.py"
chitterOracleDeployer = HOME + "/resources_override/deployOracleChitter.py"
numOracles = 1

# RIDGE
javaRidgeNodeClass = "ch.usi.dslab.bezerra.mcad.ridge.RidgeEnsembleNode"
multicastDeployer = HOME + "/resources_override/deployRidge.py"
batch_size_threshold_bytes = 30000
batch_time_threshold_ms = 50
delta_null_messages_ms = 5
latency_estimation_sample = 10
latency_estimation_devs = 0
latency_estimation_max = 0
clockSyncInterval = 3

# RETWIS
retwisServerPort = 56789
javaRetwisUserCreatorClass = "RetwisUserCreator"
userIdMapFile = HOME + "/chitter/retwisIdMap.ser"
retwisServerDeployer = HOME + "/resources_override/deployServerRetwis.py"
retwisConfigFile = HOME + "/resources_override/redis.conf"

# MONITORING
gathererDeployer = HOME + "/resources_override/deployGatherer.py"
javaGathererClass = "ch.usi.dslab.bezerra.sense.DataGatherer"
javaBWMonitorClass = "ch.usi.dslab.bezerra.sense.monitors.BWMonitor"
javaCPUMonitorClass = "ch.usi.dslab.bezerra.sense.monitors.CPUMonitor"

srvlogdirchitter = HOME + "/log/server_log_chitter"
clilogdirRetwis = HOME + "/log/client_log_retwis"
clilogdirchitter = HOME + "/log/client_log_chitter"
nonclilogdirRetwis = HOME + "/log/nonclient_log_retwis"
nonclilogdirchitter = HOME + "/log/nonclient_log_chitter"
gathererBaseLogDir = HOME + "/log/logschitter/"
gathererPort = "60000"

# GENERATOR
javaGeneratorClass = "ch.usi.dslab.lel.chitter.util.SocialNetworkGenerator"

# ===================================================
# definitions (functions and classes)

class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run(self, timeout):
        def target():
            print 'Thread started'
            run_args = shlex.split(self.cmd)
            self.process = subprocess.Popen(run_args)
            self.process.communicate()
            print 'Thread finished'

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            print 'Terminating process'
            self.process.terminate()
            thread.join()
        return self.process.returncode


def getNumLoads(min_cli, max_cli, inc_factor, inc_parcel):
    numloads = 0
    load = min_cli
    while load <= max_cli:
        numloads += 1
        load = int(math.ceil(load * inc_factor) + inc_parcel)
    return numloads


def freePort(node, port):
    sshcmd(node, "sudo fuser -k " + str(port) + "/tcp")


def getNid(node):
    return int(re.findall(r'\d+', node)[0])


def sshcmd(node, cmdstring, timeout=None):
    finalstring = "ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\""
    print finalstring
    cmd = Command(finalstring)
    return cmd.run(timeout)


def localcmd(cmdstring, timeout=None):
    print "localcmd: " + cmdstring
    cmd = Command(cmdstring)
    return cmd.run(timeout)


def sshcmdbg(node, cmdstring):
    print "ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\" &"
    os.system("ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\" &")


def localcmdbg(cmdstring):
    print "localcmdbg: " + cmdstring
    os.system(cmdstring + " &")


def sarg(i):
    return sys.argv[i]


def iarg(i):
    return int(sarg(i))


def farg(i):
    return float(sarg(i))


def get_index(lst, key, value):
    for i, dic in enumerate(lst):
        if dic[key] == value:
            return i
    return -1


def get_item(lst, key, value):
    index = get_index(lst, key, value)
    if index == -1:
        return None
    else:
        return lst[index]

# constants
NODE = 0
CLIENTS = 1


def getScreenNode():
    return availableNodes[0]


def getNonScreenNodes():
    return availableNodes[1:]


# clientMap is a list of dicts
# clientMap = [{NODE: x, CLIENTS: y}, {NODE: z, CLIENTS: w}]
def mapClientsToNodes(numClients, nodesList):
    clientMap = []
    clientsPerNode = int(numClients / len(nodesList))
    for node in nodesList:
        clientMap.append({NODE: node, CLIENTS: clientsPerNode})
    for extra in range(numClients % len(nodesList)):
        clientMap[extra][CLIENTS] += 1
    return clientMap


# clientMap is a list of dicts
# clientMap = [{NODE: x, CLIENTS: y}, {NODE: z, CLIENTS: w}]
def clientNodeIsEmpty(node, clientMap):
    for mapping in clientMap:
        if mapping[NODE] == node and mapping[CLIENTS] > 0:
            return False
    return True


def numUsedClientNodes(arg1, arg2=None):
    if arg2 == None:
        return numUsedClientNodes_1(arg1)
    elif arg2 != None:
        return numUsedClientNodes_2(arg1, arg2)


def numUsedClientNodes_2(numClients, clientNodes):
    return min(numClients, len(clientNodes))


def numUsedClientNodes_1(clientNodesMap):
    numUsed = 0
    for mapping in clientNodesMap:
        if mapping[CLIENTS] > 0:
            numUsed += 1
    return numUsed

# ===================================================
# ===================================================
