import os


def getFileInPath(path, fileName):
    for (dirname, dirs, files) in os.walk(path):
        for filename in files:
            if fileName in (filename):
                return path + filename


def readLog(logFile):
    records = []
    if logFile is None:
        return [0, 0]
    if not os.path.isfile(logFile):
        return [0, 0]
    f = open(logFile)
    reduce = 1
    for line in f:
        if line.startswith('#'):
            if "(ns)" in line:
                reduce = 1000000
            elif "(cps)" in line:
                reduce = 1000
            continue
        tmp = [float(field) for field in line.strip().split()]
        record = [tmp[0], (tmp[2] / reduce)]
        records.append(record)
    if len(records) == 1:
        return records[0]
    else:
        return records


def readLogNoReduce(logFile):
    records = []
    if not os.path.isfile(logFile):
        return [0, 0]
    f = open(logFile)
    reduce = 1;
    for line in f:
        if line.startswith('#'):
            if "(ns)" in line:
                reduce = 1000000
            elif "(cps)" in line:
                reduce = 1000
            continue
        tmp = [float(field) for field in line.strip().split()]
        record = [tmp[0], (tmp[2])]
        records.append(record)
    if len(records) == 1:
        return records[0]
    else:
        return records