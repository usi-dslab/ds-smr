import os
import sys
import inspect


def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


def read_log(log_file):
    records = []
    if log_file is None:
        return [0, 0]
    if not os.path.isfile(log_file):
        return [0, 0]
    f = open(log_file)
    reduce_factor = 1
    for line in f:
        if line.startswith('#'):
            if "(ns)" in line:
                reduce_factor = 1000000
            elif "(cps)" in line:
                reduce_factor = 1000
            continue
        tmp = [float(field) for field in line.strip().split()]
        record = [tmp[0], (tmp[2] / reduce_factor)]
        records.append(record)
    if len(records) == 1:
        return records[0]
    else:
        return records


def normalize_time_aggregate_logs(log_files, has_weight=True):
    logs = []
    # read all files
    for file_name in log_files:
        f = open(file_name)
        log = []
        for line in f:
            if line.startswith('#'):
                continue
            tmp = [float(field) for field in line.strip().split()]
            t = int(tmp[0])
            if has_weight:
                log.append({"time": t, "weight": tmp[1], "value": tmp[2]})
            else:
                if len(log) == 0:
                    weight = 1000
                else:
                    weight = t - log[len(log) - 1]["time"]
                log.append({"time": t, "weight": weight, "value": tmp[1]})
        logs.append(log)

    # get bound
    start = 0
    end = sys.maxint
    max_duration = 0
    for log in logs:
        if start < log[0]["time"]: start = log[0]["time"]
        if end > log[len(log) - 1]["time"]: end = log[len(log) - 1]["time"]
        for time in range(1, len(log)):
            max_duration = max(max_duration, log[time]["time"] - log[time - 1]["time"])

    # normalize
    i = start
    agg = []
    max_duration -= 1
    # max_duration = 999
    while i <= end:
        sum = 0
        for log in logs:
            for time in range(0, len(log)):
                if i - max_duration < log[time]["time"] < i:
                    sum += (log[time]["value"] / (log[time]["weight"] / 1000))
        agg.append([i, sum])
        i = i + max_duration + 1
    return agg
