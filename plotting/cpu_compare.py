import matplotlib.pyplot as plt
import numpy as np
import common

FOLDER = "CHITTER_purepost_uniform_clients_400_permits_1_partitions_4_post_1.0_follow_0.0_unfollow_0.0_timeline_0.0"
FOLDER = "CHITTER_purepost_zipf_clients_400_permits_1_partitions_4_post_1.0_follow_0.0_unfollow_0.0_timeline_0.0"
OUT_DIR = common.script_dir() + "/graphs/"
times = []
cpus = []
filenames = []

i = 18
for i in range(18, 26):
    if i % 2 == 0:
        filename = common.script_dir() + "/data/" + FOLDER + "/cpu_PARTITION_" + str(i) + ".log"
        time, cpu = np.loadtxt(filename, unpack=True, skiprows=3, usecols=(0, 1), dtype=long,
                               converters={0: lambda ms: (long(ms) - 1469542064404) / 1000})
        cpus.append(cpu)
        times.append(time)
#
# times = np.array(list(times))
for i in range(0, len(times)):
    plt.plot(times[i], cpus[i], label="PARTITION #" + str(i))
plt.title("CPU usage")
plt.ylabel("CPU occupied")
plt.ylim([0, 80])
plt.legend()
plt.grid(True)
plt.savefig(OUT_DIR + FOLDER + ".png", bbox_inches='tight')
# plt.show()
