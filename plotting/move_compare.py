import matplotlib.pyplot as plt
import numpy as np
import common

graph = "move command comparision with 1 warehouse"

counts = []
for i in [2, 4, 6]:
    filename = common.script_dir() + "/data/move-compare/" + "1w_" + str(i)+ "p_throughput_client_move_rate_100" + ".log"
    time, weight, count = np.loadtxt(filename, unpack=True, skiprows=3, usecols=(0, 1, 2), dtype=long)
    counts.append(count)

minLength = min(len(counts[0]), len(counts[1]), len(counts[2]))
x = []
for i in range(0, minLength + 1):
    x.append(i)

plt.plot(counts[0], label="2p")
plt.plot(counts[1], label="4p")
plt.plot(counts[2], label="6p")

plt.title(graph)
plt.ylabel("move count")
plt.legend()
plt.grid(True)
# plt.savefig(OUT_DIR + FOLDER + ".png", bbox_inches='tight')
plt.show()
