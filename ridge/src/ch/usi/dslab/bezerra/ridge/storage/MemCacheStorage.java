package ch.usi.dslab.bezerra.ridge.storage;

import java.util.LinkedHashMap;
import java.util.Map;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;

public class MemCacheStorage extends MemoryStorage {
   public MemCacheStorage() {
      super(new LinkedHashMap<Long, RidgeMessage>(10000, 0.75F, false) {
         private static final long serialVersionUID = 1L;
         protected boolean removeEldestEntry(Map.Entry<Long, RidgeMessage> eldest) {
            return size() > Storage.CACHE_SIZE; 
         }
      });
   }
}
