/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2015, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge.storage;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Random;

import org.apache.commons.io.FileUtils;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.je.CacheMode;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.Durability;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;

public class BDBStorage implements Storage {

   Environment env;
   Database db;
   Database classCatalogDb;
   ClassCatalog classCatalog;

   EntryBinding<Long> keyBinding;

   DatabaseEntry key = new DatabaseEntry();

   DatabaseEntry data = new DatabaseEntry();

   EntryBinding<RidgeMessage> dataBinding;

   DatabaseEntry ballot_data = new DatabaseEntry();
   
   public BDBStorage(String path) {
      this(path, true);
   }

   public BDBStorage(String path, boolean sync) {
      boolean readonly = false;
      boolean trash_previous = true;

      if (path == null) {
         Random rand = new SecureRandom();
         int randomNumber = rand.nextInt();
         path = "/tmp/ridge-bdb/" + randomNumber;
      }
      
      File dbfile = new File(path);
      
      if (trash_previous) {
         try {
            FileUtils.deleteDirectory(dbfile);
         } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
         }
      }
      
      dbfile.mkdirs();

      EnvironmentConfig envConfig = new EnvironmentConfig();
      DatabaseConfig dbConfig = new DatabaseConfig();

      envConfig.setReadOnly(readonly);
      dbConfig.setReadOnly(readonly);
      envConfig.setAllowCreate(!readonly);
      dbConfig.setAllowCreate(!readonly);

      // performance settings
      envConfig.setTransactional(true);
      envConfig.setCacheMode(CacheMode.DEFAULT);
      // envConfig.setCacheSize(1000000*800); // 800M
      if (sync == false) {
         dbConfig.setTransactional(false);
         envConfig.setDurability(Durability.COMMIT_NO_SYNC);
         dbConfig.setDeferredWrite(true);
      } else {
         dbConfig.setTransactional(true);
         envConfig.setDurability(Durability.COMMIT_SYNC);
         dbConfig.setDeferredWrite(false);
      }

      env = new Environment(dbfile, envConfig);
      db = env.openDatabase(null, "ridgeStorageBDB", dbConfig);
      classCatalogDb = env.openDatabase(null, "ClassCatalogDB", dbConfig);
      classCatalog = new StoredClassCatalog(classCatalogDb);
      keyBinding = TupleBinding.getPrimitiveBinding(Long.class);
      dataBinding = new SerialBinding<RidgeMessage>(classCatalog, RidgeMessage.class);
   }

   @Override
   public int putInstanceBatch(Long instanceId, RidgeMessage instanceBatch) {
      keyBinding.objectToEntry(instanceId, key);
      dataBinding.objectToEntry(instanceBatch, ballot_data);
      OperationStatus status = db.put(null, key, ballot_data);
      return status == OperationStatus.SUCCESS ? 0 : 1;
   }

   @Override
   public RidgeMessage get(Long instanceId) {
      keyBinding.objectToEntry(instanceId, key);
      RidgeMessage instanceBatch = null;
      OperationStatus status = db.get(null, key, data, LockMode.DEFAULT);
      if (status == OperationStatus.SUCCESS) {
         instanceBatch = dataBinding.entryToObject(data);
      }
      return instanceBatch;
   }

}
