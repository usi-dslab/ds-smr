/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPConnection;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPDestination;
//import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;

public class Learner extends Process implements DeliverInterface {

   static Logger logger = LogManager.getLogger("Learner");

   int nextExpectedInstanceId = 0;
   List<Ensemble> subscribedEnsembles  = new ArrayList<Ensemble>();
   Map<Integer, TCPConnection> clients = new HashMap<Integer, TCPConnection>();

   Merger merger;
   DeliverInterface deliverer;

//   ThroughputPassiveMonitor tpMonitor = null;

   public Learner(int pid, String address, int port, int... subscribedEnsembleIds) {
      super(pid, address, port);
      for (int ensembleId : subscribedEnsembleIds)
         subscribeToEnsemble(ensembleId);
      merger = new Merger(this);
      setDeliverInterface(this);
   }
   
   public void setDeliverInterface(DeliverInterface deliverer) {
      this.deliverer = deliverer;
      merger.setDeliverInterface(deliverer);
   }
   
   public void subscribeToEnsemble(int ensembleId) {
      Ensemble ensemble = Ensemble.getEnsemble(ensembleId);
      subscribeToEnsemble(ensemble);
   }

   public void subscribeToEnsemble(Ensemble ensemble) {
      ensemble.addLearner(this);
      subscribedEnsembles.add(ensemble);
   }
   
   public void subscribeToEnsembles(List<Ensemble> ensembles) {
      for (Ensemble ensemble : ensembles)
         subscribeToEnsemble(ensemble);
   }

   public int getPositionAmongEqualLarners() {
      List<Learner> equalLearners = Ensemble.getIntersectingLearners(subscribedEnsembles);
      return equalLearners.indexOf(this);
   }
   
   public boolean isConnectedToClient(int clientId) {
      return clients.containsKey(clientId);
   }

   @Override
   public void startRunning() {
      Merger.optMergerSDEBUG = merger.optMerger;
//      tpMonitor = new ThroughputPassiveMonitor(this.pid, "learner");
      merger.startRunning();
      super.startRunning();
   }

   @Override
   void uponDelivery(Message message_) {
      // type = client_credentials / decision_batch / fast request

      RidgeMessage message = (RidgeMessage) message_;
      
      int msgType = message.messageType;

      if (msgType == RidgeMessage.CLIENT_CREDENTIALS) { // message is from a client identifying itself 
         int clipid = (Integer) message.getNext();
         TCPConnection clientConnection = getConnection(message);
         clients.put(clipid, clientConnection);
         Message connectedAck = new Message("CONNECTED");
         sendReplyToClient(connectedAck, clipid);
      }

      else if (msgType == RidgeMessage.DECISION_BATCH) { // message is a batch from a learner/acceptor
         logger.info("Learner delivered batch {}", message.instanceId);

         RidgeMessage batch      = message;
         int          ensembleId = batch.ensembleId;
         long         instanceId = batch.instanceId;

         batch.rewind();

         Ensemble ensemble = Ensemble.getEnsemble(ensembleId);
         broadcastToOtherLearners(ensemble, batch, instanceId);

         ensemble.addReceivedBatch(instanceId, batch);

         merger.wakeUp();
      }

      else if (msgType == RidgeMessage.MESSAGE_MULTICAST && MulticastAgent.fastDelivery) {
         Ensemble ensemble = Ensemble.getEnsemble(message.ensembleId);
         merger.addFastMessage(message);
         merger.wakeUp();
         if (MulticastAgent.directFast == false && ensemble.getBroadcaster(message.id.pid) == this)
            broadcastToOtherLearners(ensemble, message, message.id.pid);
      }

   }

   void broadcastToOtherLearners(Ensemble e, RidgeMessage m, long parameter) {
      if (e.configuration == Ensemble.DYNAMIC && this.pid == m.getBroadcastingLearnerId()) {
         List<TCPDestination> otherLearners = new ArrayList<TCPDestination>(e.learnersList);
         otherLearners.remove(this);
         for (TCPDestination td : otherLearners) {
            Learner l = (Learner) td;
            logger.info("sending message {} to learner {}:{}", ((RidgeMessage) m).instanceId, e.ensembleId, e.getLearnerPosition(l));
         }
         send(m, otherLearners);
      }
      
      else if (e.configuration == Ensemble.RING) {
         Learner nextInRing = e.getNextLearner(this);
         if (nextInRing != null) {
            logger.info("sending batch {} to learner {}:{}", ((RidgeMessage) m).instanceId, e.ensembleId, e.getLearnerPosition(nextInRing));
            send(m, nextInRing);
         }
      }
   }

   //=================================================================
   //=================================================================
   // These methods are for debug purpose only; they are not called if 
   //       the user sets a different DeliverInterface for the Learner
   //
   public void deliverConservatively(RidgeMessage message) {
      // delivery to server application (after merging)
      message.addItems("delivery to server application (after merging)");
      message.addItems(System.currentTimeMillis());
      
      message.rewind();
      int cliPid = (Integer) message.getNext();
      int reqSeq = (Integer) message.getNext();
      byte[] raw = (byte[] ) message.getNext();
      
      String cliMsg = new String(raw);
      
      logger.info("Delivered message {}:{} ({})... Sending reply to client {}.", cliPid, reqSeq, cliMsg, cliPid);
      
      Ensemble e = Ensemble.getEnsemble(message.ensembleId);
      if (cliPid % e.learnersList.size() == e.getLearnerPosition(this)) {
         logger.info("Learner {} from ensemble {} sending reply to client {}", e.getLearnerPosition(this), message.ensembleId, cliPid);
         Message reply = new Message();
         reply.addItems(reqSeq);
         
         // seek to 1st timeline object (i.e., description of 1st timestamp)
         message.seek(4);
         while (message.hasNext())
            reply.addItems(message.getNext());
         sendReplyToClient(reply, cliPid);
      }
      
//      tpMonitor.incrementCount();
      
   }
   
   public void deliverOptimistically(RidgeMessage message) {
      logger.info("Delivered message {} optimistically", message.id);
   }
   
   public void deliverFast(RidgeMessage message) {
      logger.info("Delivered message {} fast", message.id);
   }
   
   public void sendReplyToClient(Message reply, int clientPid) {
      TCPConnection conn = clients.get(clientPid);
      if (conn == null) {
         logger.error("Trying to send reply to client {}, which is not connected to this learner (pid {}).", clientPid, this.pid);
      }
      else
         send(reply, conn);
   }
   //
   //=================================================================
   //=================================================================

   public static void main(String[] args) {

      // <command> pid configFile duration
      // if configFile=="cmd"
      // <command> pid    "cmd"   duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+      
      
      // create acceptors, learners and coordinator here.
      // then, startRunning() the acceptor with the given id

      int pid = Integer.parseInt(args[0]);
/*
      String configFile = args[1];
      int duration = Integer.parseInt(args[2]);
      
      String gathererAddress = args[3];
      int    gathererPort    = Integer.parseInt(args[4]);

      if (configFile.equals("cmd"))
         Process.loadProcesses(Arrays.copyOfRange(args, 5, args.length));
      else
         Process.loadProcesses(configFile);
      
      DataGatherer.setDuration(duration);
//      DataGatherer.enableFileSave("/tmp/eduardo");
      DataGatherer.enableGathererSend(gathererAddress, gathererPort);
*/

      Process.loadProcesses();
      Learner localLearner = (Learner) Process.getProcess(pid);
      logger.info("Learner (pid {}) starting...", localLearner.pid);
      localLearner.startRunning();
   }

}
