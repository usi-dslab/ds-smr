/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.ridge.storage.Storage;
import ch.usi.dslab.bezerra.ridge.storage.StorageFactory;

public class Acceptor extends Process  {
   
   static Logger logger = LogManager.getLogger("Acceptor");
   
   
   /*
    *  STATIC MEMBERS (except main method)
    */
   
   static Map<Integer, Acceptor> acceptorsIndex = new ConcurrentHashMap<Integer, Acceptor>();
   
   public final static int STATIC   = 0;
   public final static int ROTATING = 1;
   
   static int configuration;
   static String dbtype = null;
      
   static Acceptor getAcceptor(int accId) {
      return acceptorsIndex.get(accId);
   }
   
   static int getAcceptorCount() {
      return acceptorsIndex.size();
   }
   
   static void setConfiguration(int configuration) {
      Acceptor.configuration = configuration;
   }
   
   public static void setStorage(String dbtype) {
      Acceptor.dbtype = dbtype;
   }

   
   /*
    *  INSTANCE MEMBERS
    */
   
   int id;
   boolean isCoordinator;
   Storage storage;
   
   public Acceptor(int id, String address, int port) {
      this(id, address, port, false);
   }
   
   Acceptor(int id, String address, int port, boolean coordinator) {
      super(id, address, port);
      this.isCoordinator = coordinator;
      acceptorsIndex.put(id, this);
   }
   
   @Override
   void uponDelivery(Message batch_) {
      RidgeMessage batch = (RidgeMessage) batch_;
      batch.rewind();

      long             instanceId = batch.instanceId;
      Ensemble         ensemble   = Ensemble.getEnsemble(batch.ensembleId);
      AcceptorSequence accSeq     = ensemble.getAcceptorSequence(instanceId);
      
      logger.info("Acceptor {} received batch for consensus instance {}", pid, instanceId);
      
      if (this.isCoordinator == false || accSeq.coordinatorWrites == true)
         saveToStorage(instanceId, batch);
      
      Acceptor nextAcceptor = accSeq.getNextAcceptor(this);
      if (nextAcceptor != null)
         send(batch, nextAcceptor);
      else { // next in line is a learner
         RidgeMessage cleanBatch = cleanBatchFromFastPayloads(batch);
         Learner contactLearner = ensemble.getAcceptorContact(cleanBatch);
         send(cleanBatch, contactLearner);
      }
   }
   
   RidgeMessage cleanBatchFromFastPayloads(RidgeMessage batch) {
      if (MulticastAgent.fastDelivery == false)
         return batch;
      
      RidgeMessage cleanBatch = batch;
      
      batch.rewind();
      int pos = 0;
      while(batch.hasNext()) {
         RidgeMessage message = (RidgeMessage) batch.getNext();
//         if (MulticastAgent.fastDelivery) {
            if (cleanBatch == batch) {
               cleanBatch = RidgeMessage.getShallowCopy(batch);
               cleanBatch.copyContentsList(batch);
            }
            RidgeMessage cleanMessage = RidgeMessage.getShallowCopy(message);
            cleanMessage.stripContents();
            cleanBatch.setItem(pos, cleanMessage);
//         }
         pos++;
      }
      
      return cleanBatch;
   }
   
   void saveToStorage(long instanceId, RidgeMessage instanceBatch) {
      if (storage != null)
         storage.putInstanceBatch(instanceId, instanceBatch);
   }
   
   @Override
   public void startRunning() {
      storage = StorageFactory.buildStorage(dbtype, pid);
      super.startRunning();
   }

   
   /*
    *  MAIN METHOD
    */
   
   public static void main (String[] args) {
      
      // <command> id configFile duration gatherer_address gatherer_port
      // if configFile=="cmd"
      // <command> id   "cmd"    duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+
      
      // create acceptors, learners and coordinator here.
      // then, startRunning() the acceptor with the given id

      int id = Integer.parseInt(args[0]);

/*
      String configFile = args[1];
      int duration = Integer.parseInt(args[2]);
      
      String gathererAddress = args[3];
      int    gathererPort    = Integer.parseInt(args[4]);
      

      if (configFile.equals("cmd"))
         Process.loadProcesses(Arrays.copyOfRange(args, 5, args.length));
      else
         Process.loadProcesses(configFile);
      
      DataGatherer.setDuration(duration);
//      DataGatherer.enableFileSave("/tmp/eduardo");
//      DataGatherer.enableGathererSend(gathererAddress, gathererPort);
*/

      Process.loadProcesses();
      Acceptor localAcceptor = Acceptor.getAcceptor(id);
      logger.debug("Acceptor {} starting...", localAcceptor.pid);
      localAcceptor.startRunning();
   }
}
