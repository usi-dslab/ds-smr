package ch.usi.dslab.bezerra.ridge.optimistic;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import ch.usi.dslab.bezerra.ridge.RidgeMessage;
import ch.usi.dslab.bezerra.ridge.RidgeMessage.Timestamp;

public class OptimisticMerger implements Runnable {
   OptimisticMergerCallback callback;
   PriorityBlockingQueue<RidgeMessage> pendingMessages;
   Map<Pair<Integer, Integer>, ProcessLatencyEstimator> processesLatencies;
   double waitWindow = 0.010d;
   Semaphore messageSignal;
   Thread optimisticMergerThread;
   boolean running;
   
   Timestamp  highestDeliveredTimestamp = new Timestamp(Timestamp.ZERO);
   AtomicLong numDeliveries = new AtomicLong();
   AtomicLong numInversions = new AtomicLong();
   
   public OptimisticMerger(OptimisticMergerCallback cb) {
      callback = cb;
      running = true;
      pendingMessages = new PriorityBlockingQueue<RidgeMessage>();
      processesLatencies = new ConcurrentHashMap<Pair<Integer, Integer>, ProcessLatencyEstimator>();
      messageSignal = new Semaphore(0);
      optimisticMergerThread = new Thread(this, "OptimisticMerger");
      optimisticMergerThread.start();
   }
   
   public double getInversionRate() {
      double nd = numDeliveries.get();
      double ni = numInversions.get();
      return ni/nd;
   }
   
   public void addMessage(RidgeMessage m) {
      pendingMessages.add(m);
      
      long now = System.currentTimeMillis();
      long latency = now - m.getTimestamp().getTime();
      
      final Pair<Integer, Integer> pid_eid = new ImmutablePair<Integer, Integer>(m.getId().getPid(), m.getEnsembleId());
      ProcessLatencyEstimator ple = processesLatencies.get(pid_eid);
      if (ple == null) {
         ple = new ProcessLatencyEstimator();
         processesLatencies.put(pid_eid, ple);
      }
      ple.addLatency(latency);
      
      double estimatedLatency = ple.getEstimatedLatency();

      if (estimatedLatency > waitWindow) {
         waitWindow = estimatedLatency;
      }
      else {
         double ww = 0.0d;
         for (ProcessLatencyEstimator procAllLatencies : processesLatencies.values()) {
            double procEstimatedLatency = procAllLatencies.getEstimatedLatency();
            if (procEstimatedLatency > ww)
               ww = procEstimatedLatency;
         }
         waitWindow = ww;
      }
      
      signalMessage();
   }
   
   public void stopRunning() {
      running = false;
   }
   
   private void signalMessage() {
      messageSignal.release();
   }

   private void waitForMessageOrTime(long waitTimeMs) {
      try {
         messageSignal.tryAcquire(waitTimeMs, TimeUnit.MILLISECONDS);
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   private long checkPendingMessages() {
      long waitWindowL = Math.round(Math.ceil(waitWindow));
      long timeToNextMessage = 1000l;
      
      long now = System.currentTimeMillis();
      long maxDeliverableTimestamp = now - waitWindowL;
      while (pendingMessages.isEmpty() == false && pendingMessages.peek().getTimestamp().getTime() < maxDeliverableTimestamp) {
         RidgeMessage nextMessage = pendingMessages.poll();
         numDeliveries.incrementAndGet();
         if (nextMessage.getTimestamp().compareTo(highestDeliveredTimestamp) > 0) {
            highestDeliveredTimestamp.copy(nextMessage.getTimestamp());
         }
         else if (nextMessage.getTimestamp().compareTo(highestDeliveredTimestamp) < 0){
            numInversions.incrementAndGet();
         }
         else {
            System.out.println("ALARM! ALARM! REPEATED TIMESTAMP!!!");
            System.exit(1);
         }
         callback.uponOptimisticMerge(nextMessage);
      }
      
      if (pendingMessages.isEmpty() == false) {
         RidgeMessage pendingHead = pendingMessages.peek();
         timeToNextMessage = maxDeliverableTimestamp - pendingHead.getTimestamp().getTime();
      }
      
      return Math.max(1l, Math.min(waitWindowL, timeToNextMessage));
   }
   
   @Override
   public void run() {
      long waitTime = 1000l;
      while(running) {
         waitForMessageOrTime(waitTime);
         waitTime = checkPendingMessages();
      }
   }
}
