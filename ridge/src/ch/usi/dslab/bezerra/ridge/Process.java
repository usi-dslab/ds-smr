/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.*;
import com.google.common.collect.MapMaker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class Process implements Runnable, TCPDestination {
   static Logger logger = LogManager.getLogger("Ridge.Process");

   /*
    *  STATIC MEMBERS (except main method)
    */
   
   static Map<Integer, Process> pidsIndex = new ConcurrentHashMap<Integer, Process>();
   static Map<Message, TCPConnection> messageConnectionMap
         = new MapMaker().weakKeys().weakValues().makeMap();
   int pid;

   /*
   static void loadProcesses(String[] args) {
      int current = 0;
      
      if (args[current].equalsIgnoreCase("ring"))
         Learner.setConfiguration(Learner.RING);
      else if (args[current].equalsIgnoreCase("dynamic"))
         Learner.setConfiguration(Learner.DYNAMIC);
      
      current++;
      
      if (args[current].equalsIgnoreCase("static"))
         Acceptor.setConfiguration(Acceptor.STATIC);
      else if (args[current].equalsIgnoreCase("rotating"))
         Acceptor.setConfiguration(Acceptor.ROTATING);
      
      current++;
      
      int batchCountThreshold = Integer.parseInt(args[current++]);
      int batchTimeThreshold  = Integer.parseInt(args[current++]);
      Batcher.setMessageCountThreshold(batchCountThreshold);
      Batcher.setTimeThreshold(batchTimeThreshold);
      
      while(current < args.length && args[current].equalsIgnoreCase("accseq") == false) {
         String role = args[current++];
         int    pid  = Integer.parseInt(args[current++]);
         int    lid  = -1;
         if (role.equalsIgnoreCase("learner"))
            lid = Integer.parseInt(args[current++]);
         String host = args[current++];
         int    port = Integer.parseInt(args[current++]);
         
         if (role.equalsIgnoreCase("coordinator"))
            new Coordinator(pid, host, port);
         else if (role.equalsIgnoreCase("acceptor"))
            new Acceptor(pid, host, port);
         else if (role.equalsIgnoreCase("learner"))
            new Learner(pid, lid, host, port);
      }
      
      while(current < args.length && args[current].equalsIgnoreCase("accseq")) {
         current++;
         int            seqId             = Integer.parseInt(args[current++]);
         boolean        coordinatorWrites = args[current++].equalsIgnoreCase("coordwrite"); // "nocoordwrite" would mean the opposite, but it could be any string different from "coordwrite"
         List<Acceptor> acceptorSequence  = new ArrayList<Acceptor>();
         
         while (current < args.length && args[current].equalsIgnoreCase("accseq") == false) {
            int acceptorId = Integer.parseInt(args[current++]);
            acceptorSequence.add(Acceptor.getAcceptor(acceptorId));
         }
         
         new AcceptorSequence(seqId, coordinatorWrites, acceptorSequence);
      }
   }
   
   static int getJSInt(JSONObject jsobj, String fieldName) {
      return ((Long) jsobj.get(fieldName)).intValue();
   }
   
   static int getInt(Object obj) {
      return ((Long) obj).intValue();
   }
   
   @SuppressWarnings("unchecked")
   static void loadProcesses(String jsonConfigFileName) {
      JSONParser parser = new JSONParser();
      
      Object nodeObj = null;
      try {
         nodeObj = parser.parse(new FileReader(jsonConfigFileName));
      } catch (IOException | ParseException e) {
         e.printStackTrace();
         System.exit(1);
      }
      JSONObject config = (JSONObject) nodeObj;
      
      String configurationField = (String) config.get("learner_configuration");
      if (configurationField.equalsIgnoreCase("RING"))
         Learner.setConfiguration(Learner.RING);
      else if (configurationField.equalsIgnoreCase("DYNAMIC"))
         Learner.setConfiguration(Learner.DYNAMIC);

      int batchMsgCountThreshold = getJSInt(config, "batch_count_threshold");
      Batcher.setMessageCountThreshold((int) batchMsgCountThreshold);
      int batchTimeThreshold = getJSInt(config, "batch_time_threshold");
      Batcher.setTimeThreshold(batchTimeThreshold);
      
      JSONArray processesArray = (JSONArray) config.get("processes");         
      Iterator<Object> it_process = processesArray.iterator();

      while (it_process.hasNext()) {
         JSONObject jsprocess = (JSONObject) it_process.next();
         
         String role = (String ) jsprocess.get("role");
         int    pid  = getJSInt(jsprocess, "pid");
         String host = (String ) jsprocess.get("host");
         int    port = getJSInt(jsprocess, "port");
         
         if (role.equalsIgnoreCase("coordinator"))
            new Coordinator(pid, host, port);
         else if (role.equalsIgnoreCase("acceptor"))
            new Acceptor(pid, host, port);
         else if (role.equalsIgnoreCase("learner")) {
            int learnerId = getJSInt(jsprocess, "lid");
            new Learner(pid, learnerId, host, port);
         }
      }
      
      JSONArray acceptorSequencesArray = (JSONArray) config.get("acceptor_sequences");         
      Iterator<Object> it_accSeq = acceptorSequencesArray.iterator();
      
      while (it_accSeq.hasNext()) {
         JSONObject jsAcceptorSequence = (JSONObject) it_accSeq.next();
         
         int seqId = getJSInt(jsAcceptorSequence, "id");
         boolean coordinatorWrites = (Boolean) jsAcceptorSequence.get("coordinator_writes");
         JSONArray jsAcceptorIds = (JSONArray) jsAcceptorSequence.get("acceptors");
         
         Iterator<Object> it_acc = jsAcceptorIds.iterator();
         List<Acceptor> acceptorSequence = new ArrayList<Acceptor>();
         while (it_acc.hasNext()) {
            int acceptorId = getInt(it_acc.next());
            acceptorSequence.add(Acceptor.getAcceptor(acceptorId));
         }
         new AcceptorSequence(seqId, coordinatorWrites, acceptorSequence);
      }
      
   }
   */
   String address;
   int port;
   boolean running = false;
   
   
   /*
    *  INSTANCE MEMBERS
    */
   Thread processThread = null;
   TCPReceiver tcpReceiver = null;
   TCPSender   tcpSender   = null;
   boolean listenForConnections;
   Process(int pid) {
      this (pid, null, -1);
      listenForConnections = false;
   }

   Process(int pid, String address, int port) {
      this.pid = pid;
      this.address = address;
      this.port = port;
      pidsIndex.put(pid, this);
      listenForConnections = true;
   }

   @SuppressWarnings("unused")
   static void loadProcesses() {
      final int NUM_ENSEMBLES = 2;
      final int NUM_ACCEPTORS = 3;
      final int NUM_LEARNERS  = 2;

      for (int ensembleId = 0 ; ensembleId < NUM_ENSEMBLES ; ensembleId++) {
         Ensemble ensemble = Ensemble.getOrCreateEnsemble(ensembleId);
         int coord_id = ensembleId*(NUM_ACCEPTORS + NUM_LEARNERS);
         Coordinator coord = new Coordinator(coord_id, "localhost", 50000 + coord_id);
         ensemble.setCoordinator(coord);
         coord.setEnsemble(ensemble);

         List<Acceptor> accList = new ArrayList<Acceptor>();
         accList.add(coord);
         for (int i = 1 + coord_id ; i < NUM_ACCEPTORS + coord_id ; i++) {
            Acceptor acc  = new Acceptor (i, "localhost", 50000 + i);
            accList.add(acc);
         }
         AcceptorSequence accseq = new AcceptorSequence(ensembleId, ensembleId, true, accList);
         for (int i = NUM_ACCEPTORS + coord_id ; i < NUM_ACCEPTORS + NUM_LEARNERS + coord_id ; i++) {
            Learner learner_i = new Learner(i, "localhost", 50000 + i);
            learner_i.subscribeToEnsemble(ensemble);
         }
      }

      int mlId = NUM_ENSEMBLES*(NUM_ACCEPTORS + NUM_LEARNERS);
      Learner multiLearner = new Learner(mlId, "localhost", 50000 + mlId);
      for (int rid = 0 ; rid < NUM_ENSEMBLES ; rid++)
         multiLearner.subscribeToEnsemble(rid);
   }
   
   public static Process getProcess(int pid) {
      return pidsIndex.get(pid);
   }

   public int getPid() {
      return pid;
   }
   
   public void startRunning() {
      tcpReceiver = listenForConnections ? new TCPReceiver(port) : new TCPReceiver();
      tcpSender = new TCPSender();
      running = true;
      processThread = new Thread(this, "Process");
      processThread.start();
      if (this instanceof Client) {
         createClientConnections();
      }
      else {
         createProcessesConnections();
      }
   }
   
   public void createClientConnections() {
      // once done loading the processes, start a thread here that will keep trying to connect to each
      // learner/coordinator. exceptions are likely to be thrown, as processes start at different times, but keep
      // trying, until the client is connected to all coordinators (TODO: to all learners, in case of fast opt).

      // this is sub-optimal though. ideally, a central coordinator (e.g., ZooKeeper, ZooFence, Volery...)
      // would be used. but that would be an over-optimization, done only if this library is ever published.

      for (Process other : pidsIndex.values()) {
         if (other != this && ( /* other instanceof Learner || */ other instanceof Coordinator)) {
            connect(other);
         }
      }
   }

   public void createProcessesConnections() {
      // once done loading the processes, start a thread here that will keep trying to connect to each
      // process. exceptions are likely to be thrown, as processes start at different times, but keep
      // trying, until the whole mesh of learners and acceptors is connected.

      // this is sub-optimal though. ideally, a central coordinator (e.g., ZooKeeper, ZooFence, Volery...)
      // would be used. but that would be an over-optimization, done only if this library is ever published.

      for (Process other : pidsIndex.values()) {
         if (other != this && (other instanceof Client) == false) {
//            TCPConnection connection = connect(other);
//            tcpReceiver.addConnection(connection);
            connect(other);
         }
      }
   }

//   public TCPConnection connect(TCPDestination d) {
   public void connect(TCPDestination d) {
      TCPConnection newConnection = null;
      boolean connected = false;
      while (connected == false) {
         connected = true;
         try {
            newConnection = tcpSender.connect(d);
         }
         catch (IOException e) {
            try {
               logger.debug("Destination {}:{} refused connection, retrying...", d.getAddress(), d.getPort());
               connected = false;
               Thread.sleep(1000);
            }
            catch (InterruptedException ee) {
               ee.printStackTrace();
               System.exit(1);
            }
         }
      }
      // return newConnection
      tcpReceiver.addConnection(newConnection);
   }

   void stop() {
      running = false;
      tcpReceiver.stop();
      tcpSender.stop();
      logger.debug("Must have stopped both process and tcpreceiver...");
   }
   
   @Override
   public void run() {
      try {
         while (running) {
            TCPMessage newTcpMsg = tcpReceiver.receive(1000);
            if (newTcpMsg == null)
               continue;
            else {
               TCPConnection connection = newTcpMsg.getConnection();
               Message       contents   = newTcpMsg.getContents();
               contents.rewind();
               messageConnectionMap.put(contents, connection);
               uponDelivery(contents);
            }
         }
         logger.debug("Exiting process received messages loop...");
      }
      catch (Exception e) {
         e.printStackTrace();
         System.exit(1);
      }
   }

   abstract void uponDelivery (Message m);

   TCPConnection getConnection(Message m) {
      return messageConnectionMap.get(m);
   }
   
   @Override
   public String getAddress() {
      return address;
   }

   @Override
   public int getPort() {
      return port;
   }
   
   public void send (Message m, TCPDestination p) {
      tcpSender.send(m, p);
   }
   
   public void send (Message m, TCPConnection c) {
      tcpSender.send(m, c);
   }
   
   public void send (Message m, List<TCPDestination> lp) {
      tcpSender.multiDestinationSend(m, lp);
   }

   @Override
   public int hashCode() {
      return pid;
   }
   
}
