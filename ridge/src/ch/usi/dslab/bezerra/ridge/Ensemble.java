/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.ridge.RidgeMessage.Timestamp;

public class Ensemble {
   Logger logger = LogManager.getLogger("Ensemble");
   
   public static final int RING    = 0;
   public static final int DYNAMIC = 1;
   
   static Map<Integer, Ensemble> allEnsembles = new ConcurrentHashMap<Integer, Ensemble>();
   
   LearnerLoadBalancer balancer;
   
   public static Ensemble getEnsemble(int ensembleId) {
      return allEnsembles.get(ensembleId);
   }
   
   public static Ensemble getOrCreateEnsemble(int ensembleId) {
      if (allEnsembles.containsKey(ensembleId))
         return getEnsemble(ensembleId);
      else
         return new Ensemble(ensembleId);
   }
   
   public static void createEnsemble(int ensembleId) {
      if (allEnsembles.containsKey(ensembleId))
         return;
      else
         new Ensemble(ensembleId);
   }
   
   public static List<Learner> getIntersectingLearners(List<Ensemble> ensembles) {
      List<Learner> retval = new ArrayList<Learner>(ensembles.get(0).getLearners());
      for (Ensemble r : ensembles)
         retval.retainAll(r.getLearners());
      Collections.sort(retval, new Comparator<Learner>() {
         public int compare(Learner l1, Learner l2) {
            return l1.pid - l2.pid;
         }
      });
      return retval;
   }

   public static List<Learner> getIntersectingLearners(Ensemble... ensembles) {
      return getIntersectingLearners(Arrays.asList(ensembles));
   }
   
   
   
   
   
   
   
   List<Learner> learnersList = new ArrayList<Learner>();
   int configuration = DYNAMIC;
   
   int         ensembleId;
   Coordinator coordinator;
   
   long nextExpectedInstance  = 0;
   Timestamp highestReadyTimestamp = Timestamp.ZERO;
   Timestamp lastCollectedTimestamp = Timestamp.ZERO;
   // instanceId -> Message
   TreeMap<Long, RidgeMessage> ensembleBatches = new TreeMap<Long, RidgeMessage>();
   List<RidgeMessage>          readyMessages   = new LinkedList<RidgeMessage>();
   
   Map<Integer, AcceptorSequence> allSequences = new ConcurrentHashMap<Integer, AcceptorSequence>();

   private Ensemble(int ensembleId) {
      this.ensembleId = ensembleId;
      allEnsembles.put(ensembleId, this);
      balancer = new LearnerLoadBalancer();
   }
   
   public void setConfiguration(int configuration) {
      this.configuration = configuration;
   }
   
   public void setCoordinator(Coordinator coordinator) {
      this.coordinator = coordinator;
   }
   
   public Coordinator getCoordinator() {
      return coordinator;
   }
   
   public void addLearner(Learner l) {
      if (learnersList.contains(l) == false)
         learnersList.add(l);
      Comparator<Learner> learnerComparator = new Comparator<Learner>() {
         @Override
         public int compare(Learner o1, Learner o2) {
            return o1.pid - o2.pid;
         }
      };
      Collections.sort(learnersList, learnerComparator);
   }
   
   public List<Learner> getLearners() {
      return learnersList;
   }
   
   public Learner getLearnerAt(int pos) {
      return learnersList.get(pos);
   }
   
   // MUST be consistent among learners of each ensemble:
   // - all learners must have the same view of the learnersList arraylist!
   public int getLearnerPosition(Learner l) {
      return learnersList.indexOf(l);
   }

   public Learner getNextLearner(Learner l) {
      int nextPosition = learnersList.indexOf(l) + 1;
      if (nextPosition < learnersList.size())
         return learnersList.get(nextPosition);
      else
         return null;
   }
   
   public void addAcceptorSequence(AcceptorSequence as) {
      allSequences.put(as.id, as);
   }
   
   public AcceptorSequence getAcceptorSequence(long instanceId) {
      AcceptorSequence as = allSequences.get(instanceId % allSequences.size());
      if (as != null)
         return as;
      else
         return new ArrayList<AcceptorSequence>(allSequences.values()).get(0);
   }
   
   public Learner getAcceptorContact(RidgeMessage m) {
      if (configuration == RING) {
         return learnersList.get(0);
      }
      else if (configuration == DYNAMIC) {
         // return getBroadcaster(m.instanceId);
         return balancer.setBroadcastingLearner(m, this);
      }
      return null;
   }
   
   public Learner getBroadcaster(long parameter) {
      int bcastLearnerIndex = (int)(parameter % learnersList.size());
      return learnersList.get(bcastLearnerIndex);
   }
   
   synchronized void addReceivedBatch(Long instanceId, RidgeMessage batch) {

      // DEBUG
//      long now = System.currentTimeMillis();
//      for (int i = 0 ; i < batch.getContents().size() ; i++) {
//         RidgeMessage item = (RidgeMessage) batch.getItem(i);
//         item.t_learner_received = now;
//         if (item.messageType != RidgeMessage.NULL_MESSAGE) {
//            System.out.println(String.format("Adding instance %d to ensemble %d; " +
//            "expecting: %d; " +
//            "readyMessages: %d", instanceId, batch.ensembleId, nextExpectedInstance, readyMessages.size()));
//         }
//      }
      // =====
      
      ensembleBatches.put(instanceId, batch);
      
      while(ensembleBatches.isEmpty() == false && ensembleBatches.firstKey() == nextExpectedInstance) {
         nextExpectedInstance++;

         RidgeMessage readyBatch = ensembleBatches.pollFirstEntry().getValue();

         readyBatch.rewind();
         while(readyBatch.hasNext()) {
            // every message in the batch is packed/stripped of its contents
            RidgeMessage readyMessage = (RidgeMessage) readyBatch.getNext();
            logger.info("just got a message from the batch");
            readyMessage.unpackContents();
            readyMessages.add(readyMessage);
            // assume messages in batch are sorted in timestamp order
            highestReadyTimestamp = readyMessage.timestamp;
         }

      }
   }

   synchronized boolean hasReadyMessage() {
      return readyMessages.isEmpty() == false;
   }

   synchronized RidgeMessage getNextReadyMessage() {
      if (readyMessages.isEmpty()) {
         logger.debug("readyMessages isEmpty. returning null");
         return null;
      }
      if (readyMessages.get(0) == null)
         logger.error("readyMessages.get(0) == null");
      return readyMessages.remove(0);
   }
   
   synchronized RidgeMessage peekNextReadyMessage() {
      if (readyMessages.isEmpty()) {
         logger.error("readyMessages isEmpty. returnin null");
         return null;
      }
      return readyMessages.get(0);
   }
   
   Timestamp getLastCollectedTimestamp() {
      return lastCollectedTimestamp;
   }
   
   void setLastCollectedTimestamp(Timestamp ts) {
      lastCollectedTimestamp = ts;
   }

}