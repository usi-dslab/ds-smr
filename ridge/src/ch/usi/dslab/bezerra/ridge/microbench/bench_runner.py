#!/usr/bin/python
import shlex
import subprocess

def sshcmdbg(node, cmdstring) :
    run_args = shlex.split("ssh " + node + " " + cmdstring)
    print("xXx ssh " + node + " " + cmdstring)
    return subprocess.Popen(run_args)

nodes = range(22,61) # range(a,b) = [a, ..., b-1]
deadnodes = range(17,22) + [75] # [17, 18, 19, 20, 21, 75]
living_nodes = [ node for node in nodes if node not in deadnodes ]

# these should be turned into parameters given to this script
num_acceptors = 3
num_learners  = 14
num_clients   = 120
duration      = 60
vbcastmode    = "dynamic" # ring/dynamic
quorummode    = "static" # static/rotating
batch_count   = 30
batch_time    = 50
base_port     = 50000
gatherer_port = 40000


# objective: take a sequence of nodes.
#                 the first     nA will be acceptors
#                 the following nL will be learners(servers)
#                 and the last  nC will be clients
# value broadcast mode (ring/dynamic) also is given as parameter
# rotating quorum (true/false) is also given as parameter




#################################################################################
#
# CREATE (STATIC) SYSTEM VIEW
#
#################################################################################

datagatherer = living_nodes[ 0 ]
acceptors    = living_nodes[ 1                                : num_acceptors + 1                ]
learners     = living_nodes[ 1 + num_acceptors                : num_acceptors + num_learners + 1 ]
client_nodes = living_nodes[ 1 + num_acceptors + num_learners :                                  ]

print("datagatherer = " + str(datagatherer))
print("acceptors    = " + str(acceptors   ))
print("learners     = " + str(learners    ))
print("client_nodes = " + str(client_nodes))

# Creating string with processes data to be given to each process as a parameter
# gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+

# data gatherer info
system_info_string = "node" + str(datagatherer) + " " + str(gatherer_port) + " " + vbcastmode + " " + quorummode + " " + str(batch_count) + " " + str(batch_time) + " "

# coordinator
pid = 0
host = "node" + str(acceptors[0])
port = base_port + pid
system_info_string += "coordinator 0 " + host + " " + str(port) + " "

# acceptors
for nid in acceptors[1:] :
    pid += 1
    host = "node" + str(nid)
    port = base_port + pid
    system_info_string += "acceptor " + str(pid) + " " + host + " " + str(port) + " "

# learners
lid = -1
for nid in learners :
    pid += 1
    lid += 1
    host = "node" + str(nid)
    port = base_port + pid
    system_info_string += "learner " + str(pid) + " " + str(lid) + " " + host + " " + str(port) + " "

# a single acceptor sequence (this can be further optimized later)
system_info_string += "accseq 0 coordwrite "
for accid in range(1 + len(acceptors) // 2) :
    system_info_string += str(accid) + " "

# print("System info: " + system_info_string)


#################################################################################
#
# COMMANDS TO PROCESSES
#
#################################################################################


# coordinator: <command>              "cmd" duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+
# acceptor   : <command> id           "cmd" duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+
# learner    : <command> pid          "cmd" duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+
# client     : <command> id host port "cmd" duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+

# send the command to the gatherer, then to the coordinator, then to the acceptors, then to the learners, then to the clients

java_binary = "java -XX:+UseG1GC"

# DataGatherer
# <command> gatherer_port  <directory> {<resource> <nodetype> <count>}+
node="node" + str(datagatherer)
command  = java_binary + " -cp $HOME/sense/target/libsense-git.jar "
command += "ch.usi.dslab.bezerra.sense.DataGatherer " + str(gatherer_port) + " /tmp/eduardo/logs "
command += "throughput learner " + str(num_learners) + " "
command += "latency    client  " + str(num_clients )
sshcmdbg(node, command)


# Coordinator
# <command> "cmd" duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+
node="node" + str(acceptors[0])
command  = java_binary + " -cp $HOME/dynrp/target/dynrp-git.jar "
command += "ch.usi.dslab.bezerra.ridge.Coordinator cmd " + str(duration) + " "
command += system_info_string
sshcmdbg(node, command)

# Acceptors
# <command> pid "cmd" duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+
pid = 0
for accnode in acceptors[1:] :
    pid += 1
    node="node" + str(accnode)
    command  = java_binary + " -cp $HOME/dynrp/target/dynrp-git.jar "
    command += "ch.usi.dslab.bezerra.ridge.Acceptor " + str(pid) + " cmd " + str(duration) + " "
    command += system_info_string
    sshcmdbg(node, command)

# Learners
# <command> pid "cmd" duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+
for lenode in learners :
    pid += 1
    node="node" + str(lenode)
    command  = java_binary + " -cp $HOME/dynrp/target/dynrp-git.jar "
    command += "ch.usi.dslab.bezerra.ridge.Learner " + str(pid) + " cmd " + str(duration) + " "
    command += system_info_string
    sshcmdbg(node, command)

# Clients
# <command> pid host port "cmd" duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+
# for now, have one client per cli_node
for client_id_bigloop in range(0, num_clients, len(client_nodes)) :
    client_count = client_id_bigloop
    for clinode in client_nodes :
        ##################################
        # extra condition for the for loop
        client_count+=1
        if client_count > num_clients : break
        ##################################
        pid += 1
        node="node" + str(clinode)
        port=base_port + pid
        command  = java_binary + " -cp $HOME/dynrp/target/dynrp-git.jar "
        command += "ch.usi.dslab.bezerra.ridge.Client " + str(pid) + " " + node + " " + str(port) + " cmd " + str(duration) + " "
        command += system_info_string
        sshcmdbg(node, command)


















