/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.ridge.RidgeMessage.MessageIdentifier;
import ch.usi.dslab.bezerra.ridge.RidgeMessage.Timestamp;
import ch.usi.dslab.bezerra.ridge.optimistic.OptimisticMerger;
import ch.usi.dslab.bezerra.ridge.optimistic.OptimisticMergerCallback;


public class Coordinator extends Acceptor {
   
   public static class Batcher extends Thread implements OptimisticMergerCallback {
      
      static int  messageSizeThreshold = 0; // 30k if 1k messages are assumed
      static long timeThreshold        = 1;
      
      public static void setMessageSizeThreshold(int value) {
         Batcher.messageSizeThreshold = value;
      }
      
      public static void setTimeThreshold(int value) {
         Batcher.timeThreshold = value;
      }
      
      long lastBatchTime = 0l;
      Timestamp lastAppMessageTimestamp = Timestamp.ZERO;
      boolean running = true;
      long instanceId = 0;
      Coordinator coordinator;
      OptimisticMerger optMerger;
      BlockingQueue<RidgeMessage> pendingMessages    = new LinkedBlockingQueue<RidgeMessage>();
      PriorityBlockingQueue<RidgeMessage> orderedBatch = new PriorityBlockingQueue<RidgeMessage>();
      
      RidgeMessage currentBatch;
      long currentBatchSize = 0l;
      
      Logger logger = LogManager.getLogger("Batcher");
      
      Batcher(Coordinator c) {
         super("Batcher");
         this.coordinator = c;
         currentBatch = createNewEmptyBatch();
         if (MulticastAgent.fastDelivery == true)
        	 optMerger = new OptimisticMerger(this);
      }
      
      void startRunning() {
         this.start();
      }
      
      RidgeMessage createNewEmptyBatch() {
         MessageIdentifier id = MessageIdentifier.getNextMessageId(coordinator.pid);
         Timestamp         ts = Timestamp.createUniqueTimestamp(coordinator.pid);
         return new RidgeMessage(id, RidgeMessage.DECISION_BATCH, coordinator.ensembleId, instanceId++, ts);
      }
      
      public Timestamp getLastAppMessageTime() {
         return lastAppMessageTimestamp;
      }
      
      public void addMessage(RidgeMessage m) {
         logger.info("Received message for batching");
         
         // DEBUG
//         m.t_coord_recv = System.currentTimeMillis();
         // =====
         if (MulticastAgent.fastDelivery == true) {
            optMerger.addMessage(m);
         }
         else {
        	pendingMessages.add(m); 
         }
      }
      
      @Override
      public void uponOptimisticMerge(RidgeMessage m) {
//         m.t_coord_opt_merge = System.currentTimeMillis();
         pendingMessages.add(m);
      }
      
      public void stopRunning() {
         running = false;
      }
      
      @Override
      public void run() {
         while (running) {
            try {
               RidgeMessage next = pendingMessages.poll(timeThreshold, TimeUnit.MILLISECONDS);
               if (next != null) {
                  next.ensembleId = currentBatch.ensembleId;
                  next.instanceId = currentBatch.instanceId;
                  
                  int packSize = next.packContents();
                  currentBatchSize += packSize;
                  orderedBatch.add(next);
               }
               if (orderedBatch.size() == 0)
                  continue;
               long now = System.currentTimeMillis();
               if (currentBatchSize >= messageSizeThreshold || now > lastBatchTime + timeThreshold) {

                  // reorder messages in initial timestamp order, to increase likelihood of the optimistic
                  // order matching the conservative order
                  while(orderedBatch.isEmpty() == false) {
                     RidgeMessage head = orderedBatch.take();
                     
                     // DEBUG
//                     head.t_batch_ready = now;
                     // =====
                     
                     if (head.timestamp.compareTo(lastAppMessageTimestamp) <= 0)
                        head.timestamp.makeBiggerThan(lastAppMessageTimestamp);
                     lastAppMessageTimestamp.copy(head.timestamp);
                     currentBatch.addItems(head);
                  }
                  currentBatch.setBatchSize(currentBatchSize);
                  
                  coordinator.uponBatchReady(currentBatch);
                  logger.info("Creating new batch...");
                  currentBatch = createNewEmptyBatch();
                  lastBatchTime = now;
                  currentBatchSize = 0l;
               }
            } catch (InterruptedException e) {
               e.printStackTrace();
               System.exit(1);
            }
         }
      }
   }
   
   public static class NullMessageSender extends Thread {
      static long delta_ms = 500; //ms
      
      public static void setDelta(long delta_ms) {
         NullMessageSender.delta_ms = delta_ms;
      }
      
      public static long getDelta() {
         return delta_ms;
      }

      Coordinator coordinator;
      Batcher     batcher;
      
      boolean running = true;
      
      public NullMessageSender(Coordinator coordinator, Batcher batcher) {
         super("NullMessageSender");
         this.coordinator = coordinator;
         this.batcher     = batcher;
      }
      
      public void startRunning() {
         this.start();
      }
      
      public void stopRunning() {
         running = false;
      }
      
      public void run() {
         long waitTime = delta_ms;
         try {
            while (running) {
               // get time of last received message and waits at most for
               // delta_ms to add something to the batcher queue
               Thread.sleep(waitTime);
//               final long now = System.currentTimeMillis();
//               final Timestamp lastAppMessage = batcher.getLastAppMessageTime();
//               long elapsedTime = now - lastAppMessage.time;
//               if (elapsedTime > delta_ms) {
                  Timestamp nowTS = Timestamp.createUniqueTimestamp(coordinator.pid);
                  if (nowTS.compareTo(batcher.lastAppMessageTimestamp) <= 0)
                     nowTS.makeBiggerThan(batcher.lastAppMessageTimestamp);
                  RidgeMessage nullMessage = new RidgeMessage(RidgeMessage.NULL_MESSAGE, coordinator.ensembleId, -1, nowTS);
                  batcher.addMessage(nullMessage);
//                  batcher.addMessage(        new RidgeMessage(RidgeMessage.NULL_MESSAGE, coordinator.ensembleId, -1, now, coordinator.pid));
//                  elapsedTime = 0;
//               }
//               waitTime = delta_ms - elapsedTime;
            }
         }
         catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
         }
      }
   }
   
   Batcher batcher = null;
   NullMessageSender nullSender = null;
   int ensembleId = 0;
   
   static Logger logger = LogManager.getLogger("Coordinator");

   public Coordinator(int id, String address, int port) {
      super(id, address, port, true);
   }
   
   public static Coordinator getCoordinatorByPid(int pid) {
      return (Coordinator) Process.getProcess(pid);
   }
   
   public static Coordinator getCoordinatorByEnsemble(int rid) {
      return Ensemble.getEnsemble(rid).coordinator;
   }
   
   public static Coordinator getCoordinatorByEnsemble(Ensemble r) {
      return r.coordinator;
   }
   
   public void setEnsemble(Ensemble e) {
      ensembleId = e.ensembleId;
   }
   
   @Override
   public void startRunning() {
      batcher = new Batcher(this);
      nullSender = new NullMessageSender(this, batcher);
      super.startRunning();
      batcher.startRunning();
      nullSender.startRunning();
   }
   
   public void stopRunning() {
      batcher.stopRunning();
      nullSender.stopRunning();
      running = false;
   }
   
   @Override
   void uponDelivery(Message m_) {
      RidgeMessage m = (RidgeMessage) m_;
      logger.debug("Received messaged from client...");
      batcher.addMessage(m);
   }
   
   void uponBatchReady(RidgeMessage batch) {
      super.uponDelivery(batch);
   }
   
   public static void main(String[] args) {
      
      // <command> configFile duration gatherer_address gatherer_port
      // if configFile=="cmd"
      // <command>   "cmd"    duration gatherer_address gatherer_port ring/dynamic static/rotating batch_count batch_time {role pid [lid] host port}+ {accseq id coordwrite/nocoordwrite acc1 acc2 ... accn}+

      int id = Integer.parseInt(args[0]);
      
/*
      String configFile = args[0];
      int duration = Integer.parseInt(args[1]);
      
      String gathererAddress = args[2];
      int    gathererPort    = Integer.parseInt(args[3]);
      
      if (configFile.equals("cmd"))
         Process.loadProcesses(Arrays.copyOfRange(args, 4, args.length));
      else
         Process.loadProcesses(configFile);
      DataGatherer.setDuration(duration);
//      DataGatherer.enableFileSave("/tmp/eduardo");
//      DataGatherer.enableGathererSend(gathererAddress, gathererPort);
*/

      Process.loadProcesses();
      Coordinator coord = Coordinator.getCoordinatorByPid(id);
      logger.info("Coordinator starting...");
      coord.startRunning();
      
   }

}
