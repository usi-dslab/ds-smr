/*

 Ridge - A high-throughput, low-latency amcast library
 Copyright (C) 2014, University of Lugano
 
 This file is part of Ridge.
 
 Ridge is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.ridge;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

import ch.usi.dslab.bezerra.netwrapper.Message;

public class RidgeMessage extends Message implements Cloneable, Comparable<RidgeMessage> {
   
   public static class MessageIdentifier implements Serializable, Comparable<MessageIdentifier> {
      private static final long serialVersionUID = -4226088093920753379L;
      
      private static AtomicLong lastSeq = new AtomicLong(0);
      
      public static MessageIdentifier getNextMessageId(int pid) {
         return new MessageIdentifier(lastSeq.incrementAndGet(), pid);
      }
      
      public static void setSeq(long value) {
         MessageIdentifier.lastSeq.set(value);
      }
      
      long sequence;
      int pid;
      
      public MessageIdentifier(){}
      
      public MessageIdentifier(long sequence, int pid) {
         this.sequence = sequence;
         this.pid      = pid;
      }
      
      public int getPid() {
         return pid;
      }
      
      public long getSequenceNumber() {
         return sequence;
      }

      @Override
      public int compareTo(MessageIdentifier id2) {
         if (this.sequence < id2.sequence)
            return -1;
         else if (this.sequence > id2.sequence)
            return 1;
         else if (this.pid < id2.pid)
            return -1;
         else if (this.pid > id2.pid)
            return 1;
         else
            return 0;
      }
      
      @Override
      public int hashCode() {
         return ((int) sequence << 16) ^ ((int) pid);
      }
      
      @Override
      public boolean equals(Object objOther) {
         MessageIdentifier other = (MessageIdentifier) objOther;
         return this.sequence == other.sequence && this.pid == other.pid;
      }
      
      @Override
      public String toString() {
         return String.format("%d.%d", sequence, pid);
      }
   }
   
   public static class Timestamp implements Serializable, Comparable<Timestamp> {
      private static final long serialVersionUID = 6661304834979837852L;
      
      long time;
      long  seq;
      int   pid;
      long seq2; // if a process creates two messages very fast, they still must have different timestamps

      public static final Timestamp ZERO      = new Timestamp(0l, 0l, 0, 0l);
      public static final Timestamp MAX_VALUE = new Timestamp(Long.MAX_VALUE, Long.MAX_VALUE, Integer.MAX_VALUE, Long.MAX_VALUE);

      private static AtomicLong lastSeq2      = new AtomicLong(0l);

      public static Timestamp min(Timestamp ts1, Timestamp ts2) {
         return (ts1.compareTo(ts2) < 0) ? (ts1) : (ts2); 
      }

      public Timestamp(){}

      public Timestamp(Timestamp other) {
         this(other.time, other.seq, other.pid, other.seq2);
      }

      public Timestamp (long time, int pid) {
         this(time, 0, pid, lastSeq2.incrementAndGet());
      }

      public Timestamp (long time, long seq, int pid, long seq2) {
         this.time = time;
         this.seq  = seq;
         this.pid  = pid;
         this.seq2 = seq2;
      }
      
      public static Timestamp createUniqueTimestamp(int pid) {
         return new Timestamp(System.currentTimeMillis(), pid);
      }

      public void copy(Timestamp other) {
         this.time = other.time;
         this.seq  = other.seq;
         this.pid  = other.pid;
         this.seq2 = other.seq2;
      }
      
      public long getTime() {
         return time;
      }
      
      @Override
      public int compareTo(Timestamp ts2) {
         if      (this.time < ts2.time)
            return -1;
         else if (this.time > ts2.time)
            return  1;
         else if (this.seq  < ts2.seq)
            return -1;
         else if (this.seq  > ts2.seq)
            return  1;
         else if (this.pid  < ts2.pid)
            return -1;
         else if (this.pid  > ts2.pid)
            return  1;
         else if (this.seq2 < ts2.seq2)
            return -1;
         else if (this.seq2 > ts2.seq2)
            return  1;
         else
            return  0;
      }

      public void makeBiggerThan(Timestamp other) {
         time = other.time;
         seq  = other.seq + 1;
      }

      @Override
      public String toString() {
         return String.format("%d.%d.%d.%d", time, seq, pid, seq2);
      }
   }   

   private static final long serialVersionUID = -7856340421664925217L;

   public static final int NULL_MESSAGE       = 0;
   public static final int DECISION_BATCH     = 2;
   public static final int CLIENT_CREDENTIALS = 3;
   public static final int MESSAGE_MULTICAST  = 4;

   int  messageType = 0;
   int  ensembleId  = 0;
   long instanceId  = 0;
   long batchsize   = 0;
   int  broadcastingLearnerId = -1;
   Timestamp         timestamp  = null;
   MessageIdentifier id         = null;
   

   public RidgeMessage() {}
   
   public static RidgeMessage getShallowCopy(RidgeMessage other) {
      RidgeMessage copy = null;
      try {
         copy = (RidgeMessage) other.clone();
      } catch (CloneNotSupportedException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return copy;
   }

   public RidgeMessage(MessageIdentifier id, int messageType, int ensembleId, long instanceId, Timestamp timestamp, Object... objs) {
      super(objs);
      this.id = id;
      this.messageType = messageType;
      this.ensembleId  = ensembleId;
      this.instanceId  = instanceId;
      this.timestamp   = timestamp;
   }
   
   public RidgeMessage(int messageType, int ensembleId, long instanceId, Timestamp timestamp, Object... objs) {
      this(MessageIdentifier.getNextMessageId(timestamp.pid), messageType, ensembleId, instanceId, timestamp, objs);
   }
   
   public static RidgeMessage createClientRequestTSNow(int cliPid, Object... objs) {
      MessageIdentifier id = MessageIdentifier.getNextMessageId(cliPid);
      Timestamp ts = Timestamp.createUniqueTimestamp(cliPid);
      
      //                                                        {Message objs}
      return new RidgeMessage(id, MESSAGE_MULTICAST, -1, -1, ts, cliPid, objs);
   }
   
   public MessageIdentifier getId() {
      return id; 
   }
   
   public int getEnsembleId() {
      return ensembleId;
   }
   
   public long getInstanceId() {
      return instanceId;
   }
   
   public Timestamp getTimestamp() {
      return new Timestamp(timestamp);
   }
   
   public void setTimestamp(Timestamp ts) {
      this.timestamp = ts;
   }

   @Override
   public int compareTo(RidgeMessage other) {
      return this.timestamp.compareTo(other.timestamp);
   }
   
   public boolean isNull() {
      return this.messageType == RidgeMessage.NULL_MESSAGE;
   }
   
   public long getBatchSize() {
      // must have been set!
      return batchsize;
   }
   
   public void setBatchSize(long size) {
      batchsize = size;
   }
   
   public int getBroadcastingLearnerId() {
      return broadcastingLearnerId;
   }
   
   public void setBroadcastingLearnerId(int id) {
      broadcastingLearnerId = id;
   }

}
