#!/usr/bin/python

import inspect
import os


def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

#
# if len(sys.argv) not in [2,4,6] :
#     print " usage: " + sys.argv[0] + " <ridge/urp> [-c config_file] [-p num_permits]"
#     sys.exit()

client_id = "12345 "
config_file = script_dir() + "/generatedSysConfig.json "
num_permits = "1 "
#
# for i in [2,4] :
#     if len(sys.argv) > i:
#         if sys.argv[i] == "-c" :
#             config_file = sys.argv[i+1] + " "
#         if sys.argv[i] == "-p" :
#             num_permits = sys.argv[i+1] + " "

java_bin = "java -XX:+UseG1GC -cp "
app_classpath = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/target/chitter-git.jar "
client_class = "ch.usi.dslab.lel.chitter.chitterClient "
partitioning_file = script_dir() + "/generatedPartitionsConfig.json "

client_cmd = java_bin + app_classpath + client_class + client_id + config_file + partitioning_file + num_permits
print "client_cmd = " + client_cmd
os.system(client_cmd)
