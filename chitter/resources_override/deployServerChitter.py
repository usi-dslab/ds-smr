#!/usr/bin/python

import inspect
import os
import sys
import json
import threading

import benchCommon
from benchCommon import sarg

#./deployServerChitter.py /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/minimal_system_config.json /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/minimal_partitioning.json /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/socialNetworksInterestLinked/users_1000_partitions_2.json localhost 60000 90

class launcherThread(threading.Thread):
    def __init__(self, clist):
        threading.Thread.__init__(self)
        self.cmdList = clist

    def run(self):
        #         sleep(1)
        for cmd in self.cmdList:
            print ".-rRr-. executing: " + cmd["cmdstring"]
            #             benchCommon.freePort(cmd["node"], cmd["port"])
            benchCommon.sshcmdbg(cmd["node"], cmd["cmdstring"])
            # "sudo fuser -k " + str(cmd["port"]) + "/tcp ; sleep 5; " + cmd["cmdstring"])


def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


if len(sys.argv) != 10:
    print "usage: " + sarg(
        0) + " <config_file> <partitioning_file> <social_network_file> <gatherer_host> <gatherer_port> <gatherer_dir> <gatherer_duration> <gatherer_Warmup> <legacySSMR>"
    sys.exit(1)

# server_id = iarg(1)
# partition_id = iarg(2)
config_file = sarg(1)
partitioning_file = sarg(2)
social_network_file = sarg(3)
gatherer_host = sarg(4)
gatherer_port = benchCommon.iarg(5)
gatherer_dir = sarg(6)
gatherer_duration = benchCommon.iarg(7)
gatherer_warmup = benchCommon.iarg(8)
legacySSMR = sarg(9)

config_stream = open(config_file)
partition_stream = open(partitioning_file)
config = json.load(config_stream)
partition = json.load(partition_stream)


# debug_port = str(40000 + int(server_id))
# debug_server_str = " -agentlib:jdwp=transport=dt_socket,address=127.0.0.1:" + debug_port + ",server=y,suspend=n "

#   int    serverId = Integer.parseInt(args[0]);
#   int    partitionId = Integer.parseInt(args[1]);
#   String systemConfigFile = args[2];
#   String partitionsConfigFile = args[3];
#   String socialNetworkFile = args[4];

# launchNodeCmdString = [benchCommon.javabin, benchCommon.javacp, benchCommon.javachitterServerClass, server_id]
# launchNodeCmdString += [partition_id, config_file, partitioning_file, social_network_file]
# serverCmdString = " ".join([str(val) for val in launchNodeCmdString])
# benchCommon.sshcmd(serverCmdString)


cmdList = []


def getHostType(host):
    for group in partition["partitions"]:
        if host["pid"] in group["servers"]:
            return group["type"]


for member in config["group_members"]:
    pid = member["pid"]
    group = member["group"]
    host = member["host"]
    port = member["port"]
    if getHostType(member) == "PARTITION":
        launchNodeCmdString = [benchCommon.javabin,
                               benchCommon.javacp, benchCommon.CHITTER_CLASS_SERVER, pid]
        launchNodeCmdString += [group, config_file, partitioning_file, social_network_file]
        launchNodeCmdString += [gatherer_host, gatherer_port, gatherer_dir, gatherer_duration, gatherer_warmup, legacySSMR]
        launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdString])
        cmdList.append({"node": host, "port": port, "cmdstring": launchNodeCmdString})

config_stream.close()
partition_stream.close()

launcherThreads = []

thread = launcherThread(cmdList)
thread.start()
thread.join()
