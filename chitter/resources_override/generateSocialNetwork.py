import numpy as np
import json

NUM_INTEREST = 4
NUM_USER = 1000
NUM_MAX_FOLLOWER = 20
NUM_FOLLOWER_SKEW = 1.5
NUM_FOLLOWER_FOLLOWED_RATE = 14. / 9.
NUM_INTEREST_CONNECTED_RATE = 2
NUM_EDGE_CUT_RATE = 1
NUM_PARTITION_COUNT = 1
NUM_USER_PER_COMPONENT = 1000
FILE_OUTPUT = "./" + "_".join(
    ["users", str(NUM_USER_PER_COMPONENT * NUM_PARTITION_COUNT), "interest", str(NUM_INTEREST), "subgraph",
     str(NUM_PARTITION_COUNT)]) + ".json"
FILE_DEBUG_OUTPUT = "./" + "_".join(
    ["users", str(NUM_USER_PER_COMPONENT * NUM_PARTITION_COUNT), "interest", str(NUM_INTEREST), "subgraph",
     str(NUM_PARTITION_COUNT)]) + ".debug.json"


class ZipfGenerator:
    def __init__(self, a, count, max_value):
        s = np.random.zipf(a, count)
        # _s = filter(lambda x: x < max_value, _s)
        s = s[s < max_value]
        dist_map = {}
        for i in s:
            if i not in dist_map:
                dist_map[i] = 1
            else:
                dist_map[i] += 1
        for key, value in dist_map.iteritems():
            dist_map[key] = int(round((float(dist_map[key]) / len(s)) * 100))
        self.dist_map = dist_map

    def next(self):
        # Take a uniform 0-1 pseudo-random value:
        u = np.random.randint(low=1, high=100 + 1)
        count = 0
        for key, value in self.dist_map.iteritems():
            if count + value < u:
                count += value
            else:
                return key


class User:
    def __init__(self, user_id):
        self.userId = user_id
        self.interest = np.random.randint(low=1, high=NUM_INTEREST + 1)
        self.followers = []
        self.followeds = []
        self.numFollowers = 0
        self.availableFollowerSlots = 0
        self.numFolloweds = 0
        self.availableFollowedSlots = 0
        self.partitionId = (user_id % NUM_PARTITION_COUNT) + 1


class UserEncoder(json.JSONEncoder):
    def default(self, o):
        return {
            "userId": o.userId,
            "interest": o.interest,
            "followers": o.followers,
            "partitionId": o.partitionId
        }


class UserEncoderDebug(json.JSONEncoder):
    social_network = []

    def default(self, o):
        followers = []
        for i in o.followers:
            followers.append(self.social_network[i])
        followers = map(lambda user: {
            "id": user.userId,
            "interest": user.interest
        }, followers)
        return {
            "userId": o.userId,
            "interest": o.interest,
            "followers": followers,
            "partitionId": o.partitionId
        }


def generate_follower_distribution(skew, count, max_value):
    s = np.random.zipf(skew, count)
    s = map(lambda x: min(x, max_value), s)
    return s


def correlate_followeds_from_followers(num_followers):
    return int(round(num_followers * NUM_FOLLOWER_FOLLOWED_RATE, 0))


def generate_users(min_id, max_id):
    count = max_id - min_id
    follower_distribution = generate_follower_distribution(NUM_FOLLOWER_SKEW, count, NUM_MAX_FOLLOWER)
    _users = []
    for i in range(min_id, max_id):
        user = User(i)
        user.numFollowers = follower_distribution[i % count]
        user.availableFollowerSlots = user.numFollowers
        user.numFolloweds = correlate_followeds_from_followers(user.numFollowers)
        user.availableFollowedSlots = user.numFolloweds
        _users.append(user)
    return _users


def generate_connection(social_network):
    _generator = ZipfGenerator(NUM_INTEREST_CONNECTED_RATE, len(social_network), NUM_INTEREST + 1)
    for user in social_network:
        while user.availableFollowerSlots > 0:
            follower = None
            iterator = 0
            while follower is None and iterator < 100:
                follower_interest = _generator.next()
                if follower_interest == 1 and follower_interest != user.interest:
                    follower_interest = user.interest
                follower = get_follower_with_interest(social_network, user, follower_interest)
                iterator += 1
            if follower is not None:
                user.followers.append(follower.userId)
                follower.availableFollowedSlots -= 1
            user.availableFollowerSlots -= 1


def generate_subgraph_edge(social_network):
    total_edge = 0
    for sub_network in social_network:
        sub_network_edge = sum([len(user.followers) for user in sub_network])
        total_edge += sub_network_edge
    print total_edge


def get_follower_with_interest(social_network, followed, interest):
    for user in social_network:
        if followed.userId != user.userId and user.userId not in followed.followers and \
                        user.interest == interest and user.availableFollowedSlots > 0:
            return user
    return None


def save_to_file(file, social_network):
    output_file = open(file, 'w')
    wrapper = {
        "allUsers": social_network
    }
    json.dump(wrapper, output_file, cls=UserEncoder, sort_keys=False, indent=4, ensure_ascii=False)
    output_file.flush()
    output_file.close()


def save_debug_network(file, social_network):
    output_file = open(file, 'w')
    wrapper = {
        "allUsers": social_network
    }
    UserEncoderDebug.social_network = social_network
    json.dump(wrapper, output_file, cls=UserEncoderDebug, sort_keys=False, indent=4, ensure_ascii=False)
    output_file.flush()
    output_file.close()


def generate_social_network():
    network = []
    for i in range(0, NUM_PARTITION_COUNT):
        start_id = i * NUM_USER_PER_COMPONENT
        sub_network = generate_users(start_id, start_id + NUM_USER_PER_COMPONENT)
        generate_connection(sub_network)
        network.append(sub_network)
    generate_subgraph_edge(network)
    flatten = [item for sublist in network for item in sublist]
    save_to_file(FILE_OUTPUT, flatten)
    save_debug_network(FILE_DEBUG_OUTPUT, flatten)


generate_social_network()
