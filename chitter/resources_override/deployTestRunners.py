#!/usr/bin/python



# ============================================
# ============================================
# preamble

import sys

import systemConfigurer
import benchCommon
from benchCommon import sarg
from benchCommon import iarg
from benchCommon import farg


# ./deployTestRunners.py /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/socialNetworksInterestLinked/users_1000_partitions_2.json 1 100 0 0 0 CHITTERDSSMR 2 1 /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/minimal_system_config.json /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/minimal_partitioning.json

def usage():
    print "usage: " + sarg(
        0) + " socialNetworkFile numClients weightPost weightFollow weightUnfollow weightGetTimeline algorithm uniformRandom legacySSMR [(for chitter:) numPartitions minClientId configFile partsFile]"
    print 'Given args: '
    sys.exit(1)


if len(sys.argv) not in [10, 14]:
    usage()

# constants
NODE = 0
CLIENTS = 1

# deployment layout
socialNetworkFile = sarg(1)
numClients = iarg(2)

# command arguments
numPermits = benchCommon.numPermits
weightPost = farg(3)
weightFollow = farg(4)
weightUnfollow = farg(5)
weightGetTimeline = farg(6)
algorithm = sarg(7)  # RETWIS / chitterSSRM / chitterBASELINE / chitterPREFETCH
uniformRandom = sarg(8)
legacySSMR = sarg(9)
algargs = []
gathererNode = systemConfigurer.getGathererNode()
clientNodes = None
numPartitions = None
clientId = None

# chitter extra arguments

# minClientId
# configFile
# partsFile


clientId = None

# chitter's extra arguments: numPartitions minClientId configFile partsFile
print "algorithm = chitter****..."
numPartitions = iarg(10)
clientId = iarg(11)
configFile = sarg(12)
partsFile = sarg(13)
chitterConfig = systemConfigurer.generateSystemConfiguration(numPartitions, saveToFile=True)
clientNodes = systemConfigurer.getClientNodes(algorithm, numPartitions)
algargs = [clientId, algorithm, configFile, partsFile]

# ============================================
# ============================================
# begin

logDir = benchCommon.clilogdirchitter

# commonargs  = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaRunnerClass]
# commonargs += [benchCommon.duration, logDir, gathererNode, benchCommon.gathererPort]
# commonargs += [numPermits, socialNetworkFile]
# commonargs += [weightPost, weightFollow, weightUnfollow, weightGetTimeline]
print numClients
print clientNodes
clientMapping = benchCommon.mapClientsToNodes(numClients, clientNodes)
for mapping in clientMapping:
    #     for i in range(mapping[CLIENTS]) :

    if (mapping[CLIENTS] == 0):
        continue

    numPermits = mapping[CLIENTS]

    commonargs = [benchCommon.javabin, benchCommon.javacp, benchCommon.CHITTER_CLASS_TEST_RUNNER]
    commonargs += [benchCommon.SENSE_DURATION, logDir, gathererNode, benchCommon.gathererPort,
                   benchCommon.SENSE_WARMUP_TIME]
    commonargs += [numPermits, socialNetworkFile]
    commonargs += [weightPost, weightFollow, weightUnfollow, weightGetTimeline]
    commonargs += [uniformRandom, legacySSMR]

    algargs[0] = str(clientId)
    cmdString = " ".join([str(val) for val in (commonargs + algargs)])
    print cmdString
    benchCommon.sshcmdbg(mapping[NODE], cmdString)
    clientId += 1
