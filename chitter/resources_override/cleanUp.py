#!/usr/bin/python

import sys

import benchCommon

screenNode = benchCommon.getScreenNode()
nonScreenNodes = benchCommon.getNonScreenNodes()

# cleaning screen node
# print "Cleaning screen node..."
benchCommon.sshcmdbg(screenNode, "pkill -9 redis-server &> /dev/null")
benchCommon.sshcmdbg(screenNode, "killall -9 java &> /dev/null")

if len(sys.argv) > 1 :
    print "Killing runBatch.py"
    benchCommon.localcmd("pkill -9 runBatch.py")

# cleaning remote nodes
for node in nonScreenNodes + [ screenNode ] :
    # print "Cleaning worker " + node + "..."
    benchCommon.sshcmdbg(node, "killall -9 -u long &> /dev/null")
    if benchCommon.localhost:
        benchCommon.localcmd("ssh long@node249 ssh " + node + " sudo killall -9 -u long &> /dev/null &")
