#!/usr/bin/env bash

./runAllOnce.py chitterSSSMR 1 2 ~/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/socialNetworks/users_10000_partitions_2.json

## test runner
./deployTestRunners.py /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/socialNetworks/users_10000_partitions_2.json 1 0 0 0 1 chitterDSSRM 2 10 /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedSysConfig.json /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedPartitionsConfig.json

./deployTestRunners.py /home/long/apps/chitter/deploy/resources_override/socialNetworks/users_10000_partitions_2.json 1 0 0 0 1 chitterDSSRM 2 10 /home/long/apps/chitter/deploy/resources_override/generatedSysConfig.json /home/long/apps/chitter/deploy/resources_override/generatedPartitionsConfig.json

## test runner java args
60 /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/log/client_log_chitter localhost 60000 1 /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/benchmarks/users_1000_partitions_2.json 0.0 0.0 0.0 1.0 10 chitterDSSRM /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedSysConfig.json /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedPartitionsConfig.json

## chitter client args
10 /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/example_config_ridge.json /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/example_partitioning.json 1 /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/benchmarks/users_1000_partitions_2.json

##
ssh localhost "java -XX:+UseG1GC -Xmx8g -cp /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/target/chitter-git.jar ch.usi.dslab.lel.chitter.benchmarks.TestRunner 60 /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/log/client_log_chitter localhost 60000 1 /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/socialNetworks/users_10000_partitions_2.json 0.0 0.0 0.0 1.0 10 chitterDSSRM /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/example_config_ridge.json /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/example_partitioning.json"


# Generate system config script
python -c "import systemConfigurer; systemConfigurer.generateSystemConfiguration(2,True)"

## server
./deployServerchitter.py /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedSysConfig.json /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedPartitionsConfig.json /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/socialNetworks/users_10000_partitions_2.json

./deployServerchitter.py /home/long/apps/chitter/deploy/resources_override/generatedSysConfig.json /home/long/apps/chitter/deploy/resources_override/generatedPartitionsConfig.json /home/long/apps/chitter/deploy/resources_override/socialNetworks/users_10000_partitions_2.json

## oracle
./deployOraclechitter.py /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedSysConfig.json /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedPartitionsConfig.json

./deployOraclechitter.py /home/long/apps/chitter/deploy/resources_override/generatedSysConfig.json /home/long/apps/chitter/deploy/resources_override/generatedPartitionsConfig.json

## ridge
./deployRidge.py /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/generatedSysConfig.json
./deployRidge.py /home/long/apps/chitter/deploy/resources_override/generatedSysConfig.json

##gatherer
./deployGatherer.py localhost chitterDSSMR 0 1 0 0 0

export PYTHONPATH=/home/long/local/lib/python2.7/site-packages

./runAllOnce.py chitterDSSMR 1 2 /Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/resources_override/socialNetworks/users_10000_partitions_2.json
./runAllOnce.py chitterDSSMR 1 2 /home/long/apps/chitter/deploy/resources_override/socialNetworks/users_10000_partitions_2.json