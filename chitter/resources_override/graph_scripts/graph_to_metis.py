#!/usr/bin/env python
import sys, random, string, os
import subprocess
import simplejson as json

def usage():
    print 'usage: ' + sys.argv[0] + ' <graph path> <n partitions>'
    sys.exit(1)

def generate_graph(path, partitioning_path):
    graph_per_partition = {}
    partitioning_pair = {}
    with open(partitioning_path) as partitioning:
        line_n = 0
        for line in partitioning:
            n_pair = line.split()
            #METIS
            if len(n_pair) == 1:
                partitioning_pair[line_n] = int(n_pair[0]) + 1
                line_n+=1
            if len(n_pair) == 2:
                partitioning_pair[int(n_pair[0])] = int(n_pair[1])

    with open(path) as f:
        obj = json.load(f)
        
        for usr in obj['allUsers']:
            usr['partitionId'] = partitioning_pair[usr['userId']]

    print(json.dumps(obj))

def to_metis(path):
    tmp_file_name = path + '.' + ''.join(random.choice(
        string.ascii_uppercase + string.digits) for _ in range(10)) 
    metis_file = tmp_file_name + '.part.' + sys.argv[2]
    graph_per_partition = {}
    try:
        with open(path) as f:
            obj = json.load(f)
            n_users = len(obj['allUsers'])
            edges = 0
            matrix = [[] for i in range(n_users)]
            for usr in obj['allUsers']:
                edges+= len(usr['followers'])
                for f in usr['followers']:
                    matrix[usr['userId']].append(f + 1)
                    matrix[f].append(usr['userId'] + 1)
            with open(tmp_file_name, 'w+') as tmp_file:
                tmp_file.write('{} {}\n'.format(n_users, edges))
                for i in matrix:
                    tmp_file.write(' '.join(map(str, i))+'\n')
            p = subprocess.Popen(["gpmetis", tmp_file_name, sys.argv[2]], 
                    stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            if err != '':
                print('Error running metis')
        generate_graph(sys.argv[1], metis_file)
    finally:
        os.remove(tmp_file_name)
        os.remove(metis_file)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        usage()
    to_metis(sys.argv[1])

