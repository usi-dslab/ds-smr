#!/usr/bin/env python
import sys
import simplejson as json

class Graph():
    def __init__(self, path):
        self.n_usrs = 0
        self.users = {}
        self.users_per_partition = {}
        self.n_edges = 0.
        self.followed_by = {}
        with open(path) as f:
            graph = json.load(f)['allUsers']
            for usr in graph:
                self.users[usr['userId']] = usr
                self.users[usr['userId']]['cut_edges'] = 0
                self.n_usrs+=1
                self.n_edges += len(usr['followers'])

                for neighboor in usr['followers']:
                    if neighboor not in self.followed_by:
                        self.followed_by[neighboor] = []
                    self.followed_by[neighboor].append(usr['userId'])

    def calculate_edge_cut_balance(self):
        self.partitions = {}
        for usr in self.users:
            if self.users[usr]['partitionId'] not in self.partitions:
                self.partitions[self.users[usr]['partitionId']] = []

            self.partitions[self.users[usr]['partitionId']].append(usr)
        
        #Cut edges lambda = cut_edges / edges
        self.cut_edges = 0.
        for usr in self.users:
            for neighboor in self.users[usr]['followers']:
                if self.users[neighboor]['partitionId'] != self.users[usr]['partitionId']:
                    self.users[usr]['cut_edges']+=1
                    self.cut_edges+=1

        #balance p = maxV / avgV
        for p in self.partitions:
            self.users_per_partition[p] = len(self.partitions[p])

        avg_usrs = self.n_usrs/len(self.users_per_partition)
        
        (edge_cut, self.balance) = self.cut_edges/self.n_edges,\
                self.users_per_partition[\
                max(self.users_per_partition,\
                key = self.users_per_partition.get)]\
                /float(avg_usrs)
        return (edge_cut, self.balance)

    def move(self, vertex_id, from_partition, destination):
        if len(self.users_per_partition) == 0:
            self.calculate_edge_cut_balance()
        avg_usrs = self.n_usrs/len(self.users_per_partition)

        assert(self.users[vertex_id]['partitionId'] != destination)
        assert(self.users[vertex_id]['partitionId'] == from_partition)

        #P
        self.users_per_partition[destination]+=1
        self.users_per_partition[self.users[vertex_id]['partitionId']]-=1
        self.balance = self.users_per_partition[\
            max(self.users_per_partition,\
            key = self.users_per_partition.get)]\
            /float(avg_usrs)

        #L
        for neighboor in self.users[vertex_id]['followers']:
            #Neighboor is from destination
            if self.users[neighboor]['partitionId'] == destination:
                self.cut_edges-=1
                self.users[vertex_id]['cut_edges']-=1
            #Neighboor was from the partition moved
            elif self.users[neighboor]['partitionId'] == self.users[vertex_id]['partitionId']:
                self.cut_edges+=1
                self.users[vertex_id]['cut_edges']+=1

        ##Edges that come to him
        if vertex_id in self.followed_by:
            for neighboor in self.followed_by[vertex_id]:
                #Neighboor is from destination
                if self.users[neighboor]['partitionId'] == destination:
                    self.cut_edges-=1
                    self.users[neighboor]['cut_edges']-=1
                #Neighboor was from the partition moved
                elif self.users[neighboor]['partitionId'] == self.users[vertex_id]['partitionId']:
                    self.cut_edges+=1
                    self.users[neighboor]['cut_edges']+=1

        #Actually move the vertex
        self.users[vertex_id]['partitionId'] = destination
        assert(sum(map(lambda i : self.users_per_partition[i], self.users_per_partition)) == self.n_usrs)
        #assert(self.calculate_edge_cut_balance() == (self.cut_edges/self.n_edges, self.balance))
        #return self.calculate_edge_cut_balance()
        return (self.cut_edges/self.n_edges, self.balance)

    def connected_compopents(self):
        visited = []
        n_comp = 0
        def dfs(node):
            if node in visited:
                return
            visited.append(node)
            edges = self.users[node]['followers']
            if node in self.followed_by:
                edges+= self.followed_by[node] 
            for neigh in edges:
                dfs(neigh)

        for n in xrange(self.n_usrs):
            if n in visited:
                continue
            n_comp+=1
            dfs(n)
        return n_comp

def usage():
    print sys.argv[0] + ' <json graph path>'
    sys.exit()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        usage()
    g = Graph(sys.argv[1])
    (edge_cut, balance) = g.calculate_edge_cut_balance()
    print edge_cut, balance

