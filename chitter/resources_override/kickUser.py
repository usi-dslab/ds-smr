#!/usr/bin/python

import benchCommon
import sys

if (len(sys.argv) != 2) :
    print "usage: " + sys.argv[0] + " userToKick"
    sys.exit(1)

userToKick = sys.argv[1]

for node in benchCommon.availableNodes :
    benchCommon.sshcmdbg(node, "sudo killall -9 -u " + userToKick)