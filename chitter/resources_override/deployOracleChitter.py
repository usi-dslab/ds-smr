#!/usr/bin/python

# deployOracleChitter.py /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/minimal_system_config.json /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/minimal_partitioning.json /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/socialNetworksInterestLinked/users_1000_partitions_2.json

import inspect
import os
import sys
import json
import threading

import benchCommon
from benchCommon import sarg


class launcherThread(threading.Thread):
    def __init__(self, clist):
        threading.Thread.__init__(self)
        self.cmdList = clist

    def run(self):
        #         sleep(1)
        for cmd in self.cmdList:
            print ".-rRr-. executing: " + cmd["cmdstring"]
            #             benchCommon.freePort(cmd["node"], cmd["port"])
            benchCommon.sshcmdbg(cmd["node"], cmd["cmdstring"])


def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


if len(sys.argv) != 4:
    print "usage: " + sarg(
        0) + " <config_file> <partitioning_file> <socialNetwork_file>"
    sys.exit(1)

config_file = sarg(1)
partitioning_file = sarg(2)
socialNetwork_file = sarg(3)
config_stream = open(config_file)
partition_stream = open(partitioning_file)
config = json.load(config_stream)
partition = json.load(partition_stream)


def getHostType(host):
    for group in partition["partitions"]:
        if host["pid"] in group["servers"]:
            return group["type"]


cmdList = []

for member in config["group_members"]:
    pid = member["pid"]
    group = member["group"]
    host = member["host"]
    port = member["port"]
    if getHostType(member) == "ORACLE":
        oracleCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.CHITTER_CLASS_ORACLE, pid]
        oracleCmdPieces += [group, config_file, partitioning_file, socialNetwork_file]
        oracleCmdString = " ".join([str(val) for val in oracleCmdPieces])

        cmdList.append({"node": host, "port": port, "cmdstring": oracleCmdString})

config_stream.close()
partition_stream.close()

launcherThreads = []

thread = launcherThread(cmdList)
thread.start()
thread.join()
