#!/usr/bin/python

import math
import time
import sys
from os.path import isfile as file_exists

import benchCommon


def createLoads(min, max, factor, parcel):
    loads = []
    i = min
    while i <= max:
        loads.append(i)
        i = int(math.ceil(i * factor + parcel))
    return loads


def estimateBatchTime(algs, parts, wls, min_cli, max_cli, inc_factor, inc_parcel, last_run_time, already_run):
    numLoads = benchCommon.getNumLoads(min_cli, max_cli, inc_factor, inc_parcel)
    retwisRuns = 0 if "RETWIS" not in algs else len(wls) * numLoads
    chitterRuns = 0 if "CHITTER" not in algs else len(parts) * len(wls) * numLoads
    totalRuns = retwisRuns + chitterRuns - already_run
    return totalRuns * last_run_time


wp = 0
wf = 1
wu = 2
wtl = 3
mix = 4
purepost = 5
purefollow = 6
puretl = 7

workloads = {
    # "mix": { wp: 7.5, wf: 3.75, wu: 3.75, wtl: 85},
    "purepost": {wp: 1.0, wf: 0, wu: 0, wtl: 0},
    # "followunfollow": {wp: 0, wf: 1.0, wu: 1.0, wtl: 0},
    # "puretimeline": {wp: 0, wf: 0, wu: 0, wtl: 1.0},
}

# algorithms = ["chitterSSMR", "chitterBASELINE", "chitterPREFETCH", "RETWIS"]
# algorithms = ["RETWIS"]
# both ssmr_no_prefetch and ssmr_prefetch are run with chitterSSMR (this is a mess...)
algorithms = ["CHITTER"]
# flagFullOpt = 1
minClients = 5
maxClients = 500
incFactor = 1.5
incParcel = 0
# loads = createLoads(minClients, maxClients, incFactor, incParcel)
loads = [400]

numUsers = 1000
uniformRandom = "True"
legacySSMR = "False"

if not benchCommon.localhost:
    benchCommon.localcmd(benchCommon.cleaner)
    # benchCommon.localcmd(benchCommon.systemParamSetter)
    # benchCommon.localcmd(benchCommon.clockSynchronizer)
    time.sleep(5)

alreadyRan = 0
lastRunTime = 120  # estimating each run will take 2 min (120 seconds)
startTime = time.time()

for algorithm in algorithms:

    partitionings = [4]
    # partitionings = [1] if algorithm == "RETWIS" else [1]

    for numPartitions in partitionings:

        socialNetworkFile = benchCommon.socialNetworkDir + "users_" + str(numUsers) + "_partitions_" + \
            str(numPartitions) + ".json"

        if not file_exists(socialNetworkFile):
            print('Social Network File doesn\'t exist ' + socialNetworkFile)
            sys.exit(1)

        for wl in workloads:

            # ####################################
            # WORKLOAD
            weightPost = workloads[wl][wp]
            weightFollow = workloads[wl][wf]
            weightUnfollow = workloads[wl][wu]
            weightGetTimeline = workloads[wl][wtl]
            workloadName = wl
            # ####################################


            '''
            if numPartitions == 1 :
                if wl == "purepost" :
                    loads = []
                if wl == "puretimeline" :
                    loads = [1050, 1150, 1250, 1350]
                if wl == "mix" :
                    loads = [40, 60, 90, 130, 180, 250, 300, 500, 750, 1000, 2000, 4000]
                if wl == "followunfollow" :
                    pass

            if numPartitions == 2 :
                if wl == "purepost" :
                    maxClients = 0
                if wl == "puretimeline" :
                    loads = [400, 500, 750, 1000, 2000, 4000]
                if wl == "mix" :
                    loads = [400, 500, 750, 1000, 2000, 4000]
                if wl == "followunfollow" :
                    maxClients = 0

            if numPartitions == 4 :
                if wl == "purepost" :
                    maxClients = 0
                if wl == "puretimeline" :
                    loads = [750, 1000, 2000, 4000]
                if wl == "mix" :
                    loads = [750, 1000, 2000, 4000]
                if wl == "followunfollow" :
                    minClients = 70
                    maxClients = 200
            
            if numPartitions == 8 :
                if wl == "purepost" :
                    maxClients = 0
                if wl == "puretimeline" :
                    loads = [750, 1000, 2000, 4000]
                if wl == "mix" :
                    loads = [750, 1000, 2000, 4000]
                if wl == "followunfollow" :
                    minClients = 70
                    maxClients = 300
            '''



            # numClients = minClients

            # while numClients <= maxClients :
            for numClients in loads:
                # usage: onceRunner algorithm(chitterSSMR/.../RETWIS) numClients numPartitions socialNetworkFile [wp wf wu wtl]"
                now = time.time()

                remainingTime = estimateBatchTime(algorithms, partitionings, workloads, minClients, maxClients,
                                                  incFactor, incParcel, lastRunTime, alreadyRan)
                remainingTimeMessage = "[" + time.strftime("%H:%M", time.localtime(
                    now)) + "] remaining time for batch: " + time.strftime("%H:%M", time.gmtime(
                    remainingTime)) + "; expected to finish: " + time.strftime("%H:%M",
                                                                               time.localtime(now + remainingTime))
                print remainingTimeMessage

                experimentCmd = ' '.join([str(val) for val in
                                          [benchCommon.onceRunner, algorithm, numClients, numPartitions,
                                           socialNetworkFile, uniformRandom, legacySSMR, weightPost, 
                                           weightFollow, weightUnfollow, weightGetTimeline, workloadName]])
                print experimentCmd
                benchCommon.localcmd(experimentCmd)
                if not benchCommon.localhost:
                    benchCommon.localcmd(benchCommon.cleaner)

                now = time.time()
                lastRunTime = now - startTime
                startTime = now
                alreadyRan += 1

                # increase load
                # numClients  = int(math.ceil(numClients * incFactor) + incParcel)
