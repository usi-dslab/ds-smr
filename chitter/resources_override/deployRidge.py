#!/usr/bin/python

# This script initiates the helper nodes, that is, the Ridge Infrastructure:
# All ensembles, each with its coordinator and its acceptors

# try: import simplejson as json
# except ImportError: import json

import os
import sys
import inspect
import threading

import simplejson as json

import benchCommon


# ./deployRidge.py /Users/longle/Dropbox/Workspace/PhD/DS-SMR/chitter/resources_override/minimal_system_config.json

# ====================================
# ====================================

class launcherThread(threading.Thread):
    def __init__(self, clist):
        threading.Thread.__init__(self)
        self.cmdList = clist

    def run(self):
        #         sleep(1)
        for cmd in self.cmdList:
            print ".-rRr-. executing: " + cmd["cmdstring"]
            #             benchCommon.freePort(cmd["node"], cmd["port"])
            benchCommon.sshcmdbg(cmd["node"], cmd["cmdstring"])
            # "sudo fuser -k " + str(cmd["port"]) + "/tcp ; sleep 5; " + cmd["cmdstring"])


# sleep(0.5)

def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

# ====================================
# ====================================

if len(sys.argv) != 2:
    print("Usage: " + sys.argv[0] + " config_file")
    sys.exit(1)

system_config_file = os.path.abspath(sys.argv[1])

print("Deploying system helper nodes described in " + system_config_file)

config_json = open(system_config_file)

config = json.load(config_json)

# MUST ASSUME THAT EACH HELPERNODE IS IN A SINGLE RING
# AND THAT ALL NODES OF THE SAME RING ARE TOGETHER IN THE CONFIG FILE
# AND THAT NO RING HAS ID -1
ensembleCmdLists = []
lastEnsemble = -1
cmdList = []

for process in config["ensemble_processes"]:
    role = process["role"]
    pid = process["pid"]
    ensemble = process["ensemble"]
    host = process["host"]
    port = process["port"]

    launchNodeCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaRidgeNodeClass, system_config_file,
                           pid]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
    cmdList.append({"node": host, "port": port, "cmdstring": launchNodeCmdString})

    message = "launching " + role + " " + str(pid), " at ", host + ":" + str(port)

    print(message)
    # delete the acceptor's disk backup    
    # os.system(command_string)

config_json.close()

launcherThreads = []

thread = launcherThread(cmdList)
thread.start()
thread.join()
