#!/usr/bin/python

import sys
from os.path import abspath
import time

import benchCommon
import systemConfigurer
from benchCommon import sarg, localcmdbg
from benchCommon import iarg
from benchCommon import farg


# usage
def usage():
    print "usage: " \
          + sarg(0) \
          + " algorithm(RETWIS/chitterSSMR/CHITTER) numClients numPartitions socialNetworkFile " \
            "[wp wf wu wtl name]"
    sys.exit(1)


if len(sys.argv) not in [7, 12]:
    usage()

# runAll RETWIS Xclients 

# parameters
algorithm = sarg(1)
numClients = iarg(2)
numPartitions = iarg(3)
socialNetworkFile = abspath(sarg(4))
clientNodes = None
numServers = 0
numCoordinators = 0
numAcceptors = 0
uniformRandom = sarg(5)
legacySSMR = sarg(6)
if len(sys.argv) in [7]:
    weightPost = benchCommon.weightPost
    weightFollow = benchCommon.weightFollow
    weightUnfollow = benchCommon.weightUnfollow
    weightGetTimeline = benchCommon.weightGetTimeline
    workloadName = benchCommon.workloadName
elif len(sys.argv) in [12]:
    weightPost = farg(7)
    weightFollow = farg(8)
    weightUnfollow = farg(9)
    weightGetTimeline = farg(10)
    workloadName = sarg(11)
else:
    usage()

if not benchCommon.localhost:
    benchCommon.localcmd(benchCommon.cleaner)
    time.sleep(5)

retwisServerNode = None
chitterSysConfig = None
serverList = None
oracleList = None
coordinators = None
acceptors = None
chitterSysConfigFile = None
chitterPartitioningFile = None
gathererNode = None
minClientId = None

# benchCommon.localcmd(benchCommon.systemParamSetter)
if algorithm == "RETWIS":
    gathererNode = systemConfigurer.getGathererNode()
    retwisServerNode = systemConfigurer.getRetwisServerNode()
    clientNodes = systemConfigurer.getClientNodes(algorithm)
elif algorithm in ["chitterSSMR", "CHITTER"]:

    # L.Le disable clock sync
    # for node in benchCommon.getNonScreenNodes():
    #     benchCommon.sshcmdbg(node, benchCommon.continousClockSynchronizer)

    chitterSysConfig = systemConfigurer.generateSystemConfiguration(numPartitions)
    time.sleep(5)
    serverList = chitterSysConfig["server_list"]
    oracleList = chitterSysConfig["oracle_list"]
    coordinators = chitterSysConfig["coordinator_list"]
    acceptors = chitterSysConfig["acceptor_list"]
    numServers = len(serverList)
    numOracles = len(oracleList)
    numCoordinators = len(coordinators)
    numAcceptors = len(acceptors)
    chitterSysConfigFile = chitterSysConfig["config_file"]
    chitterPartitioningFile = chitterSysConfig["partitioning_file"]
    gathererNode = chitterSysConfig["gatherer_node"]
    minClientId = chitterSysConfig["client_initial_pid"]
    clientNodes = chitterSysConfig["remaining_nodes"]

# logfolder
logFolder = ""
mode = "uniform" if uniformRandom.lower() == 'true' else "zipf"
legacy = "legacySSMR" if legacySSMR.lower() == 'true' else "DSSMR"
logFolder = "_".join([str(val) for val in
                      [algorithm, workloadName, mode, legacy, "clients", numClients, "permits", benchCommon.numPermits,
                       "partitions", numPartitions, "post", weightPost, "follow", weightFollow, "unfollow",
                       weightUnfollow, "timeline", weightGetTimeline]])

###########################################################################
# LAUNCH SERVERS
###########################################################################

# launch server(s)

# launch multicast infrastructure
ridgeCmdPieces = [benchCommon.multicastDeployer, chitterSysConfigFile]
ridgeCmdString = " ".join(ridgeCmdPieces)
localcmdbg(ridgeCmdString)

# # launch replicas
chitterServerCmdPieces = [benchCommon.chitterServerDeployer, chitterSysConfigFile, chitterPartitioningFile,
                          socialNetworkFile, gathererNode, benchCommon.gathererPort, benchCommon.srvlogdirchitter,
                          str(benchCommon.SENSE_DURATION), str(benchCommon.SENSE_WARMUP_TIME), legacySSMR]
chitterServerCmdStr = " ".join(chitterServerCmdPieces)
localcmdbg(chitterServerCmdStr)

#
# launch oracle

chitterOracleCmdPieces = [benchCommon.chitterOracleDeployer, chitterSysConfigFile, chitterPartitioningFile,
                          socialNetworkFile]
chitterClientCmdString = " ".join(chitterOracleCmdPieces)
localcmdbg(chitterClientCmdString)

time.sleep(4 * numPartitions + 5)
print "Waiting for servers to be ready..."
time.sleep(4 * numPartitions + 5)
print "Servers should be ready."

###########################################################################
# LAUNCH CLIENTS
###########################################################################

# launch clients
# deployTestRunners.py: socialNetworkFile numClients weightPost weightFollow weightUnfollow weightGetTimeline algorithm [(for chitter:) numPartitions minClientId configFile partsFile]"
print 'Deploying clients'
chitterClientCmdPieces = [benchCommon.clientDeployer, socialNetworkFile, numClients, weightPost, weightFollow,
                          weightUnfollow, weightGetTimeline, algorithm, uniformRandom, legacySSMR,
                          numPartitions, minClientId, chitterSysConfigFile, chitterPartitioningFile]
chitterClientCmdString = " ".join([str(val) for val in chitterClientCmdPieces])
benchCommon.localcmd(chitterClientCmdString)

###########################################################################
###########################################################################
# LAUNCH ACTIVE MONITORS
###########################################################################
#
# # launch active monitors
# if algorithm == "RETWIS":
#     # server bw and cpu monitors
#     bwmCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass, "server", 0, gathererNode,
#                     benchCommon.gathererPort, benchCommon.SENSE_DURATION, benchCommon.nonclilogdirRetwis]
#     bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
#     retwisServerNode = systemConfigurer.getRetwisServerNode()
#     benchCommon.sshcmdbg(retwisServerNode, bwmCmdString)
#     cpumCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaCPUMonitorClass, "server", 0,
#                      gathererNode, benchCommon.gathererPort, benchCommon.SENSE_DURATION, benchCommon.nonclilogdirRetwis]
#     cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
#     benchCommon.sshcmdbg(retwisServerNode, cpumCmdString)
#     # client bw and cpu monitors
#     clientNodeMap = benchCommon.mapClientsToNodes(numClients, clientNodes)
#     cliNodeId = 0
#     for node in clientNodes:
#         if benchCommon.clientNodeIsEmpty(node, clientNodeMap):
#             continue
#         bwmCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass, "client_node",
#                         cliNodeId, gathererNode, benchCommon.gathererPort, benchCommon.SENSE_DURATION,
#                         benchCommon.clilogdirRetwis]
#         bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
#         benchCommon.sshcmdbg(node, bwmCmdString)
#         cpumCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaCPUMonitorClass, "client_node",
#                          cliNodeId, gathererNode, benchCommon.gathererPort, benchCommon.SENSE_DURATION,
#                          benchCommon.clilogdirRetwis]
#         cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
#         benchCommon.sshcmdbg(node, cpumCmdString)
#         cliNodeId += 1
# elif "CHITTER" in algorithm:
# server bw and cpu monitors
for nonClient in serverList:  # + coordinators + acceptors:
    # bwmCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass, nonClient["role"],
    #                 nonClient["pid"], chitterSysConfig["gatherer_node"], benchCommon.gathererPort,
    #                 benchCommon.SENSE_DURATION, benchCommon.nonclilogdirchitter]
    # bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
    # benchCommon.sshcmdbg(nonClient["host"], bwmCmdString)
    cpumCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaCPUMonitorClass, "PARTITION",
                     nonClient["pid"], chitterSysConfig["gatherer_node"], benchCommon.gathererPort,
                     benchCommon.SENSE_DURATION, benchCommon.nonclilogdirchitter]
    cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
    benchCommon.sshcmdbg(nonClient["host"], cpumCmdString)
    # client bw and cpu monitors
    # clientNodeMap = benchCommon.mapClientsToNodes(numClients, clientNodes)
    # cliNodeId = minClientId
    # for node in clientNodes:
    #     if benchCommon.clientNodeIsEmpty(node, clientNodeMap):
    #         continue
    #     # bwmCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass, "client_node", cliNodeId, gathererNode, benchCommon.gathererPort, benchCommon.SENSE_DURATION, benchCommon.clilogdirchitter]
    #     # bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
    #     # benchCommon.sshcmdbg(node, bwmCmdString)
    #     cpumCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaCPUMonitorClass, "client_node",
    #                      cliNodeId, gathererNode, benchCommon.gathererPort, benchCommon.SENSE_DURATION,
    #                      benchCommon.clilogdirchitter]
    #     cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
    #     benchCommon.sshcmdbg(node, cpumCmdString)
    #     cliNodeId += 1

###########################################################################
# LAUNCH GATHERER
###########################################################################

# launch gatherer last and wait for it to finish
# wait for gatherer with timeout; if timeout, kill everything, including Redis
# gathererCmdPieces = [benchCommon.gathererDeployer, gathererNode, algorithm, numPartitions, numClients, numServers,
#                      numCoordinators, numAcceptors, logFolder]

gathererCmdPieces = [benchCommon.gathererDeployer, gathererNode, algorithm, numPartitions, numClients, numServers,
                     0, 0, logFolder]

gathererCmd = " ".join([str(val) for val in gathererCmdPieces])
print gathererCmd

fullLogDir = benchCommon.gathererBaseLogDir + logFolder + "/"

benchCommon.localcmd(gathererCmd)
benchCommon.localcmd("cp " + benchCommon.benchCommonPath + " " + fullLogDir)
benchCommon.localcmd("cp " + benchCommon.sysConfigFile + " " + fullLogDir)
benchCommon.localcmd("cp " + benchCommon.partitionsFile + " " + fullLogDir)
benchCommon.localcmdbg("rm -rf " + fullLogDir + "/server_log_dchirper")
benchCommon.localcmdbg("mv " + benchCommon.srvlogdirchitter + " " + fullLogDir)
benchCommon.localcmdbg("mv " + benchCommon.clilogdirchitter + "/*client_retry_rate* " + fullLogDir)
benchCommon.localcmdbg("mv " + benchCommon.clilogdirchitter + "/*client_query_rate* " + fullLogDir)
benchCommon.localcmdbg("mv " + benchCommon.clilogdirchitter + "/*client_move_rate* " + fullLogDir)
benchCommon.localcmdbg("rm " + benchCommon.clilogdirchitter  + "/*")

###########################################################################
# clean up
if not benchCommon.localhost:
    benchCommon.localcmd(benchCommon.cleaner)
