#!/usr/bin/python

import inspect
import os
import sys

import benchCommon
from benchCommon import sarg
from benchCommon import iarg


def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


if len(sys.argv) != 4:
    print "usage: " + sarg(
        0) + " <user number> <partition_number> <socialNetwork_folder>"
    sys.exit(1)

user_number = iarg(1)
partition_number = iarg(2)
output_folder = sarg(3)

file_name = script_dir() + "/" + output_folder + "/" + "users_" + str(user_number) + "_partitions_" + str(
    partition_number) + ".json"

launchNodeCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaGeneratorClass, user_number,
                       partition_number, file_name]
launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])

os.system(launchNodeCmdString)
