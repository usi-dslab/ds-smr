#!/bin/sh

echo ; echo "Updating netwrapper..."
cd ~/netwrapper
git pull && cd ~/sense
echo ; echo "Updating sense..."
git pull && cd ~/ridge
echo ; echo "Updating ridge..."
git pull && cd ~/libmcad
echo ; echo "Updating libmcad..."
git pull && cd ~/eyrie
echo ; echo "Updating eyrie..."
git pull && cd ~/chitter
echo ; echo "Updating chitter..."
git pull
