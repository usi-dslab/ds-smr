import ch.usi.dslab.lel.chitter.util.SocialNetworkGenerator;
import org.junit.Test;

import java.util.*;

/**
 * Created by longle on 20/07/2016.
 */
public class TestGenerator {
    @Test
    public void testGenerator() {
        Map<Integer, Integer> store = new HashMap();
        double skew = 1.2d;
        int numUsers = 5000;
        SocialNetworkGenerator.RandomNumberGenerator rand = SocialNetworkGenerator.RandomNumberGenerator.getInstance(numUsers);
        for (int i = 0; i < 5000000; i++) {
            int id = rand.getZipfRandomInt(1, numUsers, skew);
            Integer count = store.get(id);
            if (count == null) count = 0;
            store.put(id, ++count);
        }
        Set<Integer> sorted = new TreeSet(store.keySet());;
        for (Integer i : sorted) {
            System.out.println(i + ","+store.get(i));
        }
    }
}
