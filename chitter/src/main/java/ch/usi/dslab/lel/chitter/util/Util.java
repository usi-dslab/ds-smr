/*

 chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter.util;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

public class Util {

    public static String get8bytesSHA1String(byte[] input) {
        return Long.toHexString(get8bytesSHA1(input));
    }
   
   public static long get8bytesSHA1(byte[] input) {
      MessageDigest md = null;
      try {
         md = MessageDigest.getInstance("SHA-1");
      } catch (NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
      byte[] sha1 = md.digest(input);
      long retval = java.nio.ByteBuffer.wrap(sha1).getLong();
      return retval;
   }
   
   /**
    * leastOdd(x): returns the number itself, if it's odd, or the closest lower odd number, otherwise.
    * e.g.: leastOdd(1) = 1, leastOdd(2) = 1, leastOdd(3) = 3, leastOdd(4) = 3 and so on
    * 
    * @param x
    * @return (x % 2 != 0) ? x : (x - 1)
    */
   
   public static int leastOdd(int x) {
      if (x % 2 != 0) return x;
      else            return x - 1;
   }
   
   public static int clamp(int value, int min, int max) {
      if (value < min) return min;
      if (value > max) return max;
      return value;
   }
   
   public static Set<SocialNetworkGenerator.RandomUser> loadSocialNetwork(String socialNetworkFile) {
      try {
//         System.out.print("Loading social network from file " + socialNetworkFile + "...");
         
         Set<SocialNetworkGenerator.RandomUser> allUsers = null;

         JSONParser parser = new JSONParser();

         JSONObject j_network = (JSONObject) parser.parse(new FileReader(socialNetworkFile));
         JSONArray  j_allUsers = (JSONArray) j_network.get("allUsers");
         allUsers = new HashSet<>(j_allUsers.size());
         
         for (Object obj_j_user : j_allUsers) {
            JSONObject j_user = (JSONObject) obj_j_user;
            SocialNetworkGenerator.RandomUser user = new SocialNetworkGenerator.RandomUser();
            user.userId      = ((Long) j_user.get("userId"     )).intValue();
            user.interest = ((Long) j_user.get("interest")).intValue();
            user.partitionId = ((Long) j_user.get("partitionId")).intValue();
            JSONArray j_followers = (JSONArray) j_user.get("followers");
            for (Object obj_j_follower : j_followers) {
               long followerId = (Long) obj_j_follower;
               user.followers.add(followerId);
            }
            allUsers.add(user);
         }

//         System.out.println(" done.");

         return allUsers;
         
      } catch (ParseException | IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return null;
   }

}
