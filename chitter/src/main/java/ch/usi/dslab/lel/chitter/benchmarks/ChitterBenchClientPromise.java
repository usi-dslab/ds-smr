/*

 chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter.benchmarks;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import ch.usi.dslab.lel.chitter.ChitterClientPromise;
import ch.usi.dslab.lel.chitter.Post;
import ch.usi.dslab.lel.chitter.User;

public class ChitterBenchClientPromise implements ClientPromiseInterface {
    ChitterClientPromise chitterClient;

    public ChitterBenchClientPromise(ChitterClientPromise client) {
        chitterClient = client;
    }

    public void loadSocialNetworkIntoCache(String file) {
        chitterClient.loadSocialNetworkIntoCache(file);
    }

    public void createSociaNetworkUsers(String file) {
        chitterClient.createSociaNetworkUsers(file);
    }

    @Override
    public void setLegacySSMR(boolean legacySSMR) {
        this.chitterClient.setLegacySSMR(legacySSMR);
    }

    @Override
    public void createUser(long userId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chitterClient.createUserNB(User.genObjId(userId), tmpAccept);
    }

    @Override
    public void post(long userId, byte[] post, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context) {
        Post postObj = new Post(chitterClient.getId(), User.genObjId(userId), System.currentTimeMillis(), post);
        Consumer tmpAccept = o -> accept.accept(o, context);
        chitterClient.postNB(User.genObjId(userId), postObj, tmpAccept);
    }

    @Override
    public void follow(long followerId, long followedId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chitterClient.followNB(User.genObjId(followerId), User.genObjId(followedId), tmpAccept);
    }

    @Override
    public void unfollow(long followerId, long followedId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chitterClient.unfollowNB(User.genObjId(followerId), User.genObjId(followedId), tmpAccept);
    }

    @Override
    public void getTimeline(long userId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chitterClient.getTimelineNB(User.genObjId(userId), tmpAccept);
    }

    @Override
    public void follow_NOP(long followerId, long followedId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chitterClient.follow_NOP_NB(User.genObjId(followerId), User.genObjId(followedId), tmpAccept);
    }

    @Override
    public void unfollow_NOP(long followerId, long followedId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context) {
        Consumer tmpAccept = o -> accept.accept(o, context);
        chitterClient.unfollow_NOP_NB(User.genObjId(followerId), User.genObjId(followedId), tmpAccept);
    }

}
