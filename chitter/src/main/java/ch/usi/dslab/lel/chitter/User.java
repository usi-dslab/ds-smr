/*

 chitter - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.codecs.Codec;
import ch.usi.dslab.bezerra.netwrapper.codecs.CodecUncompressedKryo;
import ch.usi.dslab.lel.aerie.*;

import java.util.List;

public class User extends PRObject {
    private static final long serialVersionUID = 1L;

//    public static Map<ObjId, User> usersMap = new HashMap<>();

    UserList followeds;
    UserList followers;
    PostsList posts;
    PostsList materializedTimeline;

    public User() {
        // default constructor for Kryo
    }

    public User(long userId) {
        if (PRObject.isAvailable(User.genObjId(userId)))
            return;
        this.setId(User.genObjId(userId));
//        usersMap.put(this.getId(), this);
        followeds = new UserList();
        followers = new UserList();
        posts = new PostsList();
        materializedTimeline = new PostsList();
    }

    public User(ObjId userId) {
        if (PRObject.isAvailable(userId))
            return;
        this.setId(userId);
//        usersMap.put(userId, this);
        followeds = new UserList();
        followers = new UserList();
        posts = new PostsList();
        materializedTimeline = new PostsList();
    }
    @LocalMethodCall
    public static User getUser(ObjId userId) {
        return (User) PRObject.getObjectById(userId);
//        return usersMap.get(userId);
    }
    @LocalMethodCall
    public static ObjId genObjId(long userId) {
        return new ObjId(String.valueOf(userId));
    }

    @LocalMethodCall
    public static ObjId genObjId(String userId) {
        return new ObjId(userId);
    }

    public void post(Post post) {
        posts.addPost(post);
    }

    public PostsList getUserPosts() {
        return posts;
    }

    public boolean follow(ObjId followedId) {
        return followeds.add(followedId);
    }

    public boolean unfollow(ObjId followedId) {
        return followeds.remove(followedId);
    }

    public boolean addFollower(ObjId followerId) {
        return followers.add(followerId);
    }

    public void removeFollower(ObjId followerId) {
        followers.remove(followerId);
    }

    public UserList getFollowedList() {
        return followeds;
    }

    public UserList getFollowerList() {
        return followers;
    }

    public void addToMaterializedTimeline(Post post) {
        if (followeds.contains(post.getPosterId()))
            materializedTimeline.addPost(post);
    }

    public void addToMaterializedTimeline(PostsList posts) {
        posts.postsList.stream().filter(post -> followeds.contains(post.getPosterId())).forEach(materializedTimeline::addPost);
    }

    public PostsList getMaterializedTimeline() {
        return materializedTimeline;
    }

    @Override
    public Message getSuperDiff(List<Partition> destinations) {
        return new Message(this.posts, this.materializedTimeline, this.followers, this.followeds);
    }

    @Override
    public void updateFromDiff(Message objectDiff) {
        this.posts = (PostsList) objectDiff.getNext();
        this.materializedTimeline = (PostsList) objectDiff.getNext();
        this.followers = (UserList) objectDiff.getNext();
        this.followeds = (UserList) objectDiff.getNext();
    }

    @Override
    public PRObject deepClone() {
        Codec codec = new CodecUncompressedKryo();
        return (PRObject) codec.deepDuplicate(this);
    }

    @Override
    public String toString() {
        return String.format("user %s", getId());
    }
}
