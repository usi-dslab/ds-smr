/*

 chitter - A twitter clone based on Eyrie 2 (Optimistic S-SMR)
 Copyright (C) 2014, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter;

import ch.usi.dslab.lel.aerie.ObjId;

import java.util.HashSet;
import java.util.Set;

public class UserList {
    Set<ObjId> people;

    public UserList() {
        people = new HashSet<>();
    }

    public boolean add(User followedUser) {
        return add(followedUser.getId());
    }

    public boolean add(ObjId userId) {
        return people.add(userId);
    }

    public boolean remove(User user) {
        return remove(user.getId());
    }

    public boolean remove(ObjId userId) {
        return people.remove(userId);
    }

    public Set<ObjId> getRawList() {
        return new HashSet<>(people);
    }

    public boolean contains(ObjId userId) {
        return people.contains(userId);
    }

    @Override
    public String toString() {
        String sb = "";
        for (ObjId person : people)
            sb += person + " ";
        return sb;
    }

}
