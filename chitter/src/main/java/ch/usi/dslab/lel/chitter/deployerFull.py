#!/usr/bin/python

import inspect
import os
import time
from os.path import expanduser
HOME = expanduser("~")

def script_dir():
#    returns the module path without the use of __file__.  Requires a function defined 
#    locally in the module.
#    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
   return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

# deploying (ridge) multicast infrastructure

ridge_deployer = HOME + "/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/ridge/RidgeEnsembleNodesDeployer.py "
config_file = script_dir() + "/system_config.json "
app_classpath = HOME + "/eyrie/target/eyrie-git.jar "
# syntax of the deployer:
# ...Deployer.py config.json [additional classpath]
deployment_cmd = ridge_deployer + config_file + app_classpath
os.system(deployment_cmd)



# starting servers

java_bin = "java -XX:+UseG1GC -cp "
server_class = "ch.usi.dslab.bezerra.eyrie.example.AppServer "
client_class = "ch.usi.dslab.bezerra.eyrie.example.AppClient "
partitioning_file = script_dir() + "/partitioning.json "

# server = (server_id, partition_id)
servers = [ (9, 1) , (10, 2) ]

silence_servers = False
#silence_servers = True

for server in servers :
    server_id = str(server[0]) + " "
    partition = str(server[1]) + " "
    server_cmd = java_bin + app_classpath + server_class + server_id + partition + config_file + partitioning_file
    if silence_servers : server_cmd += " &> /dev/null "
    server_cmd += " & "
    os.system(server_cmd)

time.sleep(5)

client_id = str(101) + " "

client_cmd = java_bin + app_classpath + client_class + client_id + config_file + partitioning_file
os.system(client_cmd)
