/*

 chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter.benchmarks;

import ch.usi.dslab.lel.chitter.ChitterClient;
import ch.usi.dslab.lel.chitter.Post;
import ch.usi.dslab.lel.chitter.User;
import ch.usi.dslab.lel.aerie.client.CallbackHandlerWithContext;

public class ChitterBenchClient implements ClientInterface {
    ChitterClient chitterClient;

    public ChitterBenchClient(ChitterClient client) {
        chitterClient = client;
    }

    public void loadSocialNetworkIntoCache(String file) {
        chitterClient.loadSocialNetworkIntoCache(file);
    }

    public void createSociaNetworkUsers(String file) {
        chitterClient.createSociaNetworkUsers(file);
    }

    @Override
    public void createUser(long userId, CallbackHandlerWithContext cbHandler, Object context) {
        chitterClient.createUserNB(User.genObjId(userId), cbHandler, context);
    }

    @Override
    public void post(long userId, byte[] post, CallbackHandlerWithContext cbHandler, Object context) {
        Post postObj = new Post(chitterClient.getId(), User.genObjId(userId), System.currentTimeMillis(), post);
        chitterClient.postNB(User.genObjId(userId), postObj, cbHandler, context);
    }

    @Override
    public void follow(long followerId, long followedId, CallbackHandlerWithContext cbHandler, Object context) {
        chitterClient.followNB(User.genObjId(followerId), User.genObjId(followedId), cbHandler, context);
    }

    @Override
    public void unfollow(long followerId, long followedId, CallbackHandlerWithContext cbHandler, Object context) {
        chitterClient.unfollowNB(User.genObjId(followerId), User.genObjId(followedId), cbHandler, context);
    }

    @Override
    public void getTimeline(long userId, CallbackHandlerWithContext cbHandler, Object context) {
        chitterClient.getTimelineNB(User.genObjId(userId), cbHandler, context);
    }

    @Override
    public void follow_NOP(long followerId, long followedId, CallbackHandlerWithContext cbHandler, Object context) {
        chitterClient.follow_NOP_NB(User.genObjId(followerId), User.genObjId(followedId), cbHandler, context);
    }

    @Override
    public void unfollow_NOP(long followerId, long followedId, CallbackHandlerWithContext cbHandler, Object context) {
        chitterClient.unfollow_NOP_NB(User.genObjId(followerId), User.genObjId(followedId), cbHandler, context);
    }

}
