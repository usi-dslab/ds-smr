/*

 Chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of Chitter.
 
 Chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 *
 * (18/08/2016) - Enrique Fynn: Added legacy SSMR support
 */

package ch.usi.dslab.lel.chitter;

import ch.usi.dslab.lel.aerie.CacheHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Cache.PRObjectLocation;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.client.PromiseClient;
import ch.usi.dslab.lel.chitter.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chitter.util.Util;

public class ChitterClientPromise {

    public static final Logger log = LogManager.getLogger(ChitterClientPromise.class.toString());
    public boolean ready = false;
    public PromiseClient aerieClientPromise;
    private Semaphore sendPermits;
    private Random rand = new Random(System.currentTimeMillis());
    private String socialNetworkFile;
    private Map<ObjId, SocialNetworkGenerator.RandomUser> users = new HashMap<>();
    private ChitterApplicationProcedure appProcedure = new ChitterApplicationProcedure();

    private boolean legacySSMR;
    public ChitterClientPromise(long clientId, String systemConfigFile, String partitioningFile) {
        this(clientId, systemConfigFile, partitioningFile, Integer.MAX_VALUE);
    }

    public ChitterClientPromise(long clientId, String systemConfigFile, String partitioningFile, int numPermits) {
        aerieClientPromise = new PromiseClient((int) clientId, systemConfigFile, partitioningFile, new ChitterApplicationProcedure(), new CacheHashMap());
        sendPermits = new Semaphore(numPermits);
    }

    public void setLegacySSMR(boolean legacySSMR) {
        this.legacySSMR = legacySSMR;
    }

    public static void main(String[] args) {

        log.info("Entering ChitterClient.main(...)");

        int clientId = Integer.parseInt(args[0]);
        String systemConfigFile = args[1];
        String partitionsConfigFile = args[2];
        int numPermits = args.length > 3 ? Integer.parseInt(args[3]) : 1;
//        ChitterOracle appProcedure = ChitterOracle.getInstance();
        ChitterClientPromise chitterClient = new ChitterClientPromise(clientId, systemConfigFile, partitionsConfigFile, numPermits);
        chitterClient.socialNetworkFile = args.length > 4 ? args[4] : "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/users_1000_partitions_2.json";

        // System.out.println("Running command: chitterClient.sendTTYCommand(\"NEW\", 0, 0, null);");
        // chitterClient.sendTTYCommand("NEW", Integer.MAX_VALUE, 0, null);
//        chitterClient.createSociaNetworkUsers(chitterClient.socialNetworkFile);
//        while (!chitterClient.ready) {
//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        chitterClient.loadSocialNetworkIntoCache(chitterClient.socialNetworkFile);
        chitterClient.runInteractive(chitterClient);

    }

    public void createSociaNetworkUsers(String socialNetworkFile) {
        Set<SocialNetworkGenerator.RandomUser> randomUsers = Util.loadSocialNetwork(socialNetworkFile);
        AtomicLong userCount = new AtomicLong(0);
        for (SocialNetworkGenerator.RandomUser user : randomUsers) {
            Consumer newcbh = (message) -> {
                addSendPermit();
                if (userCount.incrementAndGet() == randomUsers.size()) {
                    this.ready = true;
                    System.out.println("Client " + this.getId() + "ready");
                }
                Message reply = (Message) message;
                User u = (User) reply.getItem(0);
                FollowersCache userFollowerCache = FollowersCache.getCacheForUser(u.getId());
                userFollowerCache.setCache(u.getFollowerList().getRawList());
            };
            createUserNB(User.genObjId(user.userId), newcbh);
        }
    }


    void addSendPermit() {
        sendPermits.release();
    }

    public int getId() {
        return aerieClientPromise.getId();
    }

    void getSendPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void loadSocialNetworkIntoCache(String file) {
        Set<SocialNetworkGenerator.RandomUser> randomUsers = Util.loadSocialNetwork(file);
        for (SocialNetworkGenerator.RandomUser user : randomUsers) {

            ObjId userId = User.genObjId(user.userId);
            appProcedure.userCache.put(userId, user);
            PRObjectLocation cache = new PRObjectLocation(userId, user.partitionId);
            this.aerieClientPromise.setCache(userId, cache);
            FollowersCache followersCache = FollowersCache.getCacheForUser(User.genObjId(user.userId));
            Set<ObjId> followers = new HashSet<>();
            followers.addAll(user.followers.stream().map(User::genObjId).collect(Collectors.toList()));
            followersCache.addFollowers(followers);
        }
    }


    // ================================
    // ChitterClient User API
    // ================================

    void sendTTYCommand(String cmdtype, long arg1, long uid2, String data) {
        if (!cmdtype.equalsIgnoreCase("burstpost") && !cmdtype.equalsIgnoreCase("bursttl")) {
            getSendPermit();
        }

        System.out.println(String.format("Sending command %s %d %d %s", cmdtype, arg1, uid2, data));

        if (cmdtype.equalsIgnoreCase("new")) {
            Consumer newcbh = (message) -> {
                addSendPermit();
                Message reply = (Message) message;
                User user = (User) reply.getItem(0);
                FollowersCache userFollowerCache = FollowersCache.getCacheForUser(user.getId());
                userFollowerCache.setCache(user.getFollowerList().getRawList());
                System.out.println("(CONS) Successfully created User " + user.getId());
            };
            createUserNB(User.genObjId(arg1), newcbh);
        } else if (cmdtype.equalsIgnoreCase("f")) {
            Consumer followcbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };
            followNB(User.genObjId(arg1), User.genObjId(uid2), followcbh);
        } else if (cmdtype.equalsIgnoreCase("uf")) {
            Consumer unfollowcbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };

            unfollowNB(User.genObjId(arg1), User.genObjId(uid2), unfollowcbh);
        } else if (cmdtype.equalsIgnoreCase("p")) {
            Consumer postcbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };

            postNB(User.genObjId(arg1), new Post((long) aerieClientPromise.getId(), User.genObjId(arg1), System.currentTimeMillis(), data.getBytes()), postcbh);
        } else if (cmdtype.equalsIgnoreCase("tl")) {
            Consumer timelinecbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };

            getTimelineNB(User.genObjId(arg1), timelinecbh);
        } else if (cmdtype.equalsIgnoreCase("pl")) {
            Consumer postslistscbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };
            getPostsNB(User.genObjId(arg1), postslistscbh);
        } else if (cmdtype.equalsIgnoreCase("fr")) {
            Consumer followerscbh = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };
            getFollowersNB(User.genObjId(arg1), followerscbh);
        } else if (cmdtype.equalsIgnoreCase("fd")) {
            Consumer followedschb = (message) -> {
                addSendPermit();
                System.out.println("(CONS) Successfully done: " + message);
            };
            getFollowedsNB(User.genObjId(arg1), followedschb);
        } else if (cmdtype.equalsIgnoreCase("burstpost")) {
            for (int i = 0; i < arg1; i++) {
                int puid1 = rand.nextInt(1000);
                String postdata = "qwertyuiopasdfghjkl;zxcvbnm";
                sendTTYCommand("p", puid1, 0, postdata);
            }
        } else if (cmdtype.equalsIgnoreCase("bursttl")) {
            for (int i = 0; i < arg1; i++) {
                int tluid1 = rand.nextInt(1000);
                sendTTYCommand("tl", tluid1, 0, null);
            }
        } else {
            System.err.println("wrong command type");
        }
    }

    public void createUserNB(ObjId userId, Consumer then) {
        log.info("Calling createUserNB for user id {}.", userId);
        Command command = new Command(GenericCommand.CREATE, userId);
        aerieClientPromise.create(command).thenAccept(then);
    }

    public void postNB(ObjId userId, Post post, Consumer then) {
        log.info("User {} posting {}.", userId, post);
        Set<ObjId> estimatedFollowers = FollowersCache.getCacheForUser(userId).getFollowersCache();
        Command command = new Command(AppCommandType.POST, userId, estimatedFollowers, post);
        Consumer postReplyHandler = new PostReplyHandler(then, this, userId, post);
        //aerieClientPromise.sendCommandAsync(postReplyHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(postReplyHandler);
        else
            aerieClientPromise.update(command).thenAccept(postReplyHandler);
    }

    public void followNB(ObjId followerId, ObjId followedId, Consumer then) {
        log.info("User {} followNB'ing user {}.", followerId, followedId);
        List<ObjId> followedIds = new ArrayList<>();
        followedIds.add(followedId);
        Command command = new Command(AppCommandType.FOLLOW, followerId, followedIds);
//        aerieClientPromise.sendCommandAsync(cbHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(then);
        else
            aerieClientPromise.update(command).thenAccept(then);
    }

    public void follow_NOP_NB(ObjId followerId, ObjId followedId, Consumer then) {
        log.info("User {} follow_nop_NB'ing user {}.", followerId, followedId);
        List<ObjId> followedIds = new ArrayList<>();
        followedIds.add(followedId);
        Command command = new Command(AppCommandType.FOLLOW_NOP, followerId, followedIds);
//        aerieClientPromise.sendCommandAsync(cbHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(then);
        else
            aerieClientPromise.update(command).thenAccept(then);
    }


    public void unfollowNB(ObjId followerId, ObjId followedId, Consumer then) {
        log.info("User {} unfollowNB'ing user {}.", followerId, followedId);
        Command command = new Command(AppCommandType.UNFOLLOW, followerId, followedId);
//        aerieClientPromise.sendCommandAsync(cbHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(then);
        else
            aerieClientPromise.update(command).thenAccept(then);
    }

    public void unfollow_NOP_NB(ObjId followerId, ObjId followedId, Consumer then) {
        log.info("User {} unfollowNB'ing user {}.", followerId, followedId);
        Command command = new Command(AppCommandType.UNFOLLOW_NOP, followerId, followedId);
//        aerieClientPromise.sendCommandAsync(cbHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(then);
        else
            aerieClientPromise.update(command).thenAccept(then);
    }


    public void getTimelineNB(ObjId userId, Consumer then) {
        log.info("User {} requesting their.", userId);
        Command command = new Command(AppCommandType.GETTIMELINE, userId);
//        aerieClientPromise.sendCommandAsync(cbHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(then);
        else
            aerieClientPromise.update(command).thenAccept(then);
    }

    public void getPostsNB(ObjId userId, Consumer then) {
        log.info("User {} requesting their posts.", userId);
        Command command = new Command(AppCommandType.GETPOSTS, userId);
//        aerieClientPromise.sendCommandAsync(cbHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(then);
        else
            aerieClientPromise.update(command).thenAccept(then);
    }

    public void getFollowersNB(ObjId userId, Consumer then) {
        log.info("User {} requesting their followers.", userId);
        Command command = new Command(AppCommandType.GETFOLLOWERLIST, userId);
//        aerieClientPromise.sendCommandAsync(cbHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(then);
        else
            aerieClientPromise.update(command).thenAccept(then);
    }

    public void getFollowedsNB(ObjId userId, Consumer then) {
        log.info("User {} requesting their followeds.", userId);
        Command command = new Command(AppCommandType.GETFOLLOWEDLIST, userId);
//        aerieClientPromise.sendCommandAsync(cbHandler, context, command);
        if (this.legacySSMR)
            aerieClientPromise.sendSSMRCommand(command).thenAccept(then);
        else
            aerieClientPromise.update(command).thenAccept(then);
    }

    public void runInteractive(ChitterClientPromise chitterClient) {
        Scanner scan = new Scanner(System.in);
        String input;

        System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
        input = scan.nextLine();
        while (!input.equalsIgnoreCase("end")) {
            try {
                String[] parts = input.split(" ");
                String cmdtype = parts[0];
                int arg1 = 0;
                int uid2 = 0;
                String data = null;

                boolean validCommand = true;

                if (cmdtype.equalsIgnoreCase("new")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("p")) {
                    arg1 = Integer.parseInt(parts[1]);
                    data = parts[2];
                } else if (cmdtype.equalsIgnoreCase("f")) {
                    arg1 = Integer.parseInt(parts[1]);
                    uid2 = Integer.parseInt(parts[2]);
                } else if (cmdtype.equalsIgnoreCase("uf")) {
                    arg1 = Integer.parseInt(parts[1]);
                    uid2 = Integer.parseInt(parts[2]);
                } else if (cmdtype.equalsIgnoreCase("tl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("fr")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("fd")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("pl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("burstpost")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("bursttl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else {
                    System.out.println("Invalid command.");
                    validCommand = false;
                }

                if (validCommand)
                    chitterClient.sendTTYCommand(cmdtype, arg1, uid2, data);

                System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
            } catch (Exception e) {
                System.err.println("Probably invalid input.");
                e.printStackTrace();
                System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
            }

            input = scan.nextLine();

        }
        scan.close();
    }


    public static class FollowersCache {
        private static Map<ObjId, FollowersCache> allCaches = new ConcurrentHashMap<>();
        private Set<ObjId> followers = new HashSet<>();

        synchronized public static FollowersCache getCacheForUser(ObjId userId) {
            FollowersCache cache = allCaches.get(userId);
            if (cache == null) {
                cache = new FollowersCache();
                allCaches.put(userId, cache);
            }
            return cache;
        }

        synchronized public void setCache(Set<ObjId> followersList) {
            this.followers = new HashSet<>(followersList);
        }

        synchronized public void updateCache(Set<ObjId> followersToAdd, Set<ObjId> followersToRemove) {
            addFollowers(followersToAdd);
            removeFollowers(followersToRemove);
        }

        synchronized public void addFollowers(Set<ObjId> followersToAdd) {
            followers.addAll(followersToAdd);
        }

        synchronized public void removeFollowers(Set<ObjId> followersToRemove) {
            followers.removeAll(followersToRemove);
        }

        synchronized public Set<ObjId> getFollowersCache() {
            return new HashSet<>(followers);
        }
    }

    public static class PostReplyHandler implements Consumer {
        Consumer userHandler;
        //        Object userContext;
        ChitterClientPromise client;
        ObjId userId;
        Post post;
        // the command's followerCache never decreases, ensuring termination of the request execution
        FollowersCache commandFollowerCache;
        FollowersCache clientFollowerCache;

        public PostReplyHandler(Consumer userHandler, ChitterClientPromise client, ObjId userId, Post post) {
            this.userHandler = userHandler;
//            this.userContext = userContext;
            this.client = client;
            this.userId = userId;
            this.post = post;
            clientFollowerCache = FollowersCache.getCacheForUser(userId);
            commandFollowerCache = new FollowersCache();
            commandFollowerCache.setCache(clientFollowerCache.getFollowersCache());
        }

        @Override
        public void accept(Object message) {
            Message reply = (Message) message;
            reply.rewind();
            String status = (String) reply.getNext();

            // System.out.println("Received " + (optimistic ? "opt" : "cons") + " reply for POST with status \"" + status + "\"");

            Message replyIfOK = new Message("OK");
            replyIfOK.copyTimelineStamps(reply);

            Set<ObjId> followersToAdd = (Set<ObjId>) reply.getNext();
            Set<ObjId> followersToRemove = (Set<ObjId>) reply.getNext();
            clientFollowerCache.updateCache(followersToAdd, followersToRemove);

            if (status.equals("RETRY")) {
                System.out.println("WILL NOT COME TO THIS - Retrying post with more followers");
                commandFollowerCache.addFollowers(followersToAdd);
                Set<ObjId> estimatedFollowers = commandFollowerCache.getFollowersCache();
                Command command = new Command(AppCommandType.POST, userId, estimatedFollowers, post);
                client.aerieClientPromise.update(command);
//                client.aerieClientPromise.sendCommandAsync(this, userContext, command);
            } else { // if status.equal("OK")
//                 System.out.println("handling POST reply with status \"" + status + "\"");
                userHandler.accept(replyIfOK);
            }


        }
    }
}
