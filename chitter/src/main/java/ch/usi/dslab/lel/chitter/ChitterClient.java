/*

 Chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of Chitter.
 
 Chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Cache.PRObjectLocation;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.client.AsyncClient;
import ch.usi.dslab.lel.aerie.client.CallbackHandlerWithContext;
import ch.usi.dslab.lel.chitter.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chitter.util.Util;

public class ChitterClient {

    public static final Logger log = LogManager.getLogger(ChitterClient.class.toString());
    public boolean ready = false;
    public AsyncClient aerieAsyncClient;
    private Semaphore sendPermits;
    private Random rand = new Random(System.currentTimeMillis());
    private String socialNetworkFile;
    private Map<ObjId, SocialNetworkGenerator.RandomUser> users = new HashMap<>();
    private ChitterApplicationProcedure oracle = new ChitterApplicationProcedure();

    public ChitterClient(long clientId, String systemConfigFile, String partitioningFile) {
        this(clientId, systemConfigFile, partitioningFile, Integer.MAX_VALUE);
    }

    public ChitterClient(long clientId, String systemConfigFile, String partitioningFile, int numPermits) {
        aerieAsyncClient = new AsyncClient((int) clientId, systemConfigFile, partitioningFile, new ChitterApplicationProcedure());
        sendPermits = new Semaphore(numPermits);
    }

    public static void main(String[] args) {

        log.info("Entering ChitterClient.main(...)");

        int clientId = Integer.parseInt(args[0]);
        String systemConfigFile = args[1];
        String partitionsConfigFile = args[2];
        int numPermits = args.length > 3 ? Integer.parseInt(args[3]) : 1;
//        ChitterOracle oracle = ChitterOracle.getInstance();
        ChitterClient chitterClient = new ChitterClient(clientId, systemConfigFile, partitionsConfigFile, numPermits);
        chitterClient.socialNetworkFile = args.length > 4 ? args[4] : "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/src/main/java/ch/usi/dslab/lel/chitter/users_1000_partitions_2.json";

        // System.out.println("Running command: chitterClient.sendTTYCommand(\"NEW\", 0, 0, null);");
        // chitterClient.sendTTYCommand("NEW", Integer.MAX_VALUE, 0, null);
//        chitterClient.createSociaNetworkUsers(chitterClient.socialNetworkFile);
//        while (!chitterClient.ready) {
//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        chitterClient.loadSocialNetworkIntoCache(chitterClient.socialNetworkFile);
        chitterClient.runInteractive(chitterClient);

    }

    public void createSociaNetworkUsers(String socialNetworkFile) {
        Set<SocialNetworkGenerator.RandomUser> randomUsers = Util.loadSocialNetwork(socialNetworkFile);
        AtomicLong userCount = new AtomicLong(0);
        for (SocialNetworkGenerator.RandomUser user : randomUsers) {
            CallbackHandlerWithContext newcbh = (returnedData, context) -> {
                addSendPermit();
                if (userCount.incrementAndGet() == randomUsers.size()) {
                    this.ready = true;
                    System.out.println("Client " + this.getId() + "ready");
                }
                Message reply = (Message) returnedData;
                User u = (User) reply.getItem(0);
                FollowersCache userFollowerCache = FollowersCache.getCacheForUser(u.getId());
                userFollowerCache.setCache(u.getFollowerList().getRawList());
            };
            createUserNB(User.genObjId(user.userId), newcbh, user.userId);
        }
    }

    void addSendPermit() {
        sendPermits.release();
    }

    public int getId() {
        return aerieAsyncClient.getId();
    }

    public void createUserNB(ObjId userId, CallbackHandlerWithContext conservativeHandler, Object context) {
        log.info("Calling createUserNB for user id {}.", userId);
        Command command = new Command(GenericCommand.CREATE, userId);
        aerieAsyncClient.create(conservativeHandler, context, command);
    }

    void getSendPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void loadSocialNetworkIntoCache(String file) {
        Set<SocialNetworkGenerator.RandomUser> randomUsers = Util.loadSocialNetwork(file);
        for (SocialNetworkGenerator.RandomUser user : randomUsers) {

            ObjId userId = User.genObjId(user.userId);
            oracle.userCache.put(userId, user);
            PRObjectLocation cache = new PRObjectLocation(userId, user.partitionId);
            this.aerieAsyncClient.setCache(userId, cache);
            FollowersCache followersCache = FollowersCache.getCacheForUser(User.genObjId(user.userId));
            Set<ObjId> followers = new HashSet<>();
            followers.addAll(user.followers.stream().map(User::genObjId).collect(Collectors.toList()));
            followersCache.addFollowers(followers);
        }
    }

//    public String createUser(ObjId userId) {
//        log.info("Calling createUser for user id {}.", userId);
//        Command command = new Command(CommandType.NEWUSER, userId);
//        Message reply = aerieAsyncClient.sendCommandSync(command);
//        return (String) reply.getNext();
//    }


    // ================================
    // ChitterClient User API
    // ================================

    void sendTTYCommand(String cmdtype, long arg1, long uid2, String data) {
        if (!cmdtype.equalsIgnoreCase("burstpost") && !cmdtype.equalsIgnoreCase("bursttl")) {
            System.out.print("Waiting for permit...");
            getSendPermit();
            System.out.println(" got it.");
        }

        System.out.println(String.format("Sending command %s %d %d %s", cmdtype, arg1, uid2, data));

        if (cmdtype.equalsIgnoreCase("new")) {
            CallbackHandlerWithContext newcbh = (returnedData, context) -> {
                addSendPermit();
                Message reply = (Message) returnedData;
                User user = (User) reply.getItem(0);
                FollowersCache userFollowerCache = FollowersCache.getCacheForUser(user.getId());
                userFollowerCache.setCache(user.getFollowerList().getRawList());
                System.out.println("(CONS) Successfully created User " + user.getId());
            };
            createUserNB(User.genObjId(arg1), newcbh, arg1);
        } else if (cmdtype.equalsIgnoreCase("f")) {
            CallbackHandlerWithContext followcbh = (returnedData, context) -> {
                addSendPermit();
                String reqstring = (String) context;
                Message reply = (Message) returnedData;
                String retval = (String) reply.getNext();
                System.out.println("(CONS) Successfully done: " + reqstring + " : " + retval);
            };
            followNB(User.genObjId(arg1), User.genObjId(uid2), followcbh, "User " + arg1 + " follow user " + uid2);
        } else if (cmdtype.equalsIgnoreCase("uf")) {
            CallbackHandlerWithContext unfollowcbh = (returnedData, context) -> {
                addSendPermit();
                String reqstring = (String) context;
                Message reply = (Message) returnedData;
                String retval = (String) reply.getNext();
                System.out.println("(CONS) Successfully done: " + reqstring + " : " + retval);
            };

            unfollowNB(User.genObjId(arg1), User.genObjId(uid2), unfollowcbh, "User " + arg1 + " unfollow user " + uid2);
        } else if (cmdtype.equalsIgnoreCase("p")) {
            CallbackHandlerWithContext postcbh = (returnedData, context) -> {
                addSendPermit();
                String reqstring = (String) context;
                Message reply = (Message) returnedData;
                String retval = (String) reply.getNext();
                System.out.println("(CONS) Successfully done: " + reqstring + " : " + retval);
            };

            postNB(User.genObjId(arg1), new Post((long) aerieAsyncClient.getId(), User.genObjId(arg1), System.currentTimeMillis(), data.getBytes()), postcbh, "User " + User.genObjId(arg1) + " posted " + data);
        } else if (cmdtype.equalsIgnoreCase("tl")) {
            CallbackHandlerWithContext timelinecbh = (returnedData, context) -> {
                addSendPermit();
                String reqstring = (String) context;
                Message reply = (Message) returnedData;
                List<Post> timeline = (List<Post>) reply.getNext();
                System.out.println("(CONS) Successfully done: " + reqstring);
                System.out.println(timeline.size() + " posts in the timeline");
                timeline.forEach(System.out::println);
            };

            getTimelineNB(User.genObjId(arg1), timelinecbh, "user " + User.genObjId(arg1) + " getTimeline");
        } else if (cmdtype.equalsIgnoreCase("pl")) {
            CallbackHandlerWithContext postslistscbh = (returnedData, context) -> {
                addSendPermit();
                String reqstring = (String) context;
                Message reply = (Message) returnedData;
                List<Post> posts = (List<Post>) reply.getNext();
                System.out.println("(CONS) Successfully done: " + reqstring);
                System.out.println(posts.size() + " posts of user");
                posts.forEach(System.out::println);
            };
            getPostsNB(User.genObjId(arg1), postslistscbh, "user " + User.genObjId(arg1) + " getTimeline");
        } else if (cmdtype.equalsIgnoreCase("fr")) {
            CallbackHandlerWithContext followerscbh = (returnedData, context) -> {
                addSendPermit();
                String reqstring = (String) context;
                Message reply = (Message) returnedData;
                Set<ObjId> followers = (Set<ObjId>) reply.getNext();
                System.out.println("(CONS) Successfully done: " + reqstring);
                System.out.println(followers.size() + " followers");
                for (ObjId follower : followers)
                    System.out.println("\t" + follower);
            };
            getFollowersNB(User.genObjId(arg1), followerscbh, "user " + User.genObjId(arg1) + " getFollowers");
        } else if (cmdtype.equalsIgnoreCase("fd")) {
            CallbackHandlerWithContext followedschb = (returnedData, context) -> {
                addSendPermit();
                String reqstring = (String) context;
                Message reply = (Message) returnedData;
                Set<ObjId> followeds = (Set<ObjId>) reply.getNext();
                System.out.println("(CONS) Successfully done: " + reqstring);
                System.out.println(followeds.size() + " followeds");
                for (ObjId followed : followeds)
                    System.out.println("\t" + followed);
            };
            getFollowedsNB(User.genObjId(arg1), followedschb, "user " + User.genObjId(arg1) + " getFolloweds");
        } else if (cmdtype.equalsIgnoreCase("burstpost")) {
            for (int i = 0; i < arg1; i++) {
                int puid1 = rand.nextInt(1000);
                String postdata = "qwertyuiopasdfghjkl;zxcvbnm";
                sendTTYCommand("p", puid1, 0, postdata);
            }
        } else if (cmdtype.equalsIgnoreCase("bursttl")) {
            for (int i = 0; i < arg1; i++) {
                int tluid1 = rand.nextInt(1000);
                sendTTYCommand("tl", tluid1, 0, null);
            }
        } else {
            System.err.println("wrong command type");
        }
    }

//    @SuppressWarnings("unchecked")
//    public String post(ObjId userId, Post post) {
//        log.info("User {} requesting to post {}.", userId, post);
//        FollowersCache clientFollowerCache = FollowersCache.getCacheForUser(userId);
//        FollowersCache commandFollowerCache = new FollowersCache();
//        commandFollowerCache.setCache(clientFollowerCache.getFollowersCache());
//        String status = "none";
//        while (status.equals("OK") == false) {
//            Set<Long> estimatedFollowers = commandFollowerCache.getFollowersCache();
//            Command command = new Command(CommandType.POST, userId, estimatedFollowers, post);
//            Message reply = aerieAsyncClient.sendCommandSync(command);
//            status = (String) reply.getNext();
//            Set<Long> followersToAdd = (Set<Long>) reply.getNext();
//            Set<Long> followersToRemove = (Set<Long>) reply.getNext();
//            clientFollowerCache.updateCache(followersToAdd, followersToRemove);
//            if (status.equals("RETRY")) {
//                commandFollowerCache.addFollowers(followersToAdd);
//            }
//        }
//        return "OK";
//    }

    public void postNB(ObjId userId, Post post, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} posting {}.", userId, post);
        Set<ObjId> estimatedFollowers = FollowersCache.getCacheForUser(userId).getFollowersCache();
        Command command = new Command(AppCommandType.POST, userId, estimatedFollowers, post);
        CallbackHandlerWithContext postReplyHandler = new PostReplyHandler(cbHandler, context, this, userId, post);
//        aerieAsyncClient.sendCommandAsync(postReplyHandler, context, command);
        aerieAsyncClient.sendUpdate(postReplyHandler, context, command);
    }

//    public String follow(ObjId followerId, ObjId followedId) {
//        log.info("User {} requesting to follow {}.", followerId, followedId);
//        Command command = new Command(CommandType.FOLLOW, followerId, followedId);
//        Message reply = aerieAsyncClient.sendCommandSync(command);
//        return (String) reply.getNext();
//    }

    public void followNB(ObjId followerId, ObjId followedId, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} followNB'ing user {}.", followerId, followedId);
        List<ObjId> followedIds = new ArrayList<>();
        followedIds.add(followedId);
        Command command = new Command(AppCommandType.FOLLOW, followerId, followedIds);
//        aerieAsyncClient.sendCommandAsync(cbHandler, context, command);
        aerieAsyncClient.sendUpdate(cbHandler, context, command);
    }

    public void follow_NOP_NB(ObjId followerId, ObjId followedId, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} follow_nop_NB'ing user {}.", followerId, followedId);
        List<ObjId> followedIds = new ArrayList<>();
        followedIds.add(followedId);
        Command command = new Command(AppCommandType.FOLLOW_NOP, followerId, followedIds);
//        aerieAsyncClient.sendCommandAsync(cbHandler, context, command);
        aerieAsyncClient.sendUpdate(cbHandler, context, command);
    }

//    public String unfollow(ObjId followerId, ObjId followedId) {
//        log.info("User {} requesting to unfollow {}.", followerId, followedId);
//        Command command = new Command(CommandType.UNFOLLOW, followerId, followedId);
//        Message reply = aerieAsyncClient.sendCommandSync(command);
//        return (String) reply.getNext();
//    }

    public void unfollowNB(ObjId followerId, ObjId followedId, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} unfollowNB'ing user {}.", followerId, followedId);
        Command command = new Command(AppCommandType.UNFOLLOW, followerId, followedId);
//        aerieAsyncClient.sendCommandAsync(cbHandler, context, command);
        aerieAsyncClient.sendUpdate(cbHandler, context, command);
    }

    public void unfollow_NOP_NB(ObjId followerId, ObjId followedId, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} unfollowNB'ing user {}.", followerId, followedId);
        Command command = new Command(AppCommandType.UNFOLLOW_NOP, followerId, followedId);
//        aerieAsyncClient.sendCommandAsync(cbHandler, context, command);
        aerieAsyncClient.sendUpdate(cbHandler, context, command);
    }

//    @SuppressWarnings("unchecked")
//    public List<Post> getTimeline(ObjId userId) {
//        log.info("User {} requesting their timeline.", userId);
//        Command command = new Command(CommandType.GETTIMELINE, userId);
//        Message reply = aerieAsyncClient.sendCommandSync(command);
//        List<Post> timeline = (List<Post>) reply.getNext();
//        // needs to be sorted (sorting is done in the client to save server's cpu):
//        Collections.sort(timeline, Post.comparatorNewestFirst);
//
//        return timeline;
//    }

    public void getTimelineNB(ObjId userId, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} requesting their.", userId);
        Command command = new Command(AppCommandType.GETTIMELINE, userId);
//        aerieAsyncClient.sendCommandAsync(cbHandler, context, command);
        aerieAsyncClient.sendUpdate(cbHandler, context, command);
    }

    public void getPostsNB(ObjId userId, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} requesting their posts.", userId);
        Command command = new Command(AppCommandType.GETPOSTS, userId);
//        aerieAsyncClient.sendCommandAsync(cbHandler, context, command);
        aerieAsyncClient.sendUpdate(cbHandler, context, command);
    }

    public void getFollowersNB(ObjId userId, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} requesting their followers.", userId);
        Command command = new Command(AppCommandType.GETFOLLOWERLIST, userId);
//        aerieAsyncClient.sendCommandAsync(cbHandler, context, command);
        aerieAsyncClient.sendUpdate(cbHandler, context, command);
    }

    public void getFollowedsNB(ObjId userId, CallbackHandlerWithContext cbHandler, Object context) {
        log.info("User {} requesting their followeds.", userId);
        Command command = new Command(AppCommandType.GETFOLLOWEDLIST, userId);
//        aerieAsyncClient.sendCommandAsync(cbHandler, context, command);
        aerieAsyncClient.sendUpdate(cbHandler, context, command);
    }

    public void runInteractive(ChitterClient chitterClient) {
        Scanner scan = new Scanner(System.in);
        String input;

        System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
        input = scan.nextLine();
        while (!input.equalsIgnoreCase("end")) {
            try {
                String[] parts = input.split(" ");
                String cmdtype = parts[0];
                int arg1 = 0;
                int uid2 = 0;
                String data = null;

                boolean validCommand = true;

                if (cmdtype.equalsIgnoreCase("new")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("p")) {
                    arg1 = Integer.parseInt(parts[1]);
                    data = parts[2];
                } else if (cmdtype.equalsIgnoreCase("f")) {
                    arg1 = Integer.parseInt(parts[1]);
                    uid2 = Integer.parseInt(parts[2]);
                } else if (cmdtype.equalsIgnoreCase("uf")) {
                    arg1 = Integer.parseInt(parts[1]);
                    uid2 = Integer.parseInt(parts[2]);
                } else if (cmdtype.equalsIgnoreCase("tl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("fr")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("fd")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("pl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("burstpost")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else if (cmdtype.equalsIgnoreCase("bursttl")) {
                    arg1 = Integer.parseInt(parts[1]);
                } else {
                    System.out.println("Invalid command.");
                    validCommand = false;
                }

                if (validCommand)
                    chitterClient.sendTTYCommand(cmdtype, arg1, uid2, data);

                System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
            } catch (Exception e) {
                System.err.println("Probably invalid input.");
                e.printStackTrace();
                System.out.println("Type: new/p/f/uf/tl/fr/fd/pl/burstpost/bursttl [uid1] [uid2] [data_to_post] [burst_length]:");
            }

            input = scan.nextLine();

        }
        scan.close();
    }
//
//   public int init() {
//      return 0;
//   }
//
//   public int destroy() {
//      return 0;
//   }
//   
//   // parses server list in the format addr:port:part[,addr2:port2:part2[,...]]
//   // e.g.: node1:60001:1,node2:60002:1,node3:60003:2,node4:60004:2
//   public static Map<Integer, PartitionServers> parseServerList(String serverList) {
//      String[] servers = serverList.split(",");
//      Map<Integer, PartitionServers> partServers = new HashMap<Integer, PartitionServers>(servers.length);
//      for (String server : servers) {
//         String addr         = server.split(":")[0];
//         String portStr      = server.split(":")[1];
//         String partitionStr = server.split(":")[2];
//         int port      = Integer.parseInt(portStr);
//         int partition = Integer.parseInt(partitionStr);
//         
//         PartitionServers ps = partServers.get(partition);
//         if (ps == null) {
//            ps = new PartitionServers();
//            partServers.put(partition, ps);
//         }
//         
//         ps.add(new Address(addr, port));
//      }
//      return partServers;
//   }

    public static class FollowersCache {
        private static Map<ObjId, FollowersCache> allCaches = new ConcurrentHashMap<>();
        private Set<ObjId> followers = new HashSet<>();

        synchronized public static FollowersCache getCacheForUser(ObjId userId) {
            FollowersCache cache = allCaches.get(userId);
            if (cache == null) {
                cache = new FollowersCache();
                allCaches.put(userId, cache);
            }
            return cache;
        }

        synchronized public void setCache(Set<ObjId> followersList) {
            this.followers = new HashSet<>(followersList);
        }

        synchronized public void updateCache(Set<ObjId> followersToAdd, Set<ObjId> followersToRemove) {
            addFollowers(followersToAdd);
            removeFollowers(followersToRemove);
        }

        synchronized public void addFollowers(Set<ObjId> followersToAdd) {
            if (followersToAdd != null)
                followers.addAll(followersToAdd);
        }

        synchronized public void removeFollowers(Set<ObjId> followersToRemove) {
            if (followersToRemove != null)
                followers.removeAll(followersToRemove);
        }

        synchronized public Set<ObjId> getFollowersCache() {
            return new HashSet<>(followers);
        }
    }

    public static class PostReplyHandler implements CallbackHandlerWithContext {
        CallbackHandlerWithContext userHandler;
        Object userContext;
        ChitterClient client;
        ObjId userId;
        Post post;
        // the command's followerCache never decreases, ensuring termination of the request execution
        FollowersCache commandFollowerCache;
        FollowersCache clientFollowerCache;

        public PostReplyHandler(CallbackHandlerWithContext userHandler, Object userContext, ChitterClient client, ObjId userId, Post post) {
            this.userHandler = userHandler;
            this.userContext = userContext;
            this.client = client;
            this.userId = userId;
            this.post = post;
            clientFollowerCache = FollowersCache.getCacheForUser(userId);
            commandFollowerCache = new FollowersCache();
            commandFollowerCache.setCache(clientFollowerCache.getFollowersCache());
        }

        @SuppressWarnings("unchecked")
        public void success(Object returnedData, Object context) {
            Message reply = (Message) returnedData;
            String status = (String) reply.getNext();

            // System.out.println("Received " + (optimistic ? "opt" : "cons") + " reply for POST with status \"" + status + "\"");

            Message replyIfOK = new Message("OK");
            replyIfOK.copyTimelineStamps(reply);

            Set<ObjId> followersToAdd = (Set<ObjId>) reply.getNext();
            Set<ObjId> followersToRemove = (Set<ObjId>) reply.getNext();
            clientFollowerCache.updateCache(followersToAdd, followersToRemove);

            if (status.equals("RETRY") && followersToAdd != null) {
                System.out.println("WILL NOT COME TO THIS - Retrying post with more followers");
                commandFollowerCache.addFollowers(followersToAdd);
                Set<ObjId> estimatedFollowers = commandFollowerCache.getFollowersCache();
                Command command = new Command(AppCommandType.POST, userId, estimatedFollowers, post);
                client.aerieAsyncClient.sendUpdate(this, userContext, command);
//                client.aerieAsyncClient.sendCommandAsync(this, userContext, command);
            } else { // if status.equal("OK")
//                 System.out.println("handling POST reply with status \"" + status + "\"");
                userHandler.success(replyIfOK, userContext);
            }


        }
    }
}
