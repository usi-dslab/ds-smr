/*

 Chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of Chitter.
 
 Chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter.benchmarks;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.LatencyDistributionPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.TimelinePassiveMonitor;
import ch.usi.dslab.lel.aerie.client.EvenCallBack;
import ch.usi.dslab.lel.chitter.AppCommandType;
import ch.usi.dslab.lel.chitter.ChitterClientPromise;
import ch.usi.dslab.lel.chitter.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chitter.util.Util;

public class TestRunnerPromise {
    private static double skew = 1.2d;
    Random randomGenerator;
    SocialNetworkGenerator.RandomNumberGenerator zipfRand = SocialNetworkGenerator.RandomNumberGenerator.getInstance();
    SocialNetworkGenerator.RandomNumberGenerator interestZipfRand;

    ClientPromiseInterface client;
    Semaphore sendPermits;
    int numUsers;
    long clientId;
    double totalWeight;
    int totalChance;
    double postWeight;
    double followWeight;
    double unfollowWeight;
    double getTimelineWeight;
    BiConsumer callbackHandler;
    Set<SocialNetworkGenerator.RandomUser> socialNetwork;
    Map<Integer, SocialNetworkGenerator.RandomUser> users = new ConcurrentHashMap<>();
    Map<Integer, List<SocialNetworkGenerator.RandomUser>> partitionMapping = new ConcurrentHashMap<>();
    Map<Integer, List<SocialNetworkGenerator.RandomUser>> interestMap = new ConcurrentHashMap<>();
    Map<Integer, Integer> interestRate = new ConcurrentHashMap<>();
    int partitionNum = 0;
    boolean linked = false;
    final boolean uniformRandom;

    ThroughputPassiveMonitor moveMonitor;
    ThroughputPassiveMonitor queryMonintor;
    ThroughputPassiveMonitor retryMonitor;
    ThroughputPassiveMonitor localCommandMonitor;
    ThroughputPassiveMonitor globalCommandMonitor;

    public TestRunnerPromise(ClientPromiseInterface client, int clientId, int numPermits, String socialNetworkFile,
                             double postProbability, double followProbability, double unfollowProbability,
                             double getTimelineProbability, boolean uniformRandom, boolean legacySSMR) {

        randomGenerator = new Random(System.nanoTime());
        this.client = client;
        this.client.setLegacySSMR(legacySSMR);
        this.uniformRandom = uniformRandom;

        sendPermits = new Semaphore(numPermits);
        socialNetwork = Util.loadSocialNetwork(socialNetworkFile);
        String[] params = socialNetworkFile.split("_");
        if (socialNetworkFile.indexOf("Linked") > 0) linked = true;
//        linked = true;
        String[] tmp = params[params.length - 1].split("\\.");
        partitionNum = Integer.parseInt(tmp[0]);
        for (SocialNetworkGenerator.RandomUser user : socialNetwork) {
            users.put((int) user.userId, user);
//            partitionMapping.put(user.interest, user);
//            partitionMapping.put(user.interest, user);

            if (!interestMap.containsKey(user.interest)) {
                interestMap.put(user.interest, new ArrayList<>());
            }
            interestMap.get(user.interest).add(user);

            int key = user.interest % partitionNum;
            if (!partitionMapping.containsKey(key)) {
                partitionMapping.put(key, new ArrayList<>());
            }
            partitionMapping.get(key).add(user);
        }
        this.numUsers = socialNetwork.size();
        interestZipfRand = SocialNetworkGenerator.RandomNumberGenerator.getInstance(interestMap.keySet().size());
        for (int i : interestMap.keySet()) {
            interestRate.put(i, 0);
        }
//        for (int i = 0; i < 100; i++) {
//            int id = interestZipfRand.getZipfRandomInt(1, interestMap.keySet().size(), skew);
//            interestRate.put(id, interestRate.get(id) + 1);
//        }
//        for (int i : interestRate.keySet()) {
//            totalChance += interestRate.get(i);
//        }
        postWeight = postProbability;
        followWeight = followProbability;
        unfollowWeight = unfollowProbability;
        getTimelineWeight = getTimelineProbability;

        totalWeight = postWeight + followWeight + unfollowWeight + getTimelineWeight;

        callbackHandler = new BenchCallbackHandlerWithContext(this, clientId, true);
        this.clientId = clientId;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        int experimentDuration = Integer.parseInt(args[0]);
        String fileDirectory = args[1];
        String gathererAddress = args[2];
        int gathererPort = Integer.parseInt(args[3]);
        int warmUpTime = Integer.parseInt(args[4]);
        int numPermits = Integer.parseInt(args[5]);
        String socialNetworkFile = args[6];
        double postWeight = Double.parseDouble(args[7]);
        double followWeight = Double.parseDouble(args[8]);
        double unfollowWeight = Double.parseDouble(args[9]);
        double getTimelineWeight = Double.parseDouble(args[10]);
        boolean uniformRandom = Boolean.parseBoolean(args[11]);
        boolean legacySSMR = Boolean.parseBoolean(args[12]);
        String[] clientArgs = Arrays.copyOfRange(args, 13, args.length);
        int clientId = Integer.parseInt(args[13]);

        ClientPromiseInterface client = getClientInterface(numPermits, clientArgs);

//      boolean hasOptimistic = implementation.equals("chitterBASELINE") || implementation.equals("chitterPREFETCH");
        if (client instanceof ChitterBenchClientPromise) {
            ((ChitterBenchClientPromise) client).loadSocialNetworkIntoCache(socialNetworkFile);
        }

        // ====================================
        // setting up monitoring infrastructure
        DataGatherer.configure(experimentDuration, fileDirectory, gathererAddress, gathererPort, warmUpTime);
        // ====================================


        TestRunnerPromise runner = new TestRunnerPromise(client, clientId, numPermits, socialNetworkFile,
                postWeight, followWeight, unfollowWeight, getTimelineWeight, uniformRandom, legacySSMR);

        runner.moveMonitor = new ThroughputPassiveMonitor(clientId, "client_move_rate", true);
        runner.retryMonitor = new ThroughputPassiveMonitor(clientId, "client_retry_rate", true);
        runner.queryMonintor = new ThroughputPassiveMonitor(clientId, "client_query_rate", true);
        runner.localCommandMonitor = new ThroughputPassiveMonitor(clientId, "client_local_command_rate", true);
        runner.globalCommandMonitor = new ThroughputPassiveMonitor(clientId, "client_global_command_rate", true);

        if (client instanceof ChitterBenchClientPromise) {

            ((ChitterBenchClientPromise) client).chitterClient.aerieClientPromise.registerMonitoringEvent(
                    new MonitorCallBack(runner.queryMonintor),
                    new MonitorCallBack(runner.moveMonitor),
                    new MonitorCallBack(runner.retryMonitor),
                    new MonitorCallBack(runner.localCommandMonitor),
                    new MonitorCallBack(runner.globalCommandMonitor));
        }

        // actually run (this client's part of) the experiment
        runner.runTests(experimentDuration);

        // wait for the monitors to finish logging and sendind data to the DataGatherer

    }

    static class MonitorCallBack implements EvenCallBack {
        ThroughputPassiveMonitor monitor;

        public MonitorCallBack(ThroughputPassiveMonitor monitor) {
            this.monitor = monitor;
        }

        @Override
        public void callback(Object data) {
            monitor.incrementCount((Integer) data);
        }
    }


    static ClientPromiseInterface getClientInterface(int numPermits, String... args) {
        int clientId = Integer.parseInt(args[0]);
        String implementation = args[1];
        if (implementation.contains("CHITTER")) {
            String systemConfigFile = args[2];
            String partitioningFile = args[3];
            ChitterClientPromise chitterClient = new ChitterClientPromise(clientId, systemConfigFile, partitioningFile, numPermits);
            return new ChitterBenchClientPromise(chitterClient);
        }
        return null;
    }

    public void runTests(int durationSecs) {
        long start = System.currentTimeMillis();
        long end = start + durationSecs * 1000;
        Date startTime = new Date(start);
        Date endTime = new Date(end);
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
        System.out.println("Client " + this.clientId + " start at " + formatter.format(startTime)
                + " and expected to end at " + formatter.format(endTime));
        while (System.currentTimeMillis() < end)
            sendOneCommand();
        try {
            Thread.sleep(10 * durationSecs * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Client " + this.clientId + " stopped at " + formatter.format(new Date(System.currentTimeMillis())));
            System.exit(1);
        }
    }

    public int getUserWithInterestRate() {
        double chance = randomGenerator.nextDouble() * totalChance; //100% percent
        for (int interest : interestRate.keySet()) {
            if (chance <= interestRate.get(interest)) {
                return (int) interestMap.get(interest).get(randomGenerator.nextInt(interestMap.get(interest).size())).userId;
            }
            chance -= interestRate.get(interest);
        }
        return -1;
    }

    public void sendOneCommand() {
        getPermit();

        double randDouble = randomGenerator.nextDouble() * totalWeight;
        int userId = uniformRandom ? randomGenerator.nextInt(numUsers) : getUserWithInterestRate();
        if (userId < 0) throw new RuntimeException("Can't find user???");
        if (randDouble <= postWeight) {
            // generate postCommand
            // System.out.println(String.format("TestRunner:: user %d sending command: post(...)", userId));
            byte[] post = new byte[140];
            BenchCallbackContext context = new BenchCallbackContext(AppCommandType.POST);
            client.post(userId, post, callbackHandler, context);
            return;
        }
        randDouble -= postWeight;

        if (randDouble <= followWeight) {
            // generate followCommand
            int followerId = userId;
//            int followedId = randomGenerator.nextInt(numUsers);
            int followedId = linked ? (int) getUserByInterest(users.get(userId)).userId : randomGenerator.nextInt(numUsers);
            // System.out.println(String.format("TestRunner:: user %d sending command: follow_NOP(%d)", followerId, followedId));

            BenchCallbackContext context = new BenchCallbackContext(AppCommandType.FOLLOW_NOP);
            client.follow_NOP(followerId, followedId, callbackHandler, context);
            return;
        }
        randDouble -= followWeight;

        if (randDouble <= unfollowWeight) {
            // generate unfollowCommand
            int followerId = userId;
//            int followedId = randomGenerator.nextInt(numUsers);
            int followedId = linked ? (int) getUserByInterest(users.get(userId)).userId : randomGenerator.nextInt(numUsers);
            // System.out.println(String.format("TestRunner:: user %d sending command: unfollow_NOP(%d)", followerId, followedId));
            BenchCallbackContext context = new BenchCallbackContext(AppCommandType.UNFOLLOW_NOP);
            client.unfollow_NOP(followerId, followedId, callbackHandler, context);
            return;
        }
        randDouble -= unfollowWeight;

        if (randDouble <= getTimelineWeight) {
            // generate getTimelineCommand
            // System.out.println(String.format("TestRunner:: user %d sending command: getTimeline(...)", userId));
            BenchCallbackContext context = new BenchCallbackContext(AppCommandType.GETTIMELINE);
            client.getTimeline(userId, callbackHandler, context);
            return;
        }
    }

    void getPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private SocialNetworkGenerator.RandomUser getUserByInterest(SocialNetworkGenerator.RandomUser user) {
        int rand = zipfRand.getZipfRandomInt(1, partitionNum, 1.2d);
        int key = user.interest % partitionNum;
        if (rand != 1)
            key = randomGenerator.nextInt(partitionMapping.keySet().size());

        List<SocialNetworkGenerator.RandomUser> randUsers = partitionMapping.get(key);
        return randUsers.get(randomGenerator.nextInt(randUsers.size()));

    }

    void releasePermit() {
        sendPermits.release();
    }

    public static class BenchCallbackContext {
        private static AtomicLong lastReqId = new AtomicLong();
        long startTimeNano;
        long timelineBegin;
        AppCommandType commandType;
        long reqId;

        public BenchCallbackContext(AppCommandType commandType) {
            this.startTimeNano = System.nanoTime();
            this.timelineBegin = System.currentTimeMillis();
            this.commandType = commandType;
            this.reqId = lastReqId.incrementAndGet();
        }

        public BenchCallbackContext(long startTimeNano, long timelineBegin, AppCommandType commandType) {
            this.startTimeNano = startTimeNano;
            this.timelineBegin = timelineBegin;
            this.commandType = commandType;
            this.reqId = lastReqId.incrementAndGet();
        }
    }

    public static class BenchCallbackHandlerWithContext implements BiConsumer {
        TestRunnerPromise parentRunner;
        boolean conservative;
        private LatencyPassiveMonitor overallLatencyMonitor, postLatencyMonitor, followLatencyMonitor, unfollowLatencyMonitor, getTimelineLatencyMonitor;
        private ThroughputPassiveMonitor overallThroughputMonitor, postThroughputMonitor, followThroughputMonitor, unfollowThroughputMonitor, getTimelineThroughputMonitor;
        private TimelinePassiveMonitor overallTimelineMonitor, postTimelineMonitor, followTimelineMonitor, unfollowTimelineMonitor, getTimelineTimelineMonitor;
        private LatencyDistributionPassiveMonitor cdfMonitor;

        public BenchCallbackHandlerWithContext(TestRunnerPromise parentRunner, int clientId, boolean conservative) {
            String client_mode = "client_" + (conservative ? "conservative" : "optimistic");
            this.parentRunner = parentRunner;
            this.conservative = conservative;

            // Latency monitors
            overallLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_overall");
            postLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_post", false);
            followLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_follow", false);
            unfollowLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_unfollow", false);
            getTimelineLatencyMonitor = new LatencyPassiveMonitor(clientId, client_mode + "_gettimeline", false);
            cdfMonitor = new LatencyDistributionPassiveMonitor(clientId, client_mode + "_overall");

            // Throughput monitors
            overallThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_overall");
            postThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_post", false);
            followThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_follow", false);
            unfollowThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_unfollow", false);
            getTimelineThroughputMonitor = new ThroughputPassiveMonitor(clientId, client_mode + "_gettimeline", false);

            // Timeline monitor
            overallTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_overall");
            postTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_post", false);
            followTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_follow", false);
            unfollowTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_unfollow", false);
            getTimelineTimelineMonitor = new TimelinePassiveMonitor(clientId, client_mode + "_gettimeline", false);

        }


        @Override
        public void accept(Object reply, Object context) {
            if (conservative) parentRunner.releasePermit();
            Message replyMsg = (Message) reply;
            BenchCallbackContext benchContext = (BenchCallbackContext) context;
            long nowNano = System.nanoTime();
            long nowClock = System.currentTimeMillis();

            // log latency
            overallLatencyMonitor.logLatency(benchContext.startTimeNano, nowNano);
            overallTimelineMonitor.logTimeline("client_send", benchContext.timelineBegin,
                    "learner_delivered", replyMsg.t_learner_delivered,
                    "command_dequeued", replyMsg.t_command_dequeued,
                    "client_received", nowClock);
            cdfMonitor.logLatencyForDistribution(benchContext.startTimeNano, nowNano);

            // increment throughput count
            overallThroughputMonitor.incrementCount();

            LatencyPassiveMonitor requestLatencyMonitor = null;
            TimelinePassiveMonitor requestTimelineMonitor = null;
            ThroughputPassiveMonitor requestThroughputMonitor = null;
            switch (benchContext.commandType) {
                case POST:
                    requestLatencyMonitor = postLatencyMonitor;
                    requestTimelineMonitor = postTimelineMonitor;
                    requestThroughputMonitor = postThroughputMonitor;
                    break;
                case FOLLOW_NOP:
                    requestLatencyMonitor = followLatencyMonitor;
                    requestTimelineMonitor = followTimelineMonitor;
                    requestThroughputMonitor = followThroughputMonitor;
                    break;
                case UNFOLLOW_NOP:
                    requestLatencyMonitor = unfollowLatencyMonitor;
                    requestTimelineMonitor = unfollowTimelineMonitor;
                    requestThroughputMonitor = unfollowThroughputMonitor;
                    break;
                case GETTIMELINE:
                    requestLatencyMonitor = getTimelineLatencyMonitor;
                    requestTimelineMonitor = getTimelineTimelineMonitor;
                    requestThroughputMonitor = getTimelineThroughputMonitor;
                    break;
                default:
                    break;
            }
            requestLatencyMonitor.logLatency(benchContext.startTimeNano, nowNano);
            requestTimelineMonitor.logTimeline("client_send", benchContext.timelineBegin,
                    "learner_delivered", replyMsg.t_learner_delivered,
                    "command_dequeued", replyMsg.t_command_dequeued,
                    "client_received", nowClock);
            requestThroughputMonitor.incrementCount();
        }

    }

}
