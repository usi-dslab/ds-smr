/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.PRObject;
import ch.usi.dslab.lel.aerie.Partition;
import ch.usi.dslab.lel.aerie.StateMachine;
import ch.usi.dslab.lel.chitter.util.SocialNetworkGenerator.RandomUser;
import ch.usi.dslab.lel.chitter.util.Util;

public class ChitterServer extends StateMachine {
    public static final Logger log = LogManager.getLogger(ChitterServer.class.toString());
    int creationSeq = 0;

    boolean running = true;
    final boolean legacySSMR;
    Thread serverThread;

    public ChitterServer(int serverId, int partitionId, String systemConfig, String partitionsConfig, boolean legacySSMR) {
        super(serverId, partitionId, systemConfig, partitionsConfig, new ChitterApplicationProcedure());
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        Configuration config = ctx.getConfiguration();
        LoggerConfig loggerConfig = config.getLoggerConfig(ChitterServer.class.toString());
        loggerConfig.setLevel(Level.DEBUG);
        this.legacySSMR = legacySSMR;
    }

    public static void main(String[] args) {
        boolean legacySSMR;
        log.info("Entering main().");

        if (args.length != 6 && args.length != 11) {
            System.err.println("usage: serverId partitionId systemConfigFile partitioningFile socialNetworkFile legacySSMR");
            System.err.println("usage: serverId partitionId systemConfigFile partitioningFile socialNetworkFile gathererHost" +
                    "gathererPort monitorLogDir gathererDuration, gathererWarmup legacySSMR");
            System.exit(1);
        }

        if (args.length == 6) {
            int serverId = Integer.parseInt(args[0]);
            int partitionId = Integer.parseInt(args[1]);
            String systemConfigFile = args[2];
            String partitionsConfigFile = args[3];
            String socialNetworkFile = args[4];
            legacySSMR = Boolean.parseBoolean(args[5]);
            PRObject.setLegacySSMR(legacySSMR);
            ChitterServer chitterServer = new ChitterServer(serverId, partitionId, systemConfigFile, partitionsConfigFile, legacySSMR);

            chitterServer.loadSocialNetworkData(socialNetworkFile);
            System.out.println("Sample data loaded...");
            chitterServer.runStateMachine();
        } else {
            int serverId = Integer.parseInt(args[0]);
            int partitionId = Integer.parseInt(args[1]);
            String systemConfigFile = args[2];
            String partitionsConfigFile = args[3];
            String socialNetworkFile = args[4];
            String gathererHost = args[5];
            int gathererPort = Integer.parseInt(args[6]);
            String gathererDir = args[7];
            int gathererDuration = Integer.parseInt(args[8]);
            int gathererWarmup = Integer.parseInt(args[9]);
            legacySSMR = Boolean.parseBoolean(args[10]);
            PRObject.setLegacySSMR(legacySSMR);

            ChitterServer chitterServer = new ChitterServer(serverId, partitionId, systemConfigFile, partitionsConfigFile, legacySSMR);
            chitterServer.loadSocialNetworkData(socialNetworkFile);
            chitterServer.setupMonitoring(gathererHost, gathererPort, gathererDir, gathererDuration, gathererWarmup);
            System.out.println("Sample data loaded...");
            chitterServer.runStateMachine();
        }
    }

    public void loadSocialNetworkData(String socialNetworkFilename) {
        Set<RandomUser> socialNetwork = Util.loadSocialNetwork(socialNetworkFilename);
        int[] partitionObjectCount = new int[Partition.getPartitionsCount() + 1];
        for (RandomUser user : socialNetwork) {
            ObjId userId = User.genObjId(user.userId);
//            if (ChitterApplicationProcedure.mapUserIdToPartition(userId).getId() == this.partitionId)
                createObject(userId, null);
        }
        for (RandomUser user : socialNetwork) {
            ObjId userId = User.genObjId(user.userId);
            User chitterUser = User.getUser(userId);
            Post post = new Post(-1L, userId, -1, 1416409000000L + userId.value.hashCode(), (userId.value).getBytes());
            chitterUser.posts.addPost(post);

            for (long followerId : user.followers) {
                // conservative users
                ObjId objId = User.genObjId(followerId);
                chitterUser.followers.people.add(objId);
                User follower = User.getUser(objId);
                follower.followeds.people.add(User.genObjId(user.userId));
                follower.materializedTimeline.addPost(post);
            }
        }
        if (!this.legacySSMR)
            for (RandomUser user : socialNetwork) {
                if (user.partitionId != this.partitionId) {
                    User chirperUser = User.getUser(User.genObjId(user.userId));
                    chirperUser.unIndex();
                }
            }
    }

    public void stop() {
        running = false;
    }

    @Override
    public Message executeSSMRCommand(Command cmd) {
        return executeCommand(cmd);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Message executeCommand(Command command) {
        AppCommandType cmdType = (AppCommandType) command.getNext();
        Object obj = command.peekNext();
        ObjId userId;
        if (obj instanceof List<?>) {
            ArrayList<ObjId> objIds = (ArrayList) command.getNext();
            userId = objIds.get(0);
        } else {
            userId = (ObjId) command.getNext();
        }


        Message reply = null;
        switch (cmdType) {

            case POST: {
//                 new Command(CommandType.POST_MATERIALIZE, userId, postFollowersList, post);
                Set<ObjId> followers = (Set<ObjId>) command.getNext();
                Post post = (Post) command.getNext();
                reply = post(userId, followers, post);
                break;
            }

            case FOLLOW: {
                ObjId followerId = userId;
                List<ObjId> followedIds = (List) command.getNext();
                ObjId followedId = followedIds.get(0);

                reply = follow(followerId, followedId);
                break;
            }

            case FOLLOW_NOP: {
                ObjId followerId = userId;
                List<ObjId> followedIds = (List) command.getNext();
                ObjId followedId = followedIds.get(0);

                reply = follow_nop(followerId, followedId);
                break;
            }

            case UNFOLLOW: {
                ObjId followerId = userId;
                ObjId followedId = (ObjId) command.getNext();

                reply = unfollow(followerId, followedId);
                break;
            }

            case UNFOLLOW_NOP: {
                ObjId followerId = userId;
                ObjId followedId = (ObjId) command.getNext();

                reply = unfollow_nop(followerId, followedId);
                break;
            }

            case GETTIMELINE: {
                reply = getTimeline(userId);
                break;
            }

            case GETFOLLOWEDLIST: {
                reply = getFollowedList(userId);
                break;
            }

            case GETFOLLOWERLIST: {
                reply = getFollowerList(userId);
                break;
            }

            case GETPOSTS: {
                reply = getPosts(userId);
                break;
            }

            default:
                break;
        }

        return reply;
    }

    @Override
    public PRObject createObject(Object objId, Object value) {
        ObjId userId = (ObjId) objId;
        log.info("Creating user {}.", userId);
        User newUser = new User(userId);
        return User.getUser(userId);
    }

    Message post(ObjId userId, Set<ObjId> sentFollowers, Post post) {
        log.info("User {} posting content: {}.", userId, post);
        log.debug(String.format("User %s materializing content for all their followers: %s.", userId, post));

        User poster = User.getUser(userId);
        UserList currentFollowers = poster.getFollowerList();
        log.debug("Follower list after calling intercepted method: " + currentFollowers);
        Set<ObjId> currentFollowersIds = currentFollowers.getRawList();

//        Set<Partition> currentFollowerPartitions = ChitterOracle.getInstance().mapUserIdCollectionToPartitionSet(currentFollowersIds);
//        Set<Partition> commandPartitions = ChitterOracle.getInstance().mapUserIdCollectionToPartitionSet(sentFollowers);

        String status;
        Set<ObjId> followersToAdd;
        Set<ObjId> followersToRemove;
        if (getMissingObjects(currentFollowersIds).size() == 0) {
//        if (commandPartitions.containsAll(currentFollowerPartitions)) {
            log.debug(String.format("Adding post %s to postlist of user %s", new String(post.content), userId));
            poster.post(post);
            for (ObjId followerId : currentFollowersIds) {
                log.debug(String.format("Adding post %s to timeline of user %s", new String(post.content), followerId));
                User follower = User.getUser(followerId);
                follower.addToMaterializedTimeline(post);
            }
            status = "OK";
        } else {
            log.debug(String.format("Incomplete follower list. Please, retry."));
            status = "RETRY";
        }

        followersToAdd = new HashSet<>(getMissingObjects(currentFollowersIds));
        followersToAdd.addAll(currentFollowersIds);
        followersToAdd.removeAll(sentFollowers);

        followersToRemove = new HashSet<>(sentFollowers);
        followersToRemove.removeAll(currentFollowersIds);

        log.debug("post status: " + status);

        return new Message(status, followersToAdd, followersToRemove);
    }

    Message follow(ObjId followerId, ObjId followedId) {
        log.info("User {} now following user {}.", followerId, followedId);
        // log.debug(String.format("User %s now following user %s", followerId, followedId));

        User followed = User.getUser(followedId);
        User follower = User.getUser(followerId);
        followed.addFollower(followerId);
        follower.follow(followedId);
        follower.addToMaterializedTimeline(followed.getUserPosts());

        return new Message("OK");
    }

    @SuppressWarnings("unused")
    Message follow_nop(ObjId followerId, ObjId followedId) {
        log.info("User {} now following user {}.", followerId, followedId);
        // log.debug(String.format("User %s now following user %s (NOP really...)", followerId, followedId));

        User followed = User.getUser(followedId);
//      followed.addFollower(followerId);
        User follower = User.getUser(followerId);
//      follower.follow(followedId);
//      follower.addToMaterializedTimeline(followed.getUserPosts());
        // just do the remote read:
        followed.getUserPosts();

//      follower.
        return new Message("OK");
    }

    Message unfollow(ObjId followerId, ObjId followedId) {
        log.info("User {} now unfollowing user {}.", followerId, followedId);
        // log.debug(String.format("User %s no longer following user %s", followerId, followedId));

        User follower = User.getUser(followerId);
        boolean didFollow = follower.unfollow(followedId);
        if (didFollow)
            follower.getMaterializedTimeline().removePostsFromUser(followedId);

        return new Message("OK");
    }

    @SuppressWarnings("unused")
    Message unfollow_nop(ObjId followerId, ObjId followedId) {
        log.info("User {} now unfollowing user {}.", followerId, followedId);
        // log.debug(String.format("User %s no longer following user %s (NOP really...)", followerId, followedId));

        User follower = User.getUser(followerId);
//      boolean didFollow = follower.unfollow(followedId);
//      if (didFollow)
//         follower.getMaterializedTimeline().removePostsFromUser(followedId);
        return new Message("OK");
    }

    Message getTimeline(ObjId userId) {
        log.info("Getting timeline for user {}.", userId);
        // log.debug(String.format("Getting timeline for user %s", userId));

        User user = User.getUser(userId);
        List<Post> timeLine = user.getMaterializedTimeline().getRawList();

        return new Message(timeLine);
    }

    Message getFollowedList(ObjId userId) {
        log.info("User {} requesting their list of followeds.", userId);

        return new Message(User.getUser(userId).getFollowedList().getRawList());
    }

//   public void createBenchmarkUsers(int numUsers, int msgSize) {
//      ChitterOracle oracle = ChitterOracle.getInstance();
//      int[] partitionObjectCount = new int[Partition.getPartitionsCount() + 1];
//      for (int i = 0 ; i < numUsers ; i++) {
//         createUser(i);
//         Partition p = oracle.mapUserIdToPartition(i);
//         partitionObjectCount[p.getId()]++;
//      }
//
//      for (int i = 1 ; i <= Partition.getPartitionsCount() ; i++) {
//         // log.debug(partitionObjectCount[i] + " users in partition " + i);
//      }
//   }

    Message getFollowerList(ObjId userId) {
        log.info("User {} requesting their list of followers.", userId);

        return new Message(User.getUser(userId).getFollowerList().getRawList());
    }

    Message getPosts(ObjId userId) {
        log.info("User {} requesting their list of posts.", userId);

        return new Message(User.getUser(userId).getUserPosts().getRawList());
    }
}
