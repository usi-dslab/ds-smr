/*

 chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import ch.usi.dslab.lel.aerie.ApplicationProcedure;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.PRObject;
import ch.usi.dslab.lel.aerie.Partition;
import ch.usi.dslab.lel.chitter.util.SocialNetworkGenerator;


public class ChitterApplicationProcedure implements ApplicationProcedure {
    static Random randomGenerator = new Random(System.nanoTime());
    static Map<ObjId, SocialNetworkGenerator.RandomUser> userCache = new ConcurrentHashMap<>();

    public void setCache(Map<ObjId, SocialNetworkGenerator.RandomUser> userCache) {
        this.userCache = userCache;
    }

    public static Partition mapUserIdToPartition(ObjId uid) {
        List<Partition> allPartitions = Partition.getPartitionList();
        return allPartitions.get(Integer.parseInt(uid.value) % allPartitions.size());
    }

    public Partition mapUserToPartition(User user) {
        return mapUserIdToPartition(user.getId());
    }

    @Override
    public int getObjectPlacement(ObjId objId) {
        return mapUserIdToPartition(objId).getId();
    }

    @Override
    public int getMoveDestination(Map<ObjId, Integer> objectMap) {
        ArrayList<ObjId> objIds = new ArrayList(Arrays.asList(objectMap.keySet().toArray()));
        if (objIds.size() == 0) return -1;
        List<Integer> partitions = new ArrayList<>();
//        int count = 1, tempCount;
//
//        partitions.addAll(objIds.stream()
//                .map(objId ->
//                        userCache.get(objId).interest % Partition.getPartitionsCount() + 1)
//                .collect(Collectors.toList()));
//        int index = 0;
//        ObjId popularObj = objIds.get(0);
//        int temp = 0;
//        for (int i = 0; i < (partitions.size() - 1); i++) {
//            temp = partitions.get(i);
//            tempCount = 0;
//            for (int j = 1; j < partitions.size(); j++) {
//                if (temp == partitions.get(j))
//                    tempCount++;
//            }
//            if (tempCount > count) {
//                index = i;
//                count = tempCount;
//            }
//        }
//        return partitions.get(index);
        partitions.addAll(objIds.stream()
                .map(objectMap::get)
                .collect(Collectors.toList()));
        return (Integer) partitions.toArray()[randomGenerator.nextInt(partitions.size())];
    }

    public Set<Partition> mapUserIdToDestinations(ObjId userId) {
        return mapUserIdsToDestinations(userId);
    }

    public Set<Partition> mapUserIdsToDestinations(ObjId... userIds) {
        Set<Partition> dests = new HashSet<Partition>(userIds.length);
        for (ObjId userId : userIds)
            dests.add(mapUserIdToPartition(userId));
        return dests;
    }

    public Set<Partition> mapUserIdCollectionToPartitionSet(Collection<ObjId> userIds) {

        return userIds.stream().map(ChitterApplicationProcedure::mapUserIdToPartition).collect(Collectors.toSet());
    }

    public Partition getObjectPlacement(Command cmd) {
        return null;
    }

    public Partition getObjectPlacement(PRObject obj) {
        User user = (User) obj;
        return mapUserIdToPartition(user.getId());
    }

}
