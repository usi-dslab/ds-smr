#!/usr/bin/python


import inspect
import os
import sys
import time

def script_dir():
#    returns the module path without the use of __file__.  Requires a function defined 
#    locally in the module.
#    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
   return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

# deploying (ridge/urp) multicast infrastructure

ridge_deployer    = script_dir() + "/../../../../../../../../../libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/ridge/RidgeEnsembleNodesDeployer.py "
ridge_config_file = script_dir() + "/example_config_ridge.json "
urp_deployer      = script_dir() + "/../../../../../../../../../libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/uringpaxos/URPHelperNodesDeployer.py "
urp_config_file   = script_dir() + "/example_config_urp.json "
app_classpath = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/chitter/target/classes:/Users/longle/.m2/repository/com/google/guava/guava/16.0.1/guava-16.0.1.jar:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/classes:/Users/longle/.m2/repository/org/apache/commons/commons-collections4/4.0/commons-collections4-4.0.jar:/Users/longle/.m2/repository/com/googlecode/json-simple/json-simple/1.1/json-simple-1.1.jar:/Users/longle/.m2/repository/multicast/spread/4.4.0/spread-4.4.0.jar:/Users/longle/.m2/repository/org/apache/hadoop/zookeeper/3.3.1/zookeeper-3.3.1.jar:/Users/longle/.m2/repository/com/sleepycat/je/5.0.58/je-5.0.58.jar:/Users/longle/.m2/repository/log4j/log4j/1.2.15/log4j-1.2.15.jar:/Users/longle/.m2/repository/javax/mail/mail/1.4/mail-1.4.jar:/Users/longle/.m2/repository/javax/activation/activation/1.1/activation-1.1.jar:/Users/longle/.m2/repository/javax/jms/jms/1.1/jms-1.1.jar:/Users/longle/.m2/repository/org/apache/thrift/libthrift/0.9.0/libthrift-0.9.0.jar:/Users/longle/.m2/repository/commons-lang/commons-lang/2.5/commons-lang-2.5.jar:/Users/longle/.m2/repository/org/apache/httpcomponents/httpclient/4.1.3/httpclient-4.1.3.jar:/Users/longle/.m2/repository/commons-logging/commons-logging/1.1.1/commons-logging-1.1.1.jar:/Users/longle/.m2/repository/commons-codec/commons-codec/1.4/commons-codec-1.4.jar:/Users/longle/.m2/repository/org/apache/httpcomponents/httpcore/4.1.3/httpcore-4.1.3.jar:/Users/longle/.m2/repository/spy/spymemcached/2.8.1/spymemcached-2.8.1.jar:/Users/longle/.m2/repository/org/apache/commons/commons-math3/3.2/commons-math3-3.2.jar:/Users/longle/.m2/repository/com/h2database/h2/1.4.187/h2-1.4.187.jar:/Users/longle/.m2/repository/com/esotericsoftware/kryo/kryo/2.21/kryo-2.21.jar:/Users/longle/.m2/repository/com/esotericsoftware/reflectasm/reflectasm/1.07/reflectasm-1.07-shaded.jar:/Users/longle/.m2/repository/org/ow2/asm/asm/4.0/asm-4.0.jar:/Users/longle/.m2/repository/com/esotericsoftware/minlog/minlog/1.2/minlog-1.2.jar:/Users/longle/.m2/repository/org/objenesis/objenesis/1.2/objenesis-1.2.jar:/Users/longle/.m2/repository/org/apache/zookeeper/zookeeper/3.4.6/zookeeper-3.4.6.jar:/Users/longle/.m2/repository/org/slf4j/slf4j-api/1.6.1/slf4j-api-1.6.1.jar:/Users/longle/.m2/repository/org/slf4j/slf4j-log4j12/1.6.1/slf4j-log4j12-1.6.1.jar:/Users/longle/.m2/repository/jline/jline/0.9.94/jline-0.9.94.jar:/Users/longle/.m2/repository/io/netty/netty/3.7.0.Final/netty-3.7.0.Final.jar:/Users/longle/.m2/repository/commons-io/commons-io/2.4/commons-io-2.4.jar:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/sense/target/classes:/Users/longle/.m2/repository/org/javatuples/javatuples/1.2/javatuples-1.2.jar:/Users/longle/.m2/repository/org/apache/commons/commons-lang3/3.3.2/commons-lang3-3.3.2.jar:/Users/longle/.m2/repository/org/apache/logging/log4j/log4j-api/2.0-rc1/log4j-api-2.0-rc1.jar:/Users/longle/.m2/repository/org/apache/logging/log4j/log4j-core/2.0-rc1/log4j-core-2.0-rc1.jar:/Users/longle/.m2/repository/net/jpountz/lz4/lz4/1.2.0/lz4-1.2.0.jar:/Users/longle/.m2/repository/org/aspectj/aspectjrt/1.8.0/aspectjrt-1.8.0.jar:/Users/longle/.m2/repository/junit/junit/4.12/junit-4.12.jar:/Users/longle/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar:/Users/longle/.m2/repository/redis/clients/jedis/2.6.0/jedis-2.6.0.jar:/Users/longle/.m2/repository/org/apache/commons/commons-pool2/2.0/commons-pool2-2.0.jar:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/javaretwisclient/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/libmcad/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/netwrapper/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/URingPaxos/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/ridge/target/classes "

# syntax of the deployer:
# ...Deployer.py config.json [additional classpath]
l = len(sys.argv)
if l not in [2,3] :
    print "usage: " + sys.argv[0] + " urp/ridge [config_file]"
    sys.exit(1)

alg = sys.argv[1]
if alg == "ridge" :
    deployer    = ridge_deployer
    config_file = ridge_config_file
elif alg == "urp" :
    deployer    = urp_deployer
    config_file = urp_config_file

if l == 3 :
    config_file = sys.argv[2]

deployment_cmd = deployer + config_file + app_classpath
os.system(deployment_cmd)