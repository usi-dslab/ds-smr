/*

 chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter.benchmarks;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface ClientPromiseInterface {

    void setLegacySSMR(boolean legacySSMR);

    void createUser(long userId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context);

    void post(long userId, byte[] post, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context);

    void follow(long followerId, long followedId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context);

    void follow_NOP(long followerId, long followedId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context);

    void unfollow(long followerId, long followedId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context);

    void unfollow_NOP(long followerId, long followedId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context);

    void getTimeline(long userId, BiConsumer accept, TestRunnerPromise.BenchCallbackContext context);
}
