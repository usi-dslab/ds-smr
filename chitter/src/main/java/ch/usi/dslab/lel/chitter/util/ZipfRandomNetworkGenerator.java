/*

 chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter.util;

import org.apache.commons.math3.distribution.ZipfDistribution;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;


/*

 To create the social network:

 1) Create partitions.
 RandomPartition.generateRandomPartitions(...)

 2) Create users, without connecting them.
 RandomUser.generateRandomUsers(...)

 3) Connect the users
 RandomUser.makeAllConnections(...)

 */

public class ZipfRandomNetworkGenerator {

    static ZipfRandomNetworkGenerator instance = null;
    Map<Double, double[]> zipfSkewAccTable = new Hashtable<Double, double[]>();
    Random uniformRandom = new Random(0);

    public static void load() {
        SocialNetwork sn = SocialNetwork.loadFromFile("/Users/longle/socialnetwork_8p.bin");
        sn.applySocialNetwork();
    }

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        ZipfRandomNetworkGenerator zipf = ZipfRandomNetworkGenerator.getInstance();

        System.out.println("Running...");
//        createAndSave();
      /*
      long start = System.nanoTime();
      createAndSave(); String method = "create";
      load(); String method = "load";
      long end = System.nanoTime();

      System.out.println(method + " took " + ((double)(end - start))/1000000000.0d + " seconds");

///*

      for (int i = 0; i < 10000; i++) {
         RandomUser.getNextUserToChirp();
         RandomUser.getNextUserToGetTimeline();
      }

      int total_followers = 0;
      for (int ruId : RandomUser.mostFollowersSortedList) {
         RandomUser ru = RandomUser.get(ruId);
//         System.out.println(String.format("user %d has %d followers, %d friends in %d partitions and has made %d chirps", ru.id, ru.followers.maxSize(), ru.friends.maxSize(), ru.friendsPartitions.maxSize(), ru.chirpCounter));
         total_followers += ru.followers.maxSize();
      }

      int total_friends = 0;
      for (int ruId : RandomUser.mostFriendsSortedList) {
         RandomUser ru = RandomUser.get(ruId);
//         System.out.println(String.format("user %d has %d friends in %d partitions, %d followers and has made %d timeline requests", ru.id, ru.friends.maxSize(), ru.friendsPartitions.maxSize(), ru.followers.maxSize(), ru.getTimelineCounter));
         total_friends += ru.friends.maxSize();
      }

      System.out.println(String.format("Total followers = %d, total friends = %d", total_followers, total_friends));

//*/

        {
            double skew = 0.1;
            ZipfDistribution zip = new ZipfDistribution(100, skew);
            for (int i = 0; i <= 100; i += 5)
                System.out.println(String.format("zip(%f, %d) = %f", skew, i, zip.cumulativeProbability(i)));
        }

//        {
//            double skew = 1.0;
//            ZipfDistribution zip = new ZipfDistribution(100, skew);
//            for (int i = 0; i <= 100; i += 5)
//                System.out.println(String.format("zip(%f, %d) = %f", skew, i, zip.cumulativeProbability(i)));
//        }
//
//        {
//            double skew = 1.1;
//            ZipfDistribution zip = new ZipfDistribution(100, skew);
//            for (int i = 0; i <= 100; i += 5)
//                System.out.println(String.format("zip(%f, %d) = %f", skew, i, zip.cumulativeProbability(i)));
//        }
//
//        {
//            double skew = 1.12;
//            ZipfDistribution zip = new ZipfDistribution(100, skew);
//            for (int i = 0; i <= 100; i += 5)
//                System.out.println(String.format("zip(%f, %d) = %f", skew, i, zip.cumulativeProbability(i)));
//        }
//
//        {
//            double skew = 1.2;
//            ZipfDistribution zip = new ZipfDistribution(100, skew);
//            for (int i = 0; i <= 100; i += 5)
//                System.out.println(String.format("zip(%f, %d) = %f", skew, i, zip.cumulativeProbability(i)));
//        }


    }

    public static synchronized ZipfRandomNetworkGenerator getInstance() {
        if (instance == null)
            instance = new ZipfRandomNetworkGenerator();
        return instance;
    }

    public static void createAndSave() {
        int numPartitions = 4;

        System.out.println("Generating random partitions...");
        RandomPartition.generateRandomPartitions(numPartitions);
        System.out.println("Generating random users...");
        RandomUser.generateRandomUsers(10000, 1, 20);
        System.out.println("Making connections...");
        RandomUser.makeAllConnections();


        SocialNetwork sn = new SocialNetwork(RandomUser.allUsers, RandomUser.mostFollowersSortedList,
                RandomUser.mostFriendsSortedList, RandomPartition.allPartitions);

        System.out.println("Saving social network to disk...");
        sn.saveToFile("/Users/longle/socialnetwork_" + numPartitions + "p.bin");
        System.out.println("Saved network to disk");
    }

    public void changeSeed(long seed) {
        uniformRandom.setSeed(seed);
    }

    @SuppressWarnings("unused")
    private double getZipfAcc(int x, double skew) {
        generateZipfCdfTable(skew);
        double ret = zipfSkewAccTable.get(skew)[x];
        return ret;
    }

    private void generateZipfCdfTable(double skew) {
        if (zipfSkewAccTable.containsKey(skew))
            return;

        double[] table = new double[100];
        ZipfDistribution zipf = new ZipfDistribution(100, skew);

        for (int i = 0; i < 99; i++)
            table[i] = zipf.cumulativeProbability(i + 1);
        table[99] = 1.0d;

        zipfSkewAccTable.put(skew, table);
    }

    public synchronized int getZipfRandomInt(int max, double skew) {
        return getZipfRandomInt(0, max, skew);
    }

    public synchronized int getZipfRandomInt(int min, int max, double skew) {
        int range = max - min;

        double randomNumber = ((double) uniformRandom.nextInt(100)) / 100.0d;

        double[] cdfTable = getZipfCdfTable(skew);

        double fraction = 0.0d;
        for (int i = 0; i < 100; i++) {
            if (randomNumber < cdfTable[i]) {
                fraction = ((double) i) / 100.0d;
                break;
            }
        }

        int randomReturn = min + ((int) (fraction * ((double) range)));

        assert (min <= randomReturn && randomReturn <= max);

        return randomReturn;
    }

    private double[] getZipfCdfTable(double skew) {
        generateZipfCdfTable(skew);
        return zipfSkewAccTable.get(skew);
    }

    public synchronized int getUniformRandomInt(int min, int max) {
        return min + uniformRandom.nextInt(1 + max - min);
    }
   
   public static class SocialNetwork implements Serializable {
      private static final long serialVersionUID = -4929623507859502514L;

      Map<Integer, RandomUser>      allUsers;
      List<Integer>      usersSortedByFollowers;
      List<Integer>      usersSortedByFriends;
      Map<Integer, RandomPartition> allPartitions;

      public SocialNetwork (Map<Integer, RandomUser> users, List<Integer> usersByFollowers, List<Integer> usersByFriends, Map<Integer, RandomPartition> partitions) {
         this.allUsers = users;
         this.usersSortedByFollowers = usersByFollowers;
         this.usersSortedByFriends   = usersByFriends;
         this.allPartitions = partitions;
      }

      public static SocialNetwork loadFromFile(String filePath) {
         FileInputStream   fin = null;
         ObjectInputStream ois = null;
         SocialNetwork     sn  = null;

         try {
            fin = new FileInputStream(filePath);
            ois = new ObjectInputStream(fin);
            sn = (SocialNetwork) ois.readObject();
         } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
         } finally {
            try {
               fin.close();
               ois.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
         }
         return sn;
      }

       public void saveToFile(String filePath) {
           FileOutputStream fos = null;
           ObjectOutputStream oos = null;
           try {
               fos = new FileOutputStream(filePath);
               oos = new ObjectOutputStream(fos);
               oos.writeObject(this);
           } catch (IOException e) {
               e.printStackTrace();
           } finally {
               try {
                   fos.close();
                   oos.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
       }

      public void applySocialNetwork() {
         RandomUser.allUsers = this.allUsers;
         RandomUser.mostFollowersSortedList = this.usersSortedByFollowers;
         RandomUser.mostFriendsSortedList = this.usersSortedByFriends;
         RandomPartition.allPartitions = this.allPartitions;
      }
   }

    public static class RandomUser implements Serializable {
      private static final long serialVersionUID = 7706143491176070125L;
      static Map<Integer, RandomUser> allUsers = new Hashtable<Integer, RandomUser>();
      static List<Integer> mostFollowersSortedList = null;
      static List<Integer> mostFriendsSortedList = null;

      static int lastUserPartition = 0;

        public int id;
      int numFriends;
      int numPartitions;
      List<Integer> friends = new ArrayList<Integer>();
      List<Integer> followers = new ArrayList<Integer>();

      RandomPartition homePartition;
      List<Integer> friendsPartitions = new ArrayList<Integer>();

      int chirpCounter = 0;
      int getTimelineCounter = 0;

      public RandomUser(int userId, int total, int minFriends, int maxFriends, int availablePartitions) {
         id = userId;

         ZipfRandomNetworkGenerator chaos = ZipfRandomNetworkGenerator.getInstance();
         numPartitions = chaos.getZipfRandomInt(1, availablePartitions, 1);
          numFriends = chaos.getZipfRandomInt(minFriends, maxFriends, 1);
         numFriends = Math.min(numFriends, (numPartitions * total / availablePartitions));

          int homePartitionId = 1 + (lastUserPartition++ % (RandomPartition.allPartitions.size()));
         homePartition = RandomPartition.get(homePartitionId);
         homePartition.addUser(this);
         friendsPartitions.add(homePartition.id);
         while (friendsPartitions.size() < numPartitions) {
            RandomPartition rp = RandomPartition.get(chaos.getUniformRandomInt(1, availablePartitions));
            if (friendsPartitions.contains(rp))
               continue;
            else
               friendsPartitions.add(rp.id);
         }
      }

        public static void generateRandomUsers(int total, int minFriends, int maxFriends) {
            int availablePartitions = RandomPartition.allPartitions.size();
            maxFriends = Math.min(total, maxFriends);

            for (int i = 0; i < total; i++) {
                if (i > 0 && i % 10000 == 0)
                    System.out.println(String.format("Generated %d users of %d", i, total));
                RandomUser ru = new RandomUser(i, total, minFriends, maxFriends, availablePartitions);
                allUsers.put(i, ru);
         }
            System.out.println(String.format("Generated %d users of %d", total, total));
      }

      public static void makeAllConnections() {
         int i = 0;
         int total = allUsers.size();

          mostFollowersSortedList = new ArrayList<Integer>(allUsers.size());
         mostFriendsSortedList   = new ArrayList<Integer>(allUsers.size());

          for (Entry<Integer, RandomUser> entry : allUsers.entrySet()) {
            RandomUser user = entry.getValue();

              mostFollowersSortedList.add(user.id);
            mostFriendsSortedList  .add(user.id);

              if (i > 0 && i % 10000 == 0)
               System.out.println(String.format("Connected %d users of %d", i, total));
            i++;

              user.makeUserConnections();
         }

          System.out.println(String.format("Connected %d users of %d", total, total));

          System.out.println("Sorting users by number of followers...");

         Collections.sort(mostFollowersSortedList, new Comparator<Integer>() {
            public int compare(Integer user_a, Integer user_b) {
               return RandomUser.get(user_b).followers.size() - RandomUser.get(user_a).followers.size();
            }
         });

         System.out.println("Sorting users by number of friends...");

          Collections.sort(mostFriendsSortedList, new Comparator<Integer>() {
            public int compare(Integer user_a, Integer user_b) {
               return RandomUser.get(user_b).friends.size() - RandomUser.get(user_a).friends.size();
            }
         });
      }

        public void makeUserConnections() {
            for (int i = 0; friends.size() < numFriends; i++) {
                RandomPartition friendPartition = RandomPartition.get(friendsPartitions.get(i % friendsPartitions.size()));
                RandomUser friend = friendPartition.getZipfRandomFriend(this);
                if (friend == null) {
                    System.out.println(String.format("Partition %d had no friends for user %d", friendPartition.id, id));
                    continue; //continue to next partition
                }
                if (friends.contains(friend.id) == false) {
                    friends.add(friend.id);
                    RandomUser.get(friend.id).followers.add(this.id);
                } else {
                    System.out.println(String.format("Friend %d already added by user %d", friend.id, id));
                    System.exit(1);
                }
            }
        }

        public static RandomUser get(int id) {
            return allUsers.get(id);
        }

      public static RandomUser getNextUserToChirp() {
         ZipfRandomNetworkGenerator zipf = ZipfRandomNetworkGenerator.getInstance();
         int nextUserIndex = zipf.getZipfRandomInt(0, mostFollowersSortedList.size() - 1, 1.6);
         int nextUserId = mostFollowersSortedList.get(nextUserIndex);
         RandomUser nextUser = RandomUser.get(nextUserId);
         nextUser.chirpCounter++;
         return nextUser;
      }

      public static RandomUser getNextUserToGetTimeline() {
         ZipfRandomNetworkGenerator zipf = ZipfRandomNetworkGenerator.getInstance();
         int nextUserIndex = zipf.getZipfRandomInt(0, mostFriendsSortedList.size() - 1, 1.6);
         int nextUserId = mostFriendsSortedList.get(nextUserIndex);
         RandomUser nextUser = RandomUser.get(nextUserId);
         nextUser.getTimelineCounter++;
         return nextUser;
      }
   }

   public static class RandomPartition implements Serializable {
      private static final long serialVersionUID = 7057968523139659172L;
      static Map<Integer, RandomPartition> allPartitions = new Hashtable<Integer, RandomPartition>();
      int id;
      List<Integer> partitionUsers = new ArrayList<Integer>();

      public RandomPartition(int id) {
         this.id = id;
         allPartitions.put(id, this);
      }

       public static RandomPartition get(int id) {
           return allPartitions.get(id);
       }

      public static void generateRandomPartitions(int numPartitions) {
         for (int i = 1; i <= numPartitions; i++) {
            RandomPartition rp = new RandomPartition(i);
            allPartitions.put(i, rp);
         }
      }

      public void addUser(RandomUser user) {
          if (!partitionUsers.contains(user))
            partitionUsers.add(user.id);
         else
            System.err.println(String.format("User already belonged to partition %d!", this.id));
      }

      public RandomUser getZipfRandomFriend(RandomUser follower) {
         ZipfRandomNetworkGenerator zipf = ZipfRandomNetworkGenerator.getInstance();
         int userIndex = zipf.getZipfRandomInt(0, partitionUsers.size() - 1, 1);
         int friendId = partitionUsers.get(userIndex);
         RandomUser friend = RandomUser.get(friendId);
         int initialValue = userIndex;
         while (friend.followers.contains(follower.id)) {
            userIndex++;
            userIndex %= partitionUsers.size();

             if (initialValue == userIndex)
               return null;

             friendId = partitionUsers.get(userIndex);
            friend = RandomUser.get(friendId);
         }
         return friend;
      }
   }

}