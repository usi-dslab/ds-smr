/*

 chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano

 This file is part of chitter.

 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import ch.usi.dslab.lel.aerie.Cache.PRObjectLocation;
import ch.usi.dslab.lel.aerie.CacheHashMap;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.OracleStateMachine;
import ch.usi.dslab.lel.chitter.util.SocialNetworkGenerator;
import ch.usi.dslab.lel.chitter.util.Util;


public class ChitterOracle extends OracleStateMachine {
    public static Map<ObjId, SocialNetworkGenerator.RandomUser> userCache = new ConcurrentHashMap<>();

    public ChitterOracle(int serverId, int partitionId, String systemConfig, String partitionsConfig) {
        super(serverId, partitionId, systemConfig, partitionsConfig, new ChitterApplicationProcedure(), new CacheHashMap());
    }

    public static void main(String args[]) {
        String systemConfigFile;
        String partitionConfigFile;
        String socialNetworkFile;
        int oracleId;
        int partitionId;
        if (args.length == 5) {
            oracleId = Integer.parseInt(args[0]);
            partitionId = Integer.parseInt(args[1]);
            systemConfigFile = args[2];
            partitionConfigFile = args[3];
            socialNetworkFile = args[4];
            ChitterOracle oracle = new ChitterOracle(oracleId, partitionId, systemConfigFile, partitionConfigFile);
            oracle.loadSocialNetworkIntoCache(socialNetworkFile);
            oracle.runStateMachine();
        } else {
            System.out.print("Usage: <oracleId> <partitionId> <system config> <partition config>");
            System.exit(0);
        }
    }

    public void loadSocialNetworkIntoCache(String file) {
        Set<SocialNetworkGenerator.RandomUser> randomUsers = Util.loadSocialNetwork(file);
        for (SocialNetworkGenerator.RandomUser user : randomUsers) {
            ObjId userId = User.genObjId(user.userId);
            PRObjectLocation cache = new PRObjectLocation(userId, user.partitionId);
            this.getCache().set(userId, cache);
        }
    }


}
