/*

 chitter - A twitter clone based on Eyrie
 Copyright (C) 2015, University of Lugano
 
 This file is part of chitter.
 
 chitter is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.chitter;

import java.io.Serializable;
import java.sql.Time;
import java.util.Comparator;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import ch.usi.dslab.lel.aerie.ObjId;

public class Post implements Serializable {
    private static final long serialVersionUID = 1L;
    public static PostComparatorNewestFirst comparatorNewestFirst = new PostComparatorNewestFirst();
    private static AtomicInteger lastPostSeq = new AtomicInteger();
    ObjId postId;
    long timestamp;
    byte[] content;

    public Post() {
    }

    public Post(ObjId posterId, long time, byte[] content) {
        Random rand = new Random(System.nanoTime());
        int fakeClientId = rand.nextInt();
        String id = fakeClientId + ":" + posterId.value + ":" + lastPostSeq.incrementAndGet();
        this.postId = new ObjId();
        this.timestamp = time;
        this.content = content;
    }

    public Post(long clientId, ObjId posterId, long time, byte[] content) {
        this.postId = new ObjId((int) clientId + ":" + posterId.value + ":" + lastPostSeq.incrementAndGet());
        this.timestamp = time;
        this.content = content;
    }

    public Post(long clientId, ObjId posterId, long seq, long time, byte[] content) {
        this.postId = new ObjId((int) clientId + ":" + posterId.value + ":" + (int) seq);
        this.timestamp = time;
        this.content = content;
    }

    @Override
    public int hashCode() {
        return postId.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        Post otherPost = (Post) other;
        return this.postId.equals(otherPost.postId);
    }

    @Override
    public String toString() {
        Time formattedTime = new Time(timestamp);
//      return String.format("%s (@ %s): %s", getPosterId(), formattedTime.toLocalTime(), new String(content));
        return String.format("%s (@ %s): %s", getPosterId(), formattedTime.toLocalTime(), "POST CONTENT");
    }

    public ObjId getPosterId() {
        return User.genObjId(postId.value.split(":")[1]);
    }

    public static class PostComparatorNewestFirst implements Comparator<Post> {
        @Override
        public int compare(Post o1, Post o2) {
            return o1.timestamp > o2.timestamp ? -1 : (o1.timestamp == o2.timestamp ? 0 : 1);
        }
    }
}
