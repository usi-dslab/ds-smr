#!/bin/sh

cd ~/netwrapper
mvn clean install && cd ~/sense
mvn clean install && cd ~/ridge
mvn clean install && cd ~/libmcad
mvn clean install && cd ~/eyrie
mvn clean install && cd ~/chitter
mvn clean install
