#!/bin/sh

cd ~/netwrapper
git pull && mvn clean install && cd ~/sense
git pull && mvn clean install && cd ~/ridge
git pull && mvn clean install && cd ~/libmcad
git pull && mvn clean install && cd ~/eyrie
git pull && mvn clean install && cd ~/chitter
git pull && mvn clean install
