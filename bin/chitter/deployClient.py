#!/usr/bin/python

import inspect
import logging
import os
import sys

import common

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')

def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


if len(sys.argv) != 4:
    print "usage: " + sys.argv[0] + " <clientId> <mode> <numPermits>"
    sys.exit(1)

client_id = common.iarg(1)
mode = common.sarg(2)
numPermits = common.iarg(3)
partitioning_file = common.get_system_config_file(mode)['partitioning']
system_config_file = common.get_system_config_file(mode)['system_config']
social_network_file = common.script_dir()+"/socialNetworksInterestLinked/users_100_partitions_2.json"

logging.info('Starting client: %s', str(sys.argv))

client_cmd = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.CHITTER_CLASS_CLIENT,
              client_id, system_config_file, partitioning_file, numPermits, social_network_file]
client_cmd = " ".join([str(val) for val in client_cmd])

logging.info(client_cmd)
os.system(client_cmd)
