#!/usr/bin/python

import sys

import common

# cleaning screen node
# print "Cleaning screen node..."
# common.sshcmdbg(screenNode, "pkill -9 redis-server &> /dev/null")
# common.sshcmdbg(screenNode, "killall -9 java &> /dev/null")

print "Killing runBatch.py"
common.localcmd("pkill -9 runBatch.py")
common.localcmd("pkill -9 java")

# cleaning remote nodes
for node in common.get_available_nodes():
    common.sshcmdbg(node, "sudo killall -9 -u long &> /dev/null")
    if common.LOCALHOST:
        common.localcmd("ssh long@node249 ssh " + node + " sudo killall -9 -u long &> /dev/null &")
