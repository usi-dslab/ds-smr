#!/usr/bin/python

import inspect
import logging
import os
import sys

import common

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')


def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


if len(sys.argv) != 6 and len(sys.argv) != 11:
    print "usage: " + sys.argv[0] + " <clientId> <mode> <numPermits>"
    print "usage: " + sys.argv[0] \
          + " <clientId> <mode> <numPermits> <terminalNum> <transactionNum> monitorHost monitorPort monitorLogDir monitorDuration monitorWarmup"
    sys.exit(1)

client_id = common.iarg(1)
mode = common.sarg(2)
numPermits = common.iarg(3)
terminalNum = common.sarg(4)
transactionNum = common.sarg(5)
monitorEnabled = False

if len(sys.argv) > 6:
    monitorEnabled = True
    monitorHost = common.sarg(6)
    monitorPort = common.sarg(7)
    monitorLog = common.sarg(8)
    monitorDuration = common.sarg(9)
    monitorWarmup = common.sarg(10)

partitioning_file = common.get_system_config_file(mode)['partitioning']
system_config_file = common.get_system_config_file(mode)['system_config']

logging.info('Starting client: %s', str(sys.argv))

client_cmd = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.DSQLDB_CLASS_CLIENT,
              client_id, system_config_file, partitioning_file, numPermits,
              common.DSQLDB_DATABASE_FILE, terminalNum, transactionNum]
if monitorEnabled:
    client_cmd = client_cmd + [monitorHost, monitorPort, monitorLog, monitorDuration, monitorWarmup]
client_cmd = " ".join([str(val) for val in client_cmd])

logging.debug(client_cmd)
os.system(client_cmd)
