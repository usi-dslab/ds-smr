#!/usr/bin/python

import sys

import common

screenNode = common.getScreenNode()
nonScreenNodes = common.getNonScreenNodes()

if len(sys.argv) <= 1:
    print("Usage: " + sys.argv[0] + " script_file")
    sys.exit(1)

script = sys.argv[1]
if len(sys.argv) > 1:
    argsArr = sys.argv[2:]
args = " ".join([str(val) for val in argsArr])
# cleaning remote nodes
for node in nonScreenNodes + [screenNode] + ['node90', 'node91', 'node92']:
    common.sshcmdbg(node, common.BIN_HOME + '/dsqldb/' + script + " " + args)
