#!/usr/bin/python

import common

for node in common.get_available_nodes():
    common.sshcmdbg(node, "sudo -- sh -c 'free; sync; echo 3 > /proc/sys/vm/drop_caches; free'")
