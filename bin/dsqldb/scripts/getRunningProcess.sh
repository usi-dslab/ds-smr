#!/usr/bin/env bash
if top -bn1 | grep -q $1; then
   fout="$(hostname).out"
   echo `hostname`  >> ${fout}
   echo `top -bn1 | grep $1` >> $fout
   cat $fout
   rm $fout
fi
