#!/usr/bin/env bash
fout="$(hostname).out"
echo `hostname`: CPU Top: `top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%*\s*id.*/\1/" | awk '{print 100 - $1"%"}'` \
                Memory: `free | grep Mem | awk '{print $4/$2 * 100.0}'` \
                CPU Consume: $[100-$(vmstat 1 2|tail -1|awk '{print $15}')] >> ${fout}
cat $fout
rm $fout
