#!/usr/bin/env bash
if ! w | grep -q "0 users"; then
   fout="$(hostname).out"
   echo `hostname`: CPU: `top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1"%"}'` \
                    FreeMem: `free | grep Mem | awk '{print $4/$2 * 100.0}'` >> ${fout}
   w >> ${fout}
   sed -i '/USER/d' ${fout}
   sed -i '/load average/d' ${fout}
   echo "" >> ${fout}
   cat ${fout}
   rm ${fout}
fi