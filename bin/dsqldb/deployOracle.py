#!/usr/bin/python

import sys
import logging

import common

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')

if len(sys.argv) > 2:
    print "usage: " + sys.argv[0] + " config_mode:string "
    sys.exit(1)

mode = common.sarg(1) if len(sys.argv) >= 2 else None

partitioning_file = common.get_system_config_file(mode)['partitioning']
system_config_file = common.get_system_config_file(mode)['system_config']
paritioning = common.read_json_file(partitioning_file)
system_config = common.read_json_file(system_config_file)


def get_host_type(_host):
    for _group in paritioning["partitions"]:
        if _host["pid"] in _group["servers"]:
            return _group["type"]


logging.info('Starting all oracle...')
cmdList = []
for member in system_config["group_members"]:
    pid = member["pid"]
    group = member["group"]
    host = member["host"]
    port = member["port"]
    if get_host_type(member) == "ORACLE":
        launchNodeCmdString = [common.JAVA_BIN, common.JAVA_CLASSPATH,
                               common.DSQLDB_CLASS_ORACLE,
                               pid, group, system_config_file, partitioning_file, common.DSQLDB_DATABASE_FILE]
        launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdString])
        cmdList.append({"node": host, "port": port, "cmdstring": launchNodeCmdString})

logging.debug(cmdList)
thread = common.LauncherThread(cmdList)
thread.start()
thread.join()
