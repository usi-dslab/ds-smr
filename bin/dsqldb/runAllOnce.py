#!/usr/bin/python

import common
import systemConfigurer
import time

# common.localcmd(common.cleaner)

NUM_WAREHOUSE = 2
NUM_DISTRICT = 10
NUM_ITEM = 100000
NUM_CUSTOMER = 3000

# common.EXLCUDE_NODES = [21, 40, 68, 73, 71, 41, 75, 78]
# common.BEGIN_NODE = 1
# common.END_NODE = 19
# common.LOCALHOST = False
# common.DSQLDB_DATABASE_FILE = common.DSQLDB_DATABASE_HOME + "/" + "_".join(
#         ["w", str(NUM_WAREHOUSE), "d",
#          str(NUM_DISTRICT), "i", str(NUM_ITEM),
#          "c", str(NUM_CUSTOMER)]) + ".data"
NUM_PARTITION = 8
NUM_CLIENT = 1
# Gen system config files

log_dir = [common.DSQLDB_DATABASE_FILENAME, "_partition_", NUM_PARTITION, "_client_", NUM_CLIENT]
log_dir = common.DSQLDB_LOG + "/" + "".join([str(val) for val in log_dir]) + "_add_trans_monitor"
sysConfig = systemConfigurer.generateSystemConfiguration(common.get_available_nodes(), NUM_PARTITION,
                                                         common.numOracles,
                                                         common.replicasPerPartition, common.ensembleSize,
                                                         common.sysConfigFile,
                                                         common.partitionsFile, True,
                                                         common.ridgeProcessPerNode,
                                                         common.serverPerNode)
time.sleep(3)

GATHERER_NODE = common.getScreenNode()
# launch multicast infrastructure
ridgeCmdPieces = [common.DSQLDB_MULTICAST_DEPLOYER, "generated"]
ridgeCmdString = " ".join(ridgeCmdPieces)
common.localcmdbg(ridgeCmdString)

# launch servers
serverCmdPieces = [common.DSQLDB_SERVER_DEPLOYER, "-1", "generated", GATHERER_NODE,
                   str(common.SENSE_GATHERER_PORT), log_dir,
                   str(common.SENSE_DURATION), str(common.SENSE_WARMUP_TIME)]
serverCmdString = " ".join(serverCmdPieces)
common.localcmdbg(serverCmdString)

# launch oracle
oracleCmdPieces = [common.DSQLDB_ORACLE_DEPLOYER, "generated"]
oracleCmdString = " ".join(oracleCmdPieces)
common.localcmdbg(oracleCmdString)

# launch clients
# constants
NODE = 0
CLIENTS = 1

clientNodes = systemConfigurer.getClientNodes(NUM_PARTITION)
clientMapping = common.mapClientsToNodes(NUM_CLIENT, clientNodes)

clientId = 100
for mapping in clientMapping:
    #     for i in range(mapping[CLIENTS]) :

    if (mapping[CLIENTS] == 0):
        continue

    numPermits = mapping[CLIENTS]

    clientCmdPieces = [common.DSQLDB_CLIENT_DEPLOYER, str(clientId), "generated", str(1),
                       common.DSQLDB_TERMINAL_NUMBER, common.DSQLDB_TRANSACTION_NUMBER,
                       GATHERER_NODE, str(common.SENSE_GATHERER_PORT), log_dir,
                       str(common.SENSE_DURATION), str(common.SENSE_WARMUP_TIME)]
    clientCmdString = " ".join(clientCmdPieces)
    common.sshcmdbg(mapping[NODE], clientCmdString)
    clientId += 1

# for i in range(1, NUM_CLIENT + 1):
#     clientCmdPieces =
#
#     common.localcmdbg(clientCmdString)
#


# launch cpu monitor
serverList = sysConfig["server_list"]
for nonClient in serverList:  # + coordinators + acceptors:
    # bwmCmdPieces = [benchCommon.javabin, benchCommon.javacp, benchCommon.javaBWMonitorClass, nonClient["role"],
    #                 nonClient["pid"], chitterSysConfig["gatherer_node"], benchCommon.gathererPort,
    #                 benchCommon.SENSE_DURATION, benchCommon.nonclilogdirchitter]
    # bwmCmdString = " ".join([str(val) for val in bwmCmdPieces])
    # benchCommon.sshcmdbg(nonClient["host"], bwmCmdString)
    cpumCmdPieces = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.SENSE_CLASS_CPUMONITOR, "PARTITION",
                     nonClient["pid"], GATHERER_NODE, str(common.SENSE_GATHERER_PORT), str(common.SENSE_DURATION),
                     log_dir]
    cpumCmdString = " ".join([str(val) for val in cpumCmdPieces])
    common.sshcmdbg(nonClient["host"], cpumCmdString)

# launch monitor
monitorCmdPieces = [common.DSQLDB_MONITOR_DEPLOYER, GATHERER_NODE,
                    str(common.SENSE_GATHERER_PORT), log_dir, str(NUM_PARTITION),
                    str(NUM_CLIENT)]
monitorCmdString = " ".join(monitorCmdPieces)
common.localcmdbg(monitorCmdString)
