import inspect
import json
import logging
import os
import shlex
import socket
import subprocess
import sys
import threading

__author__ = 'longle'

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')


class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run(self, timeout):
        def target():
            logging.debug('Thread started')
            run_args = shlex.split(self.cmd)
            self.process = subprocess.Popen(run_args)
            self.process.communicate()
            logging.debug('Thread finished')

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            logging.debug('Terminating process')
            self.process.terminate()
            thread.join()
        return self.process.returncode


class LauncherThread(threading.Thread):
    def __init__(self, clist):
        threading.Thread.__init__(self)
        self.cmdList = clist

    def run(self):
        for cmd in self.cmdList:
            logging.debug("Executing: %s", cmd["cmdstring"])
            sshcmdbg(cmd["node"], cmd["cmdstring"])


def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


def sshcmd(node, cmdstring, timeout=None):
    finalstring = "ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\""
    logging.debug(finalstring)
    cmd = Command(finalstring)
    return cmd.run(timeout)


def localcmd(cmdstring, timeout=None):
    logging.debug("localcmd:%s", cmdstring)
    cmd = Command(cmdstring)
    return cmd.run(timeout)


def sshcmdbg(node, cmdstring):
    cmd = "ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\" &"
    logging.debug("sshcmdbg: %s", cmd)
    os.system(cmd)


def localcmdbg(cmdstring):
    logging.debug("localcmdbg: %s", cmdstring)
    os.system(cmdstring + " &")


def get_system_config_file(config_type):
    if config_type is None:
        return {'partitioning': SYSTEM_CONFIG_DIR + '/partitioning.json',
                'system_config': SYSTEM_CONFIG_DIR + '/system_config.json'}
    partitioning_file = SYSTEM_CONFIG_DIR + '/' + config_type + '_partitioning.json'
    system_config_file = SYSTEM_CONFIG_DIR + '/' + config_type + '_system_config.json'
    if not os.path.isfile(partitioning_file):
        logging.error('ERROR: parititoning file not found: %s', partitioning_file)
        sys.exit(1)
    if not os.path.isfile(system_config_file):
        logging.error('ERROR: system config file not found: %s', system_config_file)
        sys.exit(1)
    return {'partitioning': partitioning_file,
            'system_config': system_config_file}


def read_json_file(file_name):
    file_stream = open(file_name)
    content = json.load(file_stream)
    file_stream.close()
    return content


def sarg(i):
    return sys.argv[i]


def iarg(i):
    return int(sarg(i))


def farg(i):
    return float(sarg(i))


def get_index(lst, key, value):
    for i, dic in enumerate(lst):
        if dic[key] == value:
            return i
    return -1


def get_item(lst, key, value):
    index = get_index(lst, key, value)
    if index == -1:
        return None
    else:
        return lst[index]


def getScreenNode():
    return get_available_nodes()[0]


def getNonScreenNodes():
    return get_available_nodes()[1:]


def numUsedClientNodes(arg1, arg2=None):
    if arg2 == None:
        return numUsedClientNodes_1(arg1)
    elif arg2 != None:
        return numUsedClientNodes_2(arg1, arg2)


def numUsedClientNodes_2(numClients, clientNodes):
    return min(numClients, len(clientNodes))


def numUsedClientNodes_1(clientNodesMap):
    numUsed = 0
    for mapping in clientNodesMap:
        if mapping[CLIENTS] > 0:
            numUsed += 1
    return numUsed


def get_available_nodes():
    return localhostNodes if LOCALHOST else noderange(BEGIN_NODE, END_NODE)


localhostNodes = []
for i in range(1, 100):
    localhostNodes.append("localhost")

LOCALHOST = False

if socket.gethostname() in ['longs-usi-mbp.mobile.usilu.net', 'longs-mbp.mobile.usilu.net',
                            "Longs-MBP.fritz.box"]:
    LOCALHOST = True

# parameters
GLOBAL_HOME = os.path.normpath(script_dir() + '/../../')

BIN_HOME = os.path.normpath(GLOBAL_HOME + '/bin')

DSQLDB_HOME = os.path.normpath(GLOBAL_HOME + '/dsqldb')
DSQLDB_CP = os.path.normpath(DSQLDB_HOME + '/target/classes')
DSQLDB_CLASS_SERVER = 'ch.usi.dslab.lel.dsqldb.DSqlServer'
DSQLDB_CLASS_ORACLE = 'ch.usi.dslab.lel.dsqldb.DSqlOracle'
DSQLDB_CLASS_CLIENT = 'ch.usi.dslab.lel.dsqldb.DSqlClient'
DSQLDB_CLASS_GENERATOR = 'ch.usi.dslab.lel.dsqldb.tpcc.jTPCCDataGenerator'
DSQLDB_MULTICAST_DEPLOYER = BIN_HOME + "/dsqldb/deployMcast.py"
DSQLDB_SERVER_DEPLOYER = BIN_HOME + "/dsqldb/deployServer.py"
DSQLDB_ORACLE_DEPLOYER = BIN_HOME + "/dsqldb/deployOracle.py"
DSQLDB_CLIENT_DEPLOYER = BIN_HOME + "/dsqldb/deployClient.py"
DSQLDB_MONITOR_DEPLOYER = BIN_HOME + "/dsqldb/deployMonitor.py"
DSQLDB_CLIENT_LOG = BIN_HOME + "/dsqldb/logs/clientLogs"
DSQLDB_LOG = BIN_HOME + "/dsqldb/logs"
DSQLDB_SERVER_LOG = BIN_HOME + "/dsqldb/logs/serverLogs"

DSSMR_HOME = os.path.normpath(GLOBAL_HOME + '/aerie')
DSSMR_CP = os.path.normpath(DSSMR_HOME + '/target/classes')

LIBMCAD_HOME = os.path.normpath(GLOBAL_HOME + '/libmcad')
LIBMCAD_CP = os.path.normpath(LIBMCAD_HOME + '/target/classes')
LIBMCAD_CLASS_RIDGE = 'ch.usi.dslab.bezerra.mcad.ridge.RidgeEnsembleNode'

NETWRAPPER_HOME = os.path.normpath(GLOBAL_HOME + '/netwrapper')
NETWRAPPER_CP = os.path.normpath(NETWRAPPER_HOME + '/target/classes')

RIDGE_HOME = os.path.normpath(GLOBAL_HOME + '/ridge')
RIDGE_CP = os.path.normpath(RIDGE_HOME + '/target/classes')

SENSE_HOME = os.path.normpath(GLOBAL_HOME + '/sense')
SENSE_CP = os.path.normpath(SENSE_HOME + '/target/classes')
SENSE_CLASS_GATHERER = "ch.usi.dslab.bezerra.sense.DataGatherer"
SENSE_CLASS_BWMONITOR= "ch.usi.dslab.bezerra.sense.monitors.BWMonitor"
SENSE_CLASS_CPUMONITOR= "ch.usi.dslab.bezerra.sense.monitors.CPUMonitor"

SYSTEM_CONFIG_DIR = os.path.normpath(BIN_HOME + '/systemConfigs')

DEPENDENCIES_DIR = os.path.normpath(GLOBAL_HOME + '/dependencies/*')

_class_path = [os.path.normpath(GLOBAL_HOME + '/dependencies/guava-19.0.jar'), DSQLDB_CP, DSSMR_CP,
               LIBMCAD_CP, NETWRAPPER_CP, RIDGE_CP, SENSE_CP, DEPENDENCIES_DIR]

if LOCALHOST:
    LOG4J = os.path.normpath(BIN_HOME + '/dsqldb/log4jDebug.xml')
else:
    LOG4J = os.path.normpath(BIN_HOME + '/dsqldb/log4j.xml')

JAVA_BIN = 'java -XX:+UseG1GC -Xmx8g -Dlog4j.configuration=file:' + LOG4J
JAVA_CLASSPATH = '-cp \'' + ':'.join([str(val) for val in _class_path]) +'\''

cleaner = BIN_HOME + "/dsqldb/cleanUp.py"


# available machines
def noderange(first, last):
    return ["node" + str(val) for val in
            [node for node in range(first, last + 1) if node not in EXLCUDE_NODES]]

# RIDGE
batch_size_threshold_bytes = 30000
batch_time_threshold_ms = 50
delta_null_messages_ms = 5
latency_estimation_sample = 10
latency_estimation_devs = 0
latency_estimation_max = 0
clockSyncInterval = 3

sysConfigFile = SYSTEM_CONFIG_DIR + "/generated_system_config.json"
partitionsFile = SYSTEM_CONFIG_DIR + "/generated_partitioning.json"

# dssmr
replicasPerPartition = 2
ensembleSize = 3
ridgeProcessPerNode = 2
serverPerNode = 1
numOracles = 1

# clientMap is a list of dicts
# clientMap = [{NODE: x, CLIENTS: y}, {NODE: z, CLIENTS: w}]
# constants
NODE = 0
CLIENTS = 1


def mapClientsToNodes(numClients, nodesList):
    clientMap = []
    clientsPerNode = int(numClients / len(nodesList))
    for node in nodesList:
        clientMap.append({NODE: node, CLIENTS: clientsPerNode})
    for extra in range(numClients % len(nodesList)):
        clientMap[extra][CLIENTS] += 1
    return clientMap

DSQLDB_DATABASE_HOME = os.path.normpath(BIN_HOME + "/dsqldb/database")
# DSQLDB_DATABASE_FILE = os.path.normpath(BIN_HOME + "/dsqldb/database/w_1_d_10_i_10_c_10.data")
# DSQLDB_DATABASE_FILE = os.path.normpath(BIN_HOME + "/dsqldb/database/w_1_d_3_i_1000_c_100.data")
# DSQLDB_DATABASE_FILE = os.path.normpath(BIN_HOME + "/dsqldb/database/w_3_d_3_i_1000_c_100.data")
# DSQLDB_DATABASE_FILE = os.path.normpath(BIN_HOME + "/dsqldb/database/w_1_d_10_i_100000_c_3000.data")
DSQLDB_DATABASE_FILENAME = "w_1_d_3_i_1000_c_100"

DSQLDB_DATABASE_FILENAME = "w_4_d_10_i_100000_c_3000"
DSQLDB_DATABASE_FILENAME = "w_2_d_10_i_100000_c_3000"
DSQLDB_DATABASE_FILENAME = "w_8_d_10_i_100000_c_3000"
DSQLDB_DATABASE_FILENAME = "w_1_d_10_i_100000_c_3000"
DSQLDB_DATABASE_FILE= os.path.normpath(DSQLDB_DATABASE_HOME + "/" + DSQLDB_DATABASE_FILENAME + ".data")

DSQLDB_TERMINAL_NUMBER = "1"
DSQLDB_TRANSACTION_NUMBER = "10000"

# availableNodes = noderange(1,40) + noderange(66,72) + noderange(74,75)
EXLCUDE_NODES = [21, 40, 68, 73, 71, 41, 75, 78, 63, 80]
BEGIN_NODE = 1
END_NODE = 60

SENSE_GATHERER_PORT = 60000
SENSE_WARMUP_TIME = 600000
SENSE_DURATION = 4800