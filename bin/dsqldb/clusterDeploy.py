#!/usr/bin/python

import os
import sys

import common

TARGET_NODE = 'dslab.inf.usi.ch'
TARGET_HOME = '/home/fynn/GIT/ds-SMR'

print "usage: " + sys.argv[0] + " command [diff, deploy]"

if len(sys.argv) not in [1, 2]:
    print "Incorrect usage: Use " + sys.argv[0] + " mode <diff, deploy>"

if len(sys.argv) == 1:
    mode = "deploy"
else:
    mode = common.sarg(1)

CMD_CREATE_DIR = ["ssh", TARGET_NODE, "'mkdir -p ", TARGET_HOME, "'"]
CMD_CREATE_DIR = ' '.join([str(val) for val in CMD_CREATE_DIR])
IGNORE_FILE = common.script_dir() + '/.clusterIgnore'
if mode == "deploy":
    CMD_COPY_BULD = ["rsync", "-rav", "--exclude-from='" + IGNORE_FILE + "'",
                     common.GLOBAL_HOME + "/*", TARGET_NODE + ":" + TARGET_HOME]
else:
    CMD_COPY_BULD = ["rsync", "-azh", "--dry-run", "--delete-after",
                     "--exclude-from='" + IGNORE_FILE + "'",
                     '--out-format="[%t]:%o:%f:Last Modified %M"',
                     common.GLOBAL_HOME + "/*", TARGET_NODE + ":" + TARGET_HOME]
CMD_COPY_BULD = ' '.join([str(val) for val in CMD_COPY_BULD])

print 'Running mode: ' + mode

print CMD_COPY_BULD
os.system(CMD_CREATE_DIR)
os.system(CMD_COPY_BULD)
