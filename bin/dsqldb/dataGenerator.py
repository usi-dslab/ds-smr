#!/usr/bin/python

import inspect
import logging
import os

import common

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')


def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


target = script_dir();
logging.info('Generating data to : %s', target)

client_cmd = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.DSQLDB_CLASS_GENERATOR, target]
client_cmd = " ".join([str(val) for val in client_cmd])

logging.info(client_cmd)
os.system(client_cmd)
