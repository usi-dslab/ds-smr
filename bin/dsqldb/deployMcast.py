#!/usr/bin/python

import sys
import logging

import common

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

if len(sys.argv) > 2:
    print "usage: " + sys.argv[0] + " config_mode:string "
    sys.exit(1)

mode = common.sarg(1) if len(sys.argv) >= 2 else None
system_config_file = common.get_system_config_file(mode)['system_config']
system_config = common.read_json_file(system_config_file)

ensembleCmdLists = []
lastEnsemble = -1
cmdList = []

for process in system_config["ensemble_processes"]:
    role = process["role"]
    pid = process["pid"]
    ensemble = process["ensemble"]
    host = process["host"]
    port = process["port"]

    launchNodeCmdPieces = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.LIBMCAD_CLASS_RIDGE,
                           system_config_file, pid]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
    cmdList.append({"node": host, "port": port, "cmdstring": launchNodeCmdString})

    logging.debug("launching %s %d at %s:%d", role, pid, host, port)

launcherThreads = []

thread = common.LauncherThread(cmdList)
thread.start()
thread.join()
