#!/usr/bin/python

import sys
import logging
import systemConfigurer
import common

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')

if len(sys.argv) < 6:
    print "usage: " + sys.argv[0] + " host port logFolder numOfPartition numOfClient"
    # ./deployMonitor.py localhost 6000 logFoler 2 1
    sys.exit(1)

gathererHost = common.sarg(1)
gathererPort = common.iarg(2)
logFolder = common.sarg(3)
numPartitions = common.iarg(4)
numClients = common.iarg(5)
clientNodes = systemConfigurer.getClientNodes(numPartitions)

logging.info('Starting monitoring...')

logsargs = []
numUsedClientNodes = common.numUsedClientNodes(numClients, clientNodes)
numServers = numPartitions * 2

logsargs.append("object")
logsargs.append("server_object_count")
logsargs.append(str(numServers))

serverLogs = ["cpu"]  # , "bandwidth"]
print numServers
for log in serverLogs:
    logsargs.append(log)
    logsargs.append("PARTITION")
    logsargs.append(str(numServers))

logsargs.append("throughput")
logsargs.append("client_move_rate")
logsargs.append(str(numUsedClientNodes))

logsargs.append("throughput")
logsargs.append("client_retry_rate")
logsargs.append(str(numUsedClientNodes))

logsargs.append("throughput")
logsargs.append("client_query_rate")
logsargs.append(str(numUsedClientNodes))

logsargs.append("throughput")
logsargs.append("client_local_command_rate")
logsargs.append(str(numUsedClientNodes))

logsargs.append("throughput")
logsargs.append("client_global_command_rate")
logsargs.append(str(numUsedClientNodes))

logsargs.append("throughput")
logsargs.append("client_tpcc_transaction_count")
logsargs.append(str(numUsedClientNodes))

logsargs.append("throughput")
logsargs.append("client_tpcc_new_order_count")
logsargs.append(str(numUsedClientNodes))

logsargs.append("throughput")
logsargs.append("client_tpcc_global_trans")
logsargs.append(str(numUsedClientNodes))

logsargs.append("throughput")
logsargs.append("client_tpcc_local_trans")
logsargs.append(str(numUsedClientNodes))

cmdList = []

launchNodeCmdString = [common.JAVA_BIN, common.JAVA_CLASSPATH,
                       common.SENSE_CLASS_GATHERER, gathererPort, logFolder] + logsargs
launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdString])

cmdList.append({"node": gathererHost, "cmdstring": launchNodeCmdString})

logging.debug(cmdList)
thread = common.LauncherThread(cmdList)
thread.start()
thread.join()
