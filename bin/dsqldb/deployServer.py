#!/usr/bin/python
import logging
import os
import sys

import common

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')

if len(sys.argv) not in [1, 2, 6, 8]:
    print "usage: " + sys.argv[0] + " [serverId:number] [config_mode:string]"
# ./deployServer
# ./deployServer 1[2]
# ./deployServer 1[2] minimal
# ./deployServer 1[2] minimal localhost 6000 logs 120 20
# ./deployServer localhost 6000 logs 120 20

mode = common.sarg(2) if len(sys.argv) == 3 else None

if len(sys.argv) == 8:
    logging.debug("Monitoring is enabled")
    monitorEnabled = True
    server_to_start = common.iarg(1)
    mode = common.sarg(2)
    monitorHost = common.sarg(3)
    monitorPort = common.sarg(4)
    monitorLog = common.sarg(5)
    monitorDuration = common.sarg(6)
    monitorWarmup = common.sarg(7)
elif len(sys.argv) == 6:
    logging.debug("Monitoring is enabled")
    monitorEnabled = True
    monitorHost = common.sarg(1)
    monitorPort = common.sarg(2)
    monitorLog = common.sarg(3)
    monitorDuration = common.sarg(4)
    monitorWarmup = common.sarg(5)
else:
    monitorEnabled = False

server_to_start = common.iarg(1) if len(sys.argv) == 2 else -1
partitioning_file = common.get_system_config_file(mode)['partitioning']
system_config_file = common.get_system_config_file(mode)['system_config']

print "config file:"+system_config_file

paritioning = common.read_json_file(partitioning_file)
system_config = common.read_json_file(system_config_file)


def get_host_type(_host):
    for _group in paritioning["partitions"]:
        if _host["pid"] in _group["servers"]:
            return _group["type"]


if server_to_start == -1:

    logging.info('Starting all servers...')
    cmdList = []
    for member in system_config["group_members"]:
        pid = member["pid"]
        group = member["group"]
        host = member["host"]
        port = member["port"]
        if get_host_type(member) == "PARTITION":
            launchNodeCmdString = [common.JAVA_BIN, common.JAVA_CLASSPATH,
                                   common.DSQLDB_CLASS_SERVER,
                                   pid, group, system_config_file, partitioning_file,
                                   common.DSQLDB_DATABASE_FILE]
            if monitorEnabled:
                launchNodeCmdString = launchNodeCmdString + [monitorHost, monitorPort, monitorLog,
                                                             monitorDuration, monitorWarmup]
            launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdString])
            cmdList.append({"node": host, "port": port, "cmdstring": launchNodeCmdString})

    logging.debug(cmdList)
    thread = common.LauncherThread(cmdList)
    thread.start()
    thread.join()

else:
    logging.info('Starting server %d', server_to_start)
    launchNodeCmdString = [common.JAVA_BIN, common.JAVA_CLASSPATH,
                           common.DSQLDB_CLASS_SERVER,
                           paritioning['partitions'][server_to_start]['servers'][0],
                           paritioning['partitions'][server_to_start]['id'],
                           system_config_file, partitioning_file, common.DSQLDB_DATABASE_FILE]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdString])
    logging.debug(launchNodeCmdString)
    os.system(launchNodeCmdString)
