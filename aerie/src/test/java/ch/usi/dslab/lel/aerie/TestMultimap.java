package ch.usi.dslab.lel.aerie;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import ch.usi.dslab.bezerra.netwrapper.Message;

/**
 * Author: longle, created on 23/03/16.
 */
public class TestMultimap {
    class Base extends PRObject {
        public int value;

        public Base(int value) {
            this.value = value;
        }

        @Override
        public Message getSuperDiff(List<Partition> destinations) {
            return null;
        }

        @Override
        public void updateFromDiff(Message objectDiff) {

        }

        @Override
        public PRObject deepClone() {
            return null;
        }

        @Override
        public String toString() {
            return String.valueOf(this.value);
        }
    }

//    @Test
//    public void ValueSize() {
//        Multimap<ObjId, Base> map = HashMultimap.create();
//        ObjId a1 = new ObjId("A:1");
//        ObjId a2 = new ObjId("A:2");
//        ObjId b1 = new ObjId("B:1");
//        ObjId b2 = new ObjId("B:2");
//        ObjId a = new ObjId("A");
//        ObjId b = new ObjId("B");
//        ObjId s1 = new ObjId("1");
//        ObjId s2 = new ObjId("2");
//        Base valueA1 = new Base(11);
//        Base valueA2 = new Base(12);
//        Base valueB1 = new Base(21);
//        Base valueB2 = new Base(22);
//
//    }
    @Test
    public void MultipleKeyToMultipleValue() {
        Multimap<ObjId, Base> map = HashMultimap.create();
        ObjId a1 = new ObjId("A:1");
        ObjId a2 = new ObjId("A:2");
        ObjId b1 = new ObjId("B:1");
        ObjId b2 = new ObjId("B:2");
        ObjId a = new ObjId("A");
        ObjId b = new ObjId("B");
        ObjId s1 = new ObjId("1");
        ObjId s2 = new ObjId("2");
        Base valueA1 = new Base(11);
        Base valueA2 = new Base(12);
        Base valueB1 = new Base(21);
        Base valueB2 = new Base(22);
        map.put(a1, valueA1);
        map.put(a, valueA1);
        map.put(s1, valueA1);
        map.put(a2, valueA2);
        map.put(a, valueA2);
        map.put(s2, valueA2);

        map.put(b1, valueB1);
        map.put(b, valueB1);
        map.put(s1, valueB1);
        map.put(b2, valueB2);
        map.put(b, valueB2);
        map.put(s2, valueB2);
        valueA1.value = 999;

    }

    @Test
    public void MultimapStressTest() {
        long start = System.currentTimeMillis();
        Multimap<String, String> map = HashMultimap.create();
        String[] strs = new String[10];
        for (int i = 0; i < 10; i++) {
            strs[i] = String.valueOf(i);
        }
        ArrayList<String> comb = CacheHashMap.getAllKeys(strs);
        long combTime = System.currentTimeMillis();
        System.out.println("running comb in " + (combTime-start)+" size "+comb.size());
        comb.stream().forEach(s -> map.put(s, s));
        System.out.println("running populate in " + (System.currentTimeMillis() - combTime));
//        System.out.println(map.get("1"));
    }

}
