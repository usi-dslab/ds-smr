/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie.client;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.mcad.MulticastClient;
import ch.usi.dslab.bezerra.mcad.MulticastClientServerFactory;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.ApplicationProcedure;
import ch.usi.dslab.lel.aerie.Cache;
import ch.usi.dslab.lel.aerie.CacheHashMap;
import ch.usi.dslab.lel.aerie.Cache.PRObjectLocation;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.CommandType;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.LocalReplica;
import ch.usi.dslab.lel.aerie.LocalReplica.MessageType;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.OracleStateMachine;
import ch.usi.dslab.lel.aerie.Partition;

import static java.util.Collections.singletonList;


public class AsyncClient implements Runnable {

    private static final Logger logger = Logger.getLogger(AsyncClient.class);
    boolean running = true;
    private MulticastClient multicastClient;
    private Thread clientThread;
    private int clientId;
    private AtomicLong nextRequestId = new AtomicLong(0);
    private BlockingQueue<Message> syncDeliveredReplies;
    private Map<Long, OutstandingRequest> outstandingRequests = new ConcurrentHashMap<>();
    private Map<Long, OutstandingQuery> outstandingQueries = new ConcurrentHashMap<>();
    private ExecutorService callbackExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private ApplicationProcedure applicationProcedure;
    private Cache cache = new CacheHashMap();

    public AsyncClient(int clientId, String systemConfigFile, String partitioningFile, ApplicationProcedure applicationProcedure) {
        this.clientId = clientId;
        this.applicationProcedure = applicationProcedure;
        syncDeliveredReplies = new LinkedBlockingDeque<>();
        multicastClient = MulticastClientServerFactory.getClient(clientId, systemConfigFile);
        Partition.loadPartitions(partitioningFile);
        Partition.partitionList.values().forEach(this::connectToPartition);
        clientThread = new Thread(this, "DSSMRClientThread-" + clientId);
        clientThread.start();
    }

    private void connectToPartition(Partition partition) {
        List<Integer> partitionServers = partition.getGroup().getMembers();
        int contactServerIndex = clientId % partitionServers.size();
        int contactServer = partitionServers.get(contactServerIndex);
        multicastClient.connectToServer(contactServer);
    }

    private Message deliverSync() {
        Message reply = null;
        try {
            reply = syncDeliveredReplies.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return reply;
    }

    private void updateCache(Message command) {
        command.rewind();
        logger.debug(this.getId() + "- Try to update internal cache");
        CommandType op = (CommandType) command.getNext();

        if (op == GenericCommand.MOVE) {
            ObjId oid = (ObjId) command.getNext();
            int dest = (int) command.getNext();
            PRObjectLocation knowledge = getCache(oid);
            if (knowledge == null) {
                knowledge = new PRObjectLocation(oid, dest);
                setCache(oid, knowledge);
                logger.debug(this.getId() + "- internal cache created " + knowledge.getObjId());
            } else {
                knowledge.setPartitionId(dest);
                logger.debug(this.getId() + "- internal cache updated " + knowledge.getObjId());
            }
        } else if (op == GenericCommand.CREATE) {
            ObjId oid = (ObjId) command.getNext();
        }
    }

    @Override
    public void run() {
        while (running) {
            Message replyWrapper = multicastClient.deliverReply();
            long reqId = (Long) replyWrapper.getItem(0);
            MessageType type = (MessageType) replyWrapper.getItem(1);
            Message reply = (Message) replyWrapper.getItem(2);
            logger.debug(String.format("<<<< cons-delivered message %s", reply));
            switch (type) {
                case REPLY: {
                    reply.copyTimelineStamps(replyWrapper);
                    OutstandingRequest osreq = outstandingRequests.remove(reqId);
                    if (reply.getNext().equals("RETRY")) {
                        callbackExecutor.execute(new Callback(osreq.callback, osreq.context, reply));
                        retryCommand(osreq.callback, osreq.context, osreq.cmd, reply);
                        break;
                    }
                    this.updateCache(osreq.cmd);
                    reply.rewind();
                    callbackExecutor.execute(new Callback(osreq.callback, osreq.context, reply));
                    break;
                }
                case REPLY_QUERY: {
                    reply.copyTimelineStamps(replyWrapper);
                    OutstandingQuery osq = outstandingQueries.remove(reqId);
                    if (osq != null) {
                        callbackExecutor.execute(new Callback(osq.callback, osq.context, reply));
                    } else {
                        syncDeliveredReplies.add(reply);
                    }
                    break;
                }
                default: {
                    break;
                }
            }

        }
    }


    //    public void retryCommand(Command command, boolean isAsync) {
    private void retryCommand(CallbackHandlerWithContext callback, Object context, Object command, Message reply) {
        Command cmd = ((Command) command);
        cmd.rewind();
        logger.debug(this.getId() + "- try to retry command " + command);
        CommandType op = (CommandType) cmd.getNext();
        if (op == null) op = (CommandType) cmd.getItem(0);
        if (op == GenericCommand.MOVE) {
            ObjId oid = (ObjId) cmd.getNext();
            int destPart = (int) cmd.getNext();
            int srcPart = (int) cmd.getNext();
            removeCache(oid);
            ArrayList<ObjId> oids = new ArrayList<>(Arrays.asList(oid));
            Command retry = new Command(GenericCommand.MOVE, destPart, oids);
            sendMove(callback, context, retry, true);
        } else if (op == GenericCommand.READ_BATCH) {
            List<ObjId> oids = (ArrayList) cmd.getItem(1);
            for (ObjId oid : oids) {
                removeCache(oid);
            }
            readBatchWithAllocation(callback, context, (Command) command, true, reply, "OBJIDS");
        } else if (op == GenericCommand.READ) {
            ObjId oid = (ObjId) cmd.getItem(1);
            removeCache(oid);
            read(callback, context, (Command) command);
        } else if (!(op instanceof GenericCommand)) {
            sendUpdate(callback, context, (Command) command, true, reply);
        } else {
            ArrayList<Object> args = (ArrayList<Object>) cmd.getItem(1);
            ObjId oid = (ObjId) args.get(0);
            removeCache(oid);
            System.out.println("Cant enter here");
        }
    }

    public void sendCommand(CallbackHandlerWithContext callback, Object context, Command command, Boolean shouldOraclePropose) {
        command.rewind();
        long requestId = nextRequestId.incrementAndGet();
        command.setId(clientId, requestId);
        OutstandingRequest osr = new OutstandingRequest(callback, context, command);
        outstandingRequests.put(requestId, osr);
        CommandType op = (CommandType) command.getNext();
        ObjId oId = (ObjId) (command.getNext());
        CallbackHandlerWithContext queryCB = (reply, queryContext) -> {
            ((Message) reply).rewind();
            Map<ObjId, Integer> objectMap = (Map<ObjId, Integer>) ((Message) reply).getNext();
            if (objectMap.keySet().size() == 0) {
                System.out.println(reply);
                callbackExecutor.execute(new Callback(callback, context, new Message("Object is not in oracle's memory")));
                return;
            }
            Set<Integer> dest = new HashSet<>();
            for (ObjId objId : objectMap.keySet()) {
                dest.add(objectMap.get(objId));
            }
            sendCommandToPartition(command, dest);
        };
        queryForObject(genQueryForObject(new HashSet<>(singletonList(oId)), shouldOraclePropose), queryCB);
    }

    public void sendCommandBatch(CallbackHandlerWithContext callback, Object context, Command command) {
        command.rewind();

        CommandType op = (CommandType) command.getNext();
        final String queryPattern = (String) (command.getNext());

    }

    public void create(CallbackHandlerWithContext callback, Object context, Command command) {
        sendCommand(callback, context, command, true);
    }

    public void delete(CallbackHandlerWithContext callback, Object context, Command command) {
        sendCommand(callback, context, command, false);
    }

    public void read(CallbackHandlerWithContext callback, Object context, Command command) {
        sendCommand(callback, context, command, false);
    }

    public void readBatch(CallbackHandlerWithContext callback, Object context, Command command) {
        command.rewind();
        CommandType op = (CommandType) command.getNext();
        final String queryPattern = ((ObjId) (command.getNext())).value;
        long requestId = nextRequestId.incrementAndGet();
        command.setId(clientId, requestId);
        OutstandingRequest osr = new OutstandingRequest(callback, context, command);
        outstandingRequests.put(requestId, osr);
        AtomicInteger callbackCount = new AtomicInteger(0);
        final AtomicInteger totalObjIds = new AtomicInteger(0);
        List<Object> ret = new ArrayList<>();
        CallbackHandlerWithContext readCB = (reply, queryContext) -> {
            ((Message) reply).rewind();
            ret.add(((Message) reply).getNext());
            if (callbackCount.incrementAndGet() == totalObjIds.get()) {
                callbackExecutor.execute(new Callback(callback, context, new Message(ret)));
            }
        };
        CallbackHandlerWithContext queryCB = (reply, queryContext) -> {
            ((Message) reply).rewind();
            Map<ObjId, Integer> objectMap = (Map<ObjId, Integer>) ((Message) reply).getNext();
            totalObjIds.set(objectMap.keySet().size());
            if (totalObjIds.get() == 0) {
                callbackExecutor.execute(new Callback(callback, context, new Message(ret)));
            } else {
                for (ObjId objId : objectMap.keySet()) {
                    Command cmd = new Command(GenericCommand.READ, objId);
                    sendCommand(readCB, readCB, cmd, false);
                }
            }

        };
        queryForObject(genQueryForObject(queryPattern, false), queryCB);

    }

    public void readBatchFromQueryWithAllocation(CallbackHandlerWithContext callback, Object context, Command command) {
        readBatchWithAllocation(callback, context, command, false, null, "QUERY");
    }

    public void readBatchWithAllocation(CallbackHandlerWithContext callback, Object context, Command command, Boolean forceQuery, Message retryReply, String readFrom) {
        CommandType op = (CommandType) command.getItem(0);

        final Set<ObjId> objsToAdd = new HashSet<>();
        if (retryReply != null) {
            retryReply.rewind();
            if (retryReply.getItem(1) != null && retryReply.getItem(1) instanceof Set) {
                objsToAdd.addAll((Set) retryReply.getItem(1));
            }
        }
        CallbackHandlerWithContext queryCB = (queryReply, queryContext) -> {
            ((Message) queryReply).rewind();
            Map<ObjId, Integer> objectMap = (Map<ObjId, Integer>) ((Message) queryReply).getNext();
            ArrayList keys = new ArrayList();
            keys.addAll(objectMap.keySet());
            command.setItem(1, keys); // set objectids instead of pattern;
            prepareCommand(command, forceQuery, objsToAdd, prepareForCommandCallback(command, callback, context));
        };

        if (readFrom.equals("QUERY")) {
            final String queryPattern = ((ObjId) (command.getItem(1))).value;
            queryForObject(genQueryForObject(queryPattern, true), queryCB);
        } else if (readFrom.equals("OBJIDS")) {
            Set<ObjId> objIds;
            if (command.getItem(1).getClass() == ArrayList.class) {
                objIds = new HashSet<>();
                objIds.addAll((ArrayList) command.getItem(1));
            } else {
                objIds = (HashSet) command.getItem(1);
            }
            queryForObject(genQueryForObject(objIds, true), queryCB);
        }

    }

    public void readBatchFromObjIdsWithAllocation(CallbackHandlerWithContext callback, Object context, Command command) {
        readBatchWithAllocation(callback, context, command, false, null, "OBJIDS");
    }

    private void sendCommandToPartition(Command command, Set<Integer> destinationIds) {
        sendCommandToPartition(command, destinationIds, true, false);
    }

    private CallbackHandlerWithContext prepareForCommandCallback(Command command, CallbackHandlerWithContext callback, Object context) {
        return (reply, prepareContext) -> {
            command.rewind();
            PRObjectLocation loc = cache.getOne(applicationProcedure.extractObjectId(command).iterator().next());
            if (loc != null && loc.getPartitionId() > -1) {
                int dest = loc.getPartitionId();
                long requestId = nextRequestId.incrementAndGet();
                command.setId(clientId, requestId);
                OutstandingRequest osr = new OutstandingRequest(callback, context, command);
                outstandingRequests.put(requestId, osr);
                sendCommandToPartition(command, new HashSet<>(singletonList(dest)), false, false);
            } else {
                logger.error("Destination for update is empty " + command);
                callbackExecutor.execute(new Callback(callback, callback, new Message("Dest is empty")));
            }
        };
    }

    private CallbackHandlerWithContext objectQueryCallback(CallbackHandlerWithContext callback) {
        return (reply, context) -> {
            ((Message) reply).rewind();
            OracleStateMachine.Prophecy prophecy = (OracleStateMachine.Prophecy) ((Message) reply).getNext();
            Map<ObjId, Integer> objectMap = prophecy.objectMap;

            // update client cache
            for (ObjId i : objectMap.keySet()) {
                PRObjectLocation newKnowledge = new PRObjectLocation(i, objectMap.get(i));
                if (newKnowledge.getPartitionId() >= 0) {
                    setCache(i, newKnowledge);
                    logger.debug(this.getId() + "- Get object " + i + " location from Oracle: " + objectMap.get(i));
                } else {
                    logger.debug(this.getId() + "- Object " + i + " doesnt exist");
                    objectMap.keySet().remove(i);
                }
            }
            callbackExecutor.execute(new Callback(callback, context, new Message(objectMap)));
        };
    }


    public void queryForObject(Command query, CallbackHandlerWithContext callback) {
        CallbackHandlerWithContext queryCB = objectQueryCallback(callback);
        long requestId = nextRequestId.incrementAndGet();
        query.setId(clientId, requestId);
        OutstandingQuery osq = new OutstandingQuery(queryCB, query);
        outstandingQueries.put(requestId, osq);
        sendQuery(query);
    }

    private Command genQueryForObject(Set<ObjId> objIds, boolean shouldPropose) {
        return new Command(GenericCommand.QUERY_OBJ, shouldPropose, objIds);
    }

    // Query pattern: MODEL:id#1:att#123
    public Command genQueryForObject(String queryPattern, boolean shouldPropose) {
        return new Command(GenericCommand.QUERY_RANGE, shouldPropose, queryPattern);
    }

    private void sendCommandToPartition(Command command, Set<Integer> destinationIds, Boolean includeOracle, boolean forceSSMR) {
        if (null != destinationIds && destinationIds.size() != 0) {
            logger.debug(this.getId() + "- >>>> Sending command " + command + " to actual partition " + destinationIds);
            List<Partition> destinations = new ArrayList<>();
            List<Group> destinationGroups = new ArrayList<>();

            // also add oracle to the destination group
            if (includeOracle) {
                destinations.addAll(Partition.getOracleList());
            }

            destinations.addAll(destinationIds.stream().map(Partition::getPartition).collect(Collectors.toList()));
            destinationGroups.addAll(destinations.stream().map(Partition::getGroup).collect(Collectors.toList()));

            command.setDestinations(destinations);

            ClientMessage commandMessage;
            if (!forceSSMR) {
                commandMessage = new ClientMessage(LocalReplica.MessageType.COMMAND, command);
            } else {
                commandMessage = new ClientMessage(MessageType.SSMR_EXEC, command);
            }

            multicastClient.multicast(destinationGroups, commandMessage);
        } else {
            // TODO: deal with empty answer;
            logger.debug(this.getId() + "- <<< Can't locate object " + command.getItem(1).toString() + " in oracle's memory. Created it yet?");
        }
    }

    synchronized public void setCache(ObjId objId, PRObjectLocation knowledge) {
        if (getCache(objId) != null) {
            removeCache(objId);
        }
        this.cache.put(objId, knowledge);
    }

    public int getId() {
        return clientId;
    }

    private void sendQuery(Command command) {
        List<Group> destinationGroups = new ArrayList<>();
        List<Partition> dest = new ArrayList<>();

        dest.addAll(Partition.getOracleList());
        destinationGroups.addAll(dest.stream().map(Partition::getGroup).collect(Collectors.toList()));

        command.setDestinations(dest);
        ClientMessage commandMessage = new ClientMessage(MessageType.QUERY, command);
        multicastClient.multicast(destinationGroups, commandMessage);
    }

    synchronized public PRObjectLocation getCache(ObjId objId) {
        return this.cache.getOne(objId);
    }

    synchronized private void removeCache(ObjId objId) {
        this.cache.remove(objId);
    }

    /*
    Command format: MOVE | < <destinationObjId> | <objId1> [objId2] [objId3]... >
     */
    private void sendMove(CallbackHandlerWithContext callback, Object context, Command command, boolean forceQuery) {
        command.rewind();
        command.setId(clientId, nextRequestId.incrementAndGet());

        CommandType op = (CommandType) command.getNext();
        int doId = (int) command.getNext();
        Set<ObjId> oIds = new HashSet<>((ArrayList) command.getNext());
        boolean shouldQuery = shouldQuery(oIds, forceQuery);

        CallbackHandlerWithContext moveCB = (reply, moveContext) -> callbackExecutor.execute(new Callback(callback, moveContext, reply));

        CallbackHandlerWithContext queryCB = (reply, queryContext) -> {
            moveObjects(doId, oIds, moveCB);
        };

        if (shouldQuery) {
            logger.debug(this.getId() + "- Don't have cache of one of oids, need to query Oracle");
            queryForObject(genQueryForObject(oIds, false), queryCB);
        } else {
            logger.debug(this.getId() + "- All object caches are available. try to send command directly");
            moveObjects(doId, oIds, moveCB);
        }
    }

    public void sendUpdate(CallbackHandlerWithContext callback, Object context, Command command) {
        sendUpdate(callback, context, command, false, null);
    }

    private void sendUpdate(CallbackHandlerWithContext callback, Object context, Command command, Boolean forceQuery, Message retryReply) {
        Set<ObjId> objsToAdd = new HashSet<>();
        if (retryReply != null) {
            retryReply.rewind();
            if (retryReply.getItem(1) != null && retryReply.getItem(1) instanceof Set) {
                objsToAdd.addAll((Set) retryReply.getItem(1));
            }
        }
        prepareCommand(command, forceQuery, objsToAdd, prepareForCommandCallback(command, callback, context));
    }

    public void sendUpdateBatch(CallbackHandlerWithContext callback, Object context, Command command) {
        sendUpdateBatch(callback, context, command, false, null);
    }

    private void sendUpdateBatch(CallbackHandlerWithContext callback, Object context, Command command, Boolean forceQuery, Message retryReply) {
        CommandType op = (CommandType) command.getItem(0);
        final String queryPattern = ((ObjId) (command.getItem(1))).value;

        final Set<ObjId> objsToAdd = new HashSet<>();
        if (retryReply != null) {
            retryReply.rewind();
            if (retryReply.getItem(1) != null && retryReply.getItem(1) instanceof Set) {
                objsToAdd.addAll((Set) retryReply.getItem(1));
            }
        }
        CallbackHandlerWithContext queryCB = (queryReply, queryContext) -> {
            ((Message) queryReply).rewind();
            Map<ObjId, Integer> objectMap = (Map<ObjId, Integer>) ((Message) queryReply).getNext();
            ArrayList keys = new ArrayList();
            keys.addAll(objectMap.keySet());
            command.setItem(1, keys); // set objectids instead of pattern;
            prepareCommand(command, forceQuery, objsToAdd, prepareForCommandCallback(command, callback, context));
        };
        queryForObject(genQueryForObject(queryPattern, true), queryCB);
    }

    public void sendSSMRCommand(CallbackHandlerWithContext callback, Object context, Command command) {
        long requestId = nextRequestId.incrementAndGet();
        command.setId(clientId, requestId);
        Set<ObjId> oIds = applicationProcedure.extractObjectId(command);
        List<Partition> dest = Partition.getPartitionList();
        Set<Integer> destId = dest.stream().map(Partition::getId).collect(Collectors.toSet());
        OutstandingRequest osr = new OutstandingRequest(callback, context, command);
        outstandingRequests.put(requestId, osr);
        sendCommandToPartition(command, destId, false, true);
    }

    private Map<ObjId, Integer> getObjectMap(Set<ObjId> objIds) {
        Map<ObjId, Integer> ret = new ConcurrentHashMap<>();
        for (ObjId objId : objIds) {
            PRObjectLocation o = getCache(objId);
            if (o != null) ret.put(o.getObjId(), o.getPartitionId());
        }
        return ret;
    }

    private void prepareCommand(Command command, boolean forceQuery, Set<ObjId> objsToAdd, CallbackHandlerWithContext callback) {

        command.rewind();
        command.setId(clientId, nextRequestId.incrementAndGet());

        CommandType op = (CommandType) command.getNext();
        Set<ObjId> objIds = new HashSet<>(applicationProcedure.extractObjectId(command));
        if (objsToAdd != null && objsToAdd.size() > 0) objIds.addAll(objsToAdd);

        boolean shouldQuery = shouldQuery(objIds, forceQuery);
        CallbackHandlerWithContext moveCB = (reply, moveContext) -> callbackExecutor.execute(new Callback(callback, moveContext, reply));

        CallbackHandlerWithContext queryCB = (reply, queryContext) -> {
            if (shouldMigrate(objIds)) {
                int destId = applicationProcedure.getMoveDestination(getObjectMap(objIds));
                moveObjects(destId, objIds, moveCB);
            } else {
                callbackExecutor.execute(new Callback(callback, callback, new Message("done")));
            }
        };

        if (shouldQuery) {
            logger.debug(this.getId() + "- Don't have cache of one of oids, need to query Oracle");
            queryForObject(genQueryForObject(objIds, false), queryCB);
        } else {
            logger.debug(this.getId() + "- All object caches are available. try to send command directly");
            if (shouldMigrate(objIds)) {
                int destId = applicationProcedure.getMoveDestination(getObjectMap(objIds));

                moveObjects(destId, objIds, moveCB);
            } else {
                callbackExecutor.execute(new Callback(callback, callback, new Message("done")));
            }
        }
    }

    private boolean shouldQuery(Set<ObjId> objIds, boolean forceQuery) {
        if (forceQuery) return forceQuery;
        for (ObjId i : objIds) {
            if (getCache(i) == null) {
                return true;
            }
        }
        return false;
    }

    private boolean shouldMigrate(Set<ObjId> objIds) {
        List<ObjId> tmp = new ArrayList<>(objIds);
        for (int i = 0; i < tmp.size() - 1; i++) {
            PRObjectLocation tmpI = getCache(tmp.get(i));
            PRObjectLocation tmpJ = getCache(tmp.get(i + 1));

            if ((tmpI == null) || tmpJ == null ||
                    ((tmpI.getPartitionId() != tmpJ.getPartitionId()))) {
                return true;
            }
        }
        return false;
    }

    private void moveObjects(int destPartId, Set<ObjId> objIds, CallbackHandlerWithContext callback) {
        AtomicInteger callbackCount = new AtomicInteger(0);
        AtomicInteger movedCount = new AtomicInteger(0);
        CallbackHandlerWithContext moveCB = (reply, context) -> {
            int count = movedCount.incrementAndGet();
            logger.debug(this.getId() + "- move callback #" + count + " out of " + callbackCount.get());
            if (count == callbackCount.get()) {
                callbackExecutor.execute(new Callback(callback, context, reply));
            }
        };
        objIds.stream().forEach(i -> {
            PRObjectLocation oI = getCache(i);
            if (oI != null)
                if (oI.getPartitionId() != destPartId) { // only move objects not in same partition
                    callbackCount.incrementAndGet();
                    moveObject(destPartId, oI, moveCB);
                }
        });
        if (callbackCount.get() == 0) {
            logger.debug("no move needed, execute callback");
            callbackExecutor.execute(new Callback(callback, callback, new Message("no action needed")));
        }
    }

    private void moveObject(int destPartId, PRObjectLocation oObjId, CallbackHandlerWithContext callback) {
        final ObjId objId = oObjId.getObjId();
        int srcPartId = oObjId.getPartitionId();
        logger.debug(this.getId() + "- Moving object " + objId + " to partition " + destPartId);
        Command moveCmd = new Command(GenericCommand.MOVE, objId, destPartId, srcPartId);
        long requestId = nextRequestId.incrementAndGet();
        moveCmd.setId(clientId, requestId);

        CallbackHandlerWithContext moveCB = (reply, context) -> {
            logger.debug(this.getId() + "- Moved object " + objId + " to partition " + destPartId);
            callbackExecutor.execute(new Callback(callback, context, new Message()));
        };
        OutstandingRequest osr = new OutstandingRequest(callback, callback, moveCmd);
        outstandingRequests.put(requestId, osr);

        Set<Integer> dest = new HashSet<>();
        dest.add(destPartId);
        dest.add(srcPartId);
        sendCommandToPartition(moveCmd, dest);
    }

    private void moveObject(int destPartId, ObjId objIdRaw, CallbackHandlerWithContext callback) {
        final ObjId objId = getCache(objIdRaw).getObjId();
        int srcPartId = getCache(objId).getPartitionId();
        logger.debug(this.getId() + "- Moving object " + objId + " to partition " + destPartId);
        Command moveCmd = new Command(GenericCommand.MOVE,
                objId, destPartId, srcPartId);
        long requestId = nextRequestId.incrementAndGet();
        moveCmd.setId(clientId, requestId);

        CallbackHandlerWithContext moveCB = (reply, context) -> {
            logger.debug(this.getId() + "- Moved object " + objId + " to partition " + destPartId);
            callbackExecutor.execute(new Callback(callback, context, new Message()));
        };
        OutstandingRequest osr = new OutstandingRequest(callback, callback, moveCmd);
        outstandingRequests.put(requestId, osr);

        Set<Integer> dest = new HashSet<>();
        dest.add(destPartId);
        dest.add(srcPartId);
        sendCommandToPartition(moveCmd, dest);
    }

    private static class OutstandingQuery {
        CallbackHandlerWithContext callback;
        Object context;
        Command query;

        public OutstandingQuery(CallbackHandlerWithContext callback, Command query) {
            this.callback = callback;
            this.query = query;
        }
    }

    private static class OutstandingRequest {
        CallbackHandlerWithContext callback;
        Object context;
        Command cmd;

        public OutstandingRequest(CallbackHandlerWithContext callback, Object ctx, Command cmd) {
            this.callback = callback;
            this.context = ctx;
            this.cmd = cmd;
        }

    }

    public static class Callback implements Runnable {
        CallbackHandlerWithContext callback;
        Object context;
        Object reply;

        public Callback(CallbackHandlerWithContext callback, Object context, Object reply) {
            this.callback = callback;
            this.context = context;
            this.reply = reply;
        }

        @Override
        public void run() {
            ((Message) reply).rewind();
            if (((Message) reply).getNext().equals("RETRY")) {
                ((Message) reply).rewind();
                callback.error(reply, context);
            } else {
                ((Message) reply).rewind();
                callback.success(reply, context);
            }
        }
    }

}
