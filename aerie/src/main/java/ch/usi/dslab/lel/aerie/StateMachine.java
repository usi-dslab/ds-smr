/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.mcad.DeliveryMetadata;
import ch.usi.dslab.bezerra.mcad.MulticastClientServerFactory;
import ch.usi.dslab.bezerra.mcad.MulticastServer;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.ObjectCountPassiveMonitor;
import ch.usi.dslab.lel.aerie.exec.CommandExecution;
import ch.usi.dslab.lel.aerie.exec.CommandExecution.Signal;

public abstract class StateMachine {
    private static final Logger logger = Logger.getLogger(StateMachine.class);
    private static StateMachine instance;
    private final Lock signalsLock = new ReentrantLock();
    private final Condition signalsCondition = getSignalsLock().newCondition();
    public int partitionId;
    CommandExecution currentCommandExecution;
    List<Signal> signalQueue = new ArrayList<>();
    boolean running = true;
    Semaphore readySemaphore;
    private int serverId;
    private MulticastServer multicastServer;
    public ApplicationProcedure appProcedure;
    ObjectCountPassiveMonitor objectMonitor;

    String gathererHost = null;
    int gathererPort;
    String fileDirectory;
    int gathererDuration;
    int warmupTime;
    boolean monitoringInited = false;
    boolean isLoggingReplica = false;

    public StateMachine(int serverId, int partitionId, String systemConfig, String partitionsConfig, ApplicationProcedure appProcedure) {
        this(serverId, partitionId, systemConfig, partitionsConfig, null, appProcedure);
    }

    public StateMachine(int serverId, int partitionId, String systemConfig, String partitionsConfig, DeliveryMetadata checkpoint, ApplicationProcedure appProcedure) {
        instance = this;
        this.serverId = serverId;
        this.appProcedure = appProcedure;
        this.readySemaphore = new Semaphore(0);
        multicastServer = MulticastClientServerFactory.getServer(serverId, systemConfig);
        LocalReplica replica = LocalReplica.createLocalReplica(multicastServer, partitionsConfig);
        multicastServer.getMulticastAgent().provideMulticastCheckpoint(checkpoint);
        this.partitionId = partitionId;
        if (serverId % replica.getLocalPartition().getGroup().getMembers().size() == 0) {
            isLoggingReplica = true;
            System.out.println("Replica #"+serverId + " is logging for PARTITION #" + partitionId);
        }

    }

    public void initMonitoring() {
//        if (this.serverId % LocalReplica.getLocalReplica().getLocalPartition().getGroup().getMembers().size() == 1) {
        if (gathererHost != null) {
            System.out.println("Init benchmark on server " + this.serverId);
            DataGatherer.configure(gathererDuration, fileDirectory, gathererHost, gathererPort, warmupTime);
            objectMonitor = new ObjectCountPassiveMonitor(serverId, "server_object_count", false);
            objectMonitor.logCount(PRObject.getObjectCount());
            monitoringInited = true;
        }
    }

    public static StateMachine getMachine() {
        return instance;
    }

    public List<Signal> getSignalQueue() {
        return signalQueue;
    }

    public MulticastServer getMulticastServer() {
        return multicastServer;
    }

    void enqueueSignal(Message signalMessage) {
        signalMessage.rewind();
        signalMessage.getNext(); // skip message type
        int sourceId = (Integer) signalMessage.getNext();
        CmdId cmdId = (CmdId) signalMessage.getNext();

        Signal signal = new Signal(sourceId, cmdId);

        enqueueSignal(signal);
    }

    void enqueueSignal(Signal signal) {
        getSignalsLock().lock();
        {
            signalQueue.add(signal);
            getSignalsCondition().signalAll();
        }
        getSignalsLock().unlock();
    }

    public Lock getSignalsLock() {
        return signalsLock;
    }

    public Condition getSignalsCondition() {
        return signalsCondition;
    }

    private void log(String message) {
        if (isLoggingReplica)
            logger.debug("[PARTITION-" + this.partitionId + "/" + this.multicastServer.getId() + "]-" + message);
    }

    List<Signal> partitionListToSignalList(List<Partition> partitions, Command cmd) {
        List<Signal> slist = new ArrayList<>(partitions.size());
        slist.addAll(partitions.stream().map(p -> new Signal(p.id, cmd.id)).collect(Collectors.toList()));
        return slist;
    }

    public Semaphore getReadySemaphore() {
        return readySemaphore;
    }

    public void runStateMachine() {
        LocalReplica local = LocalReplica.getLocalReplica();
        readySemaphore.release();
        while (running) {

            CommandExecution cmdExecution = local.takeNextAwaitingExecution();
            Command currentCommand = cmdExecution.getCommand();
            log(currentCommand.getId() + "- state machine running " + currentCommand);
            if (cmdExecution.isSSMRExecution()) sendSignals(currentCommand);

            if (!monitoringInited) initMonitoring();

            //================
            // TIMELINE STUFF
            currentCommand.t_command_dequeued = System.currentTimeMillis();
            //================

            // =========================
            currentCommandExecution = cmdExecution;
            // =========================

            // inside the executeCommand(...) method, there used to be a simple implementation
            // of the user's server logic. with optimistic execution, though, such method first
            // checks if the command was already opt-executed, if its order coincides
            // with the optimistic order and confirm/rectify the execution (and the reply accordingly).
            //
            // if the execution is confirmed, *reply* is simply a "confirm opt execution"
            // otherwise, *reply* contains the reply that should've been sent originally
            Message reply;
            currentCommand.rewind();
            if (!cmdExecution.isSSMRExecution() && isCommandGeneric(currentCommand)) {
                //try to run generic command
                reply = executeGenericCommand(local, currentCommand);
            } else {
                if (cmdExecution.isSSMRExecution()) {
                    currentCommand.rewind();
                    logger.debug("execute SSMR command " + currentCommand);
                    reply = executeSSMRCommand(currentCommand);
                } else {
                    Set<ObjId> missingObjs = getMissingObjects(currentCommand);
                    if (null != missingObjs) {
                        log("Can't find objects " + missingObjs + " in this partition, tell client to retry");
                        reply = new Message("RETRY", missingObjs);
                    } else {
                        currentCommand.rewind();
                        reply = executeCommand(currentCommand);
                    }
                }
            }
            currentCommandExecution.setReply(reply);
            log(currentCommand.getId() + "-" + reply);
            if (objectMonitor != null)
                objectMonitor.logCount(PRObject.getObjectCount());
            // =========================
            // NO super-early state sent
            // =========================
            // super-early state sent: YES
            local.notifyCommandExecutionFinished(currentCommandExecution);
            // =========================

            // before proceeding to the next command (and maybe before
            // even replying to the client) wait until all signals have
            // been sent/received
            if (cmdExecution.isSSMRExecution()) currentCommandExecution.waitPendingSignals();

            reply.sourcePartitionId = partitionId;
            // send the reply to the client
            local.sendReply(reply, currentCommand);

        }
    }

    public void sendSignals(Command command) {
        LocalReplica local = LocalReplica.replica;
        if (!local.isPartitionMulticaster(command))
            return;

        List<Partition> dests = command.getDestinations();
        if (dests.size() <= 1)
            return;

        List<Partition> otherPartitions = new ArrayList<>(dests);
        otherPartitions.remove(local.localPartition);

        Message signal = new Message(LocalReplica.MessageType.SIGNAL, local.localPartition.id, command.id);
        local.reliableMulticast(otherPartitions, signal);
    }

    private boolean isCommandGeneric(Command command) {
        CommandType op = (CommandType) command.getNext();
        command.rewind();
        return op instanceof GenericCommand;
    }

    protected Message executeGenericCommand(LocalReplica local, Command command) {
        CommandType op = (CommandType) command.getNext();
        if (op == GenericCommand.READ) {
            ObjId oid = (ObjId) command.getNext();
            PRObject obj = PRObject.getObjectById(oid);
            if (obj == null) return new Message("RETRY");
            return new Message(obj);
        } else if (op == GenericCommand.READ_BATCH) {
            ArrayList<ObjId> objIds = (ArrayList<ObjId>) command.getNext();
            Set<PRObject> ret = new HashSet<>();
            Set<ObjId> missingObjs = new HashSet<>();
            for (ObjId objId : objIds) {
                PRObject obj = PRObject.getObjectById(objId);
                if (obj == null) {
                    missingObjs.add(objId);
                    logger.debug("Missing objects: " + objId);
                } else ret.add(obj);
            }
            if (missingObjs.size() > 0) {
                return new Message("RETRY", missingObjs);
            }
            return new Message(ret);
        } else if (op == GenericCommand.CREATE) {
            ObjId oid = (ObjId) command.getNext();
            Object value = command.getNext();
            PRObject obj = this.createObject(oid, value);
            return new Message(obj);
        } else if (op == GenericCommand.DELETE) {
            ObjId oid = (ObjId) command.getNext();
            PRObject obj = PRObject.getObjectById(oid);
            if (obj != null) {
                obj.unIndex();
                return new Message("OK");
            } else {
                return new Message("RETRY");
            }
        } else { //command is MOVE
            ObjId oid = (ObjId) command.getNext();
            int destPart = (int) command.getNext();
            int srcPart = (int) command.getNext();
            if (partitionId == srcPart) { // from source
                log("move object " + oid + "to partition " + destPart);
                PRObject obj = PRObject.getObjectById(oid);

                Message reply;
                ArrayList<Partition> partitions = new ArrayList<>();
                partitions.add(Partition.getPartition(destPart));
                Message exchange;
                if (obj == null) {
                    log("Can't find object in this partition. Request retry");

                    exchange = new Message(LocalReplica.MessageType.CANCEL_MOVE, oid);
                    reply = new Message("RETRY");
                    sendObject(partitions, command, exchange);
                } else {
                    PRObject transferedObj = obj.deepClone();
                    exchange = new Message(LocalReplica.MessageType.MOVE_OBJECT, transferedObj);
                    sendObject(partitions, command, exchange);
                    reply = new Message(transferedObj);
                    log("object " + oid + " unindexed from partition");
                    obj.unIndex();
                }
                return reply;
            } else {
                log("wait for object from partition " + srcPart);
                PRObject obj = local.takeExchangedObject(oid);
                if (obj == null) {
                    log("object retrieved null for " + oid);
                    return new Message("RETRY");
                } else {
                    PRObject.indexPRObject(obj, obj.getId());
                    log("object retrieved " + obj.getId());
                    return new Message(obj);

                }
            }
        }
    }

    public Set<ObjId> getMissingObjects(Command command) {
        Set<ObjId> objIds = this.appProcedure.extractObjectId(command);
        Set<ObjId> ret = getMissingObjects(objIds);
        if (ret.size() > 0) return ret;
        return null;
    }

    // executeCommand returns the reply to be sent to the client
    public abstract Message executeCommand(Command command);

    public abstract Message executeSSMRCommand(Command command);

    public abstract PRObject createObject(Object objId, Object payload);

    public void sendObject(List<Partition> destination, Command command, Message object) {
        LocalReplica local = LocalReplica.replica;
        List<Integer> partitionMembers = local.getLocalPartition().getGroup().getMembers();
        if (!local.isPartitionMulticaster(command))
            return;
        log("replica " + this.multicastServer.getId() + " answer ");
        local.reliableMulticast(destination, object);
    }

    public Set<ObjId> getMissingObjects(Set<ObjId> objIds) {
        return objIds.stream().filter(objId -> (PRObject.getObjectById(objId) == null) || (!PRObject.getObjectById(objId).getId().equals(objId))).collect(Collectors.toSet());
    }

    public void setupMonitoring(String gathererHost, int gathererPort, String fileDirectory, int gathererDuration, int warmupTime) {
        this.gathererHost = gathererHost;
        this.gathererPort = gathererPort;
        this.fileDirectory = fileDirectory;
        this.warmupTime = warmupTime;
        this.gathererDuration = gathererDuration;
    }
}
