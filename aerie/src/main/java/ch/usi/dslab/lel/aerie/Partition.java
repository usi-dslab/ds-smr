/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie;

import ch.usi.dslab.bezerra.mcad.Group;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Partition implements Comparable<Partition> {
    public static Map<Integer, Partition> partitionList = new HashMap<>();
    public int id;
    public Group pgroup;
    public String type;

    public Partition(int id, String type) {
        this.id = id;
        this.type = type;
        pgroup = Group.getOrCreateGroup(id); // assuming partition's and group's ids will match
        assert (pgroup != null);
        partitionList.put(id, this);
    }

    public static Partition getPartition(int id) {
        return partitionList.get(id);
    }

    public static void loadPartitions(String configFile) {
        try {
            JSONParser parser = new JSONParser();
            Object obj;

            obj = parser.parse(new FileReader(configFile));

            JSONObject partitions = (JSONObject) obj;

            JSONArray partitionArray = (JSONArray) partitions.get("partitions");

            for (Object aPartitionArray : partitionArray) {
                JSONObject partition = (JSONObject) aPartitionArray;
                long partition_id = (Long) partition.get("id");
                String partitionType = (String) partition.get("type");
                new Partition((int) partition_id, partitionType);
            }

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public static void loadPartitions(ArrayList<PartitionConfig> partitionList) {
        for (PartitionConfig partition : partitionList) {
            new Partition(partition.id, partition.type);
        }
    }

    public static List<Partition> getAllPartition() {
        List<Partition> partList = new ArrayList<>(partitionList.values());
        Collections.sort(partList, (o1, o2) -> o1.getId() - o2.getId());
        return partList;
    }

    public static List<Partition> getPartitionList() {
        List<Partition> partList = new ArrayList<>();
        partList.addAll(partitionList.values().stream().filter(p -> p.type.equals("PARTITION")).collect(Collectors.toList()));
        Collections.sort(partList, (o1, o2) -> o1.getId() - o2.getId());
        return partList;
    }

    public static List<Partition> getOracleList() {
        List<Partition> partList = new ArrayList<>();
        partList.addAll(partitionList.values().stream().filter(p -> p.type.equals("ORACLE")).collect(Collectors.toList()));
        Collections.sort(partList, (o1, o2) -> o1.getId() - o2.getId());
        return partList;
    }


    public static int getPartitionsCount() {
        return getPartitionList().size();
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Group getGroup() {
        return pgroup;
    }

    @Override
    public boolean equals(Object Oother) {
        Partition other = (Partition) Oother;
        return this.id == other.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(Partition other) {
        return this.pgroup.getId() - other.pgroup.getId();
    }

    @Override
    public String toString() {
        return String.format("P_%d", id);
    }

}
