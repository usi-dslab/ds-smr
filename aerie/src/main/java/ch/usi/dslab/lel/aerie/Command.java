/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - carlos.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.netwrapper.Message;
import org.apache.commons.lang.SerializationUtils;

public class Command extends Message {
    private static final long serialVersionUID = 1L;
    public CmdId id;
    public List<Integer> destinations;
    transient List<Partition> destinationPartitions = null;
    private LocalReplica.MessageType type;
    public boolean isRangeCommand = false;

    public Command() {

    }

    public void setRangeCommand(boolean isRangeCommand) {
        this.isRangeCommand = isRangeCommand;
    }

    public Command(Object... objs) {
        super(objs);
    }

    public LocalReplica.MessageType getType() {
        return type;
    }

    public void setType(LocalReplica.MessageType type) {
        this.type = type;
    }

    public CmdId getId() {
        return id;
    }

    public void setId(CmdId id) {
        this.id = id;
    }

    public void setId(int clientId, long cliCmdSeq) {
        this.id = new CmdId(clientId, cliCmdSeq);
    }

    public int getSourceClientId() {
        return id.clientId;
    }

    synchronized public List<Partition> getDestinations() {
        if (destinationPartitions == null) {
            destinationPartitions = new ArrayList<>(destinations.size());
            destinationPartitions.addAll(destinations.stream().map(Partition::getPartition).collect(Collectors.toList()));
        }
        return destinationPartitions;
    }

    synchronized public void setDestinations(List<Partition> dests) {
        destinationPartitions = dests;
        destinations = new ArrayList<>(dests.size());
        destinations.addAll(dests.stream().map(Partition::getId).collect(Collectors.toList()));
    }

    @Override
    public boolean equals(Object other) {
        return this.id.equals(((Command) other).id);
    }

    public Command clone() {
        return (Command) SerializationUtils.clone(this);
    }

}
