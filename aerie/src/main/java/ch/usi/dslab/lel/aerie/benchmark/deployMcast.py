#!/usr/bin/python
#
# import inspect
# import os
#
# os.system("clear")
#
#
# def script_dir():
#     #    returns the module path without the use of __file__.  Requires a function defined
#     #    locally in the module.
#     #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
#     return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))
#
# # deploying (ridge) multicast infrastructure
# base_dir = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject"
# ridge_deployer = base_dir + "/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/ridge/RidgeEnsembleNodesDeployer.py "
#
# app_classpath = base_dir + "aerie/target/classes:" + \
#                 base_dir + "netwrapper/target/classes:" + \
#                 base_dir + "libmcad/target/classes:/Users/longle/.m2/repository/multicast/spread/4.4.0/spread-4.4.0.jar:" + \
#                 base_dir + "URingPaxos/target/classes:/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/lib/*:" + \
#                 base_dir + "aerie/target/dependencies/* "
# # syntax of the deployer:
# # ...Deployer.py config.json [additional classpath]
# deployment_cmd = ridge_deployer + config_file + app_classpath
# os.system(deployment_cmd)

# !/usr/bin/python

# This script initiates the helper nodes, that is, the Ridge Infrastructure:
# All ensembles, each with its coordinator and its acceptors

# try: import simplejson as json
# except ImportError: import json

import os
import inspect
import threading

import simplejson as json

# ====================================
# ====================================

class launcherThread(threading.Thread):
    def __init__(self, clist):
        threading.Thread.__init__(self)
        self.cmdList = clist

    def run(self):
        #         sleep(1)
        for cmd in self.cmdList:
            print ".-rRr-. executing: " + cmd["cmdstring"]
            #             benchCommon.freePort(cmd["node"], cmd["port"])
            sshcmdbg(cmd["node"], cmd["cmdstring"])
            # "sudo fuser -k " + str(cmd["port"]) + "/tcp ; sleep 5; " + cmd["cmdstring"])


# sleep(0.5)

def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


def sshcmdbg(node, cmdstring):
    print "ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\" &"
    os.system("ssh -o StrictHostKeyChecking=no " + node + " \"" + cmdstring + "\" &")

# ====================================
# ====================================


system_config_file = script_dir() + "/system_config.json"
print("Deploying system helper nodes described in " + system_config_file)

config_json = open(system_config_file)

config = json.load(config_json)

# MUST ASSUME THAT EACH HELPERNODE IS IN A SINGLE RING
# AND THAT ALL NODES OF THE SAME RING ARE TOGETHER IN THE CONFIG FILE
# AND THAT NO RING HAS ID -1
ensembleCmdLists = []
lastEnsemble = -1
cmdList = []
java_bin = "java -XX:+UseG1GC -Dlog4j.configuration=file:" + script_dir() + "/log4j.xml -cp "
javaRidgeNodeClass = 'ch.usi.dslab.bezerra.mcad.ridge.RidgeEnsembleNode'
app_classpath = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/netwrapper/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/libmcad/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/URingPaxos/target/classes:/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/lib/*:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/dependencies/* "
for process in config["ensemble_processes"]:
    role = process["role"]
    pid = process["pid"]
    ensemble = process["ensemble"]
    host = process["host"]
    port = process["port"]

    launchNodeCmdPieces = [java_bin, app_classpath, javaRidgeNodeClass, system_config_file,
                           pid]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
    cmdList.append({"node": host, "port": port, "cmdstring": launchNodeCmdString})

    message = "launching " + role + " " + str(pid), " at ", host + ":" + str(port)

    print(message)
    # delete the acceptor's disk backup
    # os.system(command_string)

config_json.close()

launcherThreads = []

thread = launcherThread(cmdList)
thread.start()
thread.join()
