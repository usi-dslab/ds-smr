package ch.usi.dslab.lel.aerie;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Author: longle, created on 15/03/16.
 */
public class CacheMultimap implements Cache {

    static Set<Set<Integer>> keyCombination;

    static {
        Set<Integer> tmp = new HashSet<>();
        for (int i = 1; i < 10; i++) {
            tmp.add(i);
        }
        keyCombination = Sets.powerSet(tmp);
    }

    private Multimap<ObjId, PRObjectLocation> cache = HashMultimap.create();

    synchronized public List<PRObjectLocation> get(ObjId key) {
        Set<PRObjectLocation> set = (Set<PRObjectLocation>) this.cache.get(key);
        List<PRObjectLocation> ret = new ArrayList<>();
        ret.addAll(set.stream().filter(i -> i != null).collect(Collectors.toList()));
        return ret;
    }

    synchronized public List<PRObjectLocation> get(Set<ObjId> keys) {
        List<PRObjectLocation> ret = new ArrayList<>();
        keys.stream().forEach(key -> ret.addAll(get(key)));
        return ret;
    }

    @Override
    public Object getInstance() {
        return this.cache;
    }

    synchronized public PRObjectLocation getOne(ObjId key) {
        Set<PRObjectLocation> set = (Set<PRObjectLocation>) this.cache.get(key);
        if (set.size() == 0) return null;
        if (set.size() > 1) {
            // this should never happen
            return set.iterator().next();
        }
        return set.iterator().next();
    }

    synchronized public void update(ObjId key, int newPartitionId) {
        PRObjectLocation loc = getOne(key);
        loc.setPartitionId(newPartitionId);
    }


    synchronized public void put(ObjId key, PRObjectLocation location) {
        ArrayList<String> keys = getAllKeys(key.value.split(":"));
        keys.stream().forEach(keyStr -> this.cache.put(new ObjId(keyStr), location));
    }

    synchronized public void set(ObjId key, PRObjectLocation location) {
        this.put(key, location);
    }

    @Override
    public int getSize() {
        return this.cache.size();
    }

    synchronized public void removeOne(ObjId key) {
        PRObjectLocation ojb = getOne(key);
        ArrayList<String> keys = getAllKeys(key.value.split(":"));
        keys.stream().forEach(keyStr -> this.cache.remove(new ObjId(keyStr), ojb));

    }

    synchronized public void remove(ObjId key) {
        Set<PRObjectLocation> set = (Set<PRObjectLocation>) this.cache.get(key);
        if (set.size() > 1) {
            // this should never happen
            this.cache.removeAll(key);
            return;
        }
        this.cache.removeAll(key);
    }


//    synchronized public Set<ObjId> getKeys(String[] pattern) {
//        Set<ObjId> ret = new HashSet<>();
//        //^(?=.*\bWAREHOUSE\b)(?=.*\bw_id#1\b).*$
//        StringBuilder regStr = new StringBuilder("^");
//        for (String str : pattern) {
//            regStr.append("(?=.*\\b" + str + "\\b)");
//        }
//        regStr.append(".*$");
//        Pattern r = Pattern.compile(regStr.toString());
//        for (ObjId objId : this.cache.keySet()) {
//            Matcher m = r.matcher(objId.value);
//            if (m.find()) ret.add(objId);
//        }
//        return ret;
//    }

    public static ArrayList<String> getAllKeys(ObjId objId) {
        return getAllKeys(objId.value.split(":"));
    }

    public static ArrayList<String> getAllKeys(String[] pattern) {
        ArrayList<String> ret = new ArrayList<>();
        keyCombination.stream().forEach(set -> {
            ArrayList<String> strSet = new ArrayList<>();
            strSet.add(pattern[0]);
            set.stream().filter(integer -> integer < pattern.length).forEach(i -> strSet.add(pattern[i]));
            String[] str = strSet.toArray(new String[strSet.size()]);
            ret.add(String.join(":", str));
        });
        return ret;
    }

}
