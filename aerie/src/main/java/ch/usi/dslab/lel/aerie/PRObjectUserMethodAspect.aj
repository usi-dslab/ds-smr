/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie;

import java.util.ArrayList;
import java.util.Set;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.LocalReplica.MessageType;
import ch.usi.dslab.lel.aerie.LocalReplica.ObjectDiffRepository;
import ch.usi.dslab.lel.aerie.exec.CommandExecution;

public aspect PRObjectUserMethodAspect {

    pointcut sub_nonannotated(): ! execution(@LocalMethodCall * PRObject+.*(..));
    pointcut sub_annotated(): execution(@LocalMethodCall * PRObject+.*(..));

    pointcut sub(): execution(* PRObject+.*(..));

    pointcut notBase(): ! execution(* PRObject.*(..));

//    pointcut cons(): execution(public PRObject+.new(..)) && ! execution(public PRObject.new(..));

//    pointcut retrieve(): execution(* PRObject.getObjectById(ObjId));


    /* advice sub class methods but not annotation or parent */
    Object around(): sub_nonannotated() && sub() && notBase(){
        // TODO: what if the object does not exist in this replica?
        // answer: for now, we assume that every object exists everywhere,
        // but not necessarily up-to-date

        PRObject obj = (PRObject) thisJoinPoint.getTarget();
        if (!obj.getLegacySSMR())
            return proceed();
        CommandExecution currentExecution = StateMachine.getMachine().currentCommandExecution;

        currentExecution.markChangedObject(obj.id);
        Command currentCommand = currentExecution.getCommand();

        if (currentCommand.destinations.size() == 1)
            return proceed();

        Set<ObjId> exchangedObjects = currentExecution.getExchangedObjects();
        //System.out.println(String.format("ASPECT :: exchangedObjects: %s length %d", exchangedObjects, exchangedObjects.size()));
        if (!exchangedObjects.contains(obj.getId())) {

            //System.out.println(String.format("ASPECT :: exchangedObjects.contains(%s) == false", obj.getId()));

            exchangedObjects.add(obj.getId());

            //System.out.println(String.format("ASPECT :: %s.isLocal() = %b", obj.getId(), obj.isLocal()));
            if (obj.isLocal()) {
                //System.out.println("ASPECT :: 1");
                if (!LocalReplica.replica.isPartitionMulticaster(currentCommand)) {
                    //System.out.println(String.format("ASPECT :: LocalReplica.replica.isPartitionMulticaster"));
                    //System.out.println("ASPECT :: 2");
                    return proceed();
                }
                //System.out.println("ASPECT :: 3");

                // get the set of _other_ partitions that delivered this command
                ArrayList<Partition> destinations = new ArrayList<>(currentCommand.getDestinations());
                //System.out.println("ASPECT :: 4");
                //System.out.println(String.format("ASPECT :: destinations :: %s, length %d", destinations, destinations.size()));
                // TODO: if multiple partitions can own an object, then all such owners
                //       should be removed here. However, we are assuming (for now) that
                //       each object either has a single owner partition or is fully replicated
                //       not being a PRObject and its methods are not intercepted
                destinations.remove(LocalReplica.replica.localPartition);
                //System.out.println("ASPECT :: 5");
                //====================================================
                // short-circuit to avoid unnecessary calls to lower (mcast) layer
                if (destinations.isEmpty()) {
                    //System.out.println("ASPECT :: 6");
                    return proceed();
                }
                //====================================================

                // diff and send object to the other partitions
                MessageType msgType = MessageType.OBJ;
                Message objectDiff = obj.getSuperDiff(destinations);
                Message objectMessage = new Message(msgType, currentCommand.id, obj.id, objectDiff);
                //System.out.println("ASPECT :: 7 " + objectMessage);
                //System.out.println("ASPECT :: 7 " + destinations);
                LocalReplica.replica.multicastObject(destinations, objectMessage);

            }
            if (!obj.isLocal()) { // i.e., obj is a remote object
                //System.out.println("ASPECT :: 8");
                Message remoteDiff = LocalReplica.replica.takeObjectDiff(currentCommand.id, obj.id);
                //System.out.println("ASPECT :: 9");
                if (remoteDiff != ObjectDiffRepository.ERROR_DIFF)
                    obj.updateFromDiff(remoteDiff);

            }
        } else {
            //System.out.println(String.format("ASPECT :: exchangedObjects.contains(%s) == true", obj.getId()));
        }

        // object must be sent BEFORE method execution
        return proceed();
    }
    //
//    Object around(): retrieve() {
//        System.out.println("inside retrieve");
//        ObjId objId = (ObjId)thisJoinPoint.getArgs()[0];
//        Object obj= proceed();
//        if (obj!=null)
//            System.out.println(String.format("running after retrieve object %s", obj));
//        else {
//            System.out.println(String.format("running after retrieve object which is null of id %s", objId));
//            obj = new Object();
//        }
//        return obj;
//    }

}