#!/usr/bin/python

import inspect
import os
import sys

os.system("clear")
def script_dir():
   return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

java_bin = "java -XX:+UseG1GC -Dlog4j.configuration=file:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/classes/log4j.xml -cp "
server_class = "ch.usi.dslab.lel.aerie.benchmark.AppServer "
client_class = "ch.usi.dslab.lel.aerie.benchmark.AppClient "
partitioning_file = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/src/main/java/ch/usi/dslab/lel/aerie/benchmark/partitioning.json "
app_classpath = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/netwrapper/target/classes:/Users/longle/.m2/repository/org/apache/logging/log4j/log4j-api/2.0-rc1/log4j-api-2.0-rc1.jar:/Users/longle/.m2/repository/org/apache/logging/log4j/log4j-core/2.0-rc1/log4j-core-2.0-rc1.jar:/Users/longle/.m2/repository/net/jpountz/lz4/lz4/1.2.0/lz4-1.2.0.jar:/Users/longle/.m2/repository/io/dropwizard/metrics/metrics-core/3.1.2/metrics-core-3.1.2.jar:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/libmcad/target/classes:/Users/longle/.m2/repository/multicast/spread/4.4.0/spread-4.4.0.jar:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/URingPaxos/target/classes:/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/lib/*:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/dependencies/* "
config_file = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/src/main/java/ch/usi/dslab/lel/aerie/benchmark/system_config.json "
output_file = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/src/main/java/ch/usi/dslab/lel/aerie/benchmark/log.log"

monitor = "localhost "

if len(sys.argv) != 5:
    print "usage: " + sys.argv[0] + " <clientId> <outstanding> <objectNum> <useGlobalcommand? (0)/1>"
    sys.exit(1)

client_id = sys.argv[1] + " "
outstanding = str(sys.argv[2]) + " "
objectNum = str(sys.argv[3]) + " "
globalCommand = str(sys.argv[4]) + " "

print 'Starting client:', str(sys.argv)

client_cmd = java_bin + app_classpath + client_class + client_id + config_file + partitioning_file + objectNum + outstanding + monitor + globalCommand
# print client_cmd
os.system(client_cmd)
