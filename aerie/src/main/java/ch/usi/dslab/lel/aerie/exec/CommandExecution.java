/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie.exec;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.CmdId;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.LocalReplica;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.PRObject;
import ch.usi.dslab.lel.aerie.Partition;
import ch.usi.dslab.lel.aerie.StateMachine;

public class CommandExecution {

    boolean isSSMRExecution = false;
    private Set<ObjId> changedObjects = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private Message reply = null;
    private Command command;
    private Set<ObjId> exchangedObjects = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private List<Partition> partitionsWaitingForSignals = new ArrayList<>();
    private List<Signal> signalsYetToReceive = new ArrayList<>();

    public CommandExecution(Command cmd, String type, LocalReplica localReplica) {
        command = cmd;
        List<Partition> otherPartitions = new ArrayList<>(command.getDestinations());
        if (otherPartitions.size() > 1) {
            otherPartitions.remove(localReplica.getLocalPartition());
            signalsYetToReceive = Signal.partitionListToSignalList(otherPartitions, command, type);
        }
    }

    public CommandExecution(Command cmd, boolean correct) {
        command = cmd;
    }

    public CommandExecution(Command cmd) {
        command = cmd;
    }

    public Command getCommand() {
        return command;
    }

    public Message getReply() {
        return this.reply;
    }

    // reply can only be set once
    public void setReply(Message reply) {
        if (this.reply == null)
            this.reply = reply;
    }

    public void addExchangedObjects(Collection<PRObject> excObjects) {
        for (PRObject obj : excObjects)
            addExchangedObject(obj.getId());
    }

    public void addExchangedObject(ObjId oid) {
        exchangedObjects.add(oid);
    }

    public Set<ObjId> getExchangedObjects() {
        return exchangedObjects;
    }

    public void markChangedObject(ObjId oid) {
        changedObjects.add(oid);
    }

    public void setPendingSignals(List<Signal> pendingSignals) {
        signalsYetToReceive = pendingSignals;
    }

    public void waitPendingSignals() {
        // TODO: optimize the way the signals are handled. Maybe running through
        // the whole list everytime
        // this thread is woken up is not the best approach.

        if (signalsYetToReceive.isEmpty())
            return;

        StateMachine csm = StateMachine.getMachine();

        csm.getSignalsLock().lock();
        checkSignalsQueue();
        try {
            while (!signalsYetToReceive.isEmpty()) {
                csm.getSignalsCondition().await();
                checkSignalsQueue();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
        csm.getSignalsLock().unlock();

    }

    private void checkSignalsQueue() {
        StateMachine consSM = StateMachine.getMachine();
        for (Iterator<Signal> it = signalsYetToReceive.iterator(); it.hasNext(); ) {
            Signal s = it.next();
            if (!command.getId().equals(s.cmdId)) {
                System.err.println("CMDID != S.CMDID!!!!");
                System.exit(1);
            }
            if (consSM.getSignalQueue().contains(s)) {
                it.remove();
                consSM.getSignalQueue().remove(s);
            }
        }
    }

    public void setSSMRMode() {
        isSSMRExecution = true;
    }

    public boolean isSSMRExecution() {
        return isSSMRExecution;
    }

    public static class Signal {
        Partition sourcePartition;
        CmdId cmdId;

        public Signal(int partId, CmdId cmdId) {
            this.sourcePartition = Partition.getPartition(partId);
            this.cmdId = cmdId;
        }

        public static List<Signal> partitionListToSignalList(List<Partition> partitions, Command cmd, String type) {
            return partitions.stream().filter(p -> p.getType().equals(type)).map(p -> new Signal(p.getId(), cmd.getId())).collect(Collectors.toList());
        }

        @Override
        public boolean equals(Object Oother) {
            Signal other = (Signal) Oother;
            return this.sourcePartition.equals(other.sourcePartition) && this.cmdId.equals(other.cmdId);
        }
    }

}
