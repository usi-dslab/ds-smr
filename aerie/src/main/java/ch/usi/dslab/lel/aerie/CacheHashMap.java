package ch.usi.dslab.lel.aerie;

import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Author: longle, created on 15/03/16.
 */
public class CacheHashMap implements Cache {
    private Map<ObjId, PRObjectLocation> cache = new ConcurrentHashMap<>();


    synchronized public List<PRObjectLocation> get(Set<ObjId> keys) {
        List<PRObjectLocation> ret = new ArrayList<>();
        keys.stream().forEach(key -> ret.add(getOne(key)));
        return ret.stream().filter(i -> i != null).collect(Collectors.toList());
    }

    synchronized public List<PRObjectLocation> get(ObjId key) {
        return Collections.singletonList(this.cache.get(key));
    }

    @Override
    public Object getInstance() {
        return this.cache;
    }

    synchronized public PRObjectLocation getOne(ObjId key) {
        return this.cache.get(key);
    }

    synchronized public void update(ObjId key, int newPartitionId) {
        PRObjectLocation loc = getOne(key);
        loc.setPartitionId(newPartitionId);
    }


    synchronized public void put(ObjId key, PRObjectLocation location) {
        this.cache.put(key, location);
    }

    synchronized public void set(ObjId key, PRObjectLocation location) {
        this.put(key, location);
    }

    @Override
    public int getSize() {
        return this.cache.keySet().size();
    }

    synchronized public void removeOne(ObjId key) {
        this.cache.remove(key);

    }

    synchronized public void remove(ObjId key) {
        this.cache.remove(key);
    }


//    synchronized public Set<ObjId> getKeys(String[] pattern) {
//        Set<ObjId> ret = new HashSet<>();
//        //^(?=.*\bWAREHOUSE\b)(?=.*\bw_id#1\b).*$
//        StringBuilder regStr = new StringBuilder("^");
//        for (String str : pattern) {
//            regStr.append("(?=.*\\b" + str + "\\b)");
//        }
//        regStr.append(".*$");
//        Pattern r = Pattern.compile(regStr.toString());
//        for (ObjId objId : this.cache.keySet()) {
//            Matcher m = r.matcher(objId.value);
//            if (m.find()) ret.add(objId);
//        }
//        return ret;
//    }

    public static ArrayList<String> getAllKeys(ObjId objId) {
        return getAllKeys(objId.value.split(":"));
    }

    public static ArrayList<String> getAllKeys(String[] pattern) {
        Set<Integer> tmp = new HashSet<>();
        for (int i = 1; i < pattern.length; i++) {
            tmp.add(i);
        }
        ArrayList<String> ret = new ArrayList<>();
        Set<Set<Integer>> comb = Sets.powerSet(tmp);
        comb.stream().forEach(set -> {
            ArrayList<String> strSet = new ArrayList<>();
            strSet.add(pattern[0]);
            set.stream().forEach(i -> strSet.add(pattern[i]));
            String[] str = strSet.toArray(new String[strSet.size()]);
            ret.add(String.join(":", str));
        });
        return ret;
    }


}
