/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.mcad.FastMulticastAgent;
import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.mcad.MulticastAgent;
import ch.usi.dslab.bezerra.mcad.MulticastServer;
import ch.usi.dslab.bezerra.mcad.ReliableMulticastAgent;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.exec.CommandExecution;

public class LocalReplica {
    // TODO: this class should contain the necessary stuff to send and receive
    // messages
    private static final Logger logger = Logger.getLogger(LocalReplica.class);
    static LocalReplica replica = null;
    final BlockingQueue<CommandExecution> executionQueue;
    final BlockingQueue<Command> commandsQueue;
    Thread conservativeDelivererThread;
    Thread fastDelivererThread;
    Thread reliableDelivererThread;
    MulticastServer multicastServer;
    boolean running = true;
    Partition localPartition = null;
    ObjectDiffRepository receivedObjectDiffs = new ObjectDiffRepository();
    List<ObjId> nullObj = new ArrayList<>();
    private BlockingQueue<Object> objectExchangeQueue;
    private Map<ObjId, PRObject> objectExchangeDelivered;
    private ObjId oid;

    // ===========================================
    // LocalReplica construction-related methods
    // ===========================================
    private LocalReplica() {

        executionQueue = new LinkedBlockingQueue<CommandExecution>();
        commandsQueue = new LinkedBlockingQueue<Command>();
        objectExchangeQueue = new LinkedBlockingDeque<>();
        objectExchangeDelivered = new HashMap<>();
    }

    /*
     * This reads a json file containing the system's configuration, i.e., the
     * partitions that are part of it. Notice that this is agnostic to how
     * partitions are constituted; the multicast agent is the one that should
     * manage the mapping of partitions to groups/rings/whatever implements the
     * partitions.
     *
     * { "partitions" : [ {"id" : 1} , {"id" : 2} ] }
     */
    public static LocalReplica createLocalReplica(MulticastServer mcServer, String configFile) {
        LocalReplica local = new LocalReplica();
        LocalReplica.replica = local;
        local.multicastServer = mcServer;

        Partition.loadPartitions(configFile);

        int localGroupId = mcServer.getMulticastAgent().getLocalGroup().getId();
        local.localPartition = Partition.getPartition(localGroupId);
        local.start();

        return local;
    }

    public static LocalReplica getLocalReplica() {
        return replica;
    }

    public void start() {
        conservativeDelivererThread = new Thread(new ConservativeDeliverer(this), "ConservativeDeliverer");
        fastDelivererThread = new Thread(new FastDeliverer(this), "OptimisticDeliverer");
        reliableDelivererThread = new Thread(new ReliableDeliverer(this), "ReliableDeliverer");

        conservativeDelivererThread.start();
        fastDelivererThread.start();
        reliableDelivererThread.start();
    }

    public void deliverExchangedObject(Object obj) {
        objectExchangeQueue.add(obj);
    }

    public PRObject takeExchangedObject(ObjId oid) {
        PRObject ret = objectExchangeDelivered.get(oid);
        if (nullObj.contains(oid)) {
            logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-object " + oid + " is in nullList, stop waiting");
            nullObj.remove(oid);
            return null;
        }
        if (ret == null) {
            logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-object " + oid + " is not in deliveredQueue, wait for it");
            PRObject obj = null;
            do {
                try {
                    Object tmp = objectExchangeQueue.take();
                    if (tmp instanceof PRObject) {
                        obj = (PRObject) tmp;
                        if (!obj.getId().equals(oid)) {
                            logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + String.format("- got and exchange object %s, is not what waiting for %s, still wait", obj.getId(), oid));
                            objectExchangeDelivered.put(obj.getId(), obj);
                        } else {
                            logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-got an exchanged object " + oid + " return it");
                            return obj;
                        }
                    } else {
                        ObjId objId = (ObjId) tmp;
                        logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-retrieve null object id " + objId + " for id " + oid);
                        if (oid.equals(objId)) {
                            logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-client have to retry for object " + objId + " - STOP waiting");
                            return null;
                        } else {
                            logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-client have to retry for object " + objId + " not waiting for, still wait for " + oid);
                            nullObj.add(objId);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            while (obj == null || !obj.getId().equals(oid) || !nullObj.contains(oid) || !objectExchangeDelivered.containsKey(oid));
            if (nullObj.contains(oid)) {
                logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-object " + oid + " is in nullList, stop waiting");
                nullObj.remove(oid);
                return null;
            }
            if (objectExchangeDelivered.containsKey(oid)) {
                logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-object " + oid + " is already in deliveredQueue return it --ELSE");
                objectExchangeDelivered.remove(oid);
                return ret;
            }
        } else {
            logger.debug("[" + this.localPartition.getType() + "-" + this.localPartition.getId() + "]" + "-object " + oid + " is already in deliveredQueue return it");
            objectExchangeDelivered.remove(oid);
            return ret;
        }
        return null;
    }

    public void waitForStateMachineToBeReady() {
        try {
            Semaphore smReady = StateMachine.getMachine().getReadySemaphore();
            smReady.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void notifyDoneWithDiff(CmdId cmdId) {
        receivedObjectDiffs.markAsDone(cmdId);
    }

//    public void multicastCommand(Command cmd) {
//        StateMachine consSM = StateMachine.getMachine();
//        List<Partition> destinations = consSM.oracle.getProphecy(cmd).destinations;
//        multicastCommand(destinations, cmd);
//    }

    private void handleObjectUpdateMessage(Message msg) {
        @SuppressWarnings("unused")
        MessageType type = (MessageType) msg.getNext();
        CmdId cmdId = (CmdId) msg.getNext();
        ObjId objId = (ObjId) msg.getNext();
        Message objDiff = (Message) msg.getNext();

//      System.out.println("storing cons object diff for cmdId = " + cmdId);

        receivedObjectDiffs.storeObjectDiff(cmdId, objId, objDiff);
    }

    // ========================================================================
    // Lowest level for communication used by the local replica; uses mcagent
    // ========================================================================
    public void multicastCommand(List<Partition> dests, Command cmd) {
        cmd.setDestinations(dests);
        Message msg = new Message(MessageType.COMMAND, cmd);
        LocalReplica.replica.multicast(dests, msg);
    }

    public void multicast(List<Partition> dests, Message msg) {
        if (dests.isEmpty())
            return;
        // creating the array of destination groups to pass to the multicast agent
        ArrayList<Group> groupdests = new ArrayList<>(dests.size());
        groupdests.addAll(dests.stream().map(part -> part.pgroup).collect(Collectors.toList()));

        // add info about the message's destinations
        // ArrayList<Integer> destids = new ArrayList<Integer>(dests.size());
        // for (Partition part : dests)
        // destids.add(part.getId());
        // msg.destinationPartitionsIDs = destids;

        // send message through the multicast agent
        multicastServer.getMulticastAgent().multicast(groupdests, msg);
    }

    public void multicast(Partition part, Message msg) {
        // send message through the multicast agent
        multicastServer.getMulticastAgent().multicast(part.pgroup, msg);
    }

    public void multicastObject(List<Partition> dests, Message msg) {
        reliableMulticast(dests, msg);
    }

    // ==============================================================

    public void reliableMulticast(List<Partition> dests, Message msg) {
        if (dests.isEmpty())
            return;

        // creating the array of destination groups to pass to the multicast agent
        ArrayList<Group> groupdests = new ArrayList<>(dests.size());
        groupdests.addAll(dests.stream().map(part -> part.pgroup).collect(Collectors.toList()));

        // add info about the message's destinations
        // ArrayList<Integer> destids = new ArrayList<Integer>(dests.size());
        // for (Partition part : dests)
        // destids.add(part.getId());
        // msg.destinationPartitionsIDs = destids;

        // send message through the multicast agent
        ReliableMulticastAgent rma = (ReliableMulticastAgent) multicastServer.getMulticastAgent();
        rma.reliableMulticast(groupdests, msg);
    }

    public Message deliver() {
        return multicastServer.getMulticastAgent().deliverMessage();
    }

    /*
     * If a command C has already been delivered and executed, all necessary
     * synch with other partitions has been done already. The only exception is
     * in the case when then new delivery was done to a partition P that had not
     * delivered the command before, in which case P must be told to ignore the
     * already executed command.
     */
    public Command deliverCommand() {
        try {
            Command C = commandsQueue.take();
            C.rewind();
            return C;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public CommandExecution takeNextAwaitingExecution() {
        try {
            return executionQueue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }

    public void notifyCommandExecutionFinished(CommandExecution ce) {
        receivedObjectDiffs.markAsDone(ce.getCommand().getId());
    }

    public Message takeObjectDiff(CmdId cmdId, ObjId objId) {
        return receivedObjectDiffs.takeObjectDiff(cmdId, objId);
    }

    public boolean isPartitionMulticaster(Command cmd) {
        List<Integer> partitionMembers = localPartition.getGroup().getMembers();
        int multicasterIndex = cmd.id.clientId % partitionMembers.size();

        return this.multicastServer.getId() == partitionMembers.get(multicasterIndex);
    }

    // ========================================================================
    // ========================================================================

    public void sendReply(Message reply, Command command) {
        CmdId cmdId = command.getId();
        int clientId = cmdId.clientId;
        long requestId = cmdId.clientRequestSequence;
        MessageType type = command.getType() == MessageType.QUERY ? MessageType.REPLY_QUERY : MessageType.REPLY;
        // decide if this server should send the reply to this client
        if (thisIsTheReplyingReplica(command)) {
            Message replyWrapper = new Message(requestId, type, reply);
            replyWrapper.copyTimelineStamps(command);
            multicastServer.sendReply(clientId, replyWrapper);
        }
    }

    boolean thisIsTheReplyingReplica(Command command) {
        List<Partition> dests = command.getDestinations();
        List<Partition> partitions;
        if (command.getType() == MessageType.QUERY) {
            partitions = dests;
        } else {
            partitions = dests.stream().filter(partition -> partition.getType().equals("PARTITION")).collect(Collectors.toList());
        }
        int clientId = command.getSourceClientId();

        int senderPartitionIndex = clientId % partitions.size();
        Partition senderPartition = partitions.get(senderPartitionIndex);

        boolean thisIsTheReplyingPartition = LocalReplica.getLocalReplica().getLocalPartition() == senderPartition;
        boolean clientIsConnectedToThisReplica = multicastServer.isConnectedToClient(clientId);

        return thisIsTheReplyingPartition && clientIsConnectedToThisReplica;
    }

    public Partition getLocalPartition() {
        return localPartition;
    }

    public enum MessageType {
        // TODO: make the serialization of this enum to a byte, instead of String (default)
        COMMAND, RETURN_VALUE, OBJ, OBJ_BUNDLE, SIGNAL, REPLY, CANCEL_MOVE, REPLY_QUERY, MOVE_OBJECT, QUERY, SSMR_EXEC
    }

    public static class PeekWaitBlockingQueue<E> extends LinkedBlockingDeque<E> {
        private static final long serialVersionUID = 1L;

        synchronized public E peekWait() throws InterruptedException {
            E obj = take();
            this.addFirst(obj);
            return obj;
        }

        public void put(E obj) throws InterruptedException {
            super.put(obj);
        }

        synchronized public E remove() {
            return super.removeFirst();
        }
    }

    public static class ConservativeDeliverer implements Runnable {

        LocalReplica localReplica;
        ConservativeUnbatcher unBatcher;

        public ConservativeDeliverer(LocalReplica localReplica) {
            this.localReplica = localReplica;
            this.unBatcher = new ConservativeUnbatcher(localReplica);
        }

        // ==============================================================
        // LocalReplica's thread for getting and sorting mcast messages
        // ==============================================================
        @Override
        public void run() {

            localReplica.waitForStateMachineToBeReady();

            while (localReplica.running) {
                Message m = unBatcher.nextMessage();

                MessageType type = (MessageType) m.getNext();
                logger.debug("[" + localReplica.getLocalPartition().getType() + "-" + localReplica.getLocalPartition().getId() + "]" + String.format("cons-delivered message %s", m));
                switch (type) {
                    case QUERY:
                    case COMMAND: {
                        Command c = (Command) m.getNext(); // cmd (inside cmdContainer!)
                        c.t_learner_delivered = System.currentTimeMillis();
                        c.setType(type);
                        try {
                            CommandExecution ce = new CommandExecution(c, type.toString(), localReplica);
                            localReplica.executionQueue.put(ce);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                    case SSMR_EXEC: {
                        Command c = (Command) m.getNext(); // cmd (inside cmdContainer!)
                        c.t_learner_delivered = System.currentTimeMillis();
                        c.setType(type);
                        try {
                            CommandExecution ce = new CommandExecution(c, "PARTITION", localReplica);
                            ce.setSSMRMode();
                            localReplica.executionQueue.put(ce);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                    }

                    default: {
                        break;
                    }
                }
            }

        }

        // ===========================================
        // ===========================================

        public static class ConservativeUnbatcher {
            LocalReplica local;
            Queue<Message> readyMessages;

            public ConservativeUnbatcher(LocalReplica lr) {
                local = lr;
                readyMessages = new LinkedList<>();
            }

            public Message nextMessage() {
                if (!readyMessages.isEmpty()) {
                    return readyMessages.remove();
                } else {
                    Message m = local.multicastServer.getMulticastAgent().deliverMessage();
                    if (m.getItem(0) instanceof ClientMessage) {
                        // unbatch clientmessage batch
                        m.rewind();
                        while (m.hasNext()) {
                            Message oneMessage = (Message) m.getNext();
                            oneMessage.unpackContents();
                            readyMessages.add(oneMessage);
                        }
                        return readyMessages.remove();
                    } else {
                        return m;
                    }
                }
            }
        }

    }

    public static class FastDeliverer implements Runnable {
        LocalReplica localReplica;
        FastUnbatcher unBatcher;

        public FastDeliverer(LocalReplica localReplica) {
            this.localReplica = localReplica;
            this.unBatcher = new FastUnbatcher(localReplica);
        }

        // LocalReplica's thread for getting and sorting mcast messages
        // ==============================================================
        @Override
        public void run() {
            MulticastAgent ma = localReplica.multicastServer.getMulticastAgent();
            if (!(ma instanceof FastMulticastAgent))
                return;

            while (localReplica.running) {
                Message m = unBatcher.nextMessage();
                MessageType type = (MessageType) m.getNext();
                switch (type) {
                    default: {
                        break;
                    }
                }
            }
        }

        public static class FastUnbatcher {
            LocalReplica local;
            Queue<Message> readyMessages;

            public FastUnbatcher(LocalReplica lr) {
                local = lr;
                readyMessages = new LinkedList<>();
            }

            public Message nextMessage() {
                if (!readyMessages.isEmpty()) {
                    return readyMessages.remove();
                } else {
                    FastMulticastAgent oma = (FastMulticastAgent) local.multicastServer.getMulticastAgent();
                    Message m = oma.deliverMessageFast();
                    if (m.getItem(0) instanceof ClientMessage) {
                        // unbatch clientmessage batch
                        m.rewind();
                        while (m.hasNext()) {
                            Message oneMessage = (Message) m.getNext();
                            oneMessage.unpackContents();
                            readyMessages.add(oneMessage);
                        }
                        return readyMessages.remove();
                    } else {
                        return m;
                    }
                }
            }
        }        // ==============================================================


    }

    public static class ReliableDeliverer implements Runnable {
        LocalReplica localReplica;

        public ReliableDeliverer(LocalReplica localReplica) {
            this.localReplica = localReplica;
        }

        // ==============================================================
        // LocalReplica's thread for getting and sorting rmcast messages
        // ==============================================================
        @Override
        public void run() {
            ReliableMulticastAgent rma = (ReliableMulticastAgent) localReplica.multicastServer.getMulticastAgent();

            while (localReplica.running) {
                Message m = rma.reliableDeliver();
                logger.debug(String.format("reliable-delivered message %s", m));
                MessageType type = (MessageType) m.getNext();

                switch (type) {
                    case OBJ: {
                        m.rewind();
                        localReplica.handleObjectUpdateMessage(m);
                        break;
                    }

                    case OBJ_BUNDLE: {
                        while (m.hasNext()) {
                            Message objMsg = (Message) m.getNext();
                            objMsg.rewind();
                            localReplica.handleObjectUpdateMessage(objMsg);
                        }
                        break;
                    }

                    case SIGNAL: {
                        StateMachine.getMachine().enqueueSignal(m);
                        break;
                    }

                    case MOVE_OBJECT:
                    case CANCEL_MOVE: {
                        Object obj = m.getNext();
                        localReplica.deliverExchangedObject(obj);
                        break;
                    }

                    default: {
                        break;
                    }
                }
            }
        }

    }

    public static class ObjectDiffRepository {
        public static final Message ERROR_DIFF = new Message();

        Map<CmdId, Map<ObjId, Message>> allObjsReceived;


        public ObjectDiffRepository() {
            allObjsReceived = new ConcurrentHashMap<>();
        }

        synchronized public void storeObjectDiff(CmdId cmdId, ObjId objId, Message objDiff) {
            Map<ObjId, Message> cmdObjDiffs = allObjsReceived.get(cmdId);

            if (cmdObjDiffs == null) {
                cmdObjDiffs = new HashMap<>();
                allObjsReceived.put(cmdId, cmdObjDiffs);
            }

            cmdObjDiffs.put(objId, objDiff);
            notifyAll();
        }

        synchronized public Message takeObjectDiff(CmdId cmdId, ObjId objId) {
            // outter map: id is cmd's
            // inner map: id is object's
            Message wantedObjectDiff = null;
            boolean gotIt = false;
            try {
                do {

                    Map<ObjId, Message> currentCommandReceivedObjectDiffs = allObjsReceived.get(cmdId);
                    if (currentCommandReceivedObjectDiffs == null) {
                        wait();
                        continue;
                    }

                    wantedObjectDiff = currentCommandReceivedObjectDiffs.get(objId);
                    if (wantedObjectDiff == null) {
                        wait();
                        continue;
                    }

                    gotIt = true;
                } while (!gotIt);
            } catch (InterruptedException e) {
                System.err.println("!!! - something occurred while trying to get object from Map<Map<Object>>");
                e.printStackTrace();
                System.exit(1);
            }

            wantedObjectDiff.rewind();
            return wantedObjectDiff;
        }

        synchronized public void markAsDone(CmdId cmdId) {
            allObjsReceived.remove(cmdId);
        }
    }
}
