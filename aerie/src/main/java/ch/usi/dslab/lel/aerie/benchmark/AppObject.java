/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.aerie.benchmark;

import java.util.List;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.codecs.Codec;
import ch.usi.dslab.bezerra.netwrapper.codecs.CodecUncompressedKryo;
import ch.usi.dslab.lel.aerie.PRObject;
import ch.usi.dslab.lel.aerie.PRObjectReference;
import ch.usi.dslab.lel.aerie.Partition;

public class AppObject extends PRObject{
    private static final long serialVersionUID = 1l;

    // any reference to another probject must be held by a probjref kind of object,
    // so that the referred object does not get serialized unnecessarily
    PRObjectReference someOtherPRObject;

    int value;

    public AppObject() {
        super();
    }

    public AppObject(int initialValue) {
        value = initialValue;
    }

    int getValue() {
        return value;
    }

    void add(int value) {
        this.value += value;
    }

    void sub(int value) {
        this.value -= value;
    }

    void mul(int value) {
        this.value *= value;
    }

    void div(int value) {
        this.value /= value;
    }

    void localNOP() {
    }

    @Override
    public PRObject deepClone() {
        Codec codec = new CodecUncompressedKryo();
        return (PRObject) codec.deepDuplicate(this);
//        AppObject clone = new AppObject(this.value);
        // TODO: what about the reference?
//        return clone;
    }

    @Override
    public Message getSuperDiff(List<Partition> destinations) {
        Message diff = new Message(someOtherPRObject, value);
        return diff;
    }

    @Override
    public void updateFromDiff(Message objectDiff) {
        Message diff = objectDiff.duplicate();
        diff.rewind();
        PRObjectReference pror = (PRObjectReference) diff.getNext();
        int value = (Integer) diff.getNext();

        this.someOtherPRObject = pror;
        this.value = value;
    }

}
