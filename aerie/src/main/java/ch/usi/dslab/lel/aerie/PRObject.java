/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.usi.dslab.bezerra.netwrapper.Message;

public abstract class PRObject implements Serializable, Comparable<PRObject> {
    private static final long serialVersionUID = 1L;
    private static Map<ObjId, PRObject> objectIndex;
    private static Map<ObjId, Partition> objectPartitioning;
    private static boolean legacySSMR;

    static {
        objectIndex = new ConcurrentHashMap<>();
        objectPartitioning = new ConcurrentHashMap<>();
    }

    public static void setLegacySSMR(boolean legacy) {
        legacySSMR = legacy;
    }

    public static boolean getLegacySSMR() {
        return legacySSMR;
    }

    public ObjId id;
    int ownerPartitionId;

    public PRObject() {

    }

    public static int getObjectCount() {
        return objectIndex.keySet().size();
    }

    public PRObject(String id) {
        PRObject.indexPRObject(this, new ObjId(id));
    }


    public PRObject(ObjId id, int owner) {
        PRObject.indexPRObject(this, id);
        this.setPartition(Partition.getPartition(owner));
    }

    public static void indexPRObject(PRObject obj, ObjId index) {
        obj.id = index;
        objectIndex.put(index, obj);
    }

    public static PRObject getObjectById(String id) {
        return getObjectById(new ObjId(id));
    }

    public static final PRObject getObjectById(ObjId id) {
        PRObject obj = objectIndex.get(id);
//        if (obj == null) {
//            List<ObjId> keys = getKeys(id.value.split(":"));
//            if (keys.size() > 0) {
//                obj = objectIndex.get(keys.get(0));
//            }
//        }
        return obj;
    }

    public static final PRObject getObjectById(Class subclass, ObjId id) throws IllegalAccessException, InstantiationException {
        if (objectIndex.get(id) != null) return objectIndex.get(id);
        PRObject obj = (PRObject) subclass.newInstance();
        indexPRObject(obj, id);
        obj.setId(id);
        obj.setOwnerPartitionId(-1);
        return obj;
    }

    public static void loadConfig(String configFile) {
      /* requires overriding by the user's subclass
       * it's not necessary for the user class to implement this
       * method, since other ways of loading objects can be used,
       * but it may be useful gto centralize the loading in a
       * single configuration file that encodes the objects somehow.
       *
       * e.g.: {"objects" = [ {"id" : 1, "value": 123, "partition": 1} ,
                              {"id" : 2, "value": -23, "partition": 2}
                            ]
               }
       */
    }

    public static boolean isAvailable(ObjId id) {
        return (getObjectById(id) != null);
    }

    private static List<ObjId> getKeys(String[] pattern) {
        List<ObjId> ret = new ArrayList<>();
        StringBuilder regStr = new StringBuilder("^");
        for (String str : pattern) {
            regStr.append("(?=.*\\b" + str + "\\b)");
        }
        regStr.append(".*$");
        Pattern r = Pattern.compile(regStr.toString());
        for (ObjId objId : objectIndex.keySet()) {
            Matcher m = r.matcher(objId.value);
            if (m.find()) ret.add(objId);
        }
        return ret;
    }

    public void setOwnerPartitionId(int ownerPartitionId) {
        this.ownerPartitionId = ownerPartitionId;
    }

    public void setId(ObjId id, int partitionId) {
        indexPRObject(this, id);

        //L.Le Move from setId(int,int,int)
        Partition partition = Partition.getPartition(partitionId);
        setPartition(partition);
    }

    public PRObjectReference getPRReference() {
        return new PRObjectReference(id);
    }

    public boolean isLocal() {
//        Partition owner = objectPartitioning.get(this.id);
//      Partition localPartition = LocalReplica.replica.getPartition();
//      System.out.println(String.format("Partition that owns obj. %s: %s; Local partition = %s", this.id, owner, localPartition));
//        return LocalReplica.replica.getLocalPartition() == owner;
        return ownerPartitionId == LocalReplica.replica.getLocalPartition().getId();
    }

    public void setId(String id) {
        setId(new ObjId(id));
        //L.Le Move to setId(ObjId)
//        PartitioningOracle oracle = StateMachine.getMachine().getOracle();
//        setPartition(oracle.getObjectPlacement(this));
    }

    public void unIndex() {
        objectIndex.remove(this.id);
    }

    public void unsetPartition() {
        objectPartitioning.remove(this.id);
    }

    public Partition getPartition() {
        return Partition.getPartition(ownerPartitionId);
    }

    public void setPartition(Partition part) {
        objectPartitioning.put(this.id, part);
        ownerPartitionId = part.getId();
    }

    @Override
    public int compareTo(PRObject other) {
        return this.id.value.compareTo(other.id.value);
    }

    // "super" because this must return a diff that works for all
    // partitions, which may have different versions of the object
    public abstract Message getSuperDiff(List<Partition> destinations);

    public abstract void updateFromDiff(Message objectDiff);

    public abstract PRObject deepClone();

    @Override
    public String toString() {
        if (this.getId() != null)
            return this.getId().toString();
        else return "";
    }

    public ObjId getId() {
        return id;
    }

    public void setId(ObjId id) {
        indexPRObject(this, id);

        // Moved from setId(int,int,int)
        setPartition(LocalReplica.replica.getLocalPartition());
    }
}
