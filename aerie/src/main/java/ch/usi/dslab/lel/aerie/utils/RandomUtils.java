package ch.usi.dslab.lel.aerie.utils;

import java.util.Random;

/**
 * Created by longle on 20/07/15.
 */
public class RandomUtils {

    public static String randomString(int length) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static int randomNumberBetween(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }
}
