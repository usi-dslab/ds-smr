/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.aerie.benchmark;

import org.apache.log4j.Logger;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.ArrayList;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.CommandType;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.PRObject;
import ch.usi.dslab.lel.aerie.StateMachine;

public class AppServer extends StateMachine {
    static final boolean is64bit = true;
    private static final Logger logger = Logger.getLogger(AppServer.class);

    public AppServer(int serverId, int partitionId, String systemConfig, String partitionsConfig) {
        super(serverId, partitionId, systemConfig, partitionsConfig, new AppProcedure());
    }

    private static Unsafe getUnsafe() {
        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            return (Unsafe) theUnsafe.get(null);
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    public static void main(String[] args) {
        logger.debug("Starting server...");
        if (args.length != 4) {
            System.err.println("usage: serverId partitionId systemConfigFile partitioningFile");
            System.exit(1);
        }

        int serverId = Integer.parseInt(args[0]);
        int partitionId = Integer.parseInt(args[1]);

        String systemConfigFile = args[2];

        String partitionsConfigFile = args[3];

        AppServer server = new AppServer(serverId, partitionId, systemConfigFile, partitionsConfigFile);

        server.runStateMachine();
    }

    @Override
    public Message executeSSMRCommand(Command cmd) {
        return executeCommand(cmd);
    }

    @Override
    public Message executeCommand(Command cmd) {
        cmd.rewind();
        // Command format : | byte op | ObjId o1 | int value |
        logger.debug("Processing command " + cmd.getId().toString());
        CommandType op = (CommandType) cmd.getNext();
        ArrayList<ObjId> objIds = (ArrayList<ObjId>) cmd.getNext();
        ObjId obj1Id = objIds.get(0);
        ObjId obj2Id = objIds.get(1);
//        AppObject ao1 = (AppObject) PRObject.getObjectById(obj1Id);
//        AppObject ao2 = (AppObject) PRObject.getObjectById(obj2Id);
        AppObject ao1 = null;
        AppObject ao2 = null;
        try {
            ao1 = (AppObject) PRObject.getObjectById(AppObject.class, obj1Id);
            ao2 = (AppObject) PRObject.getObjectById(AppObject.class, obj2Id);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        int val1 = ao1.getValue();
        int val2 = ao2.getValue();
        int tmp;
        switch ((AppCommand) op) {
            case ADD: {
                ao1.add(val2);
                break;
            }
            case SUB: {
                ao1.sub(val2);
                break;
            }
            case MUL: {
                ao1.mul(val2);
                break;
            }
            case DIV: {
                ao1.div(val2);
                break;
            }
        }
        logger.debug("Object " + ao1.getId() + " = " + ao1.getValue());
        return new Message(ao1);
    }

    @Override
    public AppObject createObject(Object objId, Object value) {
        AppObject obj = new AppObject((int) value);
        obj.setId((ObjId) objId);
        return obj;
    }


}
