#!/usr/bin/python

import inspect
import os
import sys

os.system("clear")


def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

# starting servers

java_bin = "java -XX:+UseG1GC -Dlog4j.configuration=file:" + script_dir() + "/log4j.xml -cp "
server_class = "ch.usi.dslab.lel.aerie.benchmark.AppServer "
client_class = "ch.usi.dslab.lel.aerie.benchmark.AppClient "
partitioning_file = script_dir() + "/partitioning.json "
config_file = script_dir() + "/system_config.json "
app_classpath = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/netwrapper/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/libmcad/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/URingPaxos/target/classes:/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/lib/*:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/dependencies/* "
# server = (server_id, partition_id)
servers = [(20, 0), (21, 1), (22, 2)]

silence_servers = False
# silence_servers = True

if len(sys.argv) != 2:
    print "usage: " + sys.argv[0] + " serverId (1/2)"
    sys.exit(1)


print 'Starting partition:', str(sys.argv)

# for server in servers :
server = servers[int(sys.argv[1])];
server_id = str(server[0]) + " "
partition = str(server[1]) + " "
server_cmd = java_bin + app_classpath + server_class + server_id + partition + config_file + partitioning_file
if silence_servers: server_cmd += " &> /dev/null "
server_cmd += " & "
print(server_cmd)
os.system(server_cmd)
