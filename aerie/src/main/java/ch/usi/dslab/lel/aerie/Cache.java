package ch.usi.dslab.lel.aerie;

import java.util.List;
import java.util.Set;

/**
 * Author: longle, created on 30/05/16.
 */
public interface Cache {
//    List<PRObjectLocation> get(ObjId key);

    Object getInstance();

    PRObjectLocation getOne(ObjId key);

    List<PRObjectLocation> get(ObjId key);

    List<PRObjectLocation> get(Set<ObjId> keys);

    void update(ObjId key, int newPartitionId);

    void put(ObjId key, PRObjectLocation location);

    void set(ObjId key, PRObjectLocation location);

    int getSize();

    void remove(ObjId key);

    void removeOne(ObjId key);

    class PRObjectLocation {
        private int partitionId;
        private ObjId objId;

        public PRObjectLocation(ObjId objId, int partitionId) {
            this.partitionId = partitionId;
            this.objId = objId;
        }


        public int getPartitionId() {
            return partitionId;
        }

        public void setPartitionId(int partitionId) {
            this.partitionId = partitionId;
        }

        public ObjId getObjId() {
            return objId;
        }

        public void setObjId(ObjId objId) {
            this.objId = objId;
        }
    }
}
