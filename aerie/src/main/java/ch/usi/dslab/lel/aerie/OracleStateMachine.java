/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.aerie;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.aerie.Cache.PRObjectLocation;
import ch.usi.dslab.lel.aerie.exec.CommandExecution;
import org.apache.logging.log4j.core.helpers.SystemClock;

/**
 * Created by longle on 16/10/15.
 */
public abstract class OracleStateMachine extends StateMachine {
    private static final Logger logger = Logger.getLogger(OracleStateMachine.class);
    private Cache cache;
    private Set<ObjId> accessedObjs = new HashSet<>();
    private long tmpCount = 0;
    private int serverId;
    boolean isLoggingReplica = false;
    private int move_count = 0;

    public OracleStateMachine(int serverId, int partitionId, String systemConfig, String partitionsConfig, ApplicationProcedure appProcedure, Cache cache) {
        super(serverId, partitionId, systemConfig, partitionsConfig, null, appProcedure);
        this.cache = cache;
        this.serverId = serverId;
        if (serverId % LocalReplica.getLocalReplica().getLocalPartition().getGroup().getMembers().size() == 0) {
            isLoggingReplica = true;
            System.out.println(serverId + "is logging for ORACLE");
        }

    }

    private void log(String message) {
        if (isLoggingReplica) logger.debug("[ORACLE-" + this.partitionId + "/" + serverId + "]-" + message);
    }

    @Override
    public void runStateMachine() {
        LocalReplica local = LocalReplica.getLocalReplica();
        readySemaphore.release();
        while (running) {

            CommandExecution cmdExecution = local.takeNextAwaitingExecution();
            Command currentCommand = cmdExecution.getCommand();
            log(currentCommand.getId() + "state machine running " + currentCommand);

            //================
            // TIMELINE STUFF
            currentCommand.t_command_dequeued = System.currentTimeMillis();
            //================

            // =========================
            currentCommandExecution = cmdExecution;
            // =========================

            // inside the executeCommand(...) method, there used to be a simple implementation
            // of the user's server logic. with optimistic execution, though, such method first
            // checks if the command was already opt-executed, if its order coincides
            // with the optimistic order and confirm/rectify the execution (and the reply accordingly).
            //
            // if the execution is confirmed, *reply* is simply a "confirm opt execution"
            // otherwise, *reply* contains the reply that should've been sent originally
            Message reply = executeQuery(currentCommand);

            currentCommandExecution.setReply(reply);

            // =========================
            // NO super-early state sent
            // =========================
            // super-early state sent: YES
            local.notifyCommandExecutionFinished(currentCommandExecution);
            // =========================

            // before proceeding to the next command (and maybe before
            // even replying to the client) wait until all signals have
            // been sent/received

            // send the reply to the client
            local.sendReply(reply, currentCommand);

            // clear list of received objects
            if (currentCommand.destinations.size() > 1)
                local.notifyDoneWithDiff(currentCommand.id);

        }
    }

    public Message executeQuery(Command cmd) {
        switch (cmd.getType()) {
            case QUERY:
                return getObjectLocation(cmd);
            case COMMAND:
                updateMemory(cmd);
                return new Message();
            default:
                return new Message();
        }
    }

    private Message getProphecy(Command cmd) {
        // Command format : | int op | ObjId o1 | value |
        CommandType op = (CommandType) cmd.getNext();
        ArrayList<Object> args = (ArrayList<Object>) cmd.getNext();
        ObjId oid = (ObjId) args.get(0);
        log(">>> Got query from client for object " + oid);

        PRObjectLocation mem = cache.getOne(oid);
        Message reply;

        if (op instanceof GenericCommand) {
            if (null == mem) {
                if (op == GenericCommand.CREATE) {

                    if (args.size() < 3) {
                        mem = new PRObjectLocation(oid, appProcedure.getObjectPlacement(oid));
                        log("<<< Object " + oid + " does not exist in Oracle memory, return one possible partition " + mem.getPartitionId());
                    } else {
                        int target = (int) args.get(2);
                        mem = new PRObjectLocation(oid, target);
                        log("<<< Object " + oid + " does not exist in Oracle memory, return requested partition" + mem.getPartitionId());
                    }
                    cache.put(oid, mem);
                    reply = new Message(mem.getObjId(), mem.getPartitionId());
                } else {
                    log("<<< Object " + oid + " does not exist in Oracle memory, can't move");
                    reply = new Message();
                }
            } else {
                // TODO: deal with case create on existing object
                if (op == GenericCommand.CREATE) {
                    log("<<< Object " + oid + " is in Oracle memory, how to deal? return " + mem.getPartitionId());
                    reply = new Message(mem.getObjId(), mem.getPartitionId());
                } else {
                    int target = (int) args.get(1);
                    reply = new Message(mem.getObjId(), target, mem.getPartitionId());
                }
            }
        } else {
            if (null == mem) {
                log("<<< Object " + oid + " does not exist in Oracle memory, return null");
                reply = new Message();
            } else {
                log("<<< Object " + oid + " is in Oracle memory, return " + mem.getObjId() + " - " + mem.getPartitionId());
                reply = new Message(mem.getObjId(), mem.getPartitionId());
            }
        }

        return reply;
    }

    private void updateMemory(Command cmd) {
        CommandType op = (CommandType) cmd.getNext();
        if (op == GenericCommand.MOVE) {
            ObjId oid = (ObjId) cmd.getNext();
            int destPart = (int) cmd.getNext();
            int srcPart = (int) cmd.getNext();

            log(">>> Receive command from client, should update Oracle memory ? " + oid);

            // check if valid move
            PRObjectLocation memOid = cache.getOne(oid);
            if (memOid == null) {
                log("<<< No, object is not in memory");
                return;
            }
            // TODO: what to do with this case? currently ignore
            if (memOid.getObjId().equals(oid) && (memOid.getPartitionId() == srcPart) && (srcPart != destPart)) {
                log(move_count++ + " MOVE :: " + memOid.getObjId() + " from " + srcPart + " to " + destPart);
                memOid.setPartitionId(destPart);
            } else {
                log("invalid move command, " + memOid.getObjId() + " is on " + memOid.getPartitionId() + " and want to move to " + destPart);
            }
        } else if (op == GenericCommand.CREATE) {
            ObjId oid = (ObjId) cmd.getNext();
            int destPart = cmd.getDestinations().stream().filter(i -> i.getType().equals("PARTITION")).collect(Collectors.toList()).get(0).getId();
            if (cache.get(oid).size() > 0) {
                cache.getOne(oid).setPartitionId(destPart);
            } else {
                PRObjectLocation mem = new PRObjectLocation(oid, destPart);
                cache.put(oid, mem);
            }
        } else if (op == GenericCommand.DELETE) {
            ObjId oid = (ObjId) cmd.getNext();
            PRObjectLocation memOid = cache.getOne(oid);
            int destPart = cmd.getDestinations().stream().filter(i -> i.getType().equals("PARTITION")).collect(Collectors.toList()).get(0).getId();
            if (memOid != null && memOid.getPartitionId() == destPart) {
                int before = cache.get(oid).size();
                log("DELETE: OK " + oid);
                cache.removeOne(oid);
                int after = cache.get(oid).size();
            } else {
                logger.error("INVALID DELETE" + oid);
            }
        } else {
            log("<<< No, not interested command");
        }
    }

    private Message getObjectLocation(Command cmd) {
        cmd.rewind();
        CommandType op = (CommandType) cmd.getNext();
        Prophecy prophecy = new Prophecy();
        boolean shouldPropose = (boolean) cmd.getNext();
        Set<ObjId> objIds = (HashSet) cmd.getNext();
        if (op == GenericCommand.QUERY_OBJ) {
            for (ObjId oid : objIds) {
                PRObjectLocation mem = cache.getOne(oid);
                int parId = -1;
                if (mem == null && shouldPropose) {
                    parId = appProcedure.getObjectPlacement(oid);
                    prophecy.add(oid, parId);
                } else if (mem != null) {
                    parId = mem.getPartitionId();
                    prophecy.add(mem.getObjId(), parId);
                }
            }
        } else if (op == GenericCommand.QUERY_RANGE) {
            List<PRObjectLocation> objectLocations = new ArrayList<>();
            for (ObjId oid : objIds) {
                List<PRObjectLocation> tmp;
                // this is for dsqldb
                // objId: Customer:c_w_id=1:c_d_id=1#c_id#1#3 -> query customers have c_w_id=1 and c_d_id=1 and 1<c_id<3
                String queryPart[] = oid.value.split("#");
                if (queryPart.length >= 2) {
                    tmp = cache.get(new ObjId(queryPart[0]));
                    String field = queryPart[1];
                    if (tmp.size() > 0 && queryPart.length == 4) {
                        int lowerBound = Integer.parseInt(queryPart[2]);
                        int upperBound = Integer.parseInt(queryPart[3]);
                        tmp = filterObjectRange(tmp, field, lowerBound, upperBound);
                    } else if (tmp.size() > 0 && queryPart.length == 3) {
                        String cond = queryPart[2];
                        tmp = filterObjectCond(tmp, field, cond);
                    }
                } else {//normal case
                    tmp = cache.get(oid);
                }
                objectLocations.addAll(tmp);
            }
            objectLocations.stream().forEach(prObjectLocation -> prophecy.add(prObjectLocation.getObjId(), prObjectLocation.getPartitionId()));
        }
        //prophecy.objectMap.keySet().stream().forEach(accessedObjs::add);
        //logger.info("Accessed Objects Size: " + accessedObjs.size());
        //if (++tmpCount % 5000 == 0)
        //    logger.info("Accessed Objects Size: " + accessedObjs.size() + " vs All objects: " + cache.getSize());

        return new Message(prophecy);
    }

    private List<PRObjectLocation> filterObjectCond(List<PRObjectLocation> objectLocations, String field, String cond) {
        List<PRObjectLocation> ret = new ArrayList<>();
        switch (cond.toUpperCase()) {
            case "MIN":
                objectLocations.sort((prObjectLocation1, prObjectLocation2) -> {
                    String strObjId1[] = prObjectLocation1.getObjId().value.split(":");
                    String strObjId2[] = prObjectLocation2.getObjId().value.split(":");
                    for (int i = 0; i < strObjId1.length; i++) {
                        if (strObjId1[i].indexOf(field + "=") == 0) {
                            int value1 = Integer.parseInt((strObjId1[i].split("="))[1]);
                            int value2 = Integer.parseInt((strObjId2[i].split("="))[1]);
                            return value1 - value2;
                        }

                    }
                    return 0;
                });
                ret.add(objectLocations.get(0));
                break;
            default:
                break;
        }
        log("filterObjectCond result:" + objectLocations.get(0).getObjId());
        return ret;
    }

    private List<PRObjectLocation> filterObjectRange(List<PRObjectLocation> objectLocations, String field, int lowerBound, int upperBound) {

        return objectLocations.stream().filter(prObjectLocation -> {
            String strObjId[] = prObjectLocation.getObjId().value.split(":");
            for (int i = 0; i < strObjId.length; i++) {
                if (strObjId[i].indexOf(field + "=") == 0) {
                    int value = Integer.parseInt((strObjId[i].split("="))[1]);
                    if (value >= lowerBound && value <= upperBound) return true;
                }
            }
            return false;
        }).collect(Collectors.toList());

    }

    @Override
    public Message executeCommand(Command cmd) {
        return null;
    }

    @Override
    public Message executeSSMRCommand(Command cmd) {
        return executeCommand(cmd);
    }

    @Override
    public PRObject createObject(Object objId, Object value) {
        return null;
    }

    synchronized public void setCache(ObjId objId, PRObjectLocation knowledge) {
        this.cache.put(objId, knowledge);
    }

    synchronized public PRObjectLocation getCache(ObjId objId) {
        return this.cache.getOne(objId);
    }

    synchronized public Cache getCache() {
        return this.cache;
    }

    public static class Prophecy {
        public Map<ObjId, Integer> objectMap = new ConcurrentHashMap<>();

        public Prophecy() {
        }

        public void add(ObjId objId, int partitionId) {
            objectMap.put(objId, partitionId);
        }

        @Override
        public String toString() {
            StringBuffer str = new StringBuffer("[");
            this.objectMap.keySet().stream().forEach(key -> str.append(key + ":" + this.objectMap.get(key) + ", "));
            str.append("]");
            return str.toString();
        }
    }


}
