/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.aerie.benchmark;


import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.CommandType;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.Partition;
import ch.usi.dslab.lel.aerie.client.AsyncClient;

public class AppClient {
    private static final Logger logger = Logger.getLogger(AppClient.class);
    AsyncClient eyrieClient;
    int objPerPartition;
    String outputFile;
    int outstanding;
    ThroughputPassiveMonitor tpMonitor;
    LatencyPassiveMonitor latencyMonitor;
    long startTime;
    String monitorHost;
    private Semaphore sendPermits;

    public AppClient(int clientId, String systemConfigFile, String partitioningFile, int objPerPartition, int outstanding, String monitorHost) {
        this.startTime = System.currentTimeMillis();
        eyrieClient = new AsyncClient(clientId, systemConfigFile, partitioningFile, new AppProcedure());
        this.objPerPartition = objPerPartition;
        this.outstanding = outstanding;
        this.monitorHost = monitorHost;
        boolean connected = false;
    }

    public static void main(String[] args) {
        if (args.length == 6) {
            int clientId = Integer.parseInt(args[0]);
            String systemConfigFile = args[1];
            String partitionsConfigFile = args[2];
            int objPerPartition = Integer.parseInt(args[3]);
            int outstanding = Integer.parseInt(args[4]);
            String monitor = args[5];
//            int useGlobalCommand = Integer.parseInt(args[6]);
            AppClient appcli = new AppClient(clientId, systemConfigFile, partitionsConfigFile, objPerPartition, outstanding, monitor);

            appcli.setPermits(outstanding);
            appcli.runInteractive();
//            appcli.setupInitialState();
//            if (useGlobalCommand == 1) {
//                appcli.accessMultiplePartition();
//            } else {
//                appcli.accessSinglePartition();
//            }
        } else {
            logger.debug("USAGE: AppClient | clientId | systemConfigFile | partitionConfigFile | initialObjectCount | outstanding | monitor | useGlobal?");
            System.exit(1);
        }

    }

    public static String getTimeLine(Message reply, long sendTimeMs) {
        long t_mcast = reply.t_learner_delivered - sendTimeMs;
        long t_wait = reply.t_command_dequeued - reply.t_learner_delivered;
        long t_exec = System.currentTimeMillis() - reply.t_command_dequeued;
        long t_total = t_mcast + t_wait + t_exec;
        return String.format("mc: %d, w: %d, x: %d, total: %d", t_mcast, t_wait, t_exec, t_total);
    }

    void setPermits(int num) {
        sendPermits = new Semaphore(num);
    }

    private void setupInitialState() {
        logger.debug("Run initial setup");
        List<AppCommand> cmds = new ArrayList<>();
        List<Partition> partitions = Partition.getPartitionList();
        int min = (this.eyrieClient.getId()) * partitions.size() * objPerPartition;
        int max = min + objPerPartition;
        for (int i = min; i < max; i++) {
            getPermit();
            ObjId oid = new ObjId(String.valueOf(i));
            Command cmd = new Command(GenericCommand.CREATE, oid, i);
            eyrieClient.create((reply, context) -> {
                CallBackContext cbContext = (CallBackContext) context;
                Command cmd1 = cbContext.cmd;
                long sendTimeMs = cbContext.sendTimeMs;
                ((Message) reply).rewind();
                Object message = ((Message) reply).getNext();
                logTimeLine((Message) reply, sendTimeMs);
                logger.debug(String.format("<<< Reply for command %s: %d", cmd1.getId(), ((AppObject) message).getValue()));
                logger.debug("=======================================================");
                addPermit();
            }, new CallBackContext(cmd, System.currentTimeMillis()), cmd);
        }
    }

    private void accessMultiplePartition() {
        logger.debug("Try to access all variables");
        DataGatherer.configure(30, null, this.monitorHost, 60000, 2000);
        tpMonitor = new ThroughputPassiveMonitor(this.eyrieClient.getId(), "DSSMRClient");
        latencyMonitor = new LatencyPassiveMonitor(this.eyrieClient.getId(), "DSSMRClient");

        List<Partition> partitions = Partition.getPartitionList();
        int min = (this.eyrieClient.getId()) * partitions.size() * objPerPartition;
        int max = min + objPerPartition;
        for (int i = min; i < max; i++) {
            getPermit();
            Command cmd = new Command(AppCommand.ADD_OBJ, new ObjId(String.valueOf(i)), new ObjId(String.valueOf(++i)));
            eyrieClient.sendUpdate((Object reply, Object context) -> {
                addPermit();
                CallBackContext cbContextUpdate = (CallBackContext) context;
                Command cmd1 = cbContextUpdate.cmd;
                long sendTimeUpdate = cbContextUpdate.sendTimeMs;
                ((Message) reply).rewind();
                Object message = ((Message) reply).getNext();
                logger.debug(String.format("<<< Reply for command %s: %d", cmd1.getId(), ((AppObject) message).getValue()));
//                logMPTimeLine((Message) replyMove, sendTimeMove, endMove, (Message) replyUpdate, sendTimeUpdate, System.currentTimeMillis());
                logTimeLine((Message) reply, sendTimeUpdate);
                logger.debug("=======================================================");

                tpMonitor.incrementCount();
//                latencyMonitor.logLatency(sendTimeMove, System.currentTimeMillis());
            }, new CallBackContext(cmd, System.currentTimeMillis()), cmd);
        }
    }


    private void accessSinglePartition() {
        logger.debug("Try to access variables on each single partition");
        List<Partition> partitions = Partition.getPartitionList();
        List<AppCommand> cmds = new ArrayList<>();
        int min = (this.eyrieClient.getId()) * partitions.size() * objPerPartition;
        int max = min + objPerPartition;
        DataGatherer.configure(30, null, this.monitorHost, 60000, 2000);
        tpMonitor = new ThroughputPassiveMonitor(this.eyrieClient.getId(), "DSSMRClient");
        latencyMonitor = new LatencyPassiveMonitor(this.eyrieClient.getId(), "DSSMRClient");

        for (int i = min; i < max; i++) {
            getPermit();
            Command cmd = new Command(AppCommand.ADD, new ObjId(String.valueOf(i)), i);
            eyrieClient.sendUpdate((Object reply, Object context) -> {
                addPermit();
                CallBackContext cbContextUpdate = (CallBackContext) context;
                Command cmd1 = cbContextUpdate.cmd;
                long sentTime = cbContextUpdate.sendTimeMs;
                ((Message) reply).rewind();
                Object message = ((Message) reply).getNext();
                logger.debug(String.format("<<< Reply for command %s: %d", cmd1.getId(), ((AppObject) message).getValue()));
                logTimeLine((Message) reply, sentTime);
                logger.debug("=======================================================");

                tpMonitor.incrementCount();
                latencyMonitor.logLatency(sentTime, System.currentTimeMillis());
            }, new CallBackContext(cmd, System.currentTimeMillis()), cmd);
        }
    }

    void getPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void logTimeLine(Message reply, long sendTimeMs) {
        long t_mcast = reply.t_learner_delivered - sendTimeMs;
        long t_wait = reply.t_command_dequeued - reply.t_learner_delivered;
        long t_exec = System.currentTimeMillis() - reply.t_command_dequeued;
        long t_total = t_mcast + t_wait + t_exec;
        int packageSize = reply.getBytes().length;
        String timeline = String.format("Timeline: s: %d, mc: %d, w: %d, x: %d, total: %d", packageSize, t_mcast, t_wait, t_exec, t_total);
//        monitor.sendBusyWait(new Message(timeline));
        logger.debug(timeline);
//        FileUtils.appendToFile(this.outputFile, timeline, true);
    }

    void addPermit() {
        if (sendPermits != null)
            sendPermits.release();
    }

    public void logMPTimeLine(Message ctxMove, long startMove, long endMove, Message ctxCmd, long startCmd, long endCmd) {
        long move_t_mcast = ctxMove.t_learner_delivered - startMove;
        long move_t_wait = ctxMove.t_command_dequeued - ctxMove.t_learner_delivered;
        long move_t_exec = endMove - ctxMove.t_command_dequeued;
        long move_t_total = move_t_mcast + move_t_wait + move_t_exec;
        int move_packageSize = ctxMove.getBytes().length;

        long cmd_t_mcast = ctxCmd.t_learner_delivered - startCmd;
        long cmd_t_wait = ctxCmd.t_command_dequeued - ctxCmd.t_learner_delivered;
        long cmd_t_exec = endCmd - ctxCmd.t_command_dequeued;
        long cmd_t_total = cmd_t_mcast + cmd_t_wait + cmd_t_exec;
        int cmd_packageSize = ctxCmd.getBytes().length;

        long t_mcast = move_t_mcast + cmd_t_mcast;
        long t_wait = move_t_wait + cmd_t_wait;
        long t_exec = move_t_exec + cmd_t_exec;
        long t_total = move_t_total + cmd_t_total;
        String timeline = String.format("Timeline:s1: %d, mc1: %d, w1: %d, x1: %d, t1: %d == s2: %d, mc2: %d, w2: %d, x2: %d, t2: %d == mc: %d, w: %d, x: %d, t: %d",
                move_packageSize, move_t_mcast, move_t_wait, move_t_exec, move_t_total, cmd_packageSize, cmd_t_mcast, cmd_t_wait, cmd_t_exec, cmd_t_total, t_mcast, t_wait, t_exec, t_total);
        logger.debug(timeline);
        logger.info(String.format("timeline: s: %d, mc:%d, w:%d, x:%d, total:%d", t_mcast, t_wait, t_exec, t_total));
//        FileUtils.appendToFile(this.outputFile, timeline, true);
    }

    public void runInteractive() {
        Scanner scan = new Scanner(System.in);
        String input;

        System.out.println("input format: a(dd)/s(ub)/m(ul)/d(iv)/c(reate)/o(add object) oid1 value (or end to finish)");
        input = scan.nextLine();
        while (!input.equalsIgnoreCase("end")) {
            String[] params = input.split(" ");
            String opStr = params[0];
            ArrayList<Object> args = new ArrayList<>();

            CommandType op = null;
            if (opStr.equalsIgnoreCase("c")) {
                ObjId oid = new ObjId(params[1]);
                int value = Integer.parseInt(params[2]);
                Command cmd = new Command(GenericCommand.CREATE, oid, value);
                eyrieClient.create((reply, context) -> {
                    System.out.println("got reply " + reply);
                }, new CallBackContext(cmd, System.currentTimeMillis()), cmd);
            } else {
                if (opStr.equalsIgnoreCase("a"))
                    op = ch.usi.dslab.lel.aerie.benchmark.AppCommand.ADD;
                else if (opStr.equalsIgnoreCase("s"))
                    op = ch.usi.dslab.lel.aerie.benchmark.AppCommand.SUB;
                else if (opStr.equalsIgnoreCase("m"))
                    op = ch.usi.dslab.lel.aerie.benchmark.AppCommand.MUL;
                else if (opStr.equalsIgnoreCase("d"))
                    op = ch.usi.dslab.lel.aerie.benchmark.AppCommand.DIV;
                else if (opStr.equalsIgnoreCase("o"))
                    op = ch.usi.dslab.lel.aerie.benchmark.AppCommand.ADD_OBJ;
                ObjId oid1 = new ObjId(params[1]);
                ObjId oid2 = new ObjId(params[2]);
                ArrayList<ObjId> objIds = new ArrayList<>();
                objIds.add(oid1);
                objIds.add(oid2);
                Command cmd = new Command(op, objIds);
                eyrieClient.sendSSMRCommand((reply, context) -> {
                    System.out.println("got reply " + reply);
                }, new CallBackContext(cmd, System.currentTimeMillis()), cmd);
                eyrieClient.sendUpdate((reply, context) -> {
                    System.out.println("got reply " + reply);
                }, new CallBackContext(cmd, System.currentTimeMillis()), cmd);
            }

            input = scan.nextLine();
        }
        scan.close();
    }

    public static class CallBackContext {
        public Command cmd;
        public long sendTimeMs;

        public CallBackContext(Command cmd, long sendTimeNano) {
            this.cmd = cmd;
            this.sendTimeMs = sendTimeNano;
        }
    }
}
