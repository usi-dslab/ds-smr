package ch.usi.dslab.lel.aerie.client;

/**
 * Created by longle on 28/07/2016.
 */
public interface EvenCallBack {
    void callback(Object data);
}
