/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package ch.usi.dslab.lel.aerie.benchmark;


import ch.usi.dslab.lel.aerie.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.aerie.client.PromiseClient;

public class AppClientPromise {
    private static final Logger logger = Logger.getLogger(AppClientPromise.class);
    PromiseClient eyrieAsyncClient;
    int objPerPartition;
    String outputFile;
    int outstanding;
    ThroughputPassiveMonitor tpMonitor;
    LatencyPassiveMonitor latencyMonitor;
    long startTime;
    String monitorHost;
    private Semaphore sendPermits;

    public AppClientPromise(int clientId, String systemConfigFile, String partitioningFile, int objPerPartition, int outstanding, String monitorHost) {
        this.startTime = System.currentTimeMillis();
        eyrieAsyncClient = new PromiseClient(clientId, systemConfigFile, partitioningFile, new AppProcedure(), new CacheHashMap());
        this.objPerPartition = objPerPartition;
        this.outstanding = outstanding;
        this.monitorHost = monitorHost;
        boolean connected = false;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        if (args.length == 6) {
            int clientId = Integer.parseInt(args[0]);
            String systemConfigFile = args[1];
            String partitionsConfigFile = args[2];
            int objPerPartition = Integer.parseInt(args[3]);
            int outstanding = Integer.parseInt(args[4]);
            String monitor = args[5];
            AppClientPromise appcli = new AppClientPromise(clientId, systemConfigFile, partitionsConfigFile, objPerPartition, outstanding, monitor);

            appcli.setPermits(outstanding);
//            appcli.runBatch();
            appcli.runInteractive();
        } else {
            logger.debug("USAGE: AppClient | clientId | systemConfigFile | partitionConfigFile | initialObjectCount | outstanding | monitor");
            System.exit(1);
        }

    }

    public static String getTimeLine(Message reply, long sendTimeMs) {
        long t_mcast = reply.t_learner_delivered - sendTimeMs;
        long t_wait = reply.t_command_dequeued - reply.t_learner_delivered;
        long t_exec = System.currentTimeMillis() - reply.t_command_dequeued;
        long t_total = t_mcast + t_wait + t_exec;
        return String.format("mc: %d, w: %d, x: %d, total: %d", t_mcast, t_wait, t_exec, t_total);
    }

    void setPermits(int num) {
        sendPermits = new Semaphore(num);
    }

    void getPermit() {
        try {
            sendPermits.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    void addPermit() {
        if (sendPermits != null)
            sendPermits.release();
    }

    public void runBatch() {
        for (int i = 0; i < 100; i++) {
            getPermit();
            ObjId oid = new ObjId(String.valueOf(i));
            int value = i;
            Command cmd = new Command(GenericCommand.CREATE, oid, value);
            long startTime = System.currentTimeMillis();
            CompletableFuture<Message> cmdEx = eyrieAsyncClient.create(cmd);
            cmdEx.thenAccept(reply -> {
                addPermit();
                long endTime = System.currentTimeMillis();
                System.out.println((endTime - startTime) + "-" + value + " - " + reply);
            });
        }
    }

    public void runInteractive() throws ExecutionException, InterruptedException {
        Scanner scan = new Scanner(System.in);
        String input;

        logger.debug("input format: a(dd)/s(ub)/m(ul)/d(iv)/c(reate)/z(move)/o(add object) oid1 value (or end to finish)");
        input = scan.nextLine();
        while (!input.equalsIgnoreCase("end")) {
            String[] params = input.split(" ");
            String opStr = params[0];
            ArrayList<Object> args = new ArrayList<>();
            long startTime = System.currentTimeMillis();
            CommandType op = null;
            if (opStr.equalsIgnoreCase("q")) {
                ObjId oid = new ObjId(params[1]);
                CompletableFuture<Message> query = eyrieAsyncClient.query(oid, false);
                System.out.println(eyrieAsyncClient.getObjectMapFromQuery(query.get()));
            } else if (opStr.equalsIgnoreCase("c")) {
                ObjId oid = new ObjId(params[1]);
                int value = Integer.parseInt(params[2]);
                Command cmd = new Command(GenericCommand.CREATE, oid, value);
                CompletableFuture<Message> cmdEx = eyrieAsyncClient.create(cmd);
                System.out.println(cmdEx.get());
            } else if (opStr.equalsIgnoreCase("r")) {
                ObjId oid = new ObjId(params[1]);
                Command cmd = new Command(GenericCommand.READ, oid);
                CompletableFuture<Message> cmdEx = eyrieAsyncClient.read(cmd);
                System.out.println(cmdEx.get());
            } else if (opStr.equalsIgnoreCase("a")) {
                ObjId oid1 = new ObjId(params[1]);
                ObjId oid2 = new ObjId(params[2]);
                Command cmd = new Command(AppCommand.ADD, oid1, oid2);
                CompletableFuture<Message> cmdEx = eyrieAsyncClient.update(cmd);
                Message rep = cmdEx.get();
                long endTime = System.currentTimeMillis();
                System.out.println((endTime - startTime) + " - " + rep);
            }

            input = scan.nextLine();
        }
        scan.close();
    }

}
