package ch.usi.dslab.lel.aerie.benchmark;

import ch.usi.dslab.lel.aerie.CommandType;

/**
 * Created by longle on 04/11/15.
 */
public enum AppCommand implements CommandType{
    ADD, SUB, MUL, DIV, ADD_OBJ
}
