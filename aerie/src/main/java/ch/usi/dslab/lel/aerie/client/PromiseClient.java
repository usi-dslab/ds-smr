/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 * @author Long Hoang Le- long.hoang.le@usi.ch
 */

package ch.usi.dslab.lel.aerie.client;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import ch.usi.dslab.bezerra.mcad.ClientMessage;
import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.mcad.MulticastClient;
import ch.usi.dslab.bezerra.mcad.MulticastClientServerFactory;
import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.aerie.ApplicationProcedure;
import ch.usi.dslab.lel.aerie.Cache;
import ch.usi.dslab.lel.aerie.Cache.PRObjectLocation;
import ch.usi.dslab.lel.aerie.CacheMultimap;
import ch.usi.dslab.lel.aerie.Command;
import ch.usi.dslab.lel.aerie.CommandType;
import ch.usi.dslab.lel.aerie.GenericCommand;
import ch.usi.dslab.lel.aerie.LocalReplica;
import ch.usi.dslab.lel.aerie.LocalReplica.MessageType;
import ch.usi.dslab.lel.aerie.ObjId;
import ch.usi.dslab.lel.aerie.OracleStateMachine;
import ch.usi.dslab.lel.aerie.Partition;

import static java.util.Collections.singletonList;


public class PromiseClient implements Runnable {

    private static final Logger logger = Logger.getLogger(PromiseClient.class);
    boolean running = true;
    private MulticastClient multicastClient;
    private Thread clientThread;
    private int clientId;
    private AtomicLong nextRequestId = new AtomicLong(0);
    private Map<Long, AsyncCommand> outstandingCommands = new ConcurrentHashMap<>();
    private Map<Long, Query> outstandingQueries = new ConcurrentHashMap<>();
    private ApplicationProcedure applicationProcedure;
    private Cache cache;
    EvenCallBack queryEventCallBack = null;
    EvenCallBack moveEventCallBack = null;
    EvenCallBack retryEventCallBack = null;
    EvenCallBack localCommandEventCallBack = null;
    EvenCallBack globalCommandEventCallback = null;

    public PromiseClient(int clientId, String systemConfigFile, String partitioningFile, ApplicationProcedure applicationProcedure, Cache cache) {
        this.clientId = clientId;
        this.cache = cache;
        this.applicationProcedure = applicationProcedure;
        multicastClient = MulticastClientServerFactory.getClient(clientId, systemConfigFile);
        Partition.loadPartitions(partitioningFile);
        Partition.partitionList.values().forEach(this::connectToPartition);
        clientThread = new Thread(this, "DSSMRClientThread-" + clientId);
        clientThread.start();
    }


    private void connectToPartition(Partition partition) {
        List<Integer> partitionServers = partition.getGroup().getMembers();
        int contactServerIndex = clientId % partitionServers.size();
        int contactServer = partitionServers.get(contactServerIndex);
        multicastClient.connectToServer(contactServer);
    }

    public void registerMonitoringEvent(EvenCallBack queryEventCallBack, EvenCallBack moveEventCallBack, EvenCallBack retryEventCallBack, EvenCallBack localCommandEventCallBack, EvenCallBack globalCommandEventCallback) {
        this.queryEventCallBack = queryEventCallBack;
        this.moveEventCallBack = moveEventCallBack;
        this.retryEventCallBack = retryEventCallBack;
        this.localCommandEventCallBack = localCommandEventCallBack;
        this.globalCommandEventCallback = globalCommandEventCallback;
    }

    private void log(String message) {
        logger.debug("[CLIENT-" + this.getId() + "]-" + message);
    }

    @Override
    public void run() {
        while (running) {
            Message replyWrapper = multicastClient.deliverReply();
            long reqId = (Long) replyWrapper.getItem(0);
            MessageType type = (MessageType) replyWrapper.getItem(1);
            Message reply = (Message) replyWrapper.getItem(2);
            reply.copyTimelineStamps(replyWrapper);
            log(String.format(this.getId() + "." + reqId + "-cons-delivered message %s", reply));
            switch (type) {
                case REPLY: {
                    AsyncCommand acmd = outstandingCommands.remove(reqId);
                    if (reply.peekNext() != null && reply.getNext().equals("RETRY")) {
                        retryCommand(acmd, reply);
                        break;
                    }
                    this.updateCache(acmd.command);
                    acmd.deliver(reply);
                    break;
                }
                case REPLY_QUERY: {
                    Query osq = outstandingQueries.remove(reqId);
                    updateCacheFromQuery(reply);
                    osq.deliver(reply);
                    break;
                }
                default: {
                    break;
                }
            }

        }
    }

    private void retryCommand(AsyncCommand acmd, Message reply) {
        if (retryEventCallBack != null) retryEventCallBack.callback(1);
        Command cmd = acmd.command;
        cmd.rewind();
        log("try to retry command " + cmd);
        CommandType op = (CommandType) cmd.getNext();
        if (op == GenericCommand.READ || op == GenericCommand.DELETE) {
            ObjId objId = (ObjId) cmd.getItem(1);
            removeCache(objId);
            executeLocalCommand(acmd, false, false, false);
        } else if (op == GenericCommand.MOVE) {
            ObjId objId = (ObjId) cmd.getNext();
            removeCache(objId);
            executeMoveCommand(acmd, true);
        } else if (op == GenericCommand.READ_BATCH) {
            List<ObjId> objIds = (ArrayList) cmd.getItem(1);
            removeCache(objIds);
//            System.out.println("CLIENT-" + this.getId() + " is retrying with " + acmd.command);
            acmd.command.setItem(1, acmd.orgObjId);
//            System.out.println("CLIENT-" + this.getId() + " is retrying with " + acmd.command);
            executeGlobalCommand(acmd, true, false, false, reply);
        } else if (!(op instanceof GenericCommand)) {
            executeGlobalCommand(acmd, true, false, false, reply);
        } else {
            ArrayList<Object> args = (ArrayList<Object>) cmd.getItem(1);
            ObjId objId = (ObjId) args.get(0);
            removeCache(objId);
            System.out.println("Cant enter here");
        }
    }


    public CompletableFuture<Message> query(ObjId objId, boolean shouldOraclePropose) {
        return query(new HashSet<>(singletonList(objId)), shouldOraclePropose);
    }


    public CompletableFuture<Message> query(Set<ObjId> objIds, boolean shouldOraclePropose) {
        return query(objIds, shouldOraclePropose, false);
    }

    public CompletableFuture<Message> query(ObjId objId, boolean shouldOraclePropose, boolean isRangeQuery) {
        return query(new HashSet<>(singletonList(objId)), shouldOraclePropose, isRangeQuery);
    }

    public CompletableFuture<Message> query(Set<ObjId> objIds, boolean shouldOraclePropose, boolean isRangeQuery) {
        log("query - querying objects: " + objIds + " is range query? "+ isRangeQuery);
        if (queryEventCallBack != null) queryEventCallBack.callback(1);
        Command cmdQuery;
        cmdQuery = new Command(isRangeQuery ? GenericCommand.QUERY_RANGE : GenericCommand.QUERY_OBJ, shouldOraclePropose, objIds);
        long requestId = nextRequestId.incrementAndGet();
        cmdQuery.setId(clientId, requestId);
        Query osq = new Query(cmdQuery);
        outstandingQueries.put(requestId, osq);
        multicastQuery(cmdQuery);
        return osq;
    }

    public CompletableFuture<Message> create(Command command) {
        return executeLocalCommand(command, true, true, true);
    }

    public CompletableFuture<Message> read(Command command) {
        return executeLocalCommand(command, false, false, false);
    }

    public CompletableFuture<Message> readBatch(Command command) {
        return executeGlobalCommand(command, true, false, false);
    }

    public CompletableFuture<Message> update(Command command) {
        return executeGlobalCommand(command, false, false, false);
    }

    public CompletableFuture<Message> updateBatch(Command command) {
        return executeGlobalCommand(command, true, false, false);
    }

    public CompletableFuture<Message> delete(Command command) {
        return executeLocalCommand(command, false, false, true);
    }

    private CompletableFuture<Message> executeLocalCommand(Command command, boolean forceQuery, boolean shouldOraclePropose, boolean shouldIncludeOracle) {
        return executeLocalCommand(new AsyncCommand(command), forceQuery, shouldOraclePropose, shouldIncludeOracle);
    }

    private CompletableFuture<Message> executeGlobalCommand(Command command, boolean forceQuery, boolean shouldOraclePropose, boolean shouldIncludeOracle) {
        return executeGlobalCommand(new AsyncCommand(command), forceQuery, shouldOraclePropose, shouldIncludeOracle, null);
    }

    private CompletableFuture<Message> executeLocalCommand(AsyncCommand acmd, boolean forceQuery, boolean shouldOraclePropose, boolean shouldIncludeOracle) {
        acmd.command.rewind();
        log("executeLocalCommand - Executing command: " + acmd.command);
        CommandType op = (CommandType) acmd.command.getNext();
        ObjId objId = (ObjId) (acmd.command.getNext());

        CompletableFuture<Message> query;
        boolean shouldQuery = shouldQuery(objId, forceQuery);
        if (shouldQuery) {
            query = query(objId, shouldOraclePropose);
        } else {
            query = CompletableFuture.supplyAsync(() -> null);
        }
        query.thenAccept((reply) -> {
            if (reply == null) {
                log("executeLocalCommand - no need to query");
            } else {
                log("executeLocalCommand - reply location from oracle " + reply);
            }
            Map<ObjId, Integer> objectMap;
            if (reply == null) objectMap = getObjectMapFromCache(objId);
            else objectMap = getObjectMapFromQuery(reply);
            List<ObjId> keys = new ArrayList<ObjId>();
            keys.addAll(objectMap.keySet());
            if (objectMap.keySet().size() == 0) {
                acmd.deliver(new Message("Object is not in oracle's memory"));
                return;
            }
            acmd.command.setItem(1, keys.get(0)); // set objectids instead of pattern;
            long requestId = nextRequestId.incrementAndGet();
            acmd.command.setId(clientId, requestId);
            outstandingCommands.put(requestId, acmd);
            Set<Integer> dest = objectMap.keySet().stream().map(objectMap::get).collect(Collectors.toSet());
            localCommandEventCallBack.callback(1);
            multicastCommand(acmd.command, dest, shouldIncludeOracle, false);
        });
        return acmd;
    }

    private CompletableFuture<Message> executeGlobalCommand(AsyncCommand acmd, boolean forceQuery, boolean shouldOraclePropose, boolean shouldIncludeOracle, Message retryReply) {
        log("executeGlobalCommand - Executing command: " + acmd.command);
        Set<ObjId> objsToAdd = new HashSet<>();
        if (retryReply != null && !acmd.isRangeCommand) {
            retryReply.rewind();
            if (retryReply.getItem(1) != null && retryReply.getItem(1) instanceof Set) {
                log("executeGlobalCommand - update command as retry reply" + retryReply.getItem(1));
                objsToAdd.addAll((Set) retryReply.getItem(1));
            }
        }
        Set<ObjId> objIds = applicationProcedure.extractObjectId(acmd.command);
        if (objsToAdd.size() > 0) objIds.addAll(objsToAdd);
        boolean shouldQuery = shouldQuery(objIds, forceQuery);
        CompletableFuture<Message> query;
        if (shouldQuery) {
            query = query(objIds, shouldOraclePropose, acmd.isRangeCommand);
        } else {
            query = CompletableFuture.supplyAsync(() -> null);
        }
        query.thenCompose((reply) -> {
            if (reply == null) {
                log("executeGlobalCommand - no need to query");
            } else {
                log("executeGlobalCommand - reply location from oracle " + reply);
            }
            Map<ObjId, Integer> objectMap;
            if (reply == null) objectMap = getObjectMapFromCache(objIds);
            else objectMap = getObjectMapFromQuery(reply);
            List<ObjId> keys = new ArrayList<>();
            keys.addAll(objectMap.keySet());
            if (objectMap.keySet().size() == 0) {
                acmd.deliver(new Message("Object is not in oracle's memory"));
            }
            acmd.command.setItem(1, keys); // set objectids instead of pattern;
            return prepare(acmd.command, false, objsToAdd);
        }).thenAccept((reply) -> {
            PRObjectLocation loc = cache.getOne(applicationProcedure.extractObjectId(acmd.command).iterator().next());
            int dest = loc.getPartitionId();
            if (dest < 0) {
                acmd.deliver(new Message("Object is not in oracle's memory"));
                return;
            }
            long requestId = nextRequestId.incrementAndGet();
            acmd.command.setId(clientId, requestId);
            outstandingCommands.put(requestId, acmd);
            multicastCommand(acmd.command, new HashSet<>(singletonList(dest)), shouldIncludeOracle, false);
        });
        return acmd;
    }

    private CompletableFuture<Void> prepare(Command command, boolean forceQuery, Set<ObjId> objsToAdd) {
        log("prepare - Preparing command: " + command);
        command.rewind();
        command.setId(clientId, nextRequestId.incrementAndGet());
        CommandType op = (CommandType) command.getNext();
        Set<ObjId> objIds = new HashSet<>(applicationProcedure.extractObjectId(command));
        if (objsToAdd != null && objsToAdd.size() > 0) objIds.addAll(objsToAdd);
        boolean shouldQuery = shouldQuery(objIds, forceQuery);
        if (shouldQuery) {
            log("prepare - Don't have cache of one of oids, need to query Oracle");
            return query(objIds, false).thenCompose(reply -> {
                Map<ObjId, Integer> dest = getObjectMapFromQuery(reply);
                return moveObjects(applicationProcedure.getMoveDestination(dest), objIds);
            });
        } else {
            log("prepare - All object caches are available. try to send command directly");
            Map<ObjId, Integer> dest = getObjectMapFromCache(objIds);
            return moveObjects(applicationProcedure.getMoveDestination(dest), objIds);
        }

    }


    private CompletableFuture<Void> moveObjects(int destPartId, Set<ObjId> objIds) {
        log("moveObjects - Moving objects: " + objIds + " to partition " + destPartId);
        List<CompletableFuture> moveCmds = new ArrayList<>();
        AtomicInteger moveCount = new AtomicInteger(0);
        objIds.stream().forEach(objId -> {
            PRObjectLocation objLoc = getCache(objId);
            if (objLoc != null)
                if (objLoc.getPartitionId() != destPartId) { // only moveObject objects not in same partition
                    Command moveCmd = new Command(GenericCommand.MOVE, objId, destPartId, objLoc.getPartitionId());
                    moveCmds.add(moveObject(moveCmd));
                    moveCount.incrementAndGet();
                }
        });
        if (moveCount.get() == 0) {
            log("moveObjects - no move needed");
            localCommandEventCallBack.callback(1);
            return CompletableFuture.supplyAsync(() -> null);
        } else {
            log("moveObjects - perform " + moveCmds.size() + " moves: " + moveCmds);
            globalCommandEventCallback.callback(1);
            return CompletableFuture.allOf(moveCmds.toArray(new CompletableFuture[moveCmds.size()]));
        }
    }

    //Command(GenericCommand.MOVE, objId, destPartId, srcPartId);
    private CompletableFuture<Message> moveObject(Command cmd) {
        return executeMoveCommand(new AsyncCommand(cmd), false);
    }

    private CompletableFuture<Message> executeMoveCommand(AsyncCommand acmd, boolean forceQuery) {
        acmd.command.rewind();
        CommandType op = (CommandType) acmd.command.getNext();
        ObjId objId = (ObjId) acmd.command.getNext();
        int destPartId = (int) acmd.command.getNext();
        int srcPartId = (int) acmd.command.getNext();

        log("executeMoveCommand - Moving object " + objId + " to partition " + destPartId);
        CompletableFuture<Message> query;
        boolean shouldQuery = shouldQuery(objId, forceQuery);
        if (shouldQuery) {
            query = query(objId, false);
        } else {
            query = CompletableFuture.supplyAsync(() -> null);
        }
        query.thenAccept((reply) -> {
            if (reply == null) {
                log("executeMoveCommand - no need to query");
            } else {
                log("executeMoveCommand - reply location from oracle " + reply);
            }
            Map<ObjId, Integer> objectMap = getObjectMapFromCache(objId);
            int srcPartFromQuery = objectMap.get(objId);
            log("executeMoveCommand - object map" + objectMap.keySet() + " - " + objectMap.values() + "-" + srcPartFromQuery + "-" + destPartId);
            if (srcPartFromQuery == destPartId) {
                acmd.deliver(new Message("Object in place!"));
                return;
            }
            if (srcPartFromQuery != srcPartId) acmd.command.setItem(3, srcPartFromQuery);
            long requestId = nextRequestId.incrementAndGet();
            acmd.command.setId(clientId, requestId);
            outstandingCommands.put(requestId, acmd);
            Set<Integer> dest = new HashSet<>();
            dest.add(destPartId);
            dest.add(srcPartFromQuery);
            multicastCommand(acmd.command, dest, true, false);
            if (moveEventCallBack != null) moveEventCallBack.callback(1);
        });
        return acmd;
    }

    private void multicastCommand(Command command, Set<Integer> destinationIds, Boolean includeOracle, boolean forceSSMR) {
        if (null != destinationIds && destinationIds.size() != 0) {
            log("multicastCommand - Sending command " + command + " to actual partition " + destinationIds);
            List<Partition> destinations = new ArrayList<>();
            List<Group> destinationGroups = new ArrayList<>();

            // also add oracle to the destination group
            if (includeOracle) {
                destinations.addAll(Partition.getOracleList());
            }

            destinations.addAll(destinationIds.stream().map(Partition::getPartition).collect(Collectors.toList()));
            destinationGroups.addAll(destinations.stream().map(Partition::getGroup).collect(Collectors.toList()));

            command.setDestinations(destinations);

            ClientMessage commandMessage;
            if (!forceSSMR) {
                commandMessage = new ClientMessage(LocalReplica.MessageType.COMMAND, command);
            } else {
                commandMessage = new ClientMessage(MessageType.SSMR_EXEC, command);
            }

            multicastClient.multicast(destinationGroups, commandMessage);
        } else {
            // TODO: deal with empty answer;
            log("multicastCommand - Can't locate object " + command.getItem(1).toString() + " in oracle's memory. Created it yet?");
        }
    }

    public CompletableFuture<Message> sendSSMRCommand(Command command) {
        long requestId = nextRequestId.incrementAndGet();
        AsyncCommand acmd = new AsyncCommand(command);
        acmd.command.setId(clientId, requestId);
        outstandingCommands.put(requestId, acmd);
        Set<ObjId> objIds = applicationProcedure.extractObjectId(acmd.command);
        Set<Integer> destIds = cache.get(objIds).stream().map(prObjectLocation ->
            {return prObjectLocation.getPartitionId();}).collect(Collectors.toSet());
        log("Destionation size:"+destIds.size());
        multicastCommand(acmd.command, destIds, false, true);
        return acmd;
    }

    private void multicastQuery(Command command) {
        List<Group> destinationGroups = new ArrayList<>();
        List<Partition> dest = new ArrayList<>();

        dest.addAll(Partition.getOracleList());
        destinationGroups.addAll(dest.stream().map(Partition::getGroup).collect(Collectors.toList()));

        command.setDestinations(dest);
        ClientMessage commandMessage = new ClientMessage(MessageType.QUERY, command);
        multicastClient.multicast(destinationGroups, commandMessage);
    }

    public Map<ObjId, Integer> getObjectMapFromQuery(Message query) {
        query.rewind();
        OracleStateMachine.Prophecy prophecy = (OracleStateMachine.Prophecy) query.getNext();
        prophecy.objectMap.keySet().stream().filter(objId -> prophecy.objectMap.get(objId) < 0).forEach(objId -> {
            prophecy.objectMap.remove(objId);
        });
        return prophecy.objectMap;
    }

    private Map<ObjId, Integer> getObjectMapFromCache(ObjId objId) {
        return getObjectMapFromCache(new HashSet<>(singletonList(objId)));
    }

    private Map<ObjId, Integer> getObjectMapFromCache(Set<ObjId> objIds) {
        Map<ObjId, Integer> ret = new ConcurrentHashMap<>();
        for (ObjId objId : objIds) {
            PRObjectLocation o = getCache(objId);
            if (o != null) ret.put(o.getObjId(), o.getPartitionId());
        }
        return ret;
    }

    synchronized public PRObjectLocation getCache(ObjId objId) {
        return this.cache.getOne(objId);
    }

    synchronized private void removeCache(ObjId objId) {
        this.cache.removeOne(objId);
    }

    synchronized private void removeCache(List<ObjId> objIds) {
        for (ObjId objId : objIds) {
            removeCache(objId);
        }
    }

    synchronized public void setCache(ObjId objId, PRObjectLocation knowledge) {
        if (getCache(objId) != null) {
            removeCache(objId);
        }
        this.cache.put(objId, knowledge);
    }

    public int getId() {
        return clientId;
    }

    private boolean shouldQuery(ObjId objId, boolean forceQuery) {
        return shouldQuery(new HashSet<>(singletonList(objId)), forceQuery);
    }

    private boolean shouldQuery(Set<ObjId> objIds, boolean forceQuery) {
        if (forceQuery) return true;
        for (ObjId i : objIds) {
            if (getCache(i) == null) {
                return true;
            }
        }
        return false;
    }

    private boolean shouldMigrate(Set<ObjId> objIds) {
        List<ObjId> tmp = new ArrayList<>(objIds);
        for (int i = 0; i < tmp.size() - 1; i++) {
            PRObjectLocation tmpI = getCache(tmp.get(i));
            PRObjectLocation tmpJ = getCache(tmp.get(i + 1));

            if ((tmpI == null) || tmpJ == null ||
                    ((tmpI.getPartitionId() != tmpJ.getPartitionId()))) {
                return true;
            }
        }
        return false;
    }

    private void updateCacheFromQuery(Message reply) {
        log("updateCacheFromQuery - try to update internal cache from query");
        Map<ObjId, Integer> objectMap = getObjectMapFromQuery(reply);

        // update client cache
        for (ObjId i : objectMap.keySet()) {
            PRObjectLocation newKnowledge = new PRObjectLocation(i, objectMap.get(i));
            if (newKnowledge.getPartitionId() >= 0) {
                setCache(i, newKnowledge);
                log("updateCacheFromQuery - Get object " + i + " location from Oracle: " + objectMap.get(i));
            } else {
                log("updateCacheFromQuery - Object " + i + " doesnt exist");
                objectMap.keySet().remove(i);
            }
        }
    }

    private void updateCache(Message command) {
        command.rewind();
        log("updateCache - Try to update internal cache from command result");
        CommandType op = (CommandType) command.getNext();

        if (op == GenericCommand.MOVE) {
            ObjId oid = (ObjId) command.getNext();
            int dest = (int) command.getNext();
            PRObjectLocation knowledge = getCache(oid);
            if (knowledge == null) {
                knowledge = new PRObjectLocation(oid, dest);
                setCache(oid, knowledge);
                log("updateCache - internal cache created " + knowledge.getObjId());
            } else {
                knowledge.setPartitionId(dest);
                log("updateCache - internal cache updated " + knowledge.getObjId());
            }
        } else if (op == GenericCommand.CREATE) {
            ObjId oid = (ObjId) command.getNext();
        } else if (op == GenericCommand.DELETE) {
            ObjId oid = (ObjId) command.getNext();
            this.cache.removeOne(oid);
        }
    }

    private static class Query extends CompletableFuture<Message> {
        Command query;

        public Query(Command query) {
            this.query = query;
        }

        public void deliver(Message message) {
            this.complete(message);
        }
    }

    private static class AsyncCommand extends CompletableFuture<Message> {
        Command command;
        Object orgObjId;
        boolean isRangeCommand = false;

        public AsyncCommand(Command command) {
            this.command = command;
            this.orgObjId = command.getItem(1);
            this.isRangeCommand = command.isRangeCommand;
        }

        public void deliver(Message message) {
            this.complete(message);
        }

        @Override
        public String toString() {
            return command.toString();
        }
    }


}
