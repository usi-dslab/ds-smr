package ch.usi.dslab.lel.aerie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by longle on 03/12/15.
 */
public interface ApplicationProcedure {

    int getObjectPlacement(ObjId objId);

    int getMoveDestination(Map<ObjId, Integer> objectMap);

    default Set<ObjId> extractObjectId(Command cmd) {
        cmd.rewind();
        Set<ObjId> objIds = new HashSet<>();
        while (cmd.hasNext()) {
            Object test = cmd.getNext();
            if (test instanceof ObjId) {
                objIds.add((ObjId) test);
            } else if (test instanceof List) {
                List<ObjId> tmp = (List<ObjId>) ((List) test).stream().filter(o -> o instanceof ObjId).collect(Collectors.toList());
                objIds.addAll(tmp);
            } else if (test instanceof Set) {
                List<ObjId> tmp = (List<ObjId>) ((Set) test).stream().filter(o -> o instanceof ObjId).collect(Collectors.toList());
                objIds.addAll(tmp);
            } else if (test instanceof Map) {
                Set<ObjId> tmp = new HashSet<>((ArrayList) ((((Map) test).keySet()).stream()
                        .filter(o -> o instanceof ObjId).collect(Collectors.toList())));
                objIds.addAll(tmp);

            }
        }
        return objIds;
    }
}
