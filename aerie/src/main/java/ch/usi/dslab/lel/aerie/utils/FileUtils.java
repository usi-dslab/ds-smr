package ch.usi.dslab.lel.aerie.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by longle on 11/11/15.
 */
public class FileUtils {
    public static void appendToFile(String filename, String content, boolean newLine) {
        BufferedWriter bw = null;
        try {
            // APPEND MODE SET HERE
            bw = new BufferedWriter(new FileWriter(filename, true));
            bw.write(content);
            if (newLine) bw.newLine();
            bw.flush();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {                       // always close the file
            if (bw != null) try {
                bw.close();
            } catch (IOException ioe2) {
                // just ignore it
            }
        } // end try/catch/finally

    } // end test()
}
