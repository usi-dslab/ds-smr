package ch.usi.dslab.lel.aerie.benchmark;

import ch.usi.dslab.lel.aerie.CacheHashMap;
import ch.usi.dslab.lel.aerie.benchmark.AppProcedure;
import ch.usi.dslab.lel.aerie.OracleStateMachine;

/**
 * Created by longle on 22/11/15.
 */
public class AppOracle extends OracleStateMachine {
    public AppOracle(int serverId, int partitionId, String systemConfig, String partitionsConfig) {
        super(serverId, partitionId, systemConfig, partitionsConfig, new AppProcedure(), new CacheHashMap());
    }

    public static void main(String args[]) {
        String systemConfigFile;
        String partitionConfigFile;
        int oracleId;
        int partitionId;
        if (args.length == 4) {
            oracleId = Integer.parseInt(args[0]);
            partitionId = Integer.parseInt(args[1]);
            systemConfigFile = args[2];
            partitionConfigFile = args[3];
            AppOracle oracle = new AppOracle(oracleId, partitionId, systemConfigFile, partitionConfigFile);
            oracle.runStateMachine();
        } else {
            System.out.println("USAGE: AppOracle | oracleId | partitionId | systemConfigFile | partitionConfigFile");
            System.exit(1);
        }
    }

}
