/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.lel.aerie;

import java.io.Serializable;

public class ObjId implements Serializable, Comparable<ObjId> {
    private static final long serialVersionUID = 1L;

    public int hash;
    public String value;

    public ObjId() {
    }

    public ObjId(ObjId other) {
        this.value = other.value;
    }

    public ObjId(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        byte[] bytes = this.value.getBytes();
        int sum = 0;
        for (byte b : bytes) sum += b;
        return sum;
    }

    @Override
    public boolean equals(Object other_) {
        ObjId other = ((ObjId) other_);
        return this.value.equals(other.value);
    }

    @Override
    public String toString() {
        return "(" + value + ")";
    }

    @Override
    public int compareTo(ObjId o) {
        int d1 = this.value.compareTo(o.value);
        if (d1 != 0) return d1;
        return 0;
    }
}
