#!/usr/bin/python

import inspect
import os

os.system("clear")


def script_dir():
    #    returns the module path without the use of __file__.  Requires a function defined
    #    locally in the module.
    #    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

# deploying (ridge) multicast infrastructure


# starting servers

java_bin = "java -XX:+UseG1GC -Dlog4j.configuration=file:" + script_dir() + "/log4j.xml -cp "
server_class = "ch.usi.dslab.lel.aerie.benchmark.AppServer "
oracle_class = "ch.usi.dslab.lel.aerie.benchmark.AppOracle "
partitioning_file = script_dir() + "/partitioning.json "
app_classpath = "/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/netwrapper/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/libmcad/target/classes:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/URingPaxos/target/classes:/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/lib/*:/Users/longle/Dropbox/Workspace/PhD/CosmmusProject/aerie/target/dependencies/* "
config_file = script_dir() + "/system_config.json "

oracle_id = "23 "
oracle_partition = "3 "

print 'Starting oracle: 3'

oracle_cmd = java_bin + app_classpath + oracle_class + oracle_id + oracle_partition + config_file + partitioning_file
os.system(oracle_cmd)
